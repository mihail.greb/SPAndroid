package com.c2call.android.emoji;

import android.content.Context;
import android.widget.GridView;

public class EmojiiconRecentsGridView extends EmojiiconGridView implements EmojiiconRecents
{
	private EmojiAdapter mAdapter;

	public EmojiiconRecentsGridView(Context context, com.c2call.android.emoji.emoji.Emojicon[] emojicons,
									EmojiiconRecents recents, EmojiiconsFragment emojiconsFragment) {
		super(context, emojicons, recents, emojiconsFragment);
		EmojiiconRecentsManager recents1 = EmojiiconRecentsManager
	            .getInstance(rootView.getContext());
		mAdapter = new com.c2call.android.emoji.EmojiAdapter(rootView.getContext(),  recents1);
		mAdapter.setEmojiClickListener(new OnEmojiconClickedListener() {
			
			@Override
			public void onEmojiconClicked(com.c2call.android.emoji.emoji.Emojicon emojicon) {
				if (mEmojiconFragment.onEmojiconClickedListener != null) {
					mEmojiconFragment.onEmojiconClickedListener.onEmojiconClicked(emojicon);
		        }
		    }
		});
        GridView gridView = (GridView) rootView.findViewById(R.id.Emoji_GridView);
        gridView.setAdapter(mAdapter);
    }

    @Override
    public void addRecentEmoji(Context context, com.c2call.android.emoji.emoji.Emojicon emojicon) {
        EmojiiconRecentsManager recents = EmojiiconRecentsManager
            .getInstance(context);
        recents.push(emojicon);

        // notify dataset changed
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }

}
