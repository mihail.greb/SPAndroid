package com.c2call.android.emoji;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;

import com.c2call.android.emoji.emoji.Emojicon;
import com.c2call.android.emoji.emoji.People;

import java.util.Arrays;


public class EmojiiconGridView
{
	public View rootView;
	EmojiiconsFragment mEmojiconFragment;
    EmojiiconRecents mRecents;
    com.c2call.android.emoji.emoji.Emojicon[] mData;
    
    public EmojiiconGridView(Context context, com.c2call.android.emoji.emoji.Emojicon[] emojicons, EmojiiconRecents recents, EmojiiconsFragment emojiconFragment) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		mEmojiconFragment = emojiconFragment;
		rootView = inflater.inflate(R.layout.emojicon_grid, null);
		setRecents(recents);
		 GridView gridView = (GridView) rootView.findViewById(R.id.Emoji_GridView);
	        if (emojicons== null) {
	            mData = People.DATA;
	        } else {
	            Object[] o = (Object[]) emojicons;
	            mData = Arrays.asList(o).toArray(new Emojicon[o.length]);
	        }
	        com.c2call.android.emoji.EmojiAdapter mAdapter = new com.c2call.android.emoji.EmojiAdapter(rootView.getContext(), mData);
	        mAdapter.setEmojiClickListener(new OnEmojiconClickedListener() {
				
				@Override
				public void onEmojiconClicked(com.c2call.android.emoji.emoji.Emojicon emojicon) {
					if (mEmojiconFragment.onEmojiconClickedListener != null) {
						mEmojiconFragment.onEmojiconClickedListener.onEmojiconClicked(emojicon);
			        }
			        if (mRecents != null) {
			            mRecents.addRecentEmoji(rootView.getContext(), emojicon);
			        }
				}
			});
	        gridView.setAdapter(mAdapter);
	}
    
	private void setRecents(EmojiiconRecents recents) {
        mRecents = recents;
    }

    public interface OnEmojiconClickedListener {
        void onEmojiconClicked(com.c2call.android.emoji.emoji.Emojicon emojicon);
    }
    
}
