package com.c2call.android.emoji;

import android.content.Context;

import com.c2call.android.emoji.emoji.Emojicon;

public interface EmojiiconRecents
{
    public void addRecentEmoji(Context context, Emojicon emojicon);
}
