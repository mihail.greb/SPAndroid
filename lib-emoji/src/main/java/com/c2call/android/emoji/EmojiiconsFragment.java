package com.c2call.android.emoji;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.c2call.android.emoji.emoji.Emojicon;
import com.c2call.android.emoji.emoji.Nature;
import com.c2call.android.emoji.emoji.Objects;
import com.c2call.android.emoji.emoji.People;
import com.c2call.android.emoji.emoji.Places;
import com.c2call.android.emoji.emoji.Symbols;

import java.util.Arrays;
import java.util.List;


public class EmojiiconsFragment extends Fragment implements ViewPager.OnPageChangeListener, EmojiiconRecents
{

    public static final String KEYBOARD_KEY = "KEYBOARD_HEIGHT";

    private int mEmojiTabLastSelectedIndex = -1;
    private View[] mEmojiTabs;
    private PagerAdapter mEmojisAdapter;
    private EmojiiconRecentsManager mRecentsManager;
    private int keyBoardHeight = 0;
    private Boolean isKeyboardOpened = false;
    private Boolean isEmojiOpened = false;
    public EmojiiconGridView.OnEmojiconClickedListener onEmojiconClickedListener;
    public OnEmojiconBackspaceClickedListener onEmojiconBackspaceClickedListener;
    public OnSoftKeyboardOpenCloseListener onSoftKeyboardOpenCloseListener;
    private View rootView;

    private ViewPager emojisPager;
    private RelativeLayout _emojiLayout;
    private AnimationListener _animationListener;

    public interface AnimationListener {
        void onAnimationStart();
        void onAnimationEnd();
    }

    public static EmojiiconsFragment create()
    {
        final EmojiiconsFragment f = new EmojiiconsFragment();
        return f;
    }

    public void setAnimationListener(final AnimationListener listener) {
        _animationListener = listener;
    }

    public void setEmojiLayout(RelativeLayout emojiLayout)
    {
        _emojiLayout = emojiLayout;
    }

    public void setRootView(View view)
    {
        rootView = view;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.emojicons, container, false);
        initChildren(view);
        int defaultHeight = (int) getResources().getDimension(R.dimen.keyboard_height);

        int resourceId = rootView.getContext().getResources()
                .getIdentifier("status_bar_height",
                        "dimen", "android");
        if (resourceId > 0)
        {
            defaultHeight += rootView.getContext().getResources()
                    .getDimensionPixelSize(resourceId);
        }
        int savedHeight = PreferenceManager.getDefaultSharedPreferences(view.getContext())
                .getInt(KEYBOARD_KEY, defaultHeight);
        setSize(WindowManager.LayoutParams.MATCH_PARENT, savedHeight);
        return view;
    }

    private void initChildren(View view)
    {
        emojisPager = (ViewPager) view.findViewById(R.id.emojis_pager);
        emojisPager.setOnPageChangeListener(this);
        EmojiiconRecents recents = this;
        mEmojisAdapter = new EmojisPagerAdapter(
                Arrays.asList(
                        new EmojiiconRecentsGridView(getActivity(), null, null, this),
                        new EmojiiconGridView(getActivity(), People.DATA, recents, this),
                        new EmojiiconGridView(getActivity(), Nature.DATA, recents, this),
                        new EmojiiconGridView(getActivity(), Objects.DATA, recents, this),
                        new EmojiiconGridView(getActivity(), Places.DATA, recents, this),
                        new EmojiiconGridView(getActivity(), Symbols.DATA, recents, this)
                )
        );
        emojisPager.setAdapter(mEmojisAdapter);
        mEmojiTabs = new View[6];
        mEmojiTabs[0] = view.findViewById(R.id.emojis_tab_0_recents);
        mEmojiTabs[1] = view.findViewById(R.id.emojis_tab_1_people);
        mEmojiTabs[2] = view.findViewById(R.id.emojis_tab_2_nature);
        mEmojiTabs[3] = view.findViewById(R.id.emojis_tab_3_objects);
        mEmojiTabs[4] = view.findViewById(R.id.emojis_tab_4_cars);
        mEmojiTabs[5] = view.findViewById(R.id.emojis_tab_5_punctuation);
        for (int i = 0; i < mEmojiTabs.length; i++)
        {
            final int position = i;
            mEmojiTabs[i].setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    emojisPager.setCurrentItem(position);
                }
            });
        }
        view.findViewById(R.id.emojis_backspace).setOnTouchListener(new RepeatListener(1000, 50, new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                if (onEmojiconBackspaceClickedListener != null)
                    onEmojiconBackspaceClickedListener.onEmojiconBackspaceClicked(v);
            }
        }));

        // get last selected page
        mRecentsManager = EmojiiconRecentsManager.getInstance(view.getContext());
        int page = mRecentsManager.getRecentPage();
        // last page was recents, check if there are recents to use
        // if none was found, go to page 1
        if (page == 0 && mRecentsManager.size() == 0)
        {
            page = 1;
        }

        if (page == 0)
        {
            onPageSelected(page);
        } else
        {
            emojisPager.setCurrentItem(page, false);
        }
    }

    /**
     * Set the listener for the event of keyboard opening or closing.
     */
    public void setOnSoftKeyboardOpenCloseListener(OnSoftKeyboardOpenCloseListener listener)
    {
        this.onSoftKeyboardOpenCloseListener = listener;
    }

    /**
     * Set the listener for the event when any of the emojicon is clicked
     */
    public void setOnEmojiiconClickedListener(EmojiiconGridView.OnEmojiconClickedListener listener)
    {
        this.onEmojiconClickedListener = listener;
    }

    /**
     * Set the listener for the event when backspace on emojicon popup is clicked
     */
    public void setOnEmojiiconBackspaceClickedListener(OnEmojiconBackspaceClickedListener listener)
    {
        this.onEmojiconBackspaceClickedListener = listener;
    }

    /**
     * @return Returns true if the soft keyboard is open, false otherwise.
     */
    public Boolean isKeyBoardOpen()
    {
        return isKeyboardOpened;
    }

    public void setEmojiOpen(boolean isOpened)
    {
        isEmojiOpened = isOpened;
    }

    public boolean isEmojiOpen()
    {
        return isEmojiOpened;
    }

    @Override
    public void onDestroy()
    {
        EmojiiconRecentsManager
                .getInstance(getActivity()).saveRecents();
        super.onDestroy();
    }

    /**
     * Call this function to resize the emoji popup according to your soft keyboard size
     */
    public void setSizeForSoftKeyboard()
    {
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
        {
            @Override
            public void onGlobalLayout()
            {
                Rect r = new Rect();
                rootView.getWindowVisibleDisplayFrame(r);

                int screenHeight = getUsableScreenHeight(rootView.getContext());
                int heightDifference = screenHeight
                        - (r.bottom - r.top);
                int resourceId = rootView.getContext().getResources()
                        .getIdentifier("status_bar_height",
                                "dimen", "android");
                if (resourceId > 0)
                {
                    heightDifference -= rootView.getContext().getResources()
                            .getDimensionPixelSize(resourceId);
                }

                if (heightDifference > 100)
                {
                    if (keyBoardHeight != heightDifference)
                    {
                        keyBoardHeight = heightDifference;
                        setSize(WindowManager.LayoutParams.MATCH_PARENT, keyBoardHeight);

                        SharedPreferences preferences = PreferenceManager
                                .getDefaultSharedPreferences(rootView.getContext());
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt(KEYBOARD_KEY, keyBoardHeight);
                        editor.apply();
                    }

                    if (isKeyboardOpened == false)
                    {
                        if (onSoftKeyboardOpenCloseListener != null)
                            onSoftKeyboardOpenCloseListener.onKeyboardOpen(keyBoardHeight);
                    }
                    isKeyboardOpened = true;

                } else
                {
                    isKeyboardOpened = false;
                    if (onSoftKeyboardOpenCloseListener != null)
                        onSoftKeyboardOpenCloseListener.onKeyboardClose();
                }
            }
        });
    }

    public void setSize(int width, int height)
    {
        if (_emojiLayout != null)
        {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);
            _emojiLayout.setLayoutParams(params);
        }
    }

    @Override
    public void addRecentEmoji(Context context, Emojicon emojicon)
    {
        EmojiiconRecentsGridView fragment = ((EmojisPagerAdapter) emojisPager.getAdapter()).getRecentFragment();
        fragment.addRecentEmoji(context, emojicon);
    }


    @Override
    public void onPageScrolled(int i, float v, int i2)
    {
    }

    @Override
    public void onPageSelected(int i)
    {
        if (mEmojiTabLastSelectedIndex == i)
        {
            return;
        }
        switch (i)
        {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                if (mEmojiTabLastSelectedIndex >= 0 && mEmojiTabLastSelectedIndex < mEmojiTabs.length)
                {
                    mEmojiTabs[mEmojiTabLastSelectedIndex].setSelected(false);
                }
                mEmojiTabs[i].setSelected(true);
                mEmojiTabLastSelectedIndex = i;
                mRecentsManager.setRecentPage(i);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int i)
    {
    }

    private static class EmojisPagerAdapter extends PagerAdapter
    {
        private List<EmojiiconGridView> views;

        public EmojiiconRecentsGridView getRecentFragment()
        {
            for (EmojiiconGridView it : views)
            {
                if (it instanceof EmojiiconRecentsGridView)
                    return (EmojiiconRecentsGridView) it;
            }
            return null;
        }

        public EmojisPagerAdapter(List<EmojiiconGridView> views)
        {
            super();
            this.views = views;
        }

        @Override
        public int getCount()
        {
            return views.size();
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            View v = views.get(position).rootView;
            ((ViewPager) container).addView(v, 0);
            return v;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object view)
        {
            ((ViewPager) container).removeView((View) view);
        }

        @Override
        public boolean isViewFromObject(View view, Object key)
        {
            return key == view;
        }
    }

    /**
     * A class, that can be used as a TouchListener on any view (e.g. a Button).
     * It cyclically runs a clickListener, emulating keyboard-like behaviour. First
     * click is fired immediately, next before initialInterval, and subsequent before
     * normalInterval.
     * <p/>
     * <p>Interval is scheduled before the onClick completes, so it has to run fast.
     * If it runs slow, it does not generate skipped onClicks.
     */
    public static class RepeatListener implements View.OnTouchListener
    {

        private Handler handler = new Handler();

        private int initialInterval;
        private final int normalInterval;
        private final View.OnClickListener clickListener;

        private Runnable handlerRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                if (downView == null)
                {
                    return;
                }
                handler.removeCallbacksAndMessages(downView);
                handler.postAtTime(this, downView, SystemClock.uptimeMillis() + normalInterval);
                clickListener.onClick(downView);
            }
        };

        private View downView;

        /**
         * @param initialInterval The interval before first click event
         * @param normalInterval  The interval before second and subsequent click
         *                        events
         * @param clickListener   The OnClickListener, that will be called
         *                        periodically
         */
        public RepeatListener(int initialInterval, int normalInterval, View.OnClickListener clickListener)
        {
            if (clickListener == null)
                throw new IllegalArgumentException("null runnable");
            if (initialInterval < 0 || normalInterval < 0)
                throw new IllegalArgumentException("negative interval");

            this.initialInterval = initialInterval;
            this.normalInterval = normalInterval;
            this.clickListener = clickListener;
        }

        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            switch (motionEvent.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    downView = view;
                    handler.removeCallbacks(handlerRunnable);
                    handler.postAtTime(handlerRunnable, downView, SystemClock.uptimeMillis() + initialInterval);
                    clickListener.onClick(view);
                    return true;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_OUTSIDE:
                    handler.removeCallbacksAndMessages(downView);
                    downView = null;
                    return true;
            }
            return false;
        }
    }

    public interface OnEmojiconBackspaceClickedListener
    {
        void onEmojiconBackspaceClicked(View v);
    }

    public interface OnSoftKeyboardOpenCloseListener
    {
        void onKeyboardOpen(int keyBoardHeight);

        void onKeyboardClose();
    }

    private int getUsableScreenHeight(Context context)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
        {
            DisplayMetrics metrics = new DisplayMetrics();

            WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            windowManager.getDefaultDisplay().getMetrics(metrics);

            return metrics.heightPixels;

        } else
        {
            return rootView.getRootView().getHeight();
        }
    }

    @Override
    public Animator onCreateAnimator(int transit, boolean enter, int nextAnim)
    {
        final Animator anim = AnimatorInflater.loadAnimator(getActivity(), nextAnim);
        anim.addListener(new AnimatorListenerAdapter()
        {
            @Override
            public void onAnimationStart(Animator animation)
            {
                if (_animationListener != null)
                {
                    _animationListener.onAnimationStart();
                }
            }

            @Override
            public void onAnimationEnd(Animator animation)
            {
                if (_animationListener != null)
                {
                    _animationListener.onAnimationEnd();
                }
            }
        });
        return anim;
    }
}
