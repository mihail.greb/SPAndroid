package com.app.android.shadow.main.tapmanager.settings;

import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.app.android.shadow.game.alarm.preferences.AlarmPreferenceListAdapter;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.dbhelper.ShadowDB;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.main.util.AlertDialogClass;
import com.app.android.shadow.game.alarm.preferences.AlarmPreferencesFragment;
import com.app.android.shadow.game.calculator.Calculator;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.R;
import com.app.android.shadow.main.util.Logger;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;


public class Modes extends BaseFragment implements OnClickListener,
        CompoundButton.OnCheckedChangeListener, OnCheckedChangeListener {

    public static final String LOGIN_TAB_KEY = "loginTabKey";

    View view = null;
    ViewHolder holder;
    boolean isProApp;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        closeSoftKeyboard();
        if (view == null) {
            isProApp = Shadow.isProApp();
            view = inflater.inflate(R.layout.shadow_modes, container, false);
            setHeader(view);
            holder = new ViewHolder();
            inti();
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        setModeData();

        /*if (Shadow.getAppPreferences().getBoolean(Const.FIRST_TIME_LOGGED, true)) {
            Logger.printLog(Logger.ERROR, "IS_REGISTRATION true " + Shadow.getAppPreferences().getBoolean(Const.FIRST_TIME_LOGGED, false));
            //int msgId = Shadow.getFirstTimeMessage();
            //AlertDialogClass.showPopupWindow(getActivity(), resources.getString(msgId),this);
            //IntroActivity.showPopupWindow(getActivity(), resources.getString(msgId),this);
            Shadow.editor.putBoolean(Const.FIRST_TIME_LOGGED, false).commit();
            //View v1 = getActivity().getCurrentFocus();
            //if (v1 != null) {
             //   closeSoftKeyboard(v1);
            //}
            //closeSoftKeyboard();
        } else {
            Logger.printLog(Logger.ERROR, "IS_REGISTRATION " + Shadow.getAppPreferences().getBoolean(Const.FIRST_TIME_LOGGED, false));
        }*/
        return view;
    }

    private void inti() {

        holder.header_layout = (RelativeLayout) view.findViewById(R.id.header_layout);
        holder.header_layout.setBackgroundColor(Color.parseColor("#0B0F13"));

        /*TextView*/
        holder.logout_time_safe = (TextView) view.findViewById(R.id.logout_time_safe);
        if(!Shadow.isProApp()){
            int value = getLogoutTime(ShadowDB.LOGOUT_TIME);
            if(value == -1){
                value = 15;
            }
            holder.logout_time_safe.setText(getResources().getString(R.string.logout_time_sp, value));
        }
        holder.logout_time_unsafe = (TextView) view.findViewById(R.id.logout_time_unsafe);
        holder.tab_times_safe = (TextView) view.findViewById(R.id.tab_times_safe);
        holder.tab_times_unsafe = (TextView) view.findViewById(R.id.tab_times_unsafe);
        holder.tap_count_text_safe = (TextView) view.findViewById(R.id.tap_count_text_safe);
        holder.tab_x_time_unsafe = (TextView) view.findViewById(R.id.tab_x_time_unsafe);
        holder.tab_x_time_unsafe.setOnClickListener(this);
        if(Shadow.isYellowPuzzle()){
            holder.tab_times_safe.setVisibility(View.VISIBLE);
            holder.tab_times_unsafe.setVisibility(View.VISIBLE);
            holder.tab_times_safe.setOnClickListener(this);
            holder.tab_times_unsafe.setOnClickListener(this);
        }
        holder.logout_time_safe.setOnClickListener(this);
        holder.logout_time_unsafe.setOnClickListener(this);
        holder.safe_mode = (TextView) view.findViewById(R.id.safe_mode);
        holder.unsafe_mode = (TextView) view.findViewById(R.id.unsafe_mode);
        //holder.safe_mode.setOnClickListener(this);
        //holder.unsafe_mode.setOnClickListener(this);

        /*LinearLayout*/
        holder.safeLayout = (LinearLayout) view.findViewById(R.id.safe_layout);
        holder.unSafeLayout = (LinearLayout) view.findViewById(R.id.unsafe_layout);

        /*LinearLayout*/
        holder.layout_safe_title = (LinearLayout) view.findViewById(R.id.layout_safe_title);
        holder.layout_safe_title.setOnClickListener(this);
        holder.layout_unsafe_title = (LinearLayout) view.findViewById(R.id.layout_unsafe_title);
        holder.layout_unsafe_title.setOnClickListener(this);
        /*ImageView */
        holder.safe_select = (ImageView) view.findViewById(R.id.safe_select);
        holder.unsafe_select = (ImageView) view.findViewById(R.id.unsafe_select);

        /*Radio buttons*/
        holder.access_choose_group_unsafe = (RadioGroup) view.findViewById(R.id.access_choose_group_unsafe);
        holder.access_choose_group_unsafe.setOnCheckedChangeListener(this);
        holder.with_pass_unsafe = (RadioButton) view.findViewById(R.id.with_pass_unsafe);
        holder.without_pass_unsafe = (RadioButton) view.findViewById(R.id.without_pass_unsafe);
        holder.with_pass_unsafe.setButtonDrawable(null);
        holder.without_pass_unsafe.setButtonDrawable(null);
        holder.with_pass_unsafe.setLeft(0);
        holder.without_pass_unsafe.setLeft(0);

        /*Check box*/
        holder.vibrate_call_safe = (CheckBox) view.findViewById(R.id.vibrate_call_safe);
        holder.vibrate_call_safe.setOnCheckedChangeListener(this);
        holder.vibrate_message_safe = (CheckBox) view.findViewById(R.id.vibrate_message_safe);
        holder.vibrate_message_safe.setOnCheckedChangeListener(this);
        holder.shake_safe = (CheckBox) view.findViewById(R.id.shake_safe);
        holder.shake_safe.setOnCheckedChangeListener(this);
        holder.enter_name_pass_safe = (CheckBox) view.findViewById(R.id.enter_name_pass_safe);
        holder.enter_name_pass_safe.setOnCheckedChangeListener(this);
        holder.tap_count_safe = (CheckBox) view.findViewById(R.id.tap_count_safe);
        holder.tap_count_safe.setOnCheckedChangeListener(this);
        holder.enter_name_pass_text_filed_unsafe = (TextView) view.findViewById(R.id.enter_name_pass_text_filed_unsafe);

        holder.cover_message_unsafe = (CheckBox) view.findViewById(R.id.cover_message_unsafe);
        holder.cover_message_unsafe.setOnCheckedChangeListener(this);
        holder.ring_tone_call_unsafe = (CheckBox) view.findViewById(R.id.ring_tone_call_unsafe);
        holder.ring_tone_call_unsafe.setOnCheckedChangeListener(this);
        holder.vibration_call_unsafe = (CheckBox) view.findViewById(R.id.vibration_call_unsafe);
        holder.vibration_call_unsafe.setOnCheckedChangeListener(this);
        holder.chat_sound_unsafe = (CheckBox) view.findViewById(R.id.chat_sound_unsafe);
        holder.chat_sound_unsafe.setOnCheckedChangeListener(this);
        holder.chat_vibrate_unsafe = (CheckBox) view.findViewById(R.id.chat_vibrate_unsafe);
        holder.chat_vibrate_unsafe.setOnCheckedChangeListener(this);
        holder.shake_unsafe = (CheckBox) view.findViewById(R.id.shake_unsafe);
        holder.shake_unsafe.setOnCheckedChangeListener(this);
        holder.enter_name_pass_unsafe = (CheckBox) view.findViewById(R.id.enter_name_pass_unsafe);
        holder.enter_name_pass_unsafe.setOnCheckedChangeListener(this);

        /*new added data */
        holder.enter_name_pass_text_filed_unsafe = (TextView) view.findViewById(R.id.enter_name_pass_text_filed_unsafe);
        holder.enter_name_pass_text_filed_safe = (TextView) view.findViewById(R.id.enter_name_pass_text_filed_safe);
        holder.tab_check_free_unsafe_box = (LinearLayout) view.findViewById(R.id.tab_check_free_unsafe_box);
        holder.pro_app_tap_count = (LinearLayout) view.findViewById(R.id.pro_app_tap_count);
        holder.tab_check_for_free = (CheckBox) view.findViewById(R.id.tab_check_for_free);
        holder.tab_check_for_free.setOnCheckedChangeListener(this);

        if (!isProApp) {
            holder.enter_name_pass_text_filed_unsafe.setText(resources.getString(R.string.enter_name_pass_free));
            holder.enter_name_pass_text_filed_safe.setText(resources.getString(R.string.enter_name_pass_free));
            holder.tab_check_free_unsafe_box.setVisibility(View.VISIBLE);
            holder.access_choose_group_unsafe.setVisibility(View.GONE);
            holder.pro_app_tap_count.setVisibility(View.GONE);
        } else {
            holder.enter_name_pass_text_filed_unsafe.setText(resources.getString(R.string.enter_name_pass));
            holder.enter_name_pass_text_filed_safe.setText(resources.getString(R.string.enter_name_pass));
            holder.tab_check_free_unsafe_box.setVisibility(View.GONE);
            if(!Shadow.isAlarmGame() && !Shadow.isCalculator()) {
                holder.access_choose_group_unsafe.setVisibility(View.VISIBLE);
                holder.pro_app_tap_count.setVisibility(View.VISIBLE);
            }else {
                holder.access_choose_group_unsafe.setVisibility(View.GONE);
                holder.pro_app_tap_count.setVisibility(View.GONE);
            }
        }

        if (Shadow.isAlarmGame() || Shadow.isCalculator()) {
            initAlarm();
        }else if(Shadow.isYellowPuzzle() || (!Shadow.isProApp())){
            setTabText();
        }
    }


    private void initAlarm() {
        holder.forAlarm_unsafe = (LinearLayout) view.findViewById(R.id.forAlarm_safe);
        holder.forAlarm_safe = (LinearLayout) view.findViewById(R.id.forAlarm_unsafe);
        holder.safe_enter_name_pass_check_layout= (LinearLayout) view.findViewById(R.id.safe_enter_name_pass_check_layout);
        holder.safe_enter_name_pass_check_layout.setVisibility(View.GONE);
        holder.forAlarm_unsafe.setVisibility(View.VISIBLE);
        holder.forAlarm_safe.setVisibility(View.VISIBLE);
        holder.set_login_time_safe = (TextView) view.findViewById(R.id.set_login_time_safe);
        holder.enter_name_pass_check_ll = (LinearLayout) view.findViewById(R.id.enter_name_pass_check_ll);
        holder.enter_name_pass_check_ll.setVisibility(View.GONE);
        holder.set_login_time_unsafe = (TextView) view.findViewById(R.id.set_login_time_unsafe);
        holder.set_login_time_safe.setOnClickListener(this);
        holder.set_login_time_unsafe.setOnClickListener(this);

        holder.alarm_swipe_safe = (LinearLayout) view.findViewById(R.id.alarm_swipe_safe);
        holder.alarm_swipe_safe.setVisibility(View.VISIBLE);
        holder.tap_count_text_swipe_safe = (TextView) view.findViewById(R.id.tap_count_text_swipe_safe);
        holder.tab_count_swipe_safe = (CheckBox) view.findViewById(R.id.tab_count_swipe_safe);
        holder.tab_count_swipe_safe.setOnCheckedChangeListener(this);
        holder.tab_count_swipe_safe.setChecked(getStatus(ShadowDB.IS_SWIPE_X_TIMES));

        holder.access_choose_group_swipe_unsafe = (RadioGroup) view.findViewById(R.id.access_choose_group_swipe_unsafe);
        holder.access_choose_group_swipe_unsafe.setVisibility(View.VISIBLE);
        holder.access_choose_group_swipe_unsafe.setOnCheckedChangeListener(this);
        holder.with_pass_swipe_unsafe = (RadioButton) view.findViewById(R.id.with_pass_swipe_unsafe);
        holder.without_pass_swipe_unsafe = (RadioButton) view.findViewById(R.id.without_pass_swipe_unsafe);
        holder.with_pass_swipe_unsafe.setButtonDrawable(null);
        holder.without_pass_swipe_unsafe.setButtonDrawable(null);
        holder.with_pass_swipe_unsafe.setLeft(0);
        holder.without_pass_swipe_unsafe.setLeft(0);
        setUnsafeAlarmWTA(getStatus(ShadowDB.UN_IS_PASS_ON_X_SWIPE));


        holder.enter_name_pass_text_filed_safe.setText(resources.getString(R.string.enter_name_pass_alarm));
        holder.enter_name_pass_text_filed_unsafe.setText(resources.getString(R.string.enter_name_pass_alarm));

        holder.tap_count_safe_text = (TextView) view.findViewById(R.id.tap_count_text_safe);
        holder.tap_count_safe_text.setText(resources.getString(R.string.tap_x_time_alarm_safe));
        holder.with_pass_unsafe.setText(resources.getString(R.string.tap_x_time_alarm_safe));
        holder.without_pass_unsafe.setText(resources.getString(R.string.tap_x_time_alarm_unsafe));

        if (Shadow.isAlarmGame()) {
            setAlarmTimeText();
        }else if(Shadow.isCalculator()){
           /* holder.tap_count_text_swipe_safe.setText(resources.getString(R.string.cal_swipe_safe));
            holder.with_pass_swipe_unsafe.setText(resources.getString(R.string.cal_swipe_safe));
            holder.without_pass_swipe_unsafe.setText(resources.getString(R.string.cal_swipe_unsafe));*/
            holder.tap_count_text_swipe_safe.setVisibility(View.GONE);
            holder.tab_count_swipe_safe.setVisibility(View.GONE);
            holder.with_pass_swipe_unsafe.setVisibility(View.GONE);
            holder.without_pass_swipe_unsafe.setVisibility(View.GONE);
            holder.tab_check_swipe_cal_unsafe_box = (LinearLayout) view.findViewById(R.id.tab_check_swipe_cal_unsafe_box);
            holder.tab_check_swipe_cal_unsafe_box.setVisibility(View.VISIBLE);
            holder.tab_check_swipe_cal_unsafe = (CheckBox) view.findViewById(R.id.tab_check_swipe_cal_unsafe);
            holder.tab_check_swipe_cal_unsafe.setOnCheckedChangeListener(this);

            holder.calculation_safe = (Button) view.findViewById(R.id.calculation_safe);
            holder.calculation_unsafe = (Button) view.findViewById(R.id.calculation_unsafe);
            holder.calculation_safe.setVisibility(View.VISIBLE);
            holder.calculation_unsafe.setVisibility(View.VISIBLE);
            holder.calculation_safe.setOnClickListener(this);
            holder.calculation_unsafe.setOnClickListener(this);


            /*LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins((int) resources.getDimension(R.dimen.margin_bottom), (int) resources.getDimension(R.dimen.margin_bottom), 0, 0);
            holder.forAlarm_safe.setLayoutParams(params);*/
            setCalText();
            setUnsafeCalculatorCheckbox(getStatus(ShadowDB.UN_IS_PASS_ON_X_SWIPE));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(Shadow.isCalculator()){
            setCalText();
        }
    }

    private void setTabText() {
        String tabs = getSavedTabs();
        Logger.printLog(Logger.ERROR,"working","tabs is "+tabs);
        String tabsTime;
        if (tabs != null && tabs.length() > 0) {
            tabsTime = tabs;
        } else {
            tabsTime = Const.DEFAULT_TABS;
        }

        if(Shadow.isYellowPuzzle()) {
            String tabsStr1 = resources.getString(R.string.yp_enter_tab_count, tabsTime);
            String tabsStr2 = resources.getString(R.string.top_count_without, tabsTime);
            String tabsStr3 = resources.getString(R.string.tap_count, tabsTime);
            holder.tab_times_safe.setText(tabsStr1);
            holder.tab_times_unsafe.setText(tabsStr1);

            holder.without_pass_unsafe.setText(tabsStr2);
            holder.with_pass_unsafe.setText(tabsStr3);
            holder.tap_count_text_safe.setText(tabsStr3);
        }
        String tabsStr4 = resources.getString(R.string.tap_8_time_free,tabsTime);
        Logger.printLog(Logger.ERROR,"str is ",tabsStr4);
        holder.tab_x_time_unsafe.setText(tabsStr4);
    }
    private void setCalText() {
        String time = getSavedCalculation();
        Logger.printLog(Logger.ERROR,"working","time is "+time);
        String fullString;
        if (time != null && time.length() > 0 && !time.equals("0*0")) {
            fullString = resources.getString(R.string.cal_saved_login_calculation, time);
            Logger.printLog(Logger.ERROR,"full str","is "+fullString);
        } else {
            fullString = resources.getString(R.string.cal_set_login_calculation_def);
            Logger.printLog(Logger.ERROR,"not work","is "+fullString);
        }
        holder.set_login_time_unsafe.setText(fullString);
        holder.set_login_time_safe.setText(fullString);
    }

    public static void setSavedTabs(String getet) {
        boolean isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
        int tab;
        try {
            tab = Integer.parseInt(getet);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            tab = 8;
        }
        if(Shadow.isProApp()) {
            Shadow.getAppPreferencesEditor().putString((isSafeMode ? Const.YP_SAVE_TAB_SAFE : Const.YP_SAVE_TAB_UNSAFE), ("" + tab)).commit();
        }else if(!isSafeMode){
            Shadow.getAppPreferencesEditor().putString(Const.SP_SAVE_TAB_UNSAFE, ("" + tab)).commit();
        }
    }

    public static String getSavedTabs(){
        String tabs;
        boolean isProApp = Shadow.isProApp();
        boolean isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
        if (isSafeMode && isProApp){
            tabs = Shadow.getAppPreferences().getString(Const.YP_SAVE_TAB_SAFE, Const.DEFAULT_TABS);
        } else {
            if(isProApp)
                tabs = Shadow.getAppPreferences().getString(Const.YP_SAVE_TAB_UNSAFE, Const.DEFAULT_TABS);
            else
                tabs = Shadow.getAppPreferences().getString(Const.SP_SAVE_TAB_UNSAFE, Const.DEFAULT_TABS);
        }
        return tabs;
    }

    public static void setSavedCalculation(String getet) {
        boolean isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
        Shadow.getAppPreferencesEditor().putString((isSafeMode?Const.CAL_SAVE_LOGIN_SAFE:Const.CAL_SAVE_LOGIN_UNSAFE), getet).commit();
    }

    public static String getSavedCalculation(){
        String time;
        boolean isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
        if (isSafeMode){
            time = Shadow.getAppPreferences().getString(Const.CAL_SAVE_LOGIN_SAFE, null);
        } else {
            time = Shadow.getAppPreferences().getString(Const.CAL_SAVE_LOGIN_UNSAFE, null);
        }
        return time;
    }


    private void setAlarmTimeText() {
        if (Shadow.isAlarmGame()) {
            String time = AlarmPreferenceListAdapter.getTimeInAMPM(AlarmPreferencesFragment.getAlarmSaveTimeStr());
            String fullString;
            if (time != null && time.length() > 0) {
                fullString = resources.getString(R.string.alarm_saved_login_time, time);
            } else {
                fullString = resources.getString(R.string.alarm_set_login_time_def);
            }
            holder.set_login_time_unsafe.setText(fullString);
            holder.set_login_time_safe.setText(fullString);
        }
    }

    private void callSaveTime() {
        final int hour;
        final int minute;
        final long saveTimeinMS = AlarmPreferencesFragment.getAlarmSaveTime();
        if (saveTimeinMS != 0) {
            hour = msToHour(saveTimeinMS);
            minute = msToMin(saveTimeinMS);
        } else {
            Calendar mcurrentTime = Calendar.getInstance();
            hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            minute = mcurrentTime.get(Calendar.MINUTE);
        }
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
                Calendar newAlarmTime = GregorianCalendar.getInstance();
                newAlarmTime.setTime(new Date());
                newAlarmTime.set(Calendar.HOUR_OF_DAY, hours);
                newAlarmTime.set(Calendar.MINUTE, minutes);
                newAlarmTime.set(Calendar.SECOND, 0);
                //SimpleDateFormat df = new SimpleDateFormat("hh:mm");
                //Date d1 = null;
                try {
                    //d1 = df.parse(""+hours+":"+minutes);
                    Logger.printLog(Logger.ERROR, "set Time in ms hour min" + hours + "  " + minutes);
                    //Calendar newAlarmTime = GregorianCalendar.getInstance();
                    //newAlarmTime.setTimeZone( TimeZone.getTimeZone("GMT") );
                    // newAlarmTime.setTime(d1);
                    AlarmPreferencesFragment.saveAlarmSaveTime(newAlarmTime.getTimeInMillis());
                    Logger.printLog(Logger.ERROR, "set Time in ms " + newAlarmTime.getTimeInMillis());
                    Logger.printLog(Logger.ERROR, "set Time in ms hour" + msToHour(newAlarmTime.getTimeInMillis()));
                    Logger.printLog(Logger.ERROR, "set Time in ms hour" + msToMin(newAlarmTime.getTimeInMillis()));
                    setAlarmTimeText();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, hour, minute, false);
        timePickerDialog.setTitle(resources.getString(R.string.alarm_set_login_time_def));
        timePickerDialog.show();
    }

    public static int msToHour(long ms) {
        int hrs = (int) TimeUnit.MILLISECONDS.toHours(ms) % 24;
        return hrs;
        /*int seconds = (int) (milliseconds / 1000) % 60 ;
int minutes = (int) ((milliseconds / (1000*60)) % 60);
int hours   = (int) ((milliseconds / (1000*60*60)) % 24);*/
    }

    public static int msToMin(long ms) {
        int min = (int) TimeUnit.MILLISECONDS.toMinutes(ms) % 60;
        return min;
    }

    public static String msToTimeStr(long ms) {
        int hrs = (int) TimeUnit.MILLISECONDS.toHours(ms) % 24;
        int min = (int) TimeUnit.MILLISECONDS.toMinutes(ms) % 60;
        return String.format("%02d:%02d:%02d", hrs, min, 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_safe_title:
                setMode(true, false);
                break;
            case R.id.layout_unsafe_title:
                setMode(false, false);
                break;
            case R.id.logout_time_safe:
                showVideoPopup(ShadowDB.LOGOUT_TIME, holder.logout_time_safe);
                break;
            case R.id.logout_time_unsafe:
                showVideoPopup(ShadowDB.UN_LOGOUT_TIME, holder.logout_time_unsafe);
                break;
            case R.id.set_login_time_safe:
                if(Shadow.isCalculator()) {

                }else
                    callSaveTime();
                break;
            case R.id.set_login_time_unsafe:
                if(Shadow.isCalculator()) {

                }else
                    callSaveTime();
                break;
            case R.id.calculation_safe:
                callSaveCalculation();
                break;
            case R.id.calculation_unsafe:
                callSaveCalculation();
                break;
            case R.id.tab_times_unsafe:
                showVideoPopup(LOGIN_TAB_KEY,null);
                break;
            case R.id.tab_times_safe:
                showVideoPopup(LOGIN_TAB_KEY,null);
                break;
            case R.id.tab_x_time_unsafe:
                showVideoPopup(LOGIN_TAB_KEY,null);
                break;


        }
    }

    public void callSaveCalculation() {
        //
        Intent intent = new Intent(getActivity(),Calculator.class);
        intent.putExtra(Const.IS_SET_CALCULATION,true);
        getActivity().startActivity(intent);
    }

/*
    @Override
    public void onResume() {
        super.onResume();
        closeSoftKeyboard();
    }
*/

    public void showVideoPopup(final String key, final TextView viewText) {

        try {
            LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = layoutInflater.inflate(R.layout.popup_et, null);
            final PopupWindow popup = new PopupWindow(getActivity());
            popup.setContentView(layout);
            final EditText et = (EditText) layout.findViewById(R.id.enter_time);

            //Calculator
            if(key == LOGIN_TAB_KEY){
                et.setHint(R.string.enter_tab_times);
               /* et.setInputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_NUMBER_VARIATION_NORMAL);*/
            }
            et.requestFocus();
            et.setFocusable(true);
            if (et != null) {
                openSoftKeyboard();
            }

            //popup.setWindowLayoutMode(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            ImageView close = (ImageView) layout.findViewById(R.id.close);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeSoftKeyboard(et);
                    popup.dismiss();
                }
            });

            Button ok = (Button) layout.findViewById(R.id.ok);
            ok.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        String getet = et.getText().toString().trim();
                        if (getet.length() > 0) {
                            //Yp tabs
                            if(key == LOGIN_TAB_KEY) {
                                int x = 0;
                                try {
                                    x = Integer.parseInt(getet);
                                    if(x>=1) {
                                        //setSavedCalculation(getet);
                                        setSavedTabs(getet);
                                        popup.dismiss();
                                        //setCalText();
                                        setTabText();
                                    }else {
                                        Shadow.alertToast(resources.getString(R.string.tab_count_less_one));
                                    }
                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                    Shadow.alertToast(resources.getString(R.string.please_enter_number));
                                }
                            }else {
                                int value = Integer.parseInt(getet);
                                if (value > 0) {
                                    closeSoftKeyboard(et);
                                    popup.dismiss();
                                    putLogoutTime(key, value, viewText);
                                } else
                                    Shadow.alertToast(resources.getString(R.string.enter_valid_value));
                            }
                        } else {
                            Shadow.alertToast(resources.getString(R.string.enter_valid_value));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            //popup.setAnimationStyle(R.style.MyCustomPopupTheme);
            popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
            popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);

            popup.setFocusable(true);
            //popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            //popup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
            popup.showAtLocation(layout, Gravity.CENTER, 0, 0);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setMode(boolean isSafeMode, boolean isInit) {
        putStatus(ShadowDB.IS_SAFE_MODE, isSafeMode);
        setAlarmTimeText();
        //if(isSafeMode){
        holder.safeLayout.setVisibility(isSafeMode ? View.VISIBLE : View.GONE);
        holder.unSafeLayout.setVisibility(isSafeMode ? View.GONE : View.VISIBLE);
        holder.safe_mode.setTextColor(getColorId(isSafeMode ? R.color.white : R.color.modes_unselect_text));
        holder.unsafe_mode.setTextColor(getColorId(isSafeMode ? R.color.modes_unselect_text : android.R.color.white));
        /*}else {
            holder.safeLayout.setVisibility(View.GONE);
            holder.unSafeLayout.setVisibility(View.VISIBLE);
            holder.safe_mode.setTextColor(getColorId(android.R.color.darker_gray));
            holder.unsafe_mode.setTextColor(getColorId(R.color.white));
        }*/
        //holder.layout_safe_title.setBackgroundColor(Color.parseColor(""+getColorId(isSafeMode ? R.color.modes_select_back : R.color.modes_unselect_back)));
        //holder.layout_unsafe_title.setBackgroundColor(Color.parseColor(""+getColorId(isSafeMode ? R.color.modes_unselect_back : R.color.modes_select_back)));
        holder.layout_safe_title.setBackgroundColor(Color.parseColor(isSafeMode ? "#23272B" : "#151C23"));
        holder.layout_unsafe_title.setBackgroundColor(Color.parseColor(isSafeMode ? "#151C23" : "#23272B"));
        holder.safe_select.setVisibility(isSafeMode ? View.VISIBLE : View.INVISIBLE);
        holder.unsafe_select.setVisibility(isSafeMode ? View.INVISIBLE : View.VISIBLE);

        callMCallBack(isSafeMode, isInit);
    }

    private void callMCallBack(final boolean isSafeMode, final boolean isInit) {
        mCallback.onModeChangeEvent(isSafeMode);
        if (!isInit) {
            callMoreTouch(getLogoutTime((isSafeMode) ? ShadowDB.LOGOUT_TIME : ShadowDB.UN_LOGOUT_TIME));
        }
    }

    private void callMoreTouch(int time) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mCallback.onButtonClickEvent((Fragment) null, EventMaker.AutoLogout);
            }
        }).start();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.vibrate_call_safe:
                putStatus(ShadowDB.VIBRATE_FOR_CALL, holder.vibrate_call_safe.isChecked());
                break;
            case R.id.vibrate_message_safe:
                putStatus(ShadowDB.VIBRATE_FOR_CHAT, holder.vibrate_message_safe.isChecked());
                break;
            case R.id.shake_safe:
                putStatus(ShadowDB.SHAKE_LOGOUT, holder.shake_safe.isChecked());
                break;
            case R.id.enter_name_pass_safe:
                if (isProApp) {
                    if (!isChecked) {
                        if (!Shadow.isAlarmGame() && !Shadow.isCalculator()) {
                            /*if(!holder.tap_count_safe.isChecked() && !holder.tab_count_swipe_safe.isChecked()) {
                                holder.enter_name_pass_safe.setChecked(true);
                                new AlertDialogClass(getActivity()).open(resources.getString(R.string.message_on_unchecked_all));
                                return;
                            }
                        } else*/
                            if (!holder.tap_count_safe.isChecked()) {
                                holder.enter_name_pass_safe.setChecked(true);
                                new AlertDialogClass(getActivity()).open(resources.getString(R.string.message_on_unchecked_both));
                                return;
                            }
                        }
                    }
                    putStatus(ShadowDB.IS_ENTER_NAME_PASS, holder.enter_name_pass_safe.isChecked());
                } else {
                    holder.enter_name_pass_safe.setChecked(true);
                    /*new AlertDialogClass(getActivity()).open(resources.getString(R.string.can_not_unchecked));*/
                    return;
                }
                break;
            case R.id.tap_count_safe:
                if (!isChecked) {
                    if(!Shadow.isAlarmGame() && !Shadow.isCalculator()) {
                        /*if(!holder.enter_name_pass_safe.isChecked() && !holder.tab_count_swipe_safe.isChecked()) {
                            holder.tap_count_safe.setChecked(true);
                            new AlertDialogClass(getActivity()).open(resources.getString(R.string.message_on_unchecked_all));
                            return;
                        }
                    }else*/
                        if (!holder.enter_name_pass_safe.isChecked()) {
                            holder.tap_count_safe.setChecked(true);
                            new AlertDialogClass(getActivity()).open(resources.getString(R.string.message_on_unchecked_both));
                            return;
                        }
                    }
                }
                putStatus(ShadowDB.IS_TAB_X_TIMES, holder.tap_count_safe.isChecked());
                break;

            case R.id.tab_count_swipe_safe:
                /*if (!isChecked) {
                        if(!holder.tap_count_safe.isChecked() && !holder.enter_name_pass_safe.isChecked()) {
                            holder.tab_count_swipe_safe.setChecked(true);
                            new AlertDialogClass(getActivity()).open(resources.getString(R.string.message_on_unchecked_all));
                            return;
                        }
                }*/
                putStatus(ShadowDB.IS_SWIPE_X_TIMES, holder.tab_count_swipe_safe.isChecked());
                break;

            //Cover Message feature
           /* case R.id.cover_message_unsafe:
                putStatus(ShadowDB.UN_COVER_MESSAGE,holder.cover_message_unsafe.isChecked());
                break;*/
            case R.id.ring_tone_call_unsafe:
                putStatus(ShadowDB.UN_RINGTONE_FOR_CALL, holder.ring_tone_call_unsafe.isChecked());
                break;
            case R.id.vibration_call_unsafe:
                putStatus(ShadowDB.UN_VIBRATE_ON_CALL, holder.vibration_call_unsafe.isChecked());
                break;
            case R.id.chat_vibrate_unsafe:
                putStatus(ShadowDB.UN_VIBRATE_ON_CHAT, holder.chat_vibrate_unsafe.isChecked());
                break;
            case R.id.chat_sound_unsafe:
                putStatus(ShadowDB.UN_SOUND_ON_CHAT, holder.chat_sound_unsafe.isChecked());
                break;
            case R.id.shake_unsafe:
                putStatus(ShadowDB.UN_SHAKE_LOGOUT, holder.shake_unsafe.isChecked());
                break;
            case R.id.enter_name_pass_unsafe:
                if (!isChecked && !isProApp) {
                    if (!holder.tab_check_for_free.isChecked()) {
                        holder.enter_name_pass_unsafe.setChecked(true);
                        new AlertDialogClass(getActivity()).open(resources.getString(R.string.message_on_unchecked_both));
                        return;
                    }
                }
                putStatus(ShadowDB.UN_IS_ENTER_NAME_PASS, holder.enter_name_pass_unsafe.isChecked());
                break;
            case R.id.tab_check_for_free:
                if (!isChecked && !isProApp) {
                    if (!holder.enter_name_pass_unsafe.isChecked()) {
                        holder.tab_check_for_free.setChecked(true);
                        new AlertDialogClass(getActivity()).open(resources.getString(R.string.message_on_unchecked_both));
                        return;
                    }
                }
                putStatus(ShadowDB.UN_IS_PASS_ON_X_TABS_FREE, holder.tab_check_for_free.isChecked());
                break;
            case R.id.tab_check_swipe_cal_unsafe:
                setUnsafeCalculatorWTA(isChecked);
                break;

        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.with_pass_unsafe:
                setUnsafeWTA(true);
                break;
            case R.id.without_pass_unsafe:
                setUnsafeWTA(false);
                break;
            case R.id.with_pass_swipe_unsafe:
                setUnsafeAlarmWTA(true);
                break;
            case R.id.without_pass_swipe_unsafe:
                setUnsafeAlarmWTA(false);
                break;

        }
    }

    private void setModeData() {
        setMode(getStatus(ShadowDB.IS_SAFE_MODE), true);
        holder.vibrate_call_safe.setChecked(getStatus(ShadowDB.VIBRATE_FOR_CALL));
        holder.vibrate_message_safe.setChecked(getStatus(ShadowDB.VIBRATE_FOR_CHAT));
        holder.shake_safe.setChecked(getStatus(ShadowDB.SHAKE_LOGOUT));
        holder.enter_name_pass_safe.setChecked(getStatus(ShadowDB.IS_ENTER_NAME_PASS));
        holder.tap_count_safe.setChecked(getStatus(ShadowDB.IS_TAB_X_TIMES));
        setLogoutTime(holder.logout_time_safe, getLogoutTime(ShadowDB.LOGOUT_TIME), true);

        holder.cover_message_unsafe.setChecked(getStatus(ShadowDB.UN_COVER_MESSAGE));
        holder.ring_tone_call_unsafe.setChecked(getStatus(ShadowDB.UN_RINGTONE_FOR_CALL));
        holder.vibration_call_unsafe.setChecked(getStatus(ShadowDB.UN_VIBRATE_ON_CALL));
        holder.chat_sound_unsafe.setChecked(getStatus(ShadowDB.UN_SOUND_ON_CHAT));
        holder.chat_vibrate_unsafe.setChecked(getStatus(ShadowDB.UN_VIBRATE_ON_CHAT));
        holder.shake_unsafe.setChecked(getStatus(ShadowDB.UN_SHAKE_LOGOUT));
        holder.enter_name_pass_unsafe.setChecked(getStatus(ShadowDB.UN_IS_ENTER_NAME_PASS));
        holder.tab_check_for_free.setChecked(getStatus(ShadowDB.UN_IS_PASS_ON_X_TABS_FREE));
        setUnsafeWTA(getStatus(ShadowDB.UN_IS_PASS_ON_X_TABS));
        setLogoutTime(holder.logout_time_unsafe, getLogoutTime(ShadowDB.UN_LOGOUT_TIME), true);
    }

    private void setUnsafeWTA(boolean isPassRequired) {
        if (isPassRequired) {
            holder.with_pass_unsafe.setChecked(true);
            holder.with_pass_unsafe.setCompoundDrawablesWithIntrinsicBounds(null, null, BaseActivity.drawableGet(R.drawable.check, getActivity()), null);
            holder.without_pass_unsafe.setCompoundDrawablesWithIntrinsicBounds(null, null, BaseActivity.drawableGet(R.drawable.uncheck, getActivity()), null);
        } else {
            holder.without_pass_unsafe.setChecked(true);
            holder.with_pass_unsafe.setCompoundDrawablesWithIntrinsicBounds(null, null, BaseActivity.drawableGet(R.drawable.uncheck, getActivity()), null);
            holder.without_pass_unsafe.setCompoundDrawablesWithIntrinsicBounds(null, null, BaseActivity.drawableGet(R.drawable.check, getActivity()), null);
        }
        putStatus(ShadowDB.UN_IS_PASS_ON_X_TABS, isPassRequired);
    }

    private void setUnsafeAlarmWTA(boolean isPassRequired) {
        if (isPassRequired) {
            holder.with_pass_swipe_unsafe.setChecked(true);
            holder.with_pass_swipe_unsafe.setCompoundDrawablesWithIntrinsicBounds(null, null, BaseActivity.drawableGet(R.drawable.check,getActivity()), null);
            holder.without_pass_swipe_unsafe.setCompoundDrawablesWithIntrinsicBounds(null, null, BaseActivity.drawableGet(R.drawable.uncheck,getActivity()), null);
        } else {
            holder.without_pass_swipe_unsafe.setChecked(true);
            holder.with_pass_swipe_unsafe.setCompoundDrawablesWithIntrinsicBounds(null, null, BaseActivity.drawableGet(R.drawable.uncheck,getActivity()), null);
            holder.without_pass_swipe_unsafe.setCompoundDrawablesWithIntrinsicBounds(null, null, BaseActivity.drawableGet(R.drawable.check,getActivity()), null);
        }
        putStatus(ShadowDB.UN_IS_PASS_ON_X_SWIPE, isPassRequired);
    }

    private void setUnsafeCalculatorCheckbox(boolean isChecked) {
        holder.tab_check_swipe_cal_unsafe.setChecked(isChecked);
    }
    private void setUnsafeCalculatorWTA(boolean isPassRequired) {
        putStatus(ShadowDB.UN_IS_PASS_ON_X_SWIPE, isPassRequired);
    }

    public static boolean getStatus(String value) {
        int data = Shadow.shadowDB.getModeData(value);
        Logger.printLog(Logger.ERROR,"data getStatus ", "is " + data);
        return (data == 0) ? true : false;
    }

    public static int getLogoutTime(String value) {
        return Shadow.shadowDB.getModeData(value);
    }

    private void putStatus(String key, boolean status) {
        Shadow.shadowDB.updateModeData(key, (status) ? 0 : 1);
    }

    private void putLogoutTime(String key, int data, TextView viewText) {
        Shadow.shadowDB.updateModeData(key, data);
        setLogoutTime(viewText, data, false);
    }

    private void setLogoutTime(TextView viewText, int value, boolean isInit) {
        if(Shadow.isProApp()) {
            viewText.setText(getResources().getString(R.string.logout_time, value));
        }else {
            viewText.setText(getResources().getString(R.string.logout_time_sp, value));
        }
        if (!isInit) {
            callMoreTouch(value);
        }
    }

    private class ViewHolder {
        CheckBox vibrate_call_safe, vibrate_message_safe, shake_safe, enter_name_pass_safe, tap_count_safe, ring_tone_call_unsafe, vibration_call_unsafe, chat_sound_unsafe, chat_vibrate_unsafe,
                shake_unsafe, enter_name_pass_unsafe, tab_check_for_free, cover_message_unsafe, tab_count_swipe_safe;
        TextView logout_time_safe, logout_time_unsafe, safe_mode, unsafe_mode, enter_name_pass_text_filed_unsafe, enter_name_pass_text_filed_safe;
        TextView set_login_time_safe, set_login_time_unsafe, tap_count_safe_text,tap_count_text_swipe_safe;
        LinearLayout safeLayout, unSafeLayout, tab_check_free_unsafe_box, pro_app_tap_count,
                forAlarm_unsafe, forAlarm_safe,alarm_swipe_safe,safe_enter_name_pass_check_layout;
        RadioGroup access_choose_group_unsafe,access_choose_group_swipe_unsafe;
        RadioButton with_pass_unsafe, without_pass_unsafe,with_pass_swipe_unsafe,without_pass_swipe_unsafe;

        LinearLayout layout_safe_title, layout_unsafe_title,tab_check_swipe_cal_unsafe_box;
        ImageView safe_select, unsafe_select;
        RelativeLayout header_layout;
        public LinearLayout enter_name_pass_check_ll;
        CheckBox tab_check_swipe_cal_unsafe;
        Button calculation_safe,calculation_unsafe;
        TextView tab_times_unsafe,tab_times_safe,tap_count_text_safe,tab_x_time_unsafe;
    }

    //Header
    TextView title;

    @Override
    protected void setHeader(View v) {
        super.setHeader(v);
        title = (TextView) v.findViewById(R.id.title_head);
        title.setText(resources.getString(R.string.safe_unsafe));
    }

  /*  private void showMessagePopUp() {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.shadow_first_time_popup,null);
        final PopupWindow popupWindow = new PopupWindow(getActivity());
        popupWindow.setContentView(view);

        TextView close = (TextView) view.findViewById(R.id.ok_popup);
        TextView msg= (TextView) view.findViewById(R.id.msg_first_dialog);
        msg.setText(resources.getString(isProApp?R.string.first_time_login_dialog_pro:R.string.first_time_login_dialog_free));

        close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        popupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow.setFocusable(true);


        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
    }*/


}
