package com.app.android.shadow.common.ui.data.profile;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.gui.profile.controller.IProfileController;
import com.c2call.sdk.pub.gui.profile.decorator.SCProfileDecorator;

public class ProfileDecorator extends SCProfileDecorator
{
	@Override
	public void decorate(final IProfileController m)
	{
		super.decorate(m);
        onDecorateEditCountry(m);
	}

    public void onDecorateEditCountry(final IProfileController m)
    {
        if (m == null
            || m.getViewHolder() == null
            || m.getData() == null)
        {
            Ln.w("c2app", "* * * Warning ProfileDecorator.onDecorateEditCountry() - invalid data");
            return;
        }


        setText(((ProfileViewHolder)m.getViewHolder()).getItemEditCountry(), m.getData().getCountry());
    }
}
