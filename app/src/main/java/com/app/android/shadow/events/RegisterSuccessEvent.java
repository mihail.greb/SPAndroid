package com.app.android.shadow.events;


import com.c2call.sdk.pub.common.SCRegistrationData;

public class RegisterSuccessEvent extends SingleDataEvent<SCRegistrationData>
{

    public RegisterSuccessEvent(SCRegistrationData data)
    {
        super(data);
    }
}
