package com.app.android.shadow.samplechat.board;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.fragments.SCNewMessageFragment;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.newmessage.controller.INewMessageController;
import com.app.android.shadow.R;

public class BoardChatInputFrgament extends SCNewMessageFragment
{

    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.sc_board_part_chatinput,(ViewGroup)null);
        return view;
    }

    public static SCNewMessageFragment create(final SCFriendData friend, Bundle forwardArgs, final int layout)
    {
        final Bundle args = new Bundle();
        args.putAll(forwardArgs);

        args.putSerializable(SCExtraData.NewMessage.EXTRA_DATA_CONTACT, friend);
        args.putInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);

        final BoardChatInputFrgament fragment = new BoardChatInputFrgament();
        fragment.setArguments(args);
        return fragment;
    }

  @Override
    protected INewMessageController onCreateController(View v, SCViewDescription vd)
    {
        return new BoardChatInputController(v, vd, this, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        BoardChatInputController controller = (BoardChatInputController) getController();
        controller.enableSecureMessage(false);
        CheckBox lock = (CheckBox)view.findViewById(R.id.sc_newmessage_cb_secure);
        lock.setVisibility(View.GONE);
    }

    @Override
    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);

    }



}
