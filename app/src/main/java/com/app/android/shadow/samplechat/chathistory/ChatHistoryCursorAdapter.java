package com.app.android.shadow.samplechat.chathistory;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.adapter.SCBoardCursorAdapter;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemControllerFactory;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemType;
import com.c2call.sdk.pub.gui.boardlistitem.decorator.SCBoardListItemBaseDecorator;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;
import com.app.android.shadow.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

/**
 * The Adpter that is used to manage the items of the chat history list.
 */
public class ChatHistoryCursorAdapter extends SCBoardCursorAdapter
{

    /**
     * Default map that should be used by this controller. This map is used to create the correct Decorators for
     * a certain list item type. The map must contain all available elements of {@link SCBoardListItemType}. In 
     * this case all the item types will be decorated by a {@link ChatHistoryItemDecorator}, i.e. all items will look
     * very similar
     */
    @SuppressWarnings("rawtypes")
    public static  Map<SCBoardListItemType, SCBoardListItemBaseDecorator> DECORATOR_MAP = new TreeMap<SCBoardListItemType, SCBoardListItemBaseDecorator>();
    private static final SimpleDateFormat __dateFormat = new SimpleDateFormat(C2CallSdk.context().getString(R.string.sc_date_pattern_without_time));

    static
    {
        DECORATOR_MAP.put(SCBoardListItemType.Call, new ChatHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Text, new ChatHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Image, new ChatHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Video, new ChatHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Audio, new ChatHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Location, new ChatHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Friend, new ChatHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.File, new ChatHistoryItemDecorator());
    }


    @SuppressWarnings("rawtypes")
    public ChatHistoryCursorAdapter(final Context context,
                                    final Cursor c,
                                    final IBoardListItemControllerFactory medatorFactory,
                                    final SCViewDescriptionMap viewDescriptions,
                                    final Map<SCBoardListItemType, SCBoardListItemBaseDecorator> decorators,
                                    final int flags)
    {
        super(context,
              c,
              R.layout.sc_std_list_header,
              R.id.list_header_title, medatorFactory,
              viewDescriptions,
              decorators,
              flags);
    }


    @Override
    protected View onInflateView(final LayoutInflater inflater, final Cursor cursor, final SCBoardEventData data, final ViewGroup parent)
    {

        int layout = 0;
        final int type = getItemViewType(cursor);
        /*
        This switch is just for demonstration purposes to show all available item types used by this adapter.
        Actually we always inflate the same layout for all types here.
         */
        switch(type){
            case TYPE_IN_TEXT:
            case TYPE_OUT_TEXT:
            case TYPE_IN_AUDIO:
            case TYPE_OUT_AUDIO:
            case TYPE_IN_VIDEO:
            case TYPE_OUT_VIDEO:
            case TYPE_IN_IMAGE:
            case TYPE_OUT_IMAGE:
            case TYPE_IN_LOCATION:
            case TYPE_OUT_LOCATION:
            case TYPE_IN_FRIEND:
            case TYPE_OUT_FRIEND:
            case TYPE_IN_FILE:
            case TYPE_OUT_FILE:
            case TYPE_IN_CALL:
            case TYPE_OUT_CALL:
                layout = R.layout.app_chathistory_list_item; break;
            default:
                layout = R.layout.app_chathistory_list_item; break;
        }


        return inflater.inflate(layout, parent, false);
    }

    /**
     * Get the group name for the item at the given cursor's position. If the returned
     * group differs from the previous one then a new section header will be included at the
     * current position.
     * @param c the cursor used by this adatper
     * @param groupColumn this parameter is obsolete and will be alwyas -1. Don't use it anymore.
     * @return the group name for the current item.
     */
    @Override
    protected String getGroupName(final Cursor c, final int groupColumn)
    {
        final SCBoardEventData data = onCreateData(c);


        return  __dateFormat.format(new Date(data.getTimestamp()));
    }
}
