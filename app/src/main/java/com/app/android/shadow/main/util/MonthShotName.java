package com.app.android.shadow.main.util;

import android.text.format.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by rails-dev11 on 5/8/15.
 */
public class MonthShotName {
        public static String getCurrentDate() {

            Calendar calendar;
            calendar = Calendar.getInstance();

            int year = calendar.get(Calendar.YEAR);
            int  month = (calendar.get(Calendar.MONTH)+1);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int min = calendar.get(Calendar.MINUTE);
            int second = calendar.get(Calendar.SECOND);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss", Locale.ENGLISH);
            String dateInString = ""+day+"-"+month+"-"+year+" "+hour+":"+min+":"+second;
            //String dateInString = "22-01-2015 10:20:56";
            Date date = null;
            try {
                date = sdf.parse(dateInString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String dayOfTheWeek = (String) DateFormat.format("EEEE", date);//Thursday
            String stringMonth = (String) DateFormat.format("MMM", date); //Jun
            String intMonth = (String) DateFormat.format("MM", date); //06
            String yearStr = (String) DateFormat.format("yyyy", date); //2013
            String dayStr = (String) DateFormat.format("dd", date); //20
            return dayStr+" "+stringMonth;
        }
    }
