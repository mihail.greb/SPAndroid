package com.app.android.shadow.main.custom.call;

import android.content.Context;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;

import com.app.android.shadow.main.dbhelper.ShadowDB;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.utils.custom.StringExtra;
import com.app.android.shadow.main.tapmanager.settings.MoreSignature;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.main.util.Logger;
import com.c2call.lib.androidlog.Ln;
//import com.c2call.sdk.lib.util.extra.PreferenceExtra;
//import com.c2call.sdk.lib.n.g.ac;
//import com.c2call.sdk.lib.util.extra.ResourcesExtra;
//import com.c2call.sdk.lib.n.g.af;
import com.c2call.sdk.pub.common.SCIncomingCallData;
import com.c2call.sdk.pub.core.C2CallSdk;

//TODO UNCOMMENT THE CODE TO SATART CUSTOME
public class CustomCallIndication {
    private static final CustomCallIndication __instance = new CustomCallIndication();
    private Ringtone _ringtone;
    Vibrator _vibrator;
    private final long[] _vibrationPattern = new long[]{300L, 200L, 300L};
    //private final long[] _vibrationPattern = (new long[] { 1000, 1000, 1000, 1000, 1000 });

    public static CustomCallIndication instance() {
        return __instance;
    }
    private SCIncomingCallData data;
    ShadowDB db;
    boolean isSafeMode = true;
    private CustomCallIndication() {
        db = Shadow.shadowDB;
    }


    public synchronized void indicateCall(Context context,SCIncomingCallData data) {
        this.data = data;
        try {
            isSafeMode = db.getModeState(ShadowDB.IS_SAFE_MODE);
            this._vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);//vibrator
            AudioManager e = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);//audio

            int ringerMode = e.getRingerMode();
           /* boolean doVibrate = DeviceExtra.isVibrateActive(context, 0);
            if(doVibrate) {}*/
            if (isVibrate()) {
                this._vibrator.vibrate(this._vibrationPattern, 0);
                Logger.printLog(Logger.ERROR, "there is vibrater available");
            }
            //if(ringerMode == 2) {
            if(isRing()) {
                String ringtoneUri = this.getRingtoneUri(context);
                Ln.d("fc_tmp", "CustomCallIndication.indicateCall() - ringtoneUri: %s", new Object[]{ringtoneUri});

                this._ringtone = RingtoneManager.getRingtone(context, Uri.parse(ringtoneUri));
                if(this._ringtone != null) {
                    this._ringtone.play();
                }
            }
        } catch (Exception var6) {
            var6.printStackTrace();
        }
    }

    private String getRingtoneUri(Context context) {
        String caller = data.callerId;
        boolean isvideoCall= data.isVideoCall;

        String userTone = db.getRingTone(caller, getType(isvideoCall),false);
        String appTone = db.getRingTone(Const.DEFAULT_USER_ID,getType(isvideoCall),true);
        if(userTone != null ){
            return userTone;
        }else if(appTone != null){
            return appTone;
        }else {
            return getDefaultRing(context);
        }

        //return Environment.getExternalStorageDirectory()+"/test.wav";
    }

    public static String getDefaultRing(Context context){
        String defaultValue = "content://settings/system/ringtone";
        try {
            //String e = PreferenceExtra.getString(context, com.c2call.sdk.R.string.key_ringtoneCall, (String)null);
            //String e = ac.a(context, com.c2call.sdk.R.string.key_ringtoneCall, (String)null);
            String e = null;
            try {
                // TODO CHECK ADD CODE
                //e = ac.a(context, com.c2call.sdk.R.string.key_ringtoneCall, (String)null);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            if(e != null) {
                Ln.d("fc_tmp", "CallIndication.getRingtoneUri() - %s (byPref)", new Object[]{e});
                return e;
            } else {
                //String byRes = af.b("sc_ringtone_call");
                String byRes = getRawResourceUri("sc_ringtone_call");
                if(byRes != null) {
                    Ln.d("fc_tmp", "CallIndication.getRingtoneUri() - %s (byRes)", new Object[]{e});
                    return byRes;
                } else {
                    Ln.d("fc_tmp", "CallIndication.getRingtoneUri() - %s (byDefault)", new Object[]{"content://settings/system/ringtone"});
                    return "content://settings/system/ringtone";
                }
            }
        } catch (Exception var5) {
            var5.printStackTrace();
            return "content://settings/system/ringtone";
        }
    }

    public static String getRawResourceUri(String name) {
        if(StringExtra.isNullOrEmpty(name)) {
            return null;
        } else {
            int id = getResource(name, "raw");
            return getRawResourceUri(id);
        }
    }

    public static String getRawResourceUri(int id) {
        return id == 0?null:String.format("android.resource://%s/%d", new Object[]{C2CallSdk.context().getPackageName(), Integer.valueOf(id)});
    }

    public static int getResource(String name, String type) {
        String p = C2CallSdk.instance().getContext().getPackageName();
        int id = C2CallSdk.instance().getContext().getResources().getIdentifier(name, type, p);
        Ln.d("fc_tmp", "ResourcesExtra.getResource() - name: %s, type: %s -> 0x%s", new Object[]{name, type, p, Integer.toString(id, 16)});
        return id;
    }

    public synchronized void stopIndication() {
        if(this._ringtone != null) {
            this._ringtone.stop();
            this._ringtone = null;
        }

        if(this._vibrator != null) {
            this._vibrator.cancel();
        }
    }

    private MoreSignature getType(boolean isVideo){
        return isVideo? MoreSignature.VideoRingtone: MoreSignature.AudioRingtone;
    }

    private boolean isRing(){
        /*if(!Shadow.isProApp()){
            return true;
        }else*/
           return (isSafeMode)?false:(db.getModeState(ShadowDB.UN_RINGTONE_FOR_CALL));
    }

    private boolean isVibrate(){
       /* if(!Shadow.isProApp()){
            return false;
        }else*/
            return (db.getModeState((isSafeMode)?ShadowDB.VIBRATE_FOR_CALL:ShadowDB.UN_VIBRATE_ON_CALL));
    }

}
