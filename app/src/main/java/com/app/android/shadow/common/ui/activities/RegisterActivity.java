package com.app.android.shadow.common.ui.activities;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import com.app.android.shadow.common.ui.fragments.RegisterFragment;
import com.app.android.shadow.main.LauncherActivity;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.util.Const;
import com.c2call.sdk.pub.activities.SCRegisterFragmentActivity;

public class RegisterActivity extends SCRegisterFragmentActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                    WindowManager.LayoutParams.FLAG_SECURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    protected Fragment onCreateFragment()
    {
        return RegisterFragment.create(getIntent().getExtras());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Shadow.getAppPreferences().getBoolean(Const.IS_LOGIN,false)){
            Intent intent = new Intent(this, Shadow.getProActivity());
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RegisterActivity.this, LauncherActivity.class);
        startActivity(intent);
        finish();
    }
}
