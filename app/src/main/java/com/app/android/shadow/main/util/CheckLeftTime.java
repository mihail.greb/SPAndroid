package com.app.android.shadow.main.util;

import java.util.concurrent.TimeUnit;

public class CheckLeftTime {

    public static long convertTime(String time)  {
        Logger.printLog(Logger.ERROR,"time str","is "+time);
        int hour=0,minute=0,second=0;
        long totalTimeInMS;
        if(time.contains(":")){
            String hr = getSubStr0(time);
            time = getSubStr1(time);
            if(time.contains(":")){
                Logger.printLog(Logger.ERROR,"time hour","is "+hr+" time is "+time);
                hour = getStrToInt(hr);
                String min = getSubStr0(time);
                time = getSubStr1(time);
                Logger.printLog(Logger.ERROR,"time minute","is "+min +" time is "+time);
                minute = getStrToInt(min);
            }else {
                minute = getStrToInt(hr);
            }
            Logger.printLog(Logger.ERROR,"time second","is "+time);
            second = getStrToInt(time);
        }

        totalTimeInMS = (TimeUnit.HOURS.toMillis(hour)+TimeUnit.MINUTES.toMillis(minute)+
                            TimeUnit.SECONDS.toMillis(second));
        Logger.printLog(Logger.ERROR,"time in ms","is "+totalTimeInMS);
        return totalTimeInMS;
    }

    private static String getSubStr1(String time){
        return time.substring((time.indexOf(":")+1),time.length());
    }

    private static String getSubStr0(String time){
        return time.substring(0,time.indexOf(":"));
    }

    private static int getStrToInt(String time){
        try {
            return Integer.parseInt(time);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
