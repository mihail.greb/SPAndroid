package com.app.android.shadow.main.tapmanager.settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.main.util.SdCardManager;
import com.app.android.shadow.utils.Utils;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.R;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Custom Board Fragment to show a chat history grouped by users
 */
public class ProApp extends BaseFragment {

    OnProAppClickListner mCallback;
    public interface OnProAppClickListner {
        public void onProAppButtonClicked(Fragment f);
    }

    //SmartImageView setProimage;
    ImageView setProimage;
    String uri;
    String packageName;

    //Header
    TextView app_name;

    //Header
    TextView title;
    @Override
    protected void setHeader(View v) {
       super.setHeader(v);
        title = (TextView) v.findViewById(R.id.title_head);
        title.setText(resources.getString(R.string.pro_app_title));
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view =inflater.inflate(R.layout.pro_app, container, false);
        app_name = (TextView)_view.findViewById(R.id.app_name);
        setProimage = (ImageView) _view.findViewById(R.id.setProimage);
        callApi(getActivity());
        setHeader(_view);
        _view.findViewById(R.id.upgrade_version).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put intent to app store to update app
                //Shadow.alertToast("Already Updated");
                //String uri1 = uri.substring(uri.lastIndexOf("?id="),uri.length());
                goToPlayStore();
            }
        });

        return _view;
    }

    public void callApi(Context mContext){
        UserLogoutTask mSignUpTask = new UserLogoutTask(mContext);
        mSignUpTask.setServiceTaskMapStrParams(APIServiceAsyncTask.proAppLink, null);
        mSignUpTask.execute((Void) null);
    }

    public class UserLogoutTask extends APIServiceAsyncTask {
        UserLogoutTask(Context mContext){
            super(mContext);
        }

        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            try {
                uri = jsonObj.getString(Const.ANDROID_PRO_URL); //"https://play.google.com/store/apps/details?id=com.pro.android.shadow";
                String iconUrl = jsonObj.getString("android_pro_icon");
                Logger.printLog(Logger.ERROR, "print url update " + uri + " urlicon " + iconUrl);
                //setProimage.setImageUrl(ServiceHandler.baseUrl+iconUrl);
                packageName = uri.substring((uri.lastIndexOf("?id=")+4),uri.length());
                new SetUserImage().execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    class SetUserImage extends AsyncTask<Void,Void,String>{
        String s = " ";
        Bitmap bitmap = null;
        @Override
        protected String doInBackground(Void... params) {
            Document d = null;

            try {
                //d  = Jsoup.connect("https://play.google.com/store/apps/details?id=" + packageName).get();
                d  = Jsoup.connect(uri).get();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                Logger.printLog(Logger.ERROR,"generate Exceptions");
            }
            Logger.printLog(Logger.ERROR,"print doc to string "+d.toString() +" or use "+d.nodeName());
            //Elements div = d.getElementsByAttributeValueContaining("class", "doc-banner-icon");
            //Elements div = d.getElementsByAttributeValueContaining("class", "details-wrapper apps square-cover id-track-partial-impression id-deep-link-item");
            Elements div = d.getElementsByAttributeValueContaining("class", "cover-container");
            Elements name = d.getElementsByAttributeValueContaining("class", "document-title");//<h1 class="document-title" itemprop="name"> <div>Buddy Drinks</div> </h1>
            Logger.printLog(Logger.ERROR,"dive to string "+div.toString() +" and name is "+name.toString());
            //Elements div2 = div.get("class", "details-wrapper apps square-cover id-track-partial-impression id-deep-link-item");
            //String pat = (div.get(0).attr("src"));
            //Elements nameString = name.select("div.document-title div:eq(1)");
            Element nameString = name.get(0);
            String pat = div.select("img").attr("abs:src");
            //Logger.printLog(Logger.ERROR,"image path ","is "+pat);
            //pat = pat.substring((pat.indexOf("src=")+1),pat.indexOf("=w300-rw"));
            s = nameString.text();
            Logger.printLog(Logger.ERROR, "img path is " + pat + " app name " + nameString.toString() + " or " + nameString.text());
            //bitmap = getBitmapFromURL(pat);
            bitmap = getBitmap(pat);
            return pat;
        }

        @Override
        protected void onPostExecute(String imgPath) {
            super.onPostExecute(imgPath);
            Logger.printLog(Logger.ERROR, "image path is " + imgPath);
            //new LoadImage().execute(imgPath);
            try {
                try {
                    //setProimage.setImageUrl(imgPath);
                    if(bitmap != null) {
                       // bitmap = Bitmap.createScaledBitmap(bitmap, setProimage.getWidth(), setProimage.getHeight(), true);
                        setProimage.setImageBitmap(bitmap);
                    }else {
                        setProimage.setImageResource(R.drawable.yellow_puzzle);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
                //Toast.makeText(getActivity(), "Image Does Not exist or Network Error", Toast.LENGTH_SHORT).show();
            }
            app_name.setText(s);
        }
    }

    private Bitmap getBitmap(String url)
    {
        File f= SdCardManager.getFile(url);
        //from SD cache
        Bitmap b = decodeFile(f);
        if(b!=null)
            return b;
        //from web
        try {
            Bitmap bitmap= null ;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is=conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            Utils.CopyStream(is, os);
            os.close();
            conn.disconnect();
            bitmap = decodeFile(f);
            //}
            return bitmap;
        } catch (Throwable ex){
            ex.printStackTrace();
            return null;
        }
    }

    //decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f){
        try {
            //decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            FileInputStream stream1=new FileInputStream(f);
            BitmapFactory.decodeStream(stream1,null,o);
            stream1.close();

            //Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE=70;
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }

            //decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            FileInputStream stream2=new FileInputStream(f);
            Bitmap bitmap= BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return bitmap;
        } catch (FileNotFoundException e) {
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

 /*   public Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            //Bitmap myBitmap = getResizedBitmap(BitmapFactory.decodeStream(input));
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }
    public Bitmap getResizedBitmap(Bitmap image) {
        int width = image.getWidth();
        int height = image.getHeight();
        int maxSize = 0;
        try {
            maxSize = setProimage.getHeight();
        } catch (Exception e) {
            e.printStackTrace();
            maxSize = 120;
        }

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }*/

    private void goToPlayStore(){
        BaseActivity.setSaveTime0();
        BaseSampleActivity.isAppNotClose = true;
        final String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
        try {
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.whatsapp")));
            Logger.printLog(Logger.ERROR,"package name "+packageName);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:"+devName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.whatsapp")));
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
            Logger.printLog(Logger.ERROR,"uri name "+uri);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/search?q=pub:"+devName)));
        }
        BaseSampleActivity.isAppNotClose = false;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (OnProAppClickListner) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }



/*
    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading Image ....");
            pDialog.show();
        }
        protected Bitmap doInBackground(String... args) {
            Bitmap bitmap = null;
            try {
               // bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
                bitmap = getBitmapFromURL(args[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {

            if(image != null){
                //img.setImageBitmap(image);
                setProimage.setImageBitmap(image);
                pDialog.dismiss();

            }else{

                pDialog.dismiss();
                Toast.makeText(getActivity(), "Image Does Not exist or Network Error", Toast.LENGTH_SHORT).show();

            }
        }
    }

    public Bitmap getBitmapFromURL(String src) {
        try {
            java.net.URL url = new java.net.URL(src);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
*/


}
