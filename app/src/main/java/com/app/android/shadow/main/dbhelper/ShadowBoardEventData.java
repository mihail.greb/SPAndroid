package com.app.android.shadow.main.dbhelper;

import com.app.android.shadow.main.dbhelper.manager.ShadowBoardEventManager;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.db.datamanager.SCBoardEventManager;

/**
 * Created by rails-dev on 16/10/15.
 */
public class ShadowBoardEventData extends SCBoardEventData {

    private final ShadowBoardEventManager _manager = new ShadowBoardEventManager(this);

    public ShadowBoardEventData() {
    }

    public ShadowBoardEventData(String id) {
        this.setId(id);
        this._manager.createOrGetMediaData();
        this._manager.createOrGetSecureMessageData();
    }

    @Override
    public SCBoardEventManager getManager() {
        return this._manager;
    }

}
