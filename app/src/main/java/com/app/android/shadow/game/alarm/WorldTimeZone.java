package com.app.android.shadow.game.alarm;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.shadow.main.tapmanager.settings.Sounds;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by rails-dev on 19/2/16.
 */
public class WorldTimeZone extends BaseFragment implements View.OnClickListener {

    private AutoCompleteTextView spinnerAvailableID;
    private Calendar current;
    private TextView textTimeZone, txtCurrentTime, txtTimeZoneTime, timer_time;
    private long miliSeconds;
    private ArrayAdapter<String> idAdapter;
    private SimpleDateFormat sdf;
    private Date resultdate;
    String array[] = new String[2];

    public int hours = 00;
    public int seconds = 00;
    public int minutes = 00;

    //Header
    TextView title;

    @Override
    protected void setHeader(View v) {
        //super.setHeader(v);
        title = (TextView) v.findViewById(R.id.title_head);
        title.setText(resources.getString(R.string.world_title));
    }

    LinearLayout modesBox, DeleteAllDataBox, proAppBox;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view = inflater.inflate(R.layout.activity_worldclock, null, false);
        setHeader(_view);
      /*  SimpleChatMainActivity.mCurrentTab = Const.SETTINGS;
        _view.findViewById(R.id.alarmBtn).setOnClickListener(this);
        _view.findViewById(R.id.stopWatchBtn).setOnClickListener(this);
        _view.findViewById(R.id.alarmTimerBtn).setOnClickListener(this);*/

        String[] idArray = TimeZone.getAvailableIDs();
        spinnerAvailableID = (AutoCompleteTextView) _view.findViewById(R.id.auto_complete);
        textTimeZone = (TextView) _view.findViewById(R.id.timezonetime);
        //txtCurrentTime = (TextView) _view.findViewById(R.id.current_time);
        txtTimeZoneTime = (TextView) _view.findViewById(R.id.timezone_text);
        //timer_time = (TextView) _view.findViewById(R.id.timer_time);


        sdf = new SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm:ss", Locale.ENGLISH);
        idAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, idArray);
        idAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAvailableID.setAdapter(idAdapter);
        spinnerAvailableID.setThreshold(0);
        getGMTTime();

        spinnerAvailableID.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                getGMTTime();
                String selectedId = (String) (parent.getItemAtPosition(position));
                TimeZone timezone = TimeZone.getTimeZone(selectedId);
                String TimeZoneName = timezone.getDisplayName();
                int TimeZoneOffset = timezone.getRawOffset()
                        / (60 * 1000);
                int hrs = TimeZoneOffset / 60;
                int mins = TimeZoneOffset % 60;
                miliSeconds = miliSeconds + timezone.getRawOffset();
                resultdate = new Date(miliSeconds);

                String current_time = sdf.format(resultdate);
                array = current_time.split(":");
                seconds = Integer.parseInt(array[2]);
                minutes = Integer.parseInt(array[1]);
                String array_new[] = array[0].split(" ");
                Logger.printLog(Logger.ERROR,"array_new", "" + array_new[3]);
                hours = Integer.parseInt(array_new[4]);


                System.out.println(sdf.format(resultdate));

                String timeZoneStr = TimeZoneName + " : GMT " + hrs + "." + mins;
                textTimeZone.setText(timeZoneStr);

                if (getActivity() != null)
                    startTimerAndShow();

                miliSeconds = 0;
                AlarmMainActivity.worldTimeZoneStr = timeZoneStr;
            }
        });

        return _view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
     /*   spinnerAvailableID.setText("");
        textTimeZone.setText("");
        txtTimeZoneTime.setText("");*/
               if (AlarmMainActivity.worldTimeZoneStr != null &&
                                AlarmMainActivity.worldTimeZoneStr.length() > 0) {
                   textTimeZone.setText(AlarmMainActivity.worldTimeZoneStr);
                }
    }

    private void getGMTTime() {
        current = Calendar.getInstance();

        miliSeconds = current.getTimeInMillis();
        TimeZone tzCurrent = current.getTimeZone();
        int offset = tzCurrent.getRawOffset();
        if (tzCurrent.inDaylightTime(new Date())) {
            offset = offset + tzCurrent.getDSTSavings();
        }
        miliSeconds = miliSeconds - offset;
        resultdate = new Date(miliSeconds);


        System.out.println(sdf.format(resultdate));
    }

    @Override
    public void onClick(View v) {
        Fragment fragment;
        switch (v.getId()) {
            case R.id.alarmBtn:
                fragment = new AlarmFragment();
                break;
            case R.id.stopWatchBtn:
                fragment = new StopWatchFragment();
                break;
            case R.id.alarmTimerBtn:
                fragment = new StopWatchFragment();
                break;
        }
        mCallback.onButtonClickEvent(new Sounds(), EventMaker.SettingsFragment);
    }


    private void startTimerAndShow() {
        //Declare the timer
       /* String tempStr = textTimeZone.getText().toString();
        if(tempStr != null && tempStr.length() >0) {*/
            Timer t = new Timer();
            //Set the schedule function and rate
            t.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    try {
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (seconds == 60) {
                                        seconds = 00;
                                        minutes = minutes + 1;
                                    }
                                    if (minutes == 60) {
                                        minutes = 00;
                                        hours = hours + 1;
                                    }
                                    DateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy", Locale.ENGLISH);
                                    Date date = new Date();
                                    txtTimeZoneTime.setText(dateFormat.format(date) + " " + getIntToStr(hours) + ":" + getIntToStr(minutes) + ":" + getIntToStr(seconds));
                                    ++seconds;
                                }

                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }, 0, 1000);
      /*  }else {
            txtTimeZoneTime.setText("");
            spinnerAvailableID.setText("");
        }*/

    }



    private String getIntToStr(int value) {
        if (value < 10) {
            return "" + 0 + value;
        }
        return "" + value;
    }

}
