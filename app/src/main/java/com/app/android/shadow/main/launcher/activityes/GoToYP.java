package com.app.android.shadow.main.launcher.activityes;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;

import com.app.android.shadow.main.util.Logger;

public class GoToYP extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //goToNextScreen(GamePlay.class);
       //setContentView(R.layout.activity_go_to_yp);
    }

    protected void goToNextScreen(Class lanchClass){
        Logger.printLog(Logger.ERROR, "Working class " + lanchClass.getSimpleName());
        Intent intent = new Intent(this,lanchClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}


/*
// Code form the side
<activity android:name="com.kk.ainActivity"
  android:launchMode="singleTask"
    android:label="@string/app_name">
    <intent-filter>
        /*<action android:name="android.intent.action.MAIN" />          Remove this intent Flter
        <category android:name="android.intent.category.LAUNCHER" />*/
/*</intent-filter>
</activity>

        Step 2 : Add Activity Alias for respective icons and titles



<activity-alias android:label="@string/app_name"
        android:icon="@drawable/icon"
        android:name="com.kk.MainActivity-alpha"
        android:enabled="false"
        android:targetActivity="com.kk.MainActivity">
<intent-filter>
<action android:name="android.intent.action.MAIN" />
<category android:name="android.intent.category.LAUNCHER" />
</intent-filter>
</activity-alias>

        1
        2
        3
        4
        5
        6
        7
        8
        9
        10
        11
        12

<activity-alias android:label="@string/app_name"
        android:icon="@drawable/icon"
        android:name="com.kk.MainActivity-alpha"
        android:enabled="false"
        android:targetActivity="com.kk.MainActivity">
<intent-filter>
<action android:name="android.intent.action.MAIN" />
<category android:name="android.intent.category.LAUNCHER" />
</intent-filter>
</activity-alias>



<activity-alias android:label="@string/app_name"
        android:icon="@drawable/icon"
        android:name="com.kk.MainActivity-beta"
        android:enabled="true"
        android:targetActivity="com.kk.MainActivity">
<intent-filter>
<action android:name="android.intent.action.MAIN" />
<category android:name="android.intent.category.LAUNCHER" />
</intent-filter >
</activity-alias>

        1
        2
        3
        4
        5
        6
        7
        8
        9
        10
        11
        12

<activity-alias android:label="@string/app_name"
        android:icon="@drawable/icon"
        android:name="com.kk.MainActivity-beta"
        android:enabled="true"
        android:targetActivity="com.kk.MainActivity">
<intent-filter>
<action android:name="android.intent.action.MAIN" />
<category android:name="android.intent.category.LAUNCHER" />
</intent-filter >
</activity-alias>

        Step 3 : Now you can programmatically Enable or disable your activity aliases



        getPackageManager().setComponentEnabledSetting(
        new ComponentName("com.kk.MainActivity", "com.kk.MainActivity-alpha"),
        PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);


        1
        2
        3
        4
        5
        6

        getPackageManager().setComponentEnabledSetting(
        new ComponentName("com.kk.MainActivity", "com.kk.MainActivity-alpha"),
        PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);


 */