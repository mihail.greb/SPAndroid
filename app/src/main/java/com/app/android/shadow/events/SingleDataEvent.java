package com.app.android.shadow.events;

public class SingleDataEvent<T>
{
    protected final T _data;

    public SingleDataEvent(final T data)
    {
        _data = data;
    }

    public T getData()
    {
        return _data;
    }

    @Override
    public String toString()
    {
        return "SingleDataEvent [_data=" + _data + "]";
    }
}
