package com.app.android.shadow.main.tapmanager.settings;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.R;
import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.main.base.BaseFragment;

public class ReportInappropriate extends BaseFragment implements View.OnClickListener
{

    //Header
    @Override
    protected void setHeader(View v) {
        super.setHeader(v);
        TextView title = (TextView) v.findViewById(R.id.title_head);
        title.setText(resources.getString(R.string.report_inappropriate_activity));
    }

    EditText email,description;
    Button send;
    String emailString="",descriptionStrign="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view =inflater.inflate(R.layout.shadow_repost_inappropriate, container, false);
        setHeader(_view);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        email = (EditText) _view.findViewById(R.id.report_inappropriate_email_response);
        description = (EditText) _view.findViewById(R.id.report_inappropriate_details);
        send = (Button) _view.findViewById(R.id.report_inappropriate_send_message);
        send.setOnClickListener(this);

        initTextview(_view);


        return _view;
    }

    private void initTextview(View view) {
        String stText = resources.getString(R.string.report_inappropriate_description);
        String emailText = resources.getString(R.string.report_inappropriate_email);
        SpannableString ss = new SpannableString(stText);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                sendMail();
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, stText.indexOf(emailText.toString()), (stText.length()), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView textView = (TextView) view.findViewById(R.id.report_inappropriate_description);
        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.report_inappropriate_send_message:
                emailString = email.getText().toString()+"";
                descriptionStrign = description.getText().toString()+"";

                if(descriptionStrign.length() >0) {
                    if(emailString.length() == 0 || isValidEmail(emailString)) {
                        sendMail();
                    }else {
                        email.setError(resources.getString(R.string.enter_valid_email));
                    }
                }else {
                    description.setError(resources.getString(R.string.please_enter_detail));
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        BaseSampleActivity.isAppNotClose = false;
    }

    private void sendMail() {
        BaseActivity.setSaveTime0();
        BaseSampleActivity.isAppNotClose = true;
        Intent intent = new Intent(Intent.ACTION_SEND);// it's not ACTION_SEND it's ACTION_SENDTO
        intent.setType("message/rfc822");
        //intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"ritesh@grepruby.com","nagrawal@grepruby.com","mayank@grepruby.com"}); // Set Mail to: Email Hear
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@dandesign.dk"}); // Set Mail to: Email Hear
        intent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.report_inappropriate_activity_subject));
        //intent.putExtra(Intent.EXTRA_TEXT, (descriptionStrign + "\n\n" + emailString));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        try {
            startActivity(Intent.createChooser(intent, "Send mail"));
        } catch (android.content.ActivityNotFoundException ex) {
            Shadow.alertToast(resources.getString(R.string.no_email_app_installed));
        }
    }


    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    public void onPause() {
        super.onPause();
        closeSoftKeyboard();
    }
}
