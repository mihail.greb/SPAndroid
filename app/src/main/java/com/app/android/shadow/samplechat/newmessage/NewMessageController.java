package com.app.android.shadow.samplechat.newmessage;

import android.view.View;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.gui.core.controller.IControllerRequestListener;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.newmessage.controller.INewMessageViewHolder;
import com.c2call.sdk.pub.gui.newmessage.controller.SCNewMessageController;
import com.c2call.sdk.pub.richmessage.SCBaseRichMessageSendObject;
import com.app.android.shadow.R;

public class NewMessageController extends SCNewMessageController
{


    public NewMessageController(final View view,
                                final SCViewDescription viewDescription,
                                final IControllerRequestListener requestListener,
                                final SCFriendData contact,
                                final boolean showIdleWhileSending)
    {
        super(view, viewDescription, requestListener, null, showIdleWhileSending);

        Ln.d("c2app", "NewMessageDecorator.<init>");

        if (contact != null) {
            setData(contact);
        }
    }

    @Override
    protected void onBindViewHolder(INewMessageViewHolder vh)
    {
        Ln.d("c2app", "NewMessageDecorator.onBindViewHolder()");
        super.onBindViewHolder(vh);

        setDecorator(new NewMessageDecorator());
    }

    protected void onMessageSuccessfullySent(String message, SCBaseRichMessageSendObject sendObject)
    {
        C2CallSdk.startControl().openBoard(getContext(), null, R.layout.sc_board, getData().getId(), StartType.Activity);
        finish();
    }


}
