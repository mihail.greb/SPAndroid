package com.app.android.shadow.common.ui.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.tapmanager.gallery.GalleryFragment;
import com.app.android.shadow.samplechat.SimpleChatFriendsFragment;
import com.app.android.shadow.R;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.tapmanager.settings.SettingsFragment;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.main.util.Logger;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.common.SCFriendGroup;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.SCThreadMode;
import com.c2call.sdk.pub.eventbus.events.SCStatusChangeEvent;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.fragments.SCBoardFragment;
import com.c2call.sdk.pub.fragments.SCContactsFragment;
import com.c2call.sdk.pub.fragments.SCFriendsFragment;
import com.c2call.sdk.pub.fragments.SCInviteFragment;
import com.c2call.sdk.pub.fragments.SCNewMessageFragment;
import com.c2call.sdk.pub.fragments.SCProfileFragment;
import com.c2call.sdk.pub.fragments.SCSelectFriendsFragment;
import com.c2call.sdk.pub.fragments.communication.IBoardFragmentCommunictation;
import com.c2call.sdk.pub.fragments.communication.IFriendsFragmentCommunictation;
import com.c2call.sdk.pub.gui.core.events.SCBaseControllerEvent;
import com.c2call.sdk.pub.gui.core.loaderhandler.ILoaderHandlerContext;

import java.util.HashMap;
import java.util.Stack;

/**
 * Base class for most Activities in this sample app. It contains a simple TabHost
 * to add fragments and a basic menu
 */
public class BaseSampleActivity extends BaseActivity
        implements
        SCFriendsFragment.Callbacks,
        SCProfileFragment.Callbacks,
        SCContactsFragment.Callbacks,
        SCSelectFriendsFragment.Callbacks,
        SCInviteFragment.Callbacks,
        SCBoardFragment.Callbacks,
        SCNewMessageFragment.Callbacks,
        ILoaderHandlerContext
{
    protected String from ="";
    protected int detailPagePosi=-1;

    public static boolean isAppNotClose = false;
    public static boolean isFriendEditorOpen = false;
    /* Your Tab host */
    protected TabHost mTabHost;
    protected View baseActivityView;
    /* A HashMap of stacks, where we use tab identifier as keys..*/
    public static HashMap<String, Stack<android.app.Fragment>> mStacks;

    /*Save current tabs identifier in this..*/
    public static String mCurrentTab;
    public static Fragment mCurrentFragment;

    protected LinearLayout mainLayout;

    public static SimpleChatFriendsFragment __instance;

    private View createTabView(final int imageId,final String tag){
        View view = LayoutInflater.from(this).inflate(R.layout.app_main_tab_indicator, null);
        //final Drawable image = getResources().getDrawable(imageId);
        final TextView tabText = (TextView) view.findViewById(R.id.tab_text);
        final ImageView tabImage = (ImageView) view.findViewById(R.id.tab_image);
        tabText.setText(tag);
        tabImage.setImageResource(imageId);
       // image.setBounds(0, 0, 60, 60);
       // textView.setCompoundDrawables(null, image, null, null);
        return view;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = getLayoutInflater();
        baseActivityView = inflater.inflate(R.layout.shadow_main_tab_fragment_layout, null);
        setContentView(baseActivityView);
        //TabWidget tabWidget = (TabWidget) findViewById(android.R.id.tabs);
        mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
        //onInitActionBar();

        mStacks             =   new HashMap<String, Stack<Fragment>>();
        mStacks.put(Const.FRIENDS, new Stack<Fragment>());
        mStacks.put(Const.GALLERY, new Stack<Fragment>());
        /*if(!Shadow.isProApp())
            mStacks.put(Const.PRO_APP, new Stack<Fragment>());*/
        mStacks.put(Const.SETTINGS, new Stack<Fragment>());
        mStacks.put(Const.CLOSE, new Stack<Fragment>());

        mTabHost                =   (TabHost)findViewById(android.R.id.tabhost);
        mTabHost.setOnTabChangedListener(listener);
        mTabHost.setup();

        initializeTabs();
        setChecked();
        SCCoreFacade.instance().subscribe(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Logger.printLog(Logger.ERROR, "onStart");
        //if(!(mCurrentFragment instanceof ProfileFragment))
            BaseSampleActivity.isAppNotClose = false;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Logger.printLog(Logger.ERROR, "onRestart");
        //if(!(mCurrentFragment instanceof ProfileFragment))
        BaseSampleActivity.isAppNotClose = false;
    }





    public View getView() {
        return baseActivityView;
    }

    /* Might be useful if we want to switch tab programmatically, from inside any of the fragment.*/
    public void setCurrentTab(int val){
        mTabHost.setCurrentTab(val);
    }

    public void initializeTabs(){

        // Setup your tab icons and content views.. Nothing special in this..
        TabHost.TabSpec spec    =   mTabHost.newTabSpec(Const.FRIENDS);
        mTabHost.setCurrentTab(-3);
        // Friends
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        spec.setIndicator(createTabView(R.drawable.friend, Const.FRIEND));
        mTabHost.addTab(spec);

        // Gallery
        spec                    =   mTabHost.newTabSpec(Const.GALLERY);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        spec.setIndicator(createTabView(R.drawable.gallery, Const.GALLERY));
        mTabHost.addTab(spec);
/*
        if(!Shadow.isProApp()) {
            // ProApp
            spec = mTabHost.newTabSpec(Const.PRO_APP);
            spec.setContent(new TabHost.TabContentFactory() {
                public View createTabContent(String tag) {
                    return findViewById(R.id.realtabcontent);
                }
            });
            spec.setIndicator(createTabView(R.drawable.download, Const.PRO_APP));
            mTabHost.addTab(spec);
        }*/

        // Settings
        spec                    =   mTabHost.newTabSpec(Const.SETTINGS);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        spec.setIndicator(createTabView(R.drawable.settings, Const.SETTINGS));
        mTabHost.addTab(spec);

        // Close
        spec                    =   mTabHost.newTabSpec(Const.CLOSE);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        spec.setIndicator(createTabView(R.drawable.close, Const.CLOSE));
        mTabHost.addTab(spec);

    }

    protected void onInitActionBar()
    {
        if (getActionBar() != null) {
            getActionBar().show();
        }

    }

    TabHost.OnTabChangeListener listener    =   new TabHost.OnTabChangeListener() {
        public void onTabChanged(String tabId) {
            mCurrentTab                     =   tabId;
            Logger.printLog(Logger.ERROR,"mCurrentTab "+tabId+"  "+mCurrentTab);
            Logger.printLog(Logger.ERROR,"mCurrentTab size  "+mStacks.get(tabId).size());
//#939292   /
            Bundle bun = new Bundle();
            if(mStacks.get(tabId).size() == 0){
                  if(tabId.equals(Const.FRIENDS)){
                      __instance = new SimpleChatFriendsFragment();
                      if(from.equals("chat")){
                          bun.putInt("position", detailPagePosi);
                          __instance.setArguments(bun);
                      }
                    pushFragments(tabId, __instance, false, true);
                    //pushFragments(tabId, new FriendsListFragment(), false, true);
                  }else if(tabId.equals(Const.GALLERY)){
                    pushFragments(tabId, new GalleryFragment(), false,true);
                  }/*else if(tabId.equals(Const.PRO_APP)){
                    pushFragments(tabId, new ProApp(), false,true);
                  }*/else if(tabId.equals(Const.SETTINGS)){
                      Fragment settings = new SettingsFragment();
                      if(from.equals("alarm")){
                          settings.setArguments(bun);
                      }
                    pushFragments(tabId,settings, false,true);
                  }else if(tabId.equals(Const.CLOSE)){
                    exittoApp();
                      //pushFragments(tabId, new Close(), false,true);
                  }
            }else {
                pushFragments(tabId, mStacks.get(tabId).lastElement(), false,false);
            }
            setChecked();
        }
    };

    private void setChecked(){
        int newColor =  colorGet(android.R.color.white);
        // mTabHost.getCurrentTabView();
        int defaultColor = colorGet(android.R.color.darker_gray);
        int currentTab = mTabHost.getCurrentTab();
        for(int i =0;i<4;i++){
            View tabView =mTabHost.getTabWidget().getChildTabViewAt(i);
            //RelativeLayout rLayout =(RelativeLayout) tabView.findViewById(R.id.rlayout);
            try {
                ImageView imageView = (ImageView) tabView.findViewById(R.id.tab_image);
                TextView textView = (TextView) tabView.findViewById(R.id.tab_text);
                if(i==currentTab){
                    imageView.setColorFilter(newColor, PorterDuff.Mode.SRC_ATOP);
                    textView.setTextColor(newColor);
                }else {
                    imageView.setColorFilter(defaultColor, PorterDuff.Mode.SRC_ATOP);
                    textView.setTextColor(defaultColor);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void pushFragments(String tag, Fragment fragment,boolean shouldAnimate, boolean shouldAdd){
        Logger.printLog(Logger.ERROR,"fragment name3"+ fragment.getClass().getSimpleName());
        mCurrentFragment = fragment;
        if(shouldAdd)
            mStacks.get(tag).push(fragment);
        FragmentManager manager         =   getFragmentManager();
        FragmentTransaction ft            =   manager.beginTransaction();
        if(shouldAnimate)
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.realtabcontent, fragment);
        ft.commit();
    }

    public void popFragments(){
      /*
       *    Select the second last fragment in current tab's stack..
       *    which will be shown after the fragment transaction given below
       */
        Fragment fragment             =   getCurrentFragment();
        mCurrentFragment = fragment;

      /*pop current fragment from stack.. */
        mStacks.get(mCurrentTab).pop();

      /* We have the target fragment in hand.. Just show it.. Show a standard navigation animation*/
        FragmentManager   manager         =   getFragmentManager();
        FragmentTransaction ft            =   manager.beginTransaction();
        ft.replace(R.id.realtabcontent, fragment);
        ft.commit();
    }

   /* public void popFragments(int position){
      *//*
       *    Select the second last fragment in current tab's stack..
       *    which will be shown after the fragment transaction given below
       *//*
        Fragment fragment             =   getCurrentFragment();
        mCurrentFragment = fragment;
      *//*pop current fragment from stack.. *//*
        mStacks.get(mCurrentTab).pop();

      *//* We have the target fragment in hand.. Just show it.. Show a standard navigation animation*//*
        FragmentManager   manager         =   getFragmentManager();
        FragmentTransaction ft            =   manager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putInt("delPosition",position);
        fragment.setArguments(bundle);
        ft.replace(R.id.realtabcontent, fragment);
        ft.commit();
    }*/

    protected Fragment getCurrentFragment(){
        return mStacks.get(mCurrentTab).elementAt(mStacks.get(mCurrentTab).size() - 2);
    }

    @Override
    public void onBackPressed() {
        Logger.printLog(Logger.ERROR,"isFriendEditorOpen  "+isFriendEditorOpen);
        Logger.printLog(Logger.ERROR,"mStacks.get(mCurrentTab).size()  "+mStacks.get(mCurrentTab).size());
        Logger.printLog(Logger.ERROR," mStacks.get(mCurrentTab)  "+ mCurrentTab.equals(Const.FRIENDS));
        Logger.printLog(Logger.ERROR,"mCurrentFragment   " +(mCurrentFragment instanceof SimpleChatFriendsFragment));
        if(isFriendEditorOpen &&
                mStacks.get(mCurrentTab).size() == 1 && mCurrentTab.equals(Const.FRIENDS)
               && (mCurrentFragment instanceof SimpleChatFriendsFragment)
                ){
                    ((SimpleChatFriendsFragment)mCurrentFragment).setDeleteBtnVisiblity();
        }else {
            //if(((BaseFragment)mStacks.get(mCurrentTab).lastElement()).onBackPressed() == false){

            if (mStacks.get(mCurrentTab).size() == 1) {
                Logger.printLog(Logger.ERROR,"print stack size is " + mCurrentTab);
                //super.onBackPressed();  // or call finish..
                //exittoApp();
                GotoWaytoAccess();
            } else {
                Logger.printLog(Logger.ERROR,"print stack size is " + mStacks);
                Logger.printLog(Logger.ERROR,"print stack size is " + mStacks.get(mCurrentTab).size());
                FragmentManager manager = getFragmentManager();
                if(manager.getBackStackEntryCount()>0){
                    manager.popBackStack();
                }else
                    popFragments();
            }
        }
        /*}else{
            //do nothing.. fragment already handled back button press.
        }*/
    }

    protected void GotoWaytoAccess() {
        //callApi(APIServiceAsyncTask.setUserOfflien);
        Intent intent = new Intent(this, Shadow.getWayToAccessCls());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


    @SCEventCallback(threadMode = SCThreadMode.MainThread, isSticky = true)
    private void onEvent(SCStatusChangeEvent evt)
    {
        Ln.d("c2app", "MainTabActivity.onEvent() - evt: %s", evt);

        if (getActionBar() == null){
            return;
        }
    }

   /* @Override
    public boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event) {
        boolean b = super.onKeyMultiple(keyCode, repeatCount, event);
        if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            Logger.printLog(Logger.ERROR,"key down working");
            exittoApp();
        }
        Logger.printLog(Logger.ERROR,"button press");
        return b;
    }*/

    /*@Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.getWindow().setType(WindowManager.LayoutParams.FIRST_SYSTEM_WINDOW + 4);
    }*/

    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean b = super.onKeyDown(keyCode, event);
        if(keyCode == KeyEvent.KEYCODE_HOME)
      {
            Logger.printLog(Logger.ERROR,"key down working");
            exittoApp();
        }
        Logger.printLog(Logger.ERROR,"button press");
        return b;
    }*/

    @Override
    public void onRegisterFriendsCommunictation(IFriendsFragmentCommunictation iFriendsFragmentCommunictation)
    {

    }

    @Override
    public void onShowBoard(String s)
    {

    }

    @Override
    public void onShowContactDetail(String s, int i)
    {

    }

    @Override
    public void onShowGroupDetail(SCFriendGroup scFriendGroup)
    {

    }

    @Override
    public void onRegisterBoardCommunictation(IBoardFragmentCommunictation iBoardFragmentCommunictation)
    {

    }

    @Override
    public void onControllerEvent(SCBaseControllerEvent scBaseControllerEvent)
    {

    }

    @Override
    public Context getLoaderContext()
    {
        return this;
    }

}


 /* @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.app_menu_sample_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId()){
            case R.id.action_logout:
                logout();
                return true;
            case R.id.action_profile:
                C2CallSdk.startControl().openProfile(this, null, R.layout.sc_edit_profile, StartType.Activity);
                return true;
        }

        return false;
    }

    private void logout()
    {
        new SimpleAsyncTask<Boolean>(this, 0)
        {
            @Override
            protected Boolean doInBackground(Void... voids)
            {
                return SCCoreFacade.instance().logout();
            }

            @Override
            protected void onSuccess(Boolean result)
            {
                finish();
            }
        }.execute();
    }*/

    /*protected static class MainTabIndicator extends LinearLayout
    {
        public MainTabIndicator(final Context context, final int imageId,final String tag)
        {
            super(context);
            final Drawable image = getResources().getDrawable(imageId);
            init(context, image,tag);
        }


        private void init(final Context context, final Drawable image,final String tag)
        {
            final View tab = LayoutInflater.from(context).inflate(R.layout.app_main_tab_indicator, this);

           // final ImageView imageView = (ImageView) tab.findViewById(R.id.tab_image);
            final TextView textView = (TextView) tab.findViewById(R.id.tab_text);
            //imageView.setImageDrawable(image);
            textView.setText(tag);
            image.setBounds(0,0,60,60);
            textView.setCompoundDrawables(null,image,null,null);
        }
    }

    private FragmentTabHost _tabHost;

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.app_main);
        setContentView(R.layout.shadow_main_tab_fragment_layout);

        onInitActionBar();
        onIinitTabHost();

       SCCoreFacade.instance().subscribe(this);
    }

    private void onIinitTabHost()
    {
        _tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        _tabHost.setup(this, getFragmentManager(), R.id.content_frame);

        onAddTabs();

        _tabHost.getTabWidget().setStripEnabled(false);
        _tabHost.getTabWidget().setDividerDrawable(null);


    }


    protected void onAddTabs()
    {

    }


    *//**
 * Convenient method to add fragments to the tab host
 * @param cls The Fragment's class to show
 * @param layout the layout to pass to the fragment for inflation
 * @param tag an optional tag for this fragment
 * @param icon the icon that is used by the tab host for the fragment
 * @param extra some extra data which is passed as arguments to the fragment
 *//*
    protected void addTab(Class<? extends Fragment> cls, int layout, String tag, int icon, Bundle extra)
    {
        Bundle args;
        args = new Bundle();
        args.putInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);

        if (extra != null) {
            args.putAll(extra);
        }

       // TabHost.TabSpec tabSpec = _tabHost.newTabSpec(tag).setIndicator(new MainTabIndicator(this, icon, tag));
        //_tabHost.addTab(tabSpec, cls, args);
    }*/