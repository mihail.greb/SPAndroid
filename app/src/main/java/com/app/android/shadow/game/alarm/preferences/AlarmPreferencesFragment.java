/* Copyright 2014 Sheldon Neilson www.neilson.co.za
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package com.app.android.shadow.game.alarm.preferences;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.app.android.shadow.main.dbhelper.ShadowDB;
import com.app.android.shadow.main.tapmanager.settings.Modes;
import com.app.android.shadow.game.alarm.Alarm;
import com.app.android.shadow.game.alarm.BaseAlarmFragment;
import com.app.android.shadow.game.alarm.database.AlarmDatabase;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.R;

import java.util.Calendar;

public class AlarmPreferencesFragment extends BaseAlarmFragment {

	ImageButton deleteButton;
	TextView okButton;
	TextView cancelButton;

	View v;
	private Alarm alarm;
	private MediaPlayer mediaPlayer;

	private ListAdapter listAdapter;
	private ListView listView;

	int flag = 0;

	//Header
	TextView title;
	protected void setHeader(View v) {
		super.setHeader(v);
		v.findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (getMathAlarm().getId() < 1) {
					AlarmDatabase.createAlarm(getMathAlarm());
				} else {
					AlarmDatabase.updateAlarm(getMathAlarm());
				}
				callMathAlarmScheduleService();
				Toast.makeText(getActivity(), getMathAlarm().getTimeUntilNextAlarmMessage(), Toast.LENGTH_LONG).show();
				getActivity().onBackPressed();
			}
		});

		v.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
				dialog.setTitle("Delete");
				dialog.setMessage("Delete this alarm?");
				dialog.setPositiveButton("Ok", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						AlarmDatabase.init(getActivity());
						if (getMathAlarm().getId() < 1) {
							// Alarm not saved
						} else {
							AlarmDatabase.deleteEntry(alarm);
							callMathAlarmScheduleService();
						}
						getActivity().onBackPressed();
					}
				});
				dialog.setNegativeButton("Cancel", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				dialog.show();
			}
		});
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		v =inflater.inflate(R.layout.shadow_alarm_preferences, container, false);
		setHeader(v);
		Bundle bundle = getArguments();
		if (bundle != null && bundle.containsKey("alarm")) {
			setMathAlarm((Alarm) bundle.getSerializable("alarm"));
		} else {
			setMathAlarm(new Alarm());
		}
		if (bundle != null && bundle.containsKey("adapter")) {
			setListAdapter((AlarmPreferenceListAdapter) bundle.getSerializable("adapter"));
		} else {
			setListAdapter(new AlarmPreferenceListAdapter(getActivity(), getMathAlarm()));
		}

		getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> l, View v, int position, long id) {
				final AlarmPreferenceListAdapter alarmPreferenceListAdapter = (AlarmPreferenceListAdapter) getListAdapter();
				final AlarmPreference alarmPreference = (AlarmPreference) alarmPreferenceListAdapter.getItem(position);

				AlertDialog.Builder alert;
				v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
				switch (alarmPreference.getType()) {
					case BOOLEAN:
						CheckedTextView checkedTextView = (CheckedTextView) v;
						boolean checked = !checkedTextView.isChecked();
						((CheckedTextView) v).setChecked(checked);
						switch (alarmPreference.getKey()) {
							case ALARM_ACTIVE:
								alarm.setAlarmActive(checked);
								break;
							case ALARM_VIBRATE:
								alarm.setVibrate(checked);
								if (checked) {
									Vibrator vibrator = (Vibrator) getActivity().getSystemService(getActivity().VIBRATOR_SERVICE);
									vibrator.vibrate(1000);
								}
								break;
						}
						alarmPreference.setValue(checked);
						break;
					case INTEGER:
						break;
					case STRING:

						alert = new AlertDialog.Builder(getActivity());

						alert.setTitle(alarmPreference.getTitle());
						// alert.setMessage(message);

						// Set an EditText view to get user input
						final EditText input = new EditText(getActivity());

						input.setText(alarmPreference.getValue().toString());

						alert.setView(input);
						alert.setPositiveButton("Ok", new OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {

								alarmPreference.setValue(input.getText().toString());

								if (alarmPreference.getKey() == AlarmPreference.Key.ALARM_NAME) {
									alarm.setAlarmName(alarmPreference.getValue().toString());
								}

								alarmPreferenceListAdapter.setMathAlarm(getMathAlarm());
								alarmPreferenceListAdapter.notifyDataSetChanged();
							}
						});
						alert.show();
						break;
					case LIST:
						alert = new AlertDialog.Builder(getActivity());

						alert.setTitle(alarmPreference.getTitle());
						// alert.setMessage(message);

						CharSequence[] items = new CharSequence[alarmPreference.getOptions().length];
						for (int i = 0; i < items.length; i++)
							items[i] = alarmPreference.getOptions()[i];

						alert.setItems(items, new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								switch (alarmPreference.getKey()) {
									case ALARM_DIFFICULTY:
										Alarm.Difficulty d = Alarm.Difficulty.values()[which];
										alarm.setDifficulty(d);
										break;
									case ALARM_TONE:
										alarm.setAlarmTonePath(alarmPreferenceListAdapter.getAlarmTonePaths()[which]);
										if (alarm.getAlarmTonePath() != null) {
											if (mediaPlayer == null) {
												mediaPlayer = new MediaPlayer();
											} else {
												if (mediaPlayer.isPlaying())
													mediaPlayer.stop();
												mediaPlayer.reset();
											}
											try {
												// mediaPlayer.setVolume(1.0f, 1.0f);
												mediaPlayer.setVolume(0.2f, 0.2f);
												mediaPlayer.setDataSource(getActivity(), Uri.parse(alarm.getAlarmTonePath()));
												mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
												mediaPlayer.setLooping(false);
												mediaPlayer.prepare();
												mediaPlayer.start();

												// Force the mediaPlayer to stop after 3
												// seconds...
												if (alarmToneTimer != null)
													alarmToneTimer.cancel();
												alarmToneTimer = new CountDownTimer(3000, 3000) {
													@Override
													public void onTick(long millisUntilFinished) {

													}

													@Override
													public void onFinish() {
														try {
															if (mediaPlayer.isPlaying())
																mediaPlayer.stop();
														} catch (Exception e) {

														}
													}
												};
												alarmToneTimer.start();
											} catch (Exception e) {
												try {
													if (mediaPlayer.isPlaying())
														mediaPlayer.stop();
												} catch (Exception e2) {

												}
											}
										}
										break;
									default:
										break;
								}
								alarmPreferenceListAdapter.setMathAlarm(getMathAlarm());
								alarmPreferenceListAdapter.notifyDataSetChanged();
							}

						});

						alert.show();
						break;
					case MULTIPLE_LIST:
						alert = new AlertDialog.Builder(getActivity());

						alert.setTitle(alarmPreference.getTitle());
						// alert.setMessage(message);

						CharSequence[] multiListItems = new CharSequence[alarmPreference.getOptions().length];
						for (int i = 0; i < multiListItems.length; i++)
							multiListItems[i] = alarmPreference.getOptions()[i];

						boolean[] checkedItems = new boolean[multiListItems.length];
						for (Alarm.Day day : getMathAlarm().getDays()) {
							checkedItems[day.ordinal()] = true;
						}
						alert.setMultiChoiceItems(multiListItems, checkedItems, new OnMultiChoiceClickListener() {

							@Override
							public void onClick(final DialogInterface dialog, int which, boolean isChecked) {

								Alarm.Day thisDay = Alarm.Day.values()[which];

								if (isChecked) {
									alarm.addDay(thisDay);
								} else {
									// Only remove the day if there are more than 1
									// selected
									if (alarm.getDays().length > 1) {
										alarm.removeDay(thisDay);
									} else {
										// If the last day was unchecked, re-check
										// it
										((AlertDialog) dialog).getListView().setItemChecked(which, true);
									}
								}

							}
						});
						alert.setOnCancelListener(new OnCancelListener() {
							@Override
							public void onCancel(DialogInterface dialog) {
								alarmPreferenceListAdapter.setMathAlarm(getMathAlarm());
								alarmPreferenceListAdapter.notifyDataSetChanged();

							}
						});
						alert.show();
						break;
					case TIME:
						TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new OnTimeSetListener() {

							@Override
							public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
								Calendar newAlarmTime = Calendar.getInstance();
								newAlarmTime.set(Calendar.HOUR_OF_DAY, hours);
								newAlarmTime.set(Calendar.MINUTE, minutes);
								newAlarmTime.set(Calendar.SECOND, 0);

								if (isMatchToSaveTime(newAlarmTime.getTimeInMillis())){
										/*&& (Modes.getStatus((Modes.getStatus(ShadowDB.IS_SAFE_MODE)) ?
										 ShadowDB.IS_ENTER_NAME_PASS : ShadowDB.UN_IS_ENTER_NAME_PASS))) {*/
									new Thread(new Runnable() {
										@Override
										public void run() {
											getActivity().runOnUiThread(new Runnable() {
												@Override
												public void run() {
													if(flag == 0) {
														flag++;
														//Logger.printLog(Logger.ERROR, "OpenDialog1");
														boolean isSafeModeEnable = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
														if(isSafeModeEnable){
															mCallback.onButtonClickEvent((Fragment) null, EventMaker.OpenDialog);
															//TODO Remove feature alarm IS_ENTER_NAME_PASS Required
															/*if(Modes.getStatus(ShadowDB.IS_ENTER_NAME_PASS)){
															}else{
																mCallback.onButtonClickEvent((Fragment) null, EventMaker.NoDialogRequired);
															}*/

														}else {
															mCallback.onButtonClickEvent((Fragment) null, EventMaker.NoDialogRequired);
														}
													}else {
														flag--;
													}
													}
											});

										}
									}).start();
								} else {
									alarm.setAlarmTime(newAlarmTime);
									alarmPreferenceListAdapter.setMathAlarm(getMathAlarm());
									alarmPreferenceListAdapter.notifyDataSetChanged();
								}
							}
						}, 00, 00, false);
						timePickerDialog.setTitle(alarmPreference.getTitle());
						timePickerDialog.show();
					default:
						break;
				}
			}
		});
		return v;
	}

	public static boolean isMatchToSaveTime(long newTime){
		int newhour   = (int) ((newTime / (1000*60*60)) % 24);
		int newminutes = (int) ((newTime / (1000*60)) % 60);

		long saveTime = getAlarmSaveTime();

		if(saveTime == 0){
			return false;
		}else {
			int saveHours=0,saveMinutes=0;

			try {
				saveHours   =  Modes.msToHour(saveTime);
				saveMinutes = Modes.msToMin(saveTime);
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
			if(saveHours == newhour && saveMinutes == newminutes){
				return true;
			}
			return false;
		}
	}



	public static String getAlarmSaveTimeStr(){
		long time = (getAlarmSaveTime());//19800000
		Calendar newAlarmTime = Calendar.getInstance();
		//newAlarmTime.setTimeZone( TimeZone.getTimeZone("GMT") );
		newAlarmTime.setTimeInMillis(time);
		if(time != 0) {
			int newminutes = newAlarmTime.get(Calendar.MINUTE);
			int newhour = newAlarmTime.get(Calendar.HOUR_OF_DAY);
			return "" + (newhour<10?"0":"")+newhour + ":" + (newminutes<10?"0":"")+newminutes;
		}else {
			return "";
		}

		/*if(time != 0) {
			int newminutes =Modes.msToMin(time);
			int newhour =  Modes.msToHour(time);
			Shadow.alertToast("hour is "+newhour+" minutes is "+newminutes);
			return "" + (newhour<10?"0":"")+newhour + ":" + (newminutes<10?"0":"")+newminutes;
		}else {
			return "";
		}*/
	}
	public static long getAlarmSaveTime(){
		long time = 0;
		try {
			boolean isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
			if (isSafeMode) {
				time = Shadow.getAppPreferences().getLong(Const.ALARM_SAVE_TIME_LOGIN_SAFE, 0);
			} else {
				time = Shadow.getAppPreferences().getLong(Const.ALARM_SAVE_TIME_LOGIN_UNSAFE, 0);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return time;
	}

	public static void saveAlarmSaveTime(long time){
		try {
			if(Shadow.isAlarmGame()) {
				boolean isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
				if (isSafeMode) {
					Shadow.getAppPreferencesEditor().putLong(Const.ALARM_SAVE_TIME_LOGIN_SAFE, time).commit();
				} else {
					Shadow.getAppPreferencesEditor().putLong(Const.ALARM_SAVE_TIME_LOGIN_UNSAFE, time).commit();
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	/*@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

	}*/

/*	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);
		menu.findItem(R.id.menu_item_new).setVisible(false);
		return result;
	}*/

	/*@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_item_save:
			//AlarmDatabase.init(getApplicationContext());

			break;
		case R.id.menu_item_delete:

			break;
		}
		return super.onOptionsItemSelected(item);
	}
*/
	private CountDownTimer alarmToneTimer;

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("alarm", getMathAlarm());
		outState.putSerializable("adapter", (AlarmPreferenceListAdapter) getListAdapter());
	};

	@Override
	public void onPause() {
		super.onPause();
		try {
			if (mediaPlayer != null)
				mediaPlayer.release();
		} catch (Exception e) {
		}
		// setListAdapter(null);
	}

	@Override
	public void onResume() {
		super.onResume();
		flag = 0;
	}

	public Alarm getMathAlarm() {
		return alarm;
	}

	public void setMathAlarm(Alarm alarm) {
		this.alarm = alarm;
	}

	public ListAdapter getListAdapter() {
		return listAdapter;
	}

	public void setListAdapter(ListAdapter listAdapter) {
		this.listAdapter = listAdapter;
		getListView().setAdapter(listAdapter);

	}

	public ListView getListView() {
		if (listView == null)
			listView = (ListView) v.findViewById(android.R.id.list);
		return listView;
	}

	public void setListView(ListView listView) {
		this.listView = listView;
	}

	@Override
	public void onClick(View v) {
		// super.onClick(v);

	}
}
