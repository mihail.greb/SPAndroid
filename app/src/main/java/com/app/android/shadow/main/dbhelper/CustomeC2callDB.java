package com.app.android.shadow.main.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.tapmanager.extra.ShadowFriendDataFromTable;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.main.util.Const;
import com.c2call.sdk.pub.db.data.SCBaseData;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.db.util.core.DatabaseHelper;
import com.j256.ormlite.android.AndroidDatabaseResults;
import com.j256.ormlite.dao.ObjectCache;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rails-dev on 13/10/15.
 */
public class CustomeC2callDB extends DatabaseHelper {

    //public static String AUTHORITY;
// private static final Class[] CLASSES = new Class[]{SCGroupInfoData.class, SCFriendData.class, SCPhoneData.class, SCFriendExtraData.class, SCOpenIdData.class, SCBoardEventData.class, SCDirtyFriendData.class, SCMediaData.class, SCBoardEventExtraData.class, SCMissedCallData.class, SCMissedCallLookupData.class, SCFavoriteData.class, SCUserData.class, SCDidExt.class, SCSecureMessageData.class};

//private static String lastAccount = GlobalPrefs.instance().getString("sc_login_email", (String)null);

// private static String DB_NAME = "c2call_" + lastAccount + ".sqlite";
//private static String DB_NAME = "c2call_www@yopmail.com.sqlite";


    public CustomeC2callDB(Class<? extends SCBaseData<?>>[] classes, String name, int version, Context ctx) {
        super(classes, name, version, ctx);
    }

    /*Created by c2call SDK*/
    private static final String C2CALL_FRIEND_TABLE ="c2call_friend";
    private static final String C2CALL_MEDIA_TABLE ="c2call_media";
    private static final String C2CALL_MESSAGE_EVENT_TABLE ="c2call_event";

    public static final String DISPLAY_NAME ="dipslay_name";
    public static final String CONTACT ="Contact";
    public static final String LOC ="loc";
    public static final String MEDIA_KEY="media_key";
    public static final String MEDIA_TYPE="media_type";
    public static final String THUMB_LOC ="thumb_loc";

    /*DirtyFriend*/
    private static final String C2CALL_DIRTY_FRIEND ="c2call_dirtyfriend";
    public static final String MOD_TIME = "mod_time";
    public static final String DIRTY = "dirty";
    public static final String _ID= "_id";

    public void UpdateDrityFriend(String id,long time){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(_ID,id); //These Fields should be your String values of actual column names
        cv.put(DIRTY,0);
        cv.put(MOD_TIME,time);
        db.update(C2CALL_DIRTY_FRIEND, cv, _ID +"='"+ id+"'", null);
    }

    public String getUserName(String _id) {
        String data = null;
            String selectQuery = "SELECT "+DISPLAY_NAME +" FROM " + C2CALL_FRIEND_TABLE + " WHERE _id = '" + _id +"'";
            //Logger.printLog(Logger.ERROR,"Select query", selectQuery);
            SQLiteDatabase db = this.getReadableDatabase();
           Cursor cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    data = cursor.getString(0);
                    //Logger.printLog(Logger.ERROR,"print db",data);
                } while (cursor.moveToNext());
            }
            cursor.close();
            return data;
    }

    public void deleteBlockUserDatabase(String userId){
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM "+C2CALL_MESSAGE_EVENT_TABLE+" where "+CONTACT+"= '"+ userId +"'";;
        Log.d("query", deleteQuery);
        db.execSQL(deleteQuery);
    }

    public SCFriendData getSCFriendData(Cursor cursor,String name){
        if (cursor.getCount() > 0) {
            Logger.printLog(Logger.ERROR,"Print cursor size","is "+cursor.getCount());
            if (cursor.moveToFirst()) {
                do {
                    String getName = null;
                    try {
                        getName = (cursor.getString(cursor.getColumnIndex(DISPLAY_NAME)));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if(getName.equals(name)) {
                        SCFriendData fdata;
                        try {
                            fdata = (SCFriendData) SCFriendData.dao().mapSelectStarRow(new AndroidDatabaseResults(cursor, (ObjectCache) null));
                            Logger.printLog(Logger.ERROR,"Print friend id ", "is " + fdata.getId());
                        } catch (Exception var3) {
                            var3.printStackTrace();
                            fdata = null;
                        }
                        if (fdata != null) {
                            if (name.equals(fdata.getManager().getDisplayName().trim())) {
                                Logger.printLog(Logger.ERROR,"SCFriendData name is", " " + fdata.getManager().getDisplayName().trim());
                                return fdata;
                            }
                        } else {
                            Logger.printLog(Logger.ERROR,"SCFriendData is null", "null");
                        }
                    }
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        return null;
    }

    public ShadowFriendDataFromTable getSCFriendData(String name){
        String selectQuery = "SELECT * FROM " + C2CALL_FRIEND_TABLE + " WHERE "+ Const.EMAIL+" = '" + name+"'";
        Logger.printLog(Logger.ERROR,selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

     /*   try {
            SCFriendData e = (SCFriendData)SCFriendData.dao().mapSelectStarRow(new AndroidDatabaseResults(cursor, (ObjectCache)null));
            return e;
        } catch (Exception var3) {
            var3.printStackTrace();
            return null;
        }*/

        if (cursor.getCount() > 0) {
            Logger.printLog(Logger.ERROR,"Print cursor size","is "+cursor.getCount());
            if (cursor.moveToFirst()) {
                do {
                        ShadowFriendDataFromTable fdata = null;
                        try {
                            fdata = new ShadowFriendDataFromTable(
                                    cursor.getString(cursor.getColumnIndex("city")),
                                    cursor.getString(cursor.getColumnIndex("zip")),
                                    cursor.getString(cursor.getColumnIndex("country")),
                                    cursor.getString(cursor.getColumnIndex("custom_status")),
                                    cursor.getString(cursor.getColumnIndex("street")),
                                    cursor.getString(cursor.getColumnIndex("did")),
                                    cursor.getString(cursor.getColumnIndex("dipslay_name")),
                                    cursor.getString(cursor.getColumnIndex("email")),
                                    cursor.getString(cursor.getColumnIndex("favorite")),
                                    cursor.getString(cursor.getColumnIndex("firstname")),
                                    cursor.getString(cursor.getColumnIndex("group_info")),
                                    cursor.getString(cursor.getColumnIndex("_id")),
                                    cursor.getString(cursor.getColumnIndex("image_large")),
                                    cursor.getString(cursor.getColumnIndex("image_small")),
                                    cursor.getString(cursor.getColumnIndex("language")),
                                    cursor.getString(cursor.getColumnIndex("lastname")),
                                    cursor.getString(cursor.getColumnIndex("own_number")),
                                    cursor.getString(cursor.getColumnIndex("custom_status_time")),
                                    cursor.getString(cursor.getColumnIndex("mod_time")),
                                    cursor.getString(cursor.getColumnIndex("status")),
                                    cursor.getString(cursor.getColumnIndex("usertype")),
                                    cursor.getString(cursor.getColumnIndex("confirmed")));

                        } catch (Exception var3) {
                            var3.printStackTrace();
                        }
                    return fdata;
                } while (cursor.moveToNext());
            }
        }else {
            Logger.printLog(Logger.ERROR, "cursor size is 0");
        }
        return null;
    }

    public boolean deleteMediaData(String _Id){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(C2CALL_MEDIA_TABLE, Const._ID + "='"+_Id+"'", null) > 0;
    }


    public ArrayList getMediaData(boolean isVideo){
        ArrayList<HashMap<String,String >> list = new ArrayList<HashMap<String,String >>();
        try {
            //SCFriendData friend = SCFriendData.dao().queryForId(userid);
            //List<SCMediaData> data = SCMediaData.dao().queryForAll();
            String selectQuery = "SELECT * FROM " + C2CALL_MEDIA_TABLE;
            Logger.printLog(Logger.ERROR,"Select query", selectQuery);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            HashMap<String, String> map;
            if (cursor.moveToFirst()) {
                do {
                    map = new HashMap<>();
                    String thumb = cursor.getString(0);
                    String mediaKey = cursor.getString(cursor.getColumnIndex(MEDIA_KEY));
                    map.put(Const._ID, cursor.getString(cursor.getColumnIndex(Const._ID)));
                    map.put(LOC, cursor.getString(cursor.getColumnIndex(LOC)));
                    map.put(MEDIA_KEY, mediaKey);
                    map.put(MEDIA_TYPE, cursor.getString(cursor.getColumnIndex(MEDIA_TYPE)));
                    map.put(THUMB_LOC, cursor.getString(cursor.getColumnIndex(THUMB_LOC)));
                    if (isVideo && mediaKey.contains("video")) {
                        list.add(map);
                    } else if (!isVideo && mediaKey.contains("image")) {
                        list.add(map);
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();
            Logger.printLog(Logger.ERROR,"list isVideo " + isVideo, " is " + list);
        }catch (Exception e ){}
        return list;
    }


    public String getmissedChat(String id){
        if(!Shadow.shadowDB.isUserBlocked(id)) {
            String Query = "Select * from " + C2CALL_MESSAGE_EVENT_TABLE + " where status = '-1' and type = 'MessageIn' and userid ='" + id + "'";
            Logger.printLog(Logger.ERROR,"getRing Query", "" + Query);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(Query, null);
            Logger.printLog(Logger.ERROR,"get mode count", "" + cursor.getCount());

            if (cursor.getCount() > 0) {
                Logger.printLog(Logger.ERROR,"Print cursor size", "is " + cursor.getCount());
                if (cursor.moveToFirst()) {
                    do {
                        String getDescription = "";
                        try {
                            getDescription = (cursor.getString(cursor.getColumnIndex("description")));
                            if (getDescription.toString().equals("VoIP")) {
                                return "";
                            } else {
                                return getDescription;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } while (cursor.moveToNext());
                }
            }
        }
        return "";
    }


  /*  public Cursor gtCursor(String name){
        String selectQuery = "SELECT * FROM " + C2CALL_FRIEND_TABLE + " WHERE "+DISPLAY_NAME+" = '" + name +"'";
        Logger.printLog(Logger.ERROR,"Print query","is "+selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }*/
}
