package com.app.android.shadow.main.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by abc on 3/24/2015.
 */
public class FriendsReqAdapter extends ArrayAdapter {

    Context mcontext;
    ArrayList<HashMap<String ,String >> lst= new ArrayList<HashMap<String ,String >>();
    Resources resources;
    Fragment fragment;

    public FriendsReqAdapter(Context mcontext, List<HashMap<String ,String >> objects) {
        super(mcontext, R.layout.shadow_more_row, objects );
        this.mcontext =mcontext;
        this.lst= (ArrayList<HashMap<String ,String >>) objects;
        this.resources = mcontext.getResources();
    }

    @Override
    public int getCount() {
        return lst.size();
    }

    @Override
    public String getItem(int position) {
        return lst.get(position).get(Const.USERNAME);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final HashMap<String ,String > item = lst.get(position);
        /*TextView itemName;
                LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.shadow_request_row, parent, false);
                itemName = (TextView) convertView.findViewById(R.id.item_name);
                itemName.setText(item.get(Const.USERNAME));
        return convertView;*/
        ViewHolderItem viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.shadow_request_row, parent, false);

            viewHolder = new ViewHolderItem();
            viewHolder.title = (TextView)convertView.findViewById(R.id.item_name);
            viewHolder.accept = (TextView)convertView.findViewById(R.id.accept);
            viewHolder.reject = (TextView)convertView.findViewById(R.id.reject);
            viewHolder.msg = (TextView)convertView.findViewById(R.id.message);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolderItem) convertView.getTag();
        }
        viewHolder.title.setText(item.get(Const.USERNAME));
        viewHolder.msg.setText(item.get(Const.GET_MESSAGE));
/*
        viewHolder.ccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        viewHolder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
        return convertView;
    }

    private class ViewHolderItem{
        TextView title;
        TextView accept;
        TextView reject;
        TextView msg;
    }
}
