package com.app.android.shadow.common.ui.core;


import com.app.android.shadow.main.Shadow;

/**
 * This class defines the constants that are used as extra data keys for Activity-/Fragment arguments.
 */
public class ExtraData
{
    public static class Login
    {
        public static final String MAIN_ACTIVITY = Shadow.getpackageNameAsStr()+".LOGIN.MAINACTIVITY";
        public static final String PASS_LOGIN= Shadow.getpackageNameAsStr()+".LOGIN";
        public static final String BY_GAME= Shadow.getpackageNameAsStr()+".GAMEPLAY";
    }


    public static class FullScreenImage
    {
        public static final String IMAGE_PATH= Shadow.getpackageNameAsStr()+".FULLSCREENIMAGE.PATH";
    }


}
