package com.app.android.shadow.common.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.app.android.shadow.R;
import com.app.android.shadow.samplechat.chathistory.ChatHistoryController;
import com.app.android.shadow.samplechat.chathistory.ChatHistoryListItemControllerFactory;
import com.app.android.shadow.samplechat.newmessage.NewMessageActivity;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.fragments.SCBoardFragment;
import com.c2call.sdk.pub.gui.board.controller.IBoardController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemType;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;


/**
 * Custom Board Fragment to show a chat history grouped by users
 */
public class ChatHistoryFragment extends SCBoardFragment
{
    private static SCViewDescriptionMap _viewDescriptionMap = new SCViewDescriptionMap();

    static{
        SCViewDescription vd = C2CallSdk.vd().boardListItem().get(SCBoardListItemType.Text.value());
        _viewDescriptionMap.setDefaultViewDescription(vd);
    }

    public static ChatHistoryFragment create(final int layout)
    {
        final Bundle args = new Bundle();
        args.putInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);

        final ChatHistoryFragment f = new ChatHistoryFragment();
        f.setArguments(args);
        return f;
    }

    private final SCActivityResultDispatcher _resultDispatcher = new SCActivityResultDispatcher();


    @Override
    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);


    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(getActivity().getActionBar() != null){
            getActivity().getActionBar().setTitle(R.string.app_title_chathistory);
        }
    }

    @Override
    protected IBoardController onCreateController(final View v, final SCViewDescription vd)
    {
        /*
        Create our custom controller to manage the chat history
         */
        return new ChatHistoryController(v,
                                         vd,
                                         _viewDescriptionMap,
                                         new ChatHistoryListItemControllerFactory(_resultDispatcher),
                                         null);
    }

    @Override
    protected SCActivityResultDispatcher onCreateResultDispactcher()
    {
        return _resultDispatcher;
    }


    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater)
    {
        menu.close();

        inflater.inflate(R.menu.app_menu_chathistory, menu);

    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item)
    {
        switch(item.getItemId()){
            case R.id.menu_new_message:
                onNewMessage();
                return true;
        }

        return false;
    }

    private void onNewMessage()
    {
        final Intent intent = new Intent(getActivity(), NewMessageActivity.class);
        startActivity(intent);
    }



}
