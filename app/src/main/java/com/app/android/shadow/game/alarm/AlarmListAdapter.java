/* Copyright 2014 Sheldon Neilson www.neilson.co.za
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package com.app.android.shadow.game.alarm;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.app.android.shadow.game.alarm.database.AlarmDatabase;
import com.app.android.shadow.game.alarm.preferences.AlarmPreferenceListAdapter;
import com.app.android.shadow.R;

import java.util.ArrayList;
import java.util.List;

public class AlarmListAdapter extends BaseAdapter {

    private AlarmFragment alarmFragment;
    private Context context;
    private List<Alarm> alarms = new ArrayList<Alarm>();

    public static final String ALARM_FIELDS[] = {AlarmDatabase.COLUMN_ALARM_ACTIVE,
            AlarmDatabase.COLUMN_ALARM_TIME, AlarmDatabase.COLUMN_ALARM_DAYS};

    public AlarmListAdapter(AlarmFragment alarmFragment, Context context) {
        this.alarmFragment = alarmFragment;
        this.context = context;
//		atabase.init(larmActivity);
//		alarms = Database.etAll();
    }

    @Override
    public int getCount() {
        return alarms.size();
    }

    @Override
    public Object getItem(int position) {
        return alarms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (null == view)
            view = LayoutInflater.from(context).inflate(
                    R.layout.shadow_alarm_list_element, null);

        Alarm alarm = (Alarm) getItem(position);

     /*   Switch toggleButton = (Switch) view.findViewById(R.id.checkBox_alarm_active);
        toggleButton.setChecked(alarm.getAlarmActive());
        toggleButton.setTag(position);
        toggleButton.setOnClickListener(alarmFragment);*/

        CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox_alarm_active);
        checkBox.setChecked(alarm.getAlarmActive());
        checkBox.setTag(position);
        checkBox.setOnClickListener(alarmFragment);

        TextView alarmTimeView = (TextView) view
                .findViewById(R.id.textView_alarm_time);
        alarmTimeView.setText(AlarmPreferenceListAdapter.getTimeInAMPM(alarm.getAlarmTimeString()));
        alarmTimeView.setTextColor(Color.BLACK);

        TextView alarmDaysView = (TextView) view
                .findViewById(R.id.textView_alarm_days);
        alarmDaysView.setText(alarm.getRepeatDaysString());
        alarmDaysView.setTextColor(Color.BLACK);

        return view;
    }

    public List<Alarm> getMathAlarms() {
        return alarms;
    }

    public void setMathAlarms(List<Alarm> alarms) {
        this.alarms = alarms;
    }

}
