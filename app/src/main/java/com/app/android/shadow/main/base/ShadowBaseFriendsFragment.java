package com.app.android.shadow.main.base;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.tapmanager.extra.MoreController;
import com.c2call.sdk.pub.fragments.SCFriendsFragment;


/**
 * Created by rails-dev11 on 4/8/15.
 */
public class ShadowBaseFriendsFragment extends SCFriendsFragment {

    public BaseActivity mActivity;

    public MoreController mCallback;
    Resources resources;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity		=	(BaseActivity) this.getActivity();
        resources= getResources();
    }

    public boolean onBackPressed(){
        return false;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (MoreController) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement Interface");
        }
    }
}
