package com.app.android.shadow.main.custom.call;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.common.ui.activities.ShadowIncomingCallFragmentActivity;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.FreeAppWayToAccess;
import com.app.android.shadow.main.dbhelper.ShadowDB;
import com.app.android.shadow.main.tapmanager.settings.Modes;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.CountClickBase;
//import com.c2call.sdk.lib.data.core.C2CallServiceMediator;
//import com.c2call.sdk.lib.data.core.Providers;
//import com.c2call.sdk.lib.util.eventindication.CallIndication;
import com.app.android.shadow.main.util.Const;
import com.c2call.sdk.pub.call.incoming.SCIncomingCallHandler;
import com.c2call.sdk.pub.common.SCIncomingCallData;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.db.datamanager.SCMissedCallManager;
import com.c2call.sdk.pub.services.c2callservice.C2CallService;
import com.c2call.sdk.pub.services.c2callservice.C2CallServiceSlave;

import java.util.List;
//import com.c2call.sdk.lib.n.f.a;

public class ShadowSCIncomingCallHandler extends SCIncomingCallHandler {

    public ShadowSCIncomingCallHandler() {

    }

   public static SCIncomingCallData data;

    @Override
    public void onIncomingCall(Context context, SCIncomingCallData data) {
        this.data = data;
        //super.onIncomingCall(context, data);
        Logger.printLog(Logger.ERROR,"User data id", data.callerId);
        long checkTime = Shadow.getAppPreferences().getLong(Const.SAVE_TIME_LOGOUT, 0);
        long sysCurTime = System.currentTimeMillis();
        Logger.printLog(Logger.ERROR, "checkTime " + checkTime);
        Logger.printLog(Logger.ERROR, "checkTime2  " + sysCurTime);

        boolean isUserBlock = false;    //Shadow.shadowDB.isUserBlocked(data.callerId);
        boolean isDecoyScreen = Shadow.isProApp()? CountClickBase.isDecoyActivity(): FreeAppWayToAccess.isIsFreeDecoyActivity();
        boolean isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
        boolean isDecoWithSafeMode = ((isDecoyScreen || isAppIsInBackground(context))&& isSafeMode);
        boolean isTimeOut = (checkTime != 0 && checkTime < sysCurTime);

        Logger.printLog(Logger.ERROR,"print isUserBlock ",isUserBlock +" isSafeMode "+isSafeMode+" isTimeout "+ isTimeOut);
        Logger.printLog(Logger.ERROR,"print is1 ",isDecoyScreen +" is2 "+isSafeMode);

        if (isUserBlock || isDecoWithSafeMode|| isTimeOut ) {
            Logger.printLog(Logger.ERROR,"User data id", "finished");
            // TODO CHECK
            try {
                C2CallServiceSlave c2CallServiceSlave =new C2CallServiceSlave(new C2CallService());
                //CallIndication.instance().stopIndication();
                //a.a().b();
                SCMissedCallManager.clear(data.callerId);
                //SCMissedCallManager.clear(//com.c2call.sdk.lib.
                  //      f.c.d.c().h());
                //com.c2call.sdk.lib.
                //    f.c.b.a().w();
                //C2CallServiceMediator.instance().hangup();
                c2CallServiceSlave.hangup();
            } catch (Exception var6) {
                var6.printStackTrace();
            } finally {
                //this.finish();
            }
        //} else if(checkTime != 0 && checkTime < sysCurTime ){
        } else {
            Logger.printLog(Logger.ERROR,"Print Caller ID "+data.callerId);
            BaseActivity.setSaveTime0();
            BaseSampleActivity.isAppNotClose = true;
            Intent intent = new Intent(context, ShadowIncomingCallFragmentActivity.class);
            intent.setAction("android.intent.action.VIEW");
            intent.setFlags(268435456);
            intent.putExtra(SCExtraData.IncomingCall.EXATR_DATA_INCOMING_CALL, data);
            context.startActivity(intent);
        }
    }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        try {
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
                for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        for (String activeProcess : processInfo.pkgList) {
                            if (activeProcess.equals(context.getPackageName())) {
                                isInBackground = false;
                            }
                        }
                    }
                }
            } else {
                List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                ComponentName componentInfo = taskInfo.get(0).topActivity;
                if (componentInfo.getPackageName().equals(context.getPackageName())) {
                    isInBackground = false;
                }
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        return isInBackground;
    }
}
