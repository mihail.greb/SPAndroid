package com.app.android.shadow.common.ui.dialogs;

import android.content.Intent;
import android.net.Uri;

import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.samplechat.board.BoardActivity;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.GlobalDepot;
import com.c2call.sdk.pub.core.SCDownloadType;
import com.c2call.sdk.pub.gui.core.controller.IController;
import com.c2call.sdk.pub.gui.dialog.BaseItemRunnable;
import com.c2call.sdk.pub.gui.dialog.DialogBuilderPhoto;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.util.MediaUtil;

import java.io.File;

/**
 * Created by rails-dev on 31/12/15.
 */
public class ShadowDialogBuilderPhoto extends DialogBuilderPhoto {

    public static SCChoiceDialog build(IController<?> controller, String filename) {
        SCChoiceDialog.Builder builder = new SCChoiceDialog.Builder(controller.getContext());
        builder.addItem(com.c2call.sdk.R.string.sc_dlg_rich_message_existing_photo_title,
                com.c2call.sdk.R.string.sc_dlg_rich_message_existing_photo_summary,
                com.c2call.sdk.R.drawable.sc_content_picture,
                new ShadowDialogBuilderPhoto.ExistingRunnable(controller))
                .addItem(com.c2call.sdk.R.string.sc_dlg_rich_message_take_photo_title
                        , com.c2call.sdk.R.string.sc_dlg_rich_message_take_photo_summary,
                        com.c2call.sdk.R.drawable.sc_device_access_camera,
                        new ShadowDialogBuilderPhoto.CamRunnable(controller, filename));
        return builder.build();
    }


    public static class CamRunnable extends BaseItemRunnable<IController<?>> {
        private final String _filename;

        public CamRunnable(IController<?> controller, String filename) {
            super(controller);
            this._filename = filename;
        }

        public void run() {
            try {
                String e = this._filename != null?this._filename:".c2call_tmp_picture.png";
                String output = MediaUtil.getMediaPath(SCDownloadType.Image, e, true);
                Ln.d("fc_tmp", "handleTakePhoto() - %s", new Object[]{output});
                GlobalDepot.put(GlobalDepot.KEY_LAST_PHOTO, output);
                File file = new File(output);
                ShadowDialogBuilderPhoto.setCloseFalse();
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                intent.putExtra("output", Uri.fromFile(file));
                this.startActivityForResult(intent, 6);
            } catch (Exception var5) {
                var5.printStackTrace();
            }

        }

    }

    public static void setCloseFalse() {
        try {
            BaseActivity.setSaveTime0();
            BaseSampleActivity.isAppNotClose = true;
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        try {
            BaseActivity.setSaveTime0();
            BoardActivity.isAppNotClose = true;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public static class ExistingRunnable extends BaseItemRunnable<IController<?>> {
        public ExistingRunnable(IController<?> controller) {
            super(controller);
        }

        public void run() {
            ShadowDialogBuilderPhoto.setCloseFalse();
            Intent intent = new Intent("android.intent.action.PICK");
            intent.setType("image/*");
            this.startActivityForResult(intent, 4);
        }
    }

}
