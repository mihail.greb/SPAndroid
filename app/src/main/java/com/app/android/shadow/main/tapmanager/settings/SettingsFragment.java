package com.app.android.shadow.main.tapmanager.settings;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.shadow.common.ui.fragments.AddFriendFragment;
import com.app.android.shadow.common.ui.fragments.ProfileFragment;
import com.app.android.shadow.main.apicalls.ApiRequest;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.game.alarm.AlarmFragment;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.samplechat.SimpleChatMainActivity;
import com.app.android.shadow.R;
import com.app.android.shadow.main.util.Const;
import com.c2call.sdk.pub.facade.SCCoreFacade;

import org.json.JSONObject;



public class SettingsFragment extends BaseFragment implements View.OnClickListener
{
    LinearLayout modesBox,DeleteAllDataBox,proAppBox;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view =inflater.inflate(R.layout.shadow_more, container, false);
        SimpleChatMainActivity.mCurrentTab = Const.SETTINGS;
        _view.findViewById(R.id.find_friend).setOnClickListener(this);
        _view.findViewById(R.id.my_profile).setOnClickListener(this);
       // _view.findViewById(R.id.sounds).setOnClickListener(this);
       // _view.findViewById(R.id.shadowlog).setOnClickListener(this);
        // _view.findViewById(R.id.delete_all_data).setOnClickListener(this); Remove full code
        _view.findViewById(R.id.modes).setOnClickListener(this);
        _view.findViewById(R.id.pro_app).setOnClickListener(this);
        //_view.findViewById(R.id.testFeedBack).setOnClickListener(this);
        _view.findViewById(R.id.logout).setOnClickListener(this);
        _view.findViewById(R.id.report_inappropriate_activity).setOnClickListener(this);

        if(Shadow.isProApp()){
            proAppBox= (LinearLayout)_view.findViewById(R.id.pro_app_box);
            proAppBox.setVisibility(View.GONE);
        }
            headerInit(_view);
        try {
            if (getArguments() != null && (getArguments().getString("alarm")).equals("alarm")) {
                mCallback.onButtonClickEvent(new AlarmFragment(), EventMaker.SettingsFragment);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return _view;
    }

    private void headerInit(View v) {
        v.findViewById(R.id.back).setVisibility(View.INVISIBLE);
        TextView title = (TextView)v.findViewById(R.id.title_head);
        title.setText(Const.SETTINGS);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.find_friend:
                //C2CallSdk.startControl().openAddFriend(getActivity(), null, R.layout.shadow_findfriend, StartType.Activity);
                mCallback.onButtonClickEvent(new AddFriendFragment(), EventMaker.SettingsFragment);
                break;
            case R.id.my_profile:
                //C2CallSdk.startControl().openProfile(getActivity(), null, R.layout.sc_edit_profile, StartType.Activity);
                mCallback.onButtonClickEvent(new ProfileFragment(), EventMaker.SettingsFragment);
                break;

          /*  case R.id.sounds:
                mCallback.onButtonClickEvent(new Sounds(), EventMaker.SettingsFragment);
                break;*/

           /* case R.id.block:
                mCallback.onButtonClickEvent(new Sounds(), EventMaker.oreFragment);
                break;*/

           /* case R.id.shadowlog:
                mCallback.onButtonClickEvent(new ShadowLogs(), EventMaker.SettingsFragment);
                break;*/

            case R.id.modes:
                mCallback.onButtonClickEvent(new Modes(), EventMaker.SettingsFragment);
                break;

         /*   case R.id.delete_all_data:
                showDeleteDailog();
                break;*/

            case R.id.pro_app:
                mCallback.onButtonClickEvent(new ProApp(), EventMaker.SettingsFragment);
                break;

           /* Remove with code
            case R.id.testFeedBack:
                //mCallback.onButtonClickEvent(new Logout(),EventMaker.SettingsFragment);
                callDefaultBrowser();
                break;*/

            case R.id.report_inappropriate_activity:
                mCallback.onButtonClickEvent(new ReportInappropriate(),EventMaker.SettingsFragment);
                break;

            case R.id.logout:
            mCallback.onButtonClickEvent(new Logout(),EventMaker.SettingsFragment);
            break;

        }
    }

   /* Remove full code
   private void callDefaultBrowser() {
        BaseActivity.setSaveTime0();
        BaseSampleActivity.isAppNotClose = true;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Const.SHADOW_TEST_LINK));// it's not ACTION_SEND it's ACTION_SENDTO
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setPackage("com.android.chrome");
        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            try {
                intent.setPackage(null);
                startActivity(Intent.createChooser(intent, "Choose One"));
            } catch (android.content.ActivityNotFoundException  e) {
                e.printStackTrace();
                Shadow.alertToast(resources.getString(R.string.no_browser));
            }
        }
    }*/

    private void showDeleteDailog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_TRADITIONAL);
        alertDialogBuilder.setTitle(R.string.delete_profile);
        alertDialogBuilder.setMessage(R.string.delete_all_data_detail);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(R.string.yes_delete, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                callApi(APIServiceAsyncTask.deleteAllData);
                arg0.dismiss();
            }
        });
        alertDialogBuilder.setNegativeButton(R.string.no_cancel,new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void callApi(ApiRequest apiService) {
        ApiCall mCall = new ApiCall(getActivity());
        mCall.setServiceTaskMapStrParams(apiService, null);
        mCall.execute((Void) null);
    }

    class ApiCall extends APIServiceAsyncTask {
        ApiCall(Context mContext){super(mContext);}

        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            //super.success(jsonObj, serviceTaskType);
            Logger.printLog(Logger.ERROR, "String " + jsonObj.toString());
            if(serviceTaskType == APIServiceAsyncTask.DELETE_ALL_DATA) {
                Shadow.editor.putBoolean(Const.IS_LOGIN, false).commit();
                SCCoreFacade.instance().unsubscribe(this);
                mCallback.onButtonClickEvent((Fragment) null, EventMaker.CloseApp);
            }else {//if(serviceTaskType == CALL_HISTORY_SEEN){

            }
        }
    }

    /*private Map<String, String> getLoginParams(){
        Map<String, String> params = new HashMap<>();
        params.put("user[username]", BaseActivity.getText(email) );
        params.put("user[password]", BaseActivity.getText(pass));
        return params;
    }*/

}
