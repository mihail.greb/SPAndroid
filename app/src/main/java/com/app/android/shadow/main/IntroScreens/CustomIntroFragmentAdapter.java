package com.app.android.shadow.main.IntroScreens;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;

public class CustomIntroFragmentAdapter extends FragmentStatePagerAdapter {

    private  Fragment[] fragmentArray;

    public CustomIntroFragmentAdapter(FragmentManager fm) {
        super(fm);
        fragmentArray = getFragmentResources();
    }

    @Override
    public int getCount() {
        return fragmentArray.length;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentArray[position];
    }

    private Fragment[] frag_resources_free;

    private Fragment[] getFragmentResources(){
            return frag_resources_free;
    }

}

