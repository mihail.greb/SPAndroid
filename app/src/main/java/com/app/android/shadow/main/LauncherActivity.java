package com.app.android.shadow.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.app.android.shadow.common.ui.core.ExtraData;
import com.app.android.shadow.R;
import com.app.android.shadow.common.ui.activities.LoginActivity;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.samplechat.SimpleChatMainActivity;

public class LauncherActivity extends BaseActivity implements View.OnClickListener {

    TextView loginBtn, registrationBtn,titleHead;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launcher_activity);

        titleHead = (TextView)findViewById(R.id.title_head);
        titleHead.setText(""+Shadow.getAppName());
        loginBtn = (TextView)findViewById(R.id.login);
        registrationBtn = (TextView)findViewById(R.id.register);
        loginBtn.setOnClickListener(this);
        registrationBtn.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Shadow.getAppPreferences().getBoolean(Const.IS_LOGIN,false)){
            Intent intent = new Intent(this, Shadow.getWayToAccessCls());
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        /*if(Shadow.getAppPreferences().getBoolean(Const.IS_LOGIN,false) && Shadow.isFirstRun){
            Intent intent = new Intent(this, LoginActivity.class);
            intent.putExtra(ExtraData.Login.MAIN_ACTIVITY, SimpleChatMainActivity.class);
            intent.putExtra(Const.IS_LOGIN, true);
            startActivity(intent);
            Shadow.isFirstRun = false;
        }*/
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(ExtraData.Login.MAIN_ACTIVITY, SimpleChatMainActivity.class);
        switch (v.getId()){
            case R.id.login:
                intent.putExtra(ExtraData.Login.PASS_LOGIN, false);
                break;
            case R.id.register:
                intent.putExtra(ExtraData.Login.PASS_LOGIN, true);
                //C2CallSdk.startControl().openRegister(LauncherActivity.this, null, 0, null, StartType.Activity);
                break;
        }
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        exittoApp();
    }
}
