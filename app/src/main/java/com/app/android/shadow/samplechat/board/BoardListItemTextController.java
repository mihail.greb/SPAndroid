package com.app.android.shadow.samplechat.board;

import android.view.View;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemTextController;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;

/**
 * Basic custom board item controller. This controls a single board list item that is showing a text message
 * Further customization of the controller's functionality should be done here
 */
public class BoardListItemTextController extends SCBoardListItemTextController
{
    public BoardListItemTextController(View view, SCViewDescription vd, SCBoardEventData data)
    {
        super(view, vd, data);
        Ln.d("c2capp", "BoardListItemTextController.<init>");
    }


    @Override
    public void onMainViewLongClick(View view)
    {
        /*
        This super call is needed to fire the SCBoardItemLongClickedEvent that may be caught by other components of the app
         */
        super.onMainViewLongClick(view);

        /*
        Show a simple context dialog for this item
         */
        SCChoiceDialog dlg = C2CallSdk.dialogFactory().createBoardItemDialog(this);
        dlg.show();
    }
}
