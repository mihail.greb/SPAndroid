package com.app.android.shadow.common.ui.controller;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.view.View;

import com.app.android.shadow.common.ui.fragments.ShadowContactDetailFragment;
import com.app.android.shadow.R;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.util.AlertDialogClass;
import com.app.android.shadow.main.util.Const;
//import com.c2call.lib.androidlog.Ln;
//import com.c2call.sdk.lib.util.extra.StringExtra;
//import com.c2call.lib.xml.StringExtra;
//import com.c2call.sdk.lib.n.g;
import com.c2call.sdk.pub.gui.contactdetail.controller.IContactDetailViewHodler;
import com.c2call.sdk.pub.gui.contactdetail.controller.SCContactDetailController;
/*import com.c2call.sdk.pub.gui.contactdetail.controller.SCContactDetailControllerFactory;
import com.c2call.sdk.pub.gui.core.controller.IPictureViewHolder;
import com.c2call.sdk.pub.gui.core.controller.SCBasePictureController;*/
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
/*import com.c2call.sdk.pub.storage.SCBitmapManager;*/


public class ContactDetailController extends SCContactDetailController {

    public ContactDetailController(View view, SCViewDescription viewDescription) {
        super(view, viewDescription);
    }
    @Override
    protected void onBindViewHolder(IContactDetailViewHodler vh) {
        super.onBindViewHolder(vh);
    }

    @Override
    public void onButtonCallClick(View v) {
        if(Shadow.shadowDB.isUserBlocked(getUserid())){
            new AlertDialogClass(getContext()).open(Const.PLEASE_UNBLOCK);
        }else if(ShadowContactDetailFragment.getLeftTimeInMs() <= 0) {
            new AlertDialogClass(getContext()).open(Const.TIME_LIMIT_OVER);
        }else {
                super.onButtonCallClick(v);
            }
    }

    @Override
    public void onButtonVideoCallClick(View v) {
        if(Shadow.shadowDB.isUserBlocked(getUserid())){
            new AlertDialogClass(getContext()).open(Const.PLEASE_UNBLOCK);
        }else if(ShadowContactDetailFragment.getLeftTimeInMs() == 0) {
            new AlertDialogClass(getContext()).open(Const.TIME_LIMIT_OVER);
        }else {
            super.onButtonVideoCallClick(v);
        }
    }

    @Override
    public void onButtonMessageClick(View v) {
        if(Shadow.shadowDB.isUserBlocked(getUserid())){
            new AlertDialogClass(getContext()).open(Const.PLEASE_UNBLOCK);
        }else {
            super.onButtonMessageClick(v);
        }
    }
   /*
   OLD SDK method

   @Override
    protected void onPictureLoaded(Bitmap bitmap, final String url) {
        Resources resources = Shadow.getAppContext().getResources();
        bitmap = Bitmap.createScaledBitmap(bitmap, ((int)resources.getDimension(R.dimen.image_width)), ((int)resources.getDimension(R.dimen.image_height)), true);
        if(StringExtra.isEqual(this._pictureUrl, url)) {
            Ln.d("fc_tmp", " BasePictureMediator--- no need to u.pdate picture - ", new Object[]{url});
        } else {
            final Bitmap finalBitmap = bitmap;
            this.getHandler().post(new Runnable() {
                public void run() {
                    if(ContactDetailController.this.getViewHolder() != null) {
                        ImageView picture = ((IPictureViewHolder)ContactDetailController.this.getViewHolder()).getItemPicture();
                        if(picture != null) {
                            picture.setVisibility(View.VISIBLE);
                            picture.setImageBitmap(finalBitmap);
                            ContactDetailController.this._pictureUrl = url;
                        }

                    }
                }
            });
        }
    }*/

    @Override
    protected void onPictureLoaded(Bitmap var1, final String var2) {
        Resources resources = Shadow.getAppContext().getResources();
        var1 = Bitmap.createScaledBitmap(var1, ((int)resources.getDimension(R.dimen.image_width)), ((int)resources.getDimension(R.dimen.image_height)), true);
        super.onPictureLoaded(var1,var2);
        /*if(am.a(this._pictureUrl, var2)) {
            Ln.d("fc_tmp", " BasePictureMediator--- no need to update picture - ", new Object[]{var2});
        } else {
            final Bitmap finalVar = var1;
            this.getHandler().post(new Runnable() {
                public void run() {
                    if(ContactDetailController.this.getViewHolder() != null) {
                        ImageView var1x = ((IPictureViewHolder)ContactDetailController.this.getViewHolder()).getItemPicture();
                        if(var1x != null) {
                            var1x.setVisibility(View.VISIBLE);
                            var1x.setImageBitmap(finalVar);
                            ContactDetailController.this._pictureUrl = var2;
                        }

                    }
                }
            });
        }*/
    }
}



