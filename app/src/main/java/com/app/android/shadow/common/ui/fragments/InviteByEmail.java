package com.app.android.shadow.common.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.shadow.main.util.AlertDialogClass;
import com.app.android.shadow.utils.Email;
import com.app.android.shadow.R;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.main.util.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Customized register fragment. In addition to the default SCRegisterFragment, this
 * class will automatically login the user after successful registration
 */

public class InviteByEmail extends BaseFragment{

    EditText mobileNumber;
    EditText enter_message;
    Button sendInvitaion;
    EditText emailId;
    boolean isEmail;
    TaskSearch mTask;

    private static final String IS_EMAIL = "isEmail";

    public static InviteByEmail create(boolean isEmail) {
        final InviteByEmail f = new InviteByEmail();
        Bundle bundle = new Bundle();
        bundle.putBoolean(IS_EMAIL,isEmail);
        f.setArguments(bundle);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle != null){
            isEmail = bundle.getBoolean(IS_EMAIL);
        }
    }

    //Header
    @Override
    protected void setHeader(View v) {
        super.setHeader(v);
        TextView title = (TextView) v.findViewById(R.id.title_head);
        title.setText((isEmail)?R.string.invite_friend_email:R.string.invite_friend_sms);

        TextView connect = (TextView) v.findViewById(R.id.done);
        connect.setText(R.string.send);

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendInvitationClicked();
            }
        });
    }

    //@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.shadow_invite_email_sms, container, false);

        emailId = (EditText) layout.findViewById(R.id.enter_email);
        emailId.setSelectAllOnFocus(true);
        enter_message = (EditText) layout.findViewById(R.id.enter_message);
        enter_message.setSelectAllOnFocus(true);
        sendInvitaion = (Button) layout.findViewById(R.id.sendInvitaion);
           /* if(isEmail){
                email_invitation_box = (LinearLayout)layout.findViewById(R.id.email_invitation_box);
                emailId = (EditText) layout.findViewById(R.id.enter_time);
            }else {
                sms_invitation_box = (LinearLayout) layout.findViewById(R.id.sms_invitation_box);
                mobileNumber = (EditText) layout.findViewById(R.id.enter_time);
            }*/


        if(!isEmail){
            emailId.setHint(R.string.enter_mobile_number);
            emailId.setInputType(InputType.TYPE_CLASS_PHONE |
                    InputType.TYPE_TEXT_VARIATION_PHONETIC);
        }

        enter_message.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_FLAG_MULTI_LINE |
                InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        enter_message.setText(getResources().getString(R.string.email_invitation_message,getShadowName()));

        //popup.setWindowLayoutMode(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        ImageView close = (ImageView) layout.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(emailId != null)
                    closeSoftKeyboard(emailId);
                else
                    closeSoftKeyboard(mobileNumber);

                closeSoftKeyboard(enter_message);
                getActivity().onBackPressed();
            }
        });

        sendInvitaion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendInvitationClicked();
            }
        });
        setHeader(layout);
        return layout;
    }

    private void sendInvitationClicked(){
        if(emailId != null && Email.isValidEmail(emailId.getText().toString().trim())) {
            if(emailId != null)
                closeSoftKeyboard(emailId);
            else
                closeSoftKeyboard(mobileNumber);

            closeSoftKeyboard(enter_message);
            getActivity().onBackPressed();
            if(isEmail) {
                callApiEmailInvitation(emailId.getText().toString().trim(), enter_message.getText().toString());
            }
        }else{
            Toast.makeText(getActivity(),resources.getString(R.string.enter_valid_email),Toast.LENGTH_LONG).show();
        }
    }

    private void callApiEmailInvitation(String email,String message) {
        mTask = new TaskSearch(getActivity());
        mTask.setServiceTaskMapStrParams(APIServiceAsyncTask.emailInvitations,
                getEmailParams(email,message));
        mTask.execute((Void) null);
    }


    @Override
    public void onResume() {
        super.onResume();
        openSoftKeyboard();
    }

    @Override
    public void onPause() {
        super.onPause();
        closeSoftKeyboard(mobileNumber);
        closeSoftKeyboard(emailId);
        closeSoftKeyboard(enter_message);
        closeSoftKeyboard();
    }

    private String getShadowName(){
        return Shadow.getAppPreferences().getString(Const.USERNAME,"");
    }

    class TaskSearch extends APIServiceAsyncTask {

        Context mContext;

        TaskSearch(Context mContext) {
            super(mContext);
            this.mContext = mContext;
        }

        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            try {
                if(serviceTaskType == APIServiceAsyncTask.INVITAION){
                    String suc = jsonObj.getString(Const.SUCCESS);
                    if (suc.equals("true")) {
                        String info = jsonObj.getString(Const.INFO);
                        if (info.equals("sent")) {
                            /*JSONArray user = jsonObj.getJSONArray(Const.USER);
                            userName = user.getJSONObject(0).getString(Const.USERNAME);*/
                            new AlertDialogClass(getActivity()).openNoAlert(info);

                        } else {
                            if(info != null) {
                                Logger.printLog(Logger.ERROR, "success", info);
                                new AlertDialogClass(mContext).openNoAlert(info);
                            }
                            // new AlertDialogClass(getActivity()).openNoAlert(info);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
            super.failure(jsonObj, serviceTaskType);
            try {
                final String info = jsonObj.getString(Const.INFO);
                new Thread(){
                    public void run(){
                        // Long time consuming operation
                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                new AlertDialogClass(getActivity()).open(info);
                            }
                        });
                    }
                }.start();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Shadow.editor.putBoolean(Const.FIRST_TIME_LOGGED, false).commit();
        }

        @Override
        protected void failure(String message) {
            Shadow.editor.putBoolean(Const.FIRST_TIME_LOGGED, false).commit();
        }
    }

    private Map<String, String> getEmailParams(String email,String message) {
        Map<String, String> params = new HashMap<>();
        params.put(Const.TO, email);
        params.put(Const.MESSAGE, message);
        params.put(Const.APP_NAME, Shadow.getAppCodeName());
        return params;
    }


}



