package com.app.android.shadow.common.ui.activities;

import android.app.Fragment;

import com.app.android.shadow.common.ui.fragments.AddFriendFragment;
import com.c2call.sdk.pub.activities.SCAddFriendFragmentActivity;

public class AddFriendActivity extends SCAddFriendFragmentActivity
{
    @Override
    protected Fragment onCreateFragment()
    {
        return AddFriendFragment.create(getIntent().getExtras());
    }

}
