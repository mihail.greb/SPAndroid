package com.app.android.shadow.main.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.app.android.shadow.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abc on 3/24/2015.
 */
public class MoreAdapter extends ArrayAdapter {

    Context mcontext;
    ArrayList<String > lst= new ArrayList<String>();
    Resources resources;
    Fragment fragment;

    public MoreAdapter(Context mcontext, List<String> objects) {
        super(mcontext, R.layout.shadow_more_row, objects );
        this.mcontext =mcontext;
        this.lst= (ArrayList<String>) objects;
        this.resources = mcontext.getResources();
    }

    @Override
    public int getCount() {
        return lst.size();
    }

    @Override
    public String getItem(int position) {
        return lst.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final String item = lst.get(position);
        TextView itemName;
                LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.shadow_more_row, parent, false);
                itemName = (TextView) convertView.findViewById(R.id.item_name);
                itemName.setText(item);
        return convertView;
    }
}
