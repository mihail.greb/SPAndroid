package com.app.android.shadow.main.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;

import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.R;

import java.util.concurrent.TimeUnit;

public class AddWaterMark {

    public static Bitmap waterMark(Bitmap src, String watermark, Point location, int color, int alpha, int size, boolean underline) {
        //get source image width and height
        int w = src.getWidth();
        int h = src.getHeight();
        int s = getSizeinPersent(h,w);
        Logger.printLog(Logger.ERROR,"size ","is "+s);
        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());
        //create canvas object
        Canvas canvas = new Canvas(result);
        //draw bitmap on canvas
        canvas.drawBitmap(src, 0, 0, null);
        //create paint object
        Paint paint = new Paint();
        //apply color
        paint.setColor(color);
        //set transparency
        paint.setAlpha(alpha);
        //set text size
        paint.setTextSize(s);
        paint.setAntiAlias(true);
        //set should be underlined or not
        paint.setUnderlineText(underline);
        //draw text on given location
        canvas.drawText(watermark, getXText(h,w), getYText(h,w), paint);
        canvas.drawBitmap(BitmapFactory.decodeResource(Shadow.getAppContext().getResources(), R.drawable.ic_sc_std_picture_image_src), getXImage(h,w), getYImage(h,w), paint);

        return result;
    }

    public static int getSizeinPersent(int h,int w){
        Logger.printLog(Logger.ERROR,"height",""+h);
        Logger.printLog(Logger.ERROR,"wight",""+w);
        return (h*6/100)+(w*6/100);
    }

    public static int getXText(int h,int w){
        return (h*10/100)+(w*10/100);
    }

    public static int getYText(int h,int w){
        return (h*7/100)+(w*7/100);
    }

    public static int getXImage(int h,int w){
        return (h*2/100)+(w*2/100);
    }
    //512 384
     public static int getYImage(int h,int w){
        return (h*28/100)+(w*28/100);
    }

    public static String getTimeDuration(Context mContext,String filePath){
        MediaPlayer mp = MediaPlayer.create(mContext, Uri.parse(filePath));
        long duration = mp.getDuration();
        mp.release();
        /*convert millis to appropriate time*/
        return String.format("%d min, %d sec",
                TimeUnit.MILLISECONDS.toMinutes(duration),
                TimeUnit.MILLISECONDS.toSeconds(duration) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
        );
    }

    /*
     How to pass point
       Point p=new Point();
       p.set(180, 1000);
     */

}
