package com.app.android.shadow.main.tapmanager;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.android.shadow.common.ui.fragments.BoardFragment;
import com.app.android.shadow.common.ui.shadow.custom.IChatBoardBaseController;
import com.app.android.shadow.common.ui.shadow.custom.SCChatBoardBaseController;
import com.app.android.shadow.samplechat.board.BoardChatInputFrgament;
import com.app.android.shadow.utils.Str;
import com.app.android.shadow.R;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.fragments.core.IFragmentCallbacks;
import com.c2call.sdk.pub.fragments.core.SCBaseFragment;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;


public class ChatBoardBaseFragment extends SCBaseFragment<IChatBoardBaseController, ChatBoardBaseFragment.Callbacks> {

    private String _userid;
    private View _view;
    private int layout;
    //Header
    TextView title;
    private void setHeader(View v) {
        title = (TextView) v.findViewById(R.id.title_head);
        //title.setText(getResources().getString(R.string.chat));
        v.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        String name = C2CallSdk.contactResolver().getDisplayNameByUserid(this._userid, true);
        this.setTitle(name);
    }

    public static ChatBoardBaseFragment create(String id){
        Bundle args = new Bundle();
        args.putString(SCExtraData.Board.EXTRA_DATA_USERID, id);
        args.putInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, R.layout.sc_board);
        ChatBoardBaseFragment fragment = new ChatBoardBaseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args != null){
            _userid = args.getString(SCExtraData.Board.EXTRA_DATA_USERID);
            layout = args.getInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, 0);
            if(layout == 0) {
                layout = R.layout.sc_board;
            }
        }
    }

    public void setTitle(String name) {
        title.setText(name);
    }

    public String getUserid() {
        return this._userid;
    }

    /*protected int getBoardPartListLayout() {
        return com.c2call.sdk.R.layout.sc_board_part_list;
    }

    protected int getBoardPartInputLayout() {
        return com.c2call.sdk.R.layout.sc_board_part_chatinput;
    }*/

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SCCoreFacade.instance().subscribe(this);
    }

    @Override
    public void onDestroy() {
        SCCoreFacade.instance().unsubscribe(this);
        super.onDestroy();
    }

    @Override
    protected IChatBoardBaseController onCreateController(View view, SCViewDescription scViewDescription) {
        return new SCChatBoardBaseController(view,scViewDescription);
    }

    @Override
    protected SCViewDescription onCreateViewDescription() {
        return C2CallSdk.instance().getVD().board();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        _view =inflater.inflate(this.layout, container, false);
        setHeader(_view);
        this.restoreFragment(savedInstanceState);
        return _view;
    }

    public void restoreFragment(Bundle savedInstanceState) {
        this.onInitFragments();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(this._view != null) {
            this._view.forceLayout();
        }
    }

    private void onInitFragments() {
        Fragment boardFragment = this.onCreateBoardFragment(this._userid);
        Fragment messageInputFragment = this.onCreateMessageInputFragment(this._userid);
        FragmentManager fragmentManager = this.getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if(boardFragment != null) {
            ft.replace(com.c2call.sdk.R.id.view_board_list_container, boardFragment);
        }

        if(messageInputFragment != null) {
            ft.replace(com.c2call.sdk.R.id.view_board_input_container, messageInputFragment);
        }

        ft.commit();
    }


    /**
     * Create a custom board fragment to show the chat list
     * @param userid the userid to filter the chat. If this is empty then every message from every friend should be shown
     * @return the new fragment
     */
    protected Fragment onCreateBoardFragment(final String userid)
    {
        Ln.d("c2app", "BoardActivity.onCreateBoardFragment() - userid: %s", userid);
        //return BoardFragment.create(userid, R.layout.sc_board_part_list);
        return BoardFragment.create(userid, R.layout.sc_board_part_list, getArguments());
    }


    /**
     * Creates a custom chat input fragment
     * @param userid the userid of the chat partner where the new messages should be send to.
     *               If this <p>userid</p> is null then no chat input fragment is created.
     * @return
     */
    protected Fragment onCreateMessageInputFragment(String userid)
    {
        Ln.d("c2app", "BoardActivity.onCreateMessageInputFragment() - userid: %s", userid);
        try {
            if (userid == null) {
                return null;
            }

            SCFriendData friend = null;
            if (Str.isPhonenumber(userid)) {
                friend = new SCFriendData(userid);
            }
            else {
                friend = SCFriendData.dao().queryForId(userid);
                if (friend == null) {
                    friend = new SCFriendData(userid);
                }
            }
            return BoardChatInputFrgament.create(friend, getArguments(), R.layout.sc_board_part_chatinput);
           // return BoardChatInputFrgament.create(friend, R.layout.sc_board_part_chatinput);
        }
        catch (final Exception e) {
            e.printStackTrace();
            throw new IllegalStateException("No friend found for userid: " + userid);
        }
    }

    public interface Callbacks extends IFragmentCallbacks {
    }




    //BaseFragmentData
    public boolean onBackPressed(){
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
      //  BaseSampleActivity.mCurrentFragment = BaseFragment.this;
    }
}
