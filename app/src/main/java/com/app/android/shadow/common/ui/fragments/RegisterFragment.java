package com.app.android.shadow.common.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.android.shadow.events.RegisterSuccessEvent;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.R;
import com.app.android.shadow.main.util.Const;
import com.c2call.sdk.pub.common.SCRegistrationData;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.fragments.SCRegisterFragment;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.register.controller.IRegisterController;
import com.c2call.sdk.pub.gui.register.controller.SCRegisterController;
import com.c2call.sdk.pub.util.SimpleAsyncTask;
import com.c2call.sdk.pub.util.Validate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Customized register fragment. In addition to the default SCRegisterFragment, this
 * class will automatically login the user after successful registration
 */

public class RegisterFragment extends SCRegisterFragment implements TextWatcher
{
    String emailId = "";
    boolean _isValid;
    EditText uNameEt,phNumberEt,emaiEt,emaiEtVisible,passEt,passReEt;
    EditText uFirstName,uLaseName,uCountry;
    Spinner countryCodeEt;
    Button uButton;
    ImageView backBtn;
   /* private IValidator _uNameValidator;
    private IValidator phNumberValidator;
    private IValidator _passwordValidator;
    private IValidator _emailValidator;
    private IValidator _retypePasswordValidator;*/

    public static RegisterFragment create(Bundle args)
    {
        final RegisterFragment f = new RegisterFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        View v = inflater.inflate(R.layout.sc_register, (ViewGroup) null);

        uNameEt = (EditText) v.findViewById(R.id.sc_register_username);
        phNumberEt = (EditText) v.findViewById(R.id.sc_register_phone_number);
        emaiEt = (EditText) v.findViewById(R.id.sc_register_edit_email);
        emaiEtVisible = (EditText) v.findViewById(R.id.sc_register_edit_email_visible);
        passEt = (EditText) v.findViewById(R.id.sc_register_edit_pass);
        passReEt = (EditText) v.findViewById(R.id.sc_register_edit_pass_retype);

        countryCodeEt = (Spinner) v.findViewById(R.id.ff_country_code);
        AddFriendFragment.setCountryCodeList(countryCodeEt, getActivity());

        uFirstName = (EditText) v.findViewById(R.id.sc_register_edit_firstname);
        uLaseName = (EditText) v.findViewById(R.id.sc_register_edit_lastname);
        uCountry = (EditText) v.findViewById(R.id.sc_register_choose_country);
        uButton = (Button) v.findViewById(R.id.sc_register_btn_submit);
        backBtn = (ImageView) v.findViewById(R.id.back);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        passReEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.action_register || id == EditorInfo.IME_NULL) {
                    uButton.performClick();
                    closeSoftKeyboard(passReEt);
                    return true;
                }
                return false;
            }
        });
      return v;
    }

    public void reSetAllEt(){
        uNameEt.setText("");
        phNumberEt.setText("");
        emaiEt.setText("");
        emaiEtVisible.setText("");
        passEt.setText("");
        passReEt.setText("");
        uFirstName.setText("");
        uLaseName.setText("");
        uCountry.setText("");
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (uNameEt.getText().hashCode() == s.hashCode()){
            if(uNameEt.getText().toString().length()> 0)
                uNameEt.setError(null);
        }else  if (phNumberEt.getText().hashCode() == s.hashCode()) {
            if(phNumberEt.getText().toString().length()>0)
                phNumberEt.setError(null);
        }else  if (passEt.getText().hashCode() == s.hashCode()) {
            if(passEt.getText().toString().length()>0)
                passEt.setError(null);
        }else  if (passReEt.getText().hashCode() == s.hashCode()) {
            if(passReEt.getText().toString().length()>0)
                passReEt.setError(null);
        }else  if (emaiEtVisible.getText().hashCode() == s.hashCode()) {
            if(emaiEtVisible.getText().toString().length()>0)
                emaiEtVisible.setError(null);
        }
    }



    @Override
    protected IRegisterController onCreateController(View v, SCViewDescription vd)
    {
        final SCRegistrationData data = getArguments().getParcelable(SCExtraData.Register.EXTRA_DATA_REGISTRATION);

        return new SCRegisterController(v, vd, data)
        {
                View v;

            @Override
             public boolean onValidateInput() {
                if(this.getViewHolder() == null) {
                    return false;
                } else {
                    boolean isValid = true;
                    if(isValid) {
                        this.onInputValid();
                    } else {
                        this.onInputInvalid();
                    }

                    boolean validityChanged = _isValid != isValid;
                    _isValid = isValid;
                    if(validityChanged) {
                        this.onInputValidityChanged();
                    }
                    return isValid;
                }
            }

            @Override
            public void onButtonRegisterClicked(View v) {
                if (uNameEt.getText().toString().length() > 0) {
                if (!uNameEt.getText().toString().contains(" ")) {
                    int phLength = phNumberEt.getText().toString().length();
                    if (phLength > 9 || phLength == 0) {
                        if (passEt.getText().toString().length() > 7) {
                            if (passEt.getText().toString().equals(passReEt.getText().toString())) {
                                if (emaiEtVisible.getText().toString().length() <= 0){
                                    emaiEt.setText(uNameEt.getText().toString() + "@yopmail.com");
                                }else {
                                    emaiEt.setText(emaiEtVisible.getText().toString());
                                }
                                //if(BaseActivity.isValid(emaiEt.getText().toString()) ) {
                                        this.v = v;
                                        callApi();
                                        uFirstName.setText(uNameEt.getText().toString());
                                        uLaseName.setText("    ");
                                        //uCountry.setText("India");
                                //} else
                                  //  emaiEtVisible.setError("Enter valid email or left empty");
                            } else
                                passReEt.setError(getActivity().getResources().getString(R.string.password_not_match));
                        } else
                            passReEt.setError(getActivity().getResources().getString(R.string.password_too_short));

                    } else
                        phNumberEt.setError("Please Enter valid phone number or left empty");
                } else
                    uNameEt.setError("Shadow Name cannot have spaces");
                } else
                    uNameEt.setError("Please Enter user name");
            }

            public void customOnButtonRegisterClicked() {
                super.onButtonRegisterClicked(v);
                BaseActivity.setSaveTime0();
            }

            private void callApi(){
                UserSignUpTask mSignUpTask = new UserSignUpTask(getActivity());
                mSignUpTask.setServiceTaskMapStrParams(APIServiceAsyncTask.registration, getRegistrationParams(APIServiceAsyncTask.REGISTRATION));
                mSignUpTask.execute((Void) null);
            }


             class UserSignUpTask extends APIServiceAsyncTask {
                UserSignUpTask(Context mContext){
                    super(mContext);
                }

                 @Override
                 protected void success(JSONObject jsonObj, int serviceTaskType) {
                     if (serviceTaskType == APIServiceAsyncTask.REGISTRATION){
                         String userName = "";
                     try {
                         JSONObject data = jsonObj.getJSONObject(Const.DATA);
                         JSONObject user = data.getJSONObject(Const.USER);

                         try {
                             //TODO uncommit to maintain first time logs by API
                             //Shadow.editor.putBoolean(Const.FIRST_TIME_LOGGED, data.getBoolean(Const.FIRST_TIME_LOGGED)).commit();
                         } catch (Exception e) {
                             e.printStackTrace();
                         }

                         Shadow.editor.putString(Const.AUTH_TOKEN, data.getString(Const.AUTH_TOKEN)).commit();
                         Shadow.editor.putString(Const.ID, user.getString(Const.ID)).commit();
                         userName = user.getString(Const.USERNAME);
                         Logger.printLog(Logger.ERROR,"User is2", "is " + userName);
                         Shadow.editor.putString(Const.USERNAME, userName).commit();

                         try {
                             String phone = user.getString(Const.PHONE);
                             if (phone != null)
                                 Shadow.editor.putString(Const.PHONE, phone).commit();
                             else
                                 Shadow.editor.putString(Const.PHONE, "").commit();
                         } catch (JSONException e) {
                             e.printStackTrace();
                         }

                         String email = user.getString(Const.EMAIL);
                         Shadow.editor.putString(Const.EMAIL, email).commit();

                       /*  uFirstName.setText("test221");//
                         uLaseName.setText("");
                         emaiEt.setText("test2211455541959@dandesign.dk");*/

                         emaiEt.setText(email);
                         Shadow.editor.putString(Const.DISPLAY_EMAIL, user.getString(Const.DISPLAY_EMAIL)).commit();
                         Shadow.editor.putString(Const.SHADOW_ID, user.getString(Const.SHADOW_ID)).commit();
                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                     showProgress(false);
                     if (userName.length() > 0) {
                         uFirstName.setText(userName);
                         customOnButtonRegisterClicked();
                     } else {
                         Shadow.alertToast(getResources().getString(R.string.some_went_wrong));
                         finish();
                     }
                 }else if (serviceTaskType == APIServiceAsyncTask.DELETE_ALL_DATA){
                         Shadow.alertToast(getResources().getString(R.string.some_went_wrong));
                         reSetAllEt();
                     }
                 }

                 @Override
                 protected void failure(String message) {

                     String s ="";
                     try {
                         JSONArray msgArray = new JSONArray(message);
                         for(int i=0;i<msgArray.length();i++){
                             s +=msgArray.getString(i).toString()+",";
                             Logger.printLog(Logger.ERROR,"s is "+s);
                         }
                         if(s.length()>0) {
                             s = s.substring(0,(s.length()-1));
                             super.failure(s);
                         }else {
                             super.failure(getActivity().getResources().getString(R.string.please_try_another_user));
                         }
                     }catch (Exception e){
                         e.printStackTrace();
                         super.failure(getActivity().getResources().getString(R.string.please_try_another_user));
                     }
                 }

                 @Override
                 protected void failure(JSONObject jsonObj, int serviceTaskType) {
                     super.failure(jsonObj, serviceTaskType);
                 }
             }


            @Override
            public void onRegisterSuccess()
            {
                Logger.printLog(Logger.ERROR,"Login Success","check success"+getData());
                login(getData());
            }

            @Override
            public void onRegisterFailed(int error) {
                Logger.printLog(Logger.ERROR,"Pring error ", "failed " + error);
                super.onRegisterFailed(error);
                callApiRegFailed();
            }

            public void callApiRegFailed(){
                UserSignUpTask mSignUpTask = new UserSignUpTask(getActivity());
                mSignUpTask.setServiceTaskMapStrParams(APIServiceAsyncTask.deleteAllData, null);
                mSignUpTask.execute((Void) null);
            }

            private void login(final SCRegistrationData data){
                Validate.notNull(data, "registration data must not be null");
                Validate.notEmpty(data.getEmail(), "user name must not be empty");
                Validate.notEmpty(data.getPassword(), "password must not be empty");
                Logger.printLog(Logger.ERROR,"print working", "" + data.getEmail() + " " + data.getPassword());
                new SimpleAsyncTask<Boolean>(getActivity(), 0, null, getString(R.string.sc_msg_unknown_error))
                {
                    @Override
                    protected Boolean doInBackground(Void... voids)
                    {
                        Logger.printLog(Logger.ERROR,"print credentialse",""+data.getEmail()+" "+data.getPassword());
                        Shadow.editor.putString(Const.PASSWORD,data.getPassword()).commit();
                        int result = 0;
                        try {
                            result = SCCoreFacade.instance().login(data.getEmail(), data.getPassword(), false);
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                        Logger.printLog(Logger.ERROR,"print result","is "+result);
                        return result == 0;
                    }

                    @Override
                    protected void onSuccess(Boolean result)
                    {
                        Logger.printLog(Logger.ERROR,"Login Success", "check success " + result);
                        try {
                            SCCoreFacade.instance().postEvent(new RegisterSuccessEvent(data), false);
                            closeSoftKeyboard();
                            closeSoftKeyboard(uNameEt);
                            closeSoftKeyboard(phNumberEt);
                            closeSoftKeyboard(passEt);
                            closeSoftKeyboard(passReEt);
                            closeSoftKeyboard(emaiEtVisible);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    protected void onFailure() {
                        Logger.printLog(Logger.ERROR,"Login Success", "check failed");
                        super.onFailure();
                        callApiRegFailed();
                    }

                    @Override
                    protected void onCancelled(Boolean aBoolean) {
                        Logger.printLog(Logger.ERROR,"Login Success", "check cancel " + aBoolean);
                        super.onCancelled(aBoolean);
                    }

                }.execute();
            }
        };

    }




    private Map<String, String> getRegistrationParams(int type){
        Map<String, String> params = new HashMap<>();
        if(type == APIServiceAsyncTask.REGISTRATION) {
            Logger.printLog(Logger.ERROR,"print full number", countryCodeEt.getSelectedItem().toString() + BaseActivity.getText(phNumberEt));
            params.put("username", BaseActivity.getText(uNameEt));
            params.put("password", BaseActivity.getText(passEt));
            String pnumber = countryCodeEt.getSelectedItem().toString() + BaseActivity.getText(phNumberEt);
            if (pnumber.length() > 5)
                params.put("number", pnumber);
            else
                params.put("number", "");
            // params.put("email", BaseActivity.getText(emaiEt));
            params.put("display_email", BaseActivity.getText(emaiEtVisible));
            params.put("password_confirmation", BaseActivity.getText(passReEt));
            params.put("app_name", Shadow.getAppCodeName().toString());
            if (!Shadow.isProApp()) {
                params.put("device_id", getDeviceid());
            }
        }else if(type == APIServiceAsyncTask.UNREGISTRATION) {
            params.put("username", BaseActivity.getText(uNameEt));
            params.put("display_email", BaseActivity.getText(emaiEtVisible));
        }
        return params;
    }

    private String getDeviceid(){
         String android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        //Shadow.alertToast("android is is "+android_id);
        return android_id;
    }

    public void closeSoftKeyboard(){
        try {
            InputMethodManager imm = (InputMethodManager)
                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeSoftKeyboard(View et){
        try {
            InputMethodManager imm = (InputMethodManager)
                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
