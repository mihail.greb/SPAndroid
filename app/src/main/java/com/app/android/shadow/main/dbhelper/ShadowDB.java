package com.app.android.shadow.main.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.game.alarm.database.AlarmDatabase;
import com.app.android.shadow.main.provider.CustomC2CallContentProvider;
import com.app.android.shadow.main.tapmanager.settings.MoreSignature;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.main.util.Logger;
import com.c2call.sdk.pub.db.util.core.DatabaseHelper;

import java.util.HashMap;
import java.util.Map;

public class ShadowDB extends AlarmDatabase {

    Context context;
    private static int VERSION = 1;
    private static String DB_NAME = "shadowDB";


    public ShadowDB(Context context ) {
        super(context, DB_NAME, null, VERSION);
        this.context= context;
        Logger.printLog(Logger.ERROR,"working", "database");
    }

    private static String TABLE_RINGTONE="shadow_ringtone";
    private static String TABLE_AUDIO_RINGTONE="shadow_audio_ring";
    private static String TABLE_VIDEO_RINGTONE="shadow_video_ring";
    private static String TABLE_MESSAGE_RINGTONE="shadow_message_ring";
    private static String TABLE_MODE="shadow_mode";
    private static String TABLE_BLOCK_USERS="shadow_block_user";

    public static final String UNIQUE_ID="unique_id";
    public static final String BLOCK_BY="block_by";
    public static final String BLOCK_TO="block_to";
    public static final String ASSIGN_BY ="assign_by";
    public static final String ASSIGN_TO ="assign_to";
    public static final String RINGTONE_PATH="ringtone_path";
    public static final String TYPE="type";

    public static final String IS_SAFE_MODE ="is_safe_mode";
    public static final String VIBRATE_FOR_CALL="vibrate_for_call";
    public static final String VIBRATE_FOR_CHAT="vibrate_for_chat";
    public static final String SHAKE_LOGOUT="shake_logout";
    public static final String LOGOUT_TIME="logout_time";
    public static final String ONLINE_STATUS="online_status";
    public static final String IS_ENTER_NAME_PASS="is_enter_name_pass";
    public static final String IS_SWIPE_X_TIMES="is_swipe_x_times";
    public static final String IS_TAB_X_TIMES="is_tab_x_times";
    public static final String UN_COVER_MESSAGE="cover_message_unsafe";
    public static final String UN_RINGTONE_FOR_CALL="un_ringtone_for_call";
    public static final String UN_VIBRATE_ON_CALL="un_vibrate_for_call";
    public static final String UN_SOUND_ON_CHAT="un_sound_for_chat";
    public static final String UN_VIBRATE_ON_CHAT="un_vibrate_for_chat";
    public static final String UN_IS_ENTER_NAME_PASS="un_is_enter_name_pass";
    public static final String UN_IS_PASS_ON_X_SWIPE ="un_is_pass_on_x_swipe";
    public static final String UN_IS_PASS_ON_X_SWIPE_FREE ="un_is_pass_on_x_swipe_free";
    public static final String UN_IS_PASS_ON_X_TABS ="un_is_pass_on_x_tabs";
    public static final String UN_IS_PASS_ON_X_TABS_FREE ="un_is_pass_on_x_tabs_free";
    public static final String UN_SHAKE_LOGOUT="un_shake_logout";
    public static final String UN_LOGOUT_TIME="un_logout_time";
    public static final String UN_ONLINE_STATUS="un_online_status";

    @Override
    public void onCreate(SQLiteDatabase db) {
        //super.onCreate(db, cs);
        Logger.printLog(Logger.ERROR,"working", "database2");

        String CREATE_RINGTONE = "CREATE TABLE "
                + TABLE_RINGTONE + "(" + Const._ID+ " INTEGER PRIMARY KEY AUTOINCREMENT,"+ ASSIGN_TO + " TEXT,"
                + ASSIGN_BY+ " TEXT," + TYPE+ " TEXT," + RINGTONE_PATH + " TEXT" + ")";

        /*In integer 0 to true and 1 to false*/
        String CREATE_MODE = "CREATE TABLE "
                + TABLE_MODE + "(" + Const.USER + " TEXT PRIMARY KEY,"
                + IS_SAFE_MODE + " INTEGER,"
                + VIBRATE_FOR_CALL + " INTEGER,"
                + VIBRATE_FOR_CHAT + " INTEGER,"
                + SHAKE_LOGOUT + " INTEGER,"
                + LOGOUT_TIME + " INTEGER,"
                + IS_ENTER_NAME_PASS + " INTEGER,"
                + IS_TAB_X_TIMES + " INTEGER,"
                + IS_SWIPE_X_TIMES + " INTEGER,"
                + ONLINE_STATUS + " INTEGER,"
                + UN_COVER_MESSAGE + " INTEGER,"
                + UN_RINGTONE_FOR_CALL + " INTEGER,"
                + UN_VIBRATE_ON_CALL + " INTEGER,"
                + UN_SOUND_ON_CHAT + " INTEGER,"
                + UN_VIBRATE_ON_CHAT + " INTEGER,"
                + UN_SHAKE_LOGOUT + " INTEGER,"
                + UN_IS_ENTER_NAME_PASS + " INTEGER,"
                + UN_IS_PASS_ON_X_SWIPE + " INTEGER,"
                + UN_IS_PASS_ON_X_SWIPE_FREE + " INTEGER,"
                + UN_IS_PASS_ON_X_TABS + " INTEGER,"
                + UN_IS_PASS_ON_X_TABS_FREE + " INTEGER,"
                + UN_LOGOUT_TIME + " INTEGER,"
                + UN_ONLINE_STATUS + " INTEGER" + ")";

        String CREATE_BLOCK_USER = "CREATE TABLE "
                + TABLE_BLOCK_USERS + "(" + UNIQUE_ID+ " TEXT PRIMARY KEY,"+ BLOCK_TO + " TEXT,"
                + BLOCK_BY + " TEXT" + ")";

        String CREATE_ALARM_TABLE = "CREATE TABLE IF NOT EXISTS " + ALARM_TABLE + " ( "
                + COLUMN_ALARM_ID + " INTEGER primary key autoincrement, "
                + COLUMN_ALARM_ACTIVE + " INTEGER NOT NULL, "
                + COLUMN_ALARM_TIME + " TEXT NOT NULL, "
                + COLUMN_ALARM_DAYS + " BLOB NOT NULL, "
                + COLUMN_ALARM_DIFFICULTY + " INTEGER NOT NULL, "
                + COLUMN_ALARM_TONE + " TEXT NOT NULL, "
                + COLUMN_ALARM_VIBRATE + " INTEGER NOT NULL, "
                + COLUMN_ALARM_NAME + " TEXT NOT NULL)";

        db.execSQL(CREATE_RINGTONE);
        db.execSQL(CREATE_MODE);
        db.execSQL(CREATE_BLOCK_USER);
        db.execSQL(CREATE_ALARM_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //super.onUpgrade(db,cs,oldVersion,newVersion);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RINGTONE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MODE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BLOCK_USERS);
        db.execSQL("DROP TABLE IF EXISTS " + ALARM_TABLE);
        // create new tables
        onCreate(db);
    }

    public void callReadable(){
        SQLiteDatabase db = this.getReadableDatabase();
        db.close();
    }


    public void updateModeData(String key, int data){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(key, data);
        db.update(TABLE_MODE, values, Const.USER + "='" + getUser() + "'", null);
        //db.close();
        DatabaseHelper.instance(context);
    }

    public void initializeModeData(HashMap<String,String> map){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        for (Map.Entry<String, String > entry : map.entrySet()) {
            if(entry.getKey().equals(Const.USER)) {
                Logger.printLog(Logger.ERROR,"Key  "+entry.getKey()+" value  "+entry.getValue());
                values.put(entry.getKey(), entry.getValue().toString());
            }else {
                Logger.printLog(Logger.ERROR,"Key  "+entry.getKey()+" value  "+entry.getValue());
                values.put(entry.getKey(), Integer.parseInt(entry.getValue().toString()));
            }
        }
        db.insert(TABLE_MODE, null, values);
        //db.close();
        DatabaseHelper.instance(context);
    }

    public boolean isModeUserExist(){
        String Query = "Select * from " + TABLE_MODE + " where " + Const.USER + " = '" + getUser()+"'";
        Logger.printLog(Logger.ERROR,"is Block Query", "" + Query);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            //db.close();
            DatabaseHelper.instance(context);
            return false;
        }
        cursor.close();
        //db.close();
        DatabaseHelper.instance(context);
        return true;
    }

    public boolean getModeState(String key){
        return (getModeData(key) == 0)?true:false;
    }

    public int getModeData(String key){

        String Query = "Select "+key+" from " + TABLE_MODE + " where " + Const.USER+ " = '" + getUser()+"'";
        Logger.printLog(Logger.ERROR,"getRing Query", "" + Query);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(Query, null);
        Logger.printLog(Logger.ERROR,"get mode count", "" + cursor.getCount());
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    int curIndex = cursor.getColumnIndex(key);
                    String result = cursor.getString(curIndex);
                    cursor.close();
                    //db.close();
                    return Integer.parseInt(result);
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        //db.close();
        DatabaseHelper.instance(context);
        return -1;
    }


    public String getRingTone(String userId, MoreSignature type, boolean isName){
        Logger.printLog(Logger.ERROR,"user id",""+userId);
        String userName = (!isName)? CustomC2CallContentProvider.customeC2callDB.getUserName(userId):userId;
        Logger.printLog(Logger.ERROR,"user name",""+userName);
        String Query = "Select "+RINGTONE_PATH+" from " + TABLE_RINGTONE + " where " + ASSIGN_TO+ " = '" + userName+ "' AND "+ ASSIGN_BY + " = '" + Shadow.getAppPreferences().getString(Const.USERNAME, "")+"' AND "+ TYPE + " = '"+ getType(type)+"'";
        Logger.printLog(Logger.ERROR,"getRing Query", "" + Query);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(Query, null);
        Logger.printLog(Logger.ERROR,"getRing path", "" + cursor.getCount());
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    int curIndex = cursor.getColumnIndex(RINGTONE_PATH);
                    Logger.printLog(Logger.ERROR,"curIndex ","si "+curIndex);
                    String path = cursor.getString(curIndex);
                    Logger.printLog(Logger.ERROR,"getRing path", "" + path);
                    cursor.close();
                    //db.close();
                    return path;
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        //db.close();
        DatabaseHelper.instance(context);
        return null;
    }

    /*reset By Delete*/
    public void resetRingtone(String userId,MoreSignature type,boolean isName){
        String userName = (!isName)?CustomC2CallContentProvider.customeC2callDB.getUserName(userId):userId;
        Logger.printLog(Logger.ERROR,"delete user","is "+userName);
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM "+ TABLE_RINGTONE + " where " + ASSIGN_TO
                + " = '" + userName+ "' AND "+ ASSIGN_BY + " = '" +
                Shadow.getAppPreferences().getString(Const.USERNAME, "")+"' AND "+ TYPE + " = '"
                + getType(type)+"'";
        Log.d("query", deleteQuery);
        db.execSQL(deleteQuery);
        //db.close();
        DatabaseHelper.instance(context);
    }

    public void addRingtone(String userId,String path,MoreSignature type,boolean isName){
        resetRingtone(userId,type,isName);
        String userName = (!isName)?CustomC2CallContentProvider.customeC2callDB.getUserName(userId):userId;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        String mainUser = Shadow.getAppPreferences().getString(Const.USERNAME, "");
        values.put(ASSIGN_TO, userName);
        values.put(ASSIGN_BY, mainUser);
        values.put(RINGTONE_PATH, path);
        values.put(TYPE, getType(type));
        db.insert(TABLE_RINGTONE, null, values);
        //db.close();
        DatabaseHelper.instance(context);
    }

    public boolean isUserBlocked(String userId){
        String userName= CustomC2CallContentProvider.customeC2callDB.getUserName(userId);
       return isUserBlockedByName(userName);
    }

    public boolean isUserBlockedByName(String userName){
        try {
            String Query = "Select * from " + TABLE_BLOCK_USERS + " where " + BLOCK_TO + " = '" + userName+ "' AND "+ BLOCK_BY + " = '" + Shadow.getAppPreferences().getString(Const.USERNAME, "")+"'";
            Logger.printLog(Logger.ERROR,"is Block Query", "" + Query);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(Query, null);
            int curSize = cursor.getCount();
            if(curSize <= 0){
                Logger.printLog(Logger.ERROR,"print cur size ",""+curSize);

             /*   try {
                    cursor.moveToFirst();
                    for(int i=0;i<curSize;i++) {
                        String id = cursor.getString(0);
                        String blockTo = cursor.getString(1);
                        String blockBy = cursor.getString(2);
                        Logger.printLog(Logger.ERROR,"id  is "," "+id+" block to "+blockTo +"  block by "+blockBy);
                        cursor.moveToNext();
                    }
                    cursor.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Logger.printLog(Logger.ERROR,"print cur size ",""+curSize);*/
                //db.close();
                cursor.close();
                return false;
            }
            cursor.close();
            //db.close();
            DatabaseHelper.instance(context);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void addBlockUser(String userId,String userName){
        if(userName == null) {
            userName = CustomC2CallContentProvider.customeC2callDB.getUserName(userId);
        }
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        String mainUser = Shadow.getAppPreferences().getString(Const.USERNAME, "");
        values.put(UNIQUE_ID, userName+"_"+mainUser);
        values.put(BLOCK_TO, userName);
        values.put(BLOCK_BY, mainUser);
        try {
            db.insert(TABLE_BLOCK_USERS, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //db.close();
        DatabaseHelper.instance(context);
    }


    public void deleteBlockUser(String userId){
       // CustomC2CallContentProvider.customeC2callDB.deleteBlockUserDatabase(userId);
        String userName= CustomC2CallContentProvider.customeC2callDB.getUserName(userId);
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteQuery = "DELETE FROM "+TABLE_BLOCK_USERS+" where "+BLOCK_TO+"= '"+ userName + "' AND "+ BLOCK_BY + " = '" + Shadow.getAppPreferences().getString(Const.USERNAME, "")+"'";;
        Log.d("query", deleteQuery);
        db.execSQL(deleteQuery);
        //db.close();
        DatabaseHelper.instance(context);
    }

    private String getType(MoreSignature type){
        switch (type){
            case AudioRingtone:
                return TABLE_AUDIO_RINGTONE;
            case VideoRingtone:
                return TABLE_VIDEO_RINGTONE;
            case MessageRingtone:
                return TABLE_MESSAGE_RINGTONE;
        }
        return null;
    }

    public static String getUser(){
        return Shadow.getAppPreferences().getString(Const.USERNAME, "");
    }

}


//private static int VERSION = 32;
//public static String AUTHORITY;
// private static final Class[] CLASSES = new Class[]{SCGroupInfoData.class, SCFriendData.class, SCPhoneData.class, SCFriendExtraData.class, SCOpenIdData.class, SCBoardEventData.class, SCDirtyFriendData.class, SCMediaData.class, SCBoardEventExtraData.class, SCMissedCallData.class, SCMissedCallLookupData.class, SCFavoriteData.class, SCUserData.class, SCDidExt.class, SCSecureMessageData.class};

//private static String lastAccount = GlobalPrefs.instance().getString("sc_login_email", (String)null);

// private static String DB_NAME = "c2call_" + lastAccount + ".sqlite";
//private static String DB_NAME = "c2call_www@yopmail.com.sqlite";
