package com.app.android.shadow.main.services;

//public class TimeService extends Service {
   /* // constant
    public static final long NOTIFY_INTERVAL = 10 * 1000; // 10 seconds

    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        // cancel if already existed
        if (mTimer != null) {
            mTimer.cancel();
        } else {
            // recreate new
            mTimer = new Timer();
        }
        // schedule task
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, NOTIFY_INTERVAL);
    }

    class TimeDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    // display toast
                    Toast.makeText(getApplicationContext(), getDateTime(),
                            Toast.LENGTH_SHORT).show();
                }

            });
        }

        private String getDateTime() {
            // get date time in custom format
            SimpleDateFormat sdf = new SimpleDateFormat("[yyyy/MM/dd - HH:mm:ss]");
            return sdf.format(new Date());
        }

    }*/

   /* @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    boolean isSafeMode;
    Timer timer = null;
    long lastTime = 0;
    int time;
    TimerTask timerTask;
    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();

    void initAutoLogoutTimer(){
        isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
        time = (Modes.getLogoutTime((isSafeMode) ? ShadowDB.LOGOUT_TIME : ShadowDB.UN_LOGOUT_TIME));
        if(time == (-1)){
            time = 2;
        }
        resetAutoLogoutTimer();
    }

    void resetLastTouchTime(){
        lastTime = System.currentTimeMillis();
    }

    boolean isAutoLogout(){
        boolean isNeedToLogout = (lastTime+1000) < System.currentTimeMillis();
        return isNeedToLogout;
    }

    void resetAutoLogoutTimer(){
        if(isAutoLogout()) {
            resetLastTouchTime();
            if (timer == null) {
                resetTimer();
            } else {
                try {
                    timer.cancel();
                    timer = null;
                    resetTimer();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        gotoReenterScreen();
                    }
                });
            }
        };
    }

    public void gotoReenterScreen(){
    BaseActivity.setSaveTime0();
      *//*  BaseSampleActivity.isAppNotClose = true;
        Intent intent = new Intent(BoardActivity.this, (Shadow.isProApp() ? Shadow.getProActivity() : FreeAppWayToAccess.class));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();*//*
    }

    void resetTimer(){
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, ((long) time * 60 * 1000));
    }
    public void removeTimer() {
        try {
            if(timer != null){
                timer.cancel();
                timer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}*/