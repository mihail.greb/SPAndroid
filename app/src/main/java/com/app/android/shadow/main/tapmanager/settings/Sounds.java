package com.app.android.shadow.main.tapmanager.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.R;
import com.app.android.shadow.main.util.Const;


/**
 * Custom Board Fragment to show a chat history grouped by users
 */
public class Sounds extends BaseFragment implements View.OnClickListener
{
    Bundle bundle;
    String tabTag;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view =inflater.inflate(R.layout.shadow_more_sound, container, false);
        setHeader(_view);
        bundle = getArguments();
        if(bundle == null) {
               bundle = new Bundle();
            bundle.putString(Const.TAGS, Const.SETTINGS);
            //SimpleChatMainActivity.mCurrentTab = Const.ORE;
            tabTag = Const.SETTINGS;
        }else {
            tabTag = bundle.getString(Const.TAGS);
            //SimpleChatMainActivity.mCurrentTab = bundle.getString(Const.TAGS);
        }
        _view.findViewById(R.id.audio_ringtone).setOnClickListener(this);
        _view.findViewById(R.id.video_ringtone).setOnClickListener(this);
        _view.findViewById(R.id.message_ringtone).setOnClickListener(this);
        return _view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.audio_ringtone:
                bundle.putSerializable(Const.VALUE, MoreSignature.AudioRingtone);
                break;
            case R.id.video_ringtone:
                bundle.putSerializable(Const.VALUE, MoreSignature.VideoRingtone);
                break;
            case R.id.message_ringtone:
                bundle.putSerializable(Const.VALUE, MoreSignature.MessageRingtone);
                break;
        }

        SoundSettings f = new SoundSettings();
        f.setArguments(bundle);
        mCallback.onButtonClickEvent(f, tabTag.equals(Const.SETTINGS) ? EventMaker.SettingsFragment : EventMaker.Friends);
    }

    //Header
    TextView title;
    @Override
    protected void setHeader(View v) {
        super.setHeader(v);
        title = (TextView) v.findViewById(R.id.title_head);
        title.setText(resources.getString(R.string.sounds));
    }

}
