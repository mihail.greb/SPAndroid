package com.app.android.shadow.samplechat.board;

import com.c2call.lib.androidlog.Ln;
//import com.c2call.sdk.lib.util.extra.StringExtra;
import com.c2call.lib.xml.StringExtra;
import com.c2call.sdk.pub.common.SCBoardEventType;
import com.c2call.sdk.pub.db.data.SCBoardEventData;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by rails-dev on 16/10/15.
 */
public class ShadowNewMessageCache {
    private static ShadowNewMessageCache __instance = new ShadowNewMessageCache();
    private Map<ShadowNewMessageCache.UseridAndLine, Set<String>> _useridAndLineCache = new ConcurrentHashMap();
    private Map<String, Set<String>> _useridCache = new ConcurrentHashMap();

    public static ShadowNewMessageCache instance() {
        return __instance;
    }

    private ShadowNewMessageCache() {
    }

    public void onHandleMessage(SCBoardEventData data) {
        if(data != null) {
            SCBoardEventType type = data.getManager().getEventType();
            boolean isIncoming = type != null && type.isIncoming();
            if(isIncoming && !type.isCall()) {
                this.onHandleMessage(data.getUserid(), data.getLine(), data.getId(), data.getStatus());
            }

        }
    }

    private void onHandleMessage(String userid, String line, String callid, int status) {
        Ln.d("fc_tmp", "ShadowNewMessageCache.onHandleMessage() - userid: %s, line: %s, callid: %s, status: %d", new Object[]{userid, line, callid, Integer.valueOf(status)});
        if(!StringExtra.isNullOrEmpty(userid)) {
            if(status != 4) {
                Ln.d("fc_tmp", "\t -> add", new Object[0]);
                this.add(userid, line, callid);
            } else {
                Ln.d("fc_tmp", "\t -> remove", new Object[0]);
                this.remove(callid);
            }

        }
    }

    public void add(String userid, String line, String callid) {
        if(!StringExtra.isNullOrEmpty(userid)) {
            Set newMessages = this.getNewMessages(userid, line);
            newMessages.add(callid);
            newMessages = this.getNewMessages(userid);
            newMessages.add(callid);
        }
    }

    public void remove(String callid) {
        if(!StringExtra.isNullOrEmpty(callid)) {
            Iterator i$ = this._useridAndLineCache.values().iterator();

            Set cur;
            while(i$.hasNext()) {
                cur = (Set)i$.next();
                cur.remove(callid);
            }

            i$ = this._useridCache.values().iterator();

            while(i$.hasNext()) {
                cur = (Set)i$.next();
                cur.remove(callid);
            }

        }
    }

    public Set<String> getNewMessages(String userid) {
        if(StringExtra.isNullOrEmpty(userid)) {
            return Collections.newSetFromMap(new ConcurrentHashMap());
        } else {
            Set set = (Set)this._useridCache.get(userid);
            if(set == null) {
                set = Collections.newSetFromMap(new ConcurrentHashMap());
                this._useridCache.put(userid, set);
            }

            return set;
        }
    }

    public Set<String> getNewMessages(String userid, String line) {
        if(StringExtra.isNullOrEmpty(userid)) {
            return Collections.newSetFromMap(new ConcurrentHashMap());
        } else {
            ShadowNewMessageCache.UseridAndLine k = new ShadowNewMessageCache.UseridAndLine(userid, line);
            Set set = (Set)this._useridAndLineCache.get(k);
            if(set == null) {
                set = Collections.newSetFromMap(new ConcurrentHashMap());
                this._useridAndLineCache.put(k, set);
            }

            return set;
        }
    }

    private static class UseridAndLine {
        public String userid;
        public String line;

        public UseridAndLine(String userid, String line) {
            this.line = line;
            this.userid = userid;
        }

        public boolean equals(Object o) {
            if(this == o) {
                return true;
            } else if(o != null && this.getClass() == o.getClass()) {
                ShadowNewMessageCache.UseridAndLine key = (ShadowNewMessageCache.UseridAndLine)o;
                return !this.userid.equals(key.userid)?false:(this.line != null?this.line.equals(key.line):key.line == null);
            } else {
                return false;
            }
        }

        public int hashCode() {
            int result = this.userid.hashCode();
            result = 31 * result + (this.line != null?this.line.hashCode():0);
            return result;
        }
    }
}


