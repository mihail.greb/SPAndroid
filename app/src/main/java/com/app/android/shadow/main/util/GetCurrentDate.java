package com.app.android.shadow.main.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by rails-dev11 on 5/8/15.
 */
public class GetCurrentDate {
        public static String getCurrentDate() {

            Calendar calendar;
            calendar = Calendar.getInstance();

            int year = calendar.get(Calendar.YEAR);
            int  month = (calendar.get(Calendar.MONTH)+1);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int min = calendar.get(Calendar.MINUTE);
            int second = calendar.get(Calendar.SECOND);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.ENGLISH);
            String dateInString = ""+day+"-"+month+"-"+year+" "+hour+":"+min+":"+second;
            //String dateInString = "22-01-2015 10:20:56";
            Logger.printLog(Logger.ERROR,"Date String",dateInString);

            return dateInString;
        }
    }
