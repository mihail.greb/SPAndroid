package com.app.android.shadow.samplechat.board;

import android.content.Intent;
import android.view.View;

import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.gui.core.controller.IControllerRequestListener;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.newmessage.controller.INewMessageViewHolder;
import com.c2call.sdk.pub.gui.newmessage.controller.SCNewMessageController;
import com.c2call.sdk.pub.richmessage.SCBaseRichMessageSendObject;

/**
 * Basic custom chat input controller. Further customization of the controller's functionality should be done here
 */
public class BoardChatInputController extends SCNewMessageController
{
    public BoardChatInputController(View view, SCViewDescription viewDescription, IControllerRequestListener iControllerRequestListener, SCFriendData scFriendData)
    {
        super(view, viewDescription, iControllerRequestListener, scFriendData);

        /*
        Don't show an idle dialog when sending a message (sending is always done in the background)
         */
        setShowIdleDialogWhileSending(false);
    }

    @Override
    protected void onBindViewHolder(INewMessageViewHolder vh)
    {
        super.onBindViewHolder(vh);

        /*
        Bind custom view elements here
         */
    }

    /**
     * Called when a message was successfully send
     * @param message the message string that was send
     * @param sendObject and optional attachment
     */
    @Override
    protected void onMessageSuccessfullySent(String message, SCBaseRichMessageSendObject sendObject)
    {
        super.onMessageSuccessfullySent(message, sendObject);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            BaseActivity.setSaveTime0();
            BaseSampleActivity.isAppNotClose = true;
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        try {
            BoardActivity.isAppNotClose = true;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
