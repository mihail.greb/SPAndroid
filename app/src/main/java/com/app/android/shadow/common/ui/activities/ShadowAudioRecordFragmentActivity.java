package com.app.android.shadow.common.ui.activities;

import android.app.Fragment;

import com.app.android.shadow.common.ui.fragments.ShadowAudioRecordFragment;
import com.c2call.sdk.pub.activities.SCAudioRecordFragmentActivity;
import com.c2call.sdk.pub.core.SCExtraData;

/**
 * Created by rails-dev on 15/1/16.
 */
public class ShadowAudioRecordFragmentActivity extends SCAudioRecordFragmentActivity {

    @Override
    protected Fragment onCreateFragment() {
        String output = this.getIntent().getExtras().getString(SCExtraData.AudioRecord.EXTRA_DATA_OUTPUT_PATH);
        int layout = this.getIntent().getExtras().getInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT);
        return ShadowAudioRecordFragment.create(layout, output);
    }
}
