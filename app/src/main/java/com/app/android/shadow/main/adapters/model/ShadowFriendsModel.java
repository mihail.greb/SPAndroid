package com.app.android.shadow.main.adapters.model;

import com.app.android.shadow.main.tapmanager.extra.ShadowFriendDataFromTable;
import com.c2call.sdk.pub.db.data.SCFriendData;

public class ShadowFriendsModel {

    String id,username,phone,first_name,last_name,email,shadow_id,display_email;
    ShadowFriendDataFromTable friendTableData;
    SCFriendData data2;
    boolean audio_call_missed,video_call_missed,chat_missed,isShowDelete,isImageChange;
    String lastChat;

    public ShadowFriendsModel(String id, String username, String phone,
                              String first_name, String last_name, String email,
                              String shadow_id, String display_email, boolean audio_call_missed,
                              boolean video_call_missed,SCFriendData data2, ShadowFriendDataFromTable friendTableData) {
        this.id = id;
        this.username = username;
        this.phone = phone;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.shadow_id = shadow_id;
        this.display_email = display_email;
        this.friendTableData = friendTableData;
        this.audio_call_missed = audio_call_missed;
        this.video_call_missed = video_call_missed;
        this.data2 = data2;
    }

    public boolean isShowDelete() {
        return isShowDelete;
    }

    public void setIsShowDelete(boolean isShowDelete) {
        this.isShowDelete = isShowDelete;
    }

    public String getLastChat() {
        return lastChat;
    }

    public void setLastChat(String lastChat) {
        this.lastChat = lastChat;
    }

    public boolean isChat_missed() {
        return chat_missed;
    }

    public void setChat_missed(boolean chat_missed) {
        this.chat_missed = chat_missed;
    }

    public boolean isAudio_call_missed() {
        return audio_call_missed;
    }

    public boolean isVideo_call_missed() {
        return video_call_missed;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPhone() {
        return phone;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getEmail() {
        return email;
    }

    public String getShadow_id() {
        return shadow_id;
    }

    public String getDisplay_email() {
        return display_email;
    }

    public ShadowFriendDataFromTable getFriendTableData() {
        return friendTableData;
    }

    public void setFriendTableData(ShadowFriendDataFromTable friendTableData) {
        this.friendTableData = friendTableData;
    }

    public SCFriendData getData2() {
        return data2;
    }

    public void setData2(SCFriendData data2) {
        this.data2 = data2;
    }
}
