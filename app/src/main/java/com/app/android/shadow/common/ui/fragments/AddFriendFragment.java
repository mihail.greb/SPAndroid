package com.app.android.shadow.common.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.adapters.CountryCodeListAdapter;
import com.app.android.shadow.main.util.AlertDialogClass;
import com.app.android.shadow.R;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.utils.Email;
import com.c2call.sdk.pub.core.SCProfileHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Customized register fragment. In addition to the default SCRegisterFragment, this
 * class will automatically login the user after successful registration
 */

public class AddFriendFragment extends BaseFragment implements View.OnClickListener {

    EditText uNameEt, phNumberEt, emaiEt, shadowId, messageEt;
    Spinner countryCodeEt;
    String userName;
    TaskSearch mTask;
    TextView inviteEmail,inviteSMS;

    public static AddFriendFragment create(Bundle args) {
        final AddFriendFragment f = new AddFriendFragment();
        f.setArguments(args);
        return f;
    }

    //@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.shadow_findfriend, container, false);
        uNameEt = (EditText) v.findViewById(R.id.ff_username);
        countryCodeEt = (Spinner) v.findViewById(R.id.ff_country_code);
        phNumberEt = (EditText) v.findViewById(R.id.ff_number);
        emaiEt = (EditText) v.findViewById(R.id.ff_email);
        shadowId = (EditText) v.findViewById(R.id.ff_shadow_id);
        messageEt = (EditText) v.findViewById(R.id.ff_message);

        inviteEmail = (TextView) v.findViewById(R.id.invite_friend_email);
        inviteSMS = (TextView) v.findViewById(R.id.invite_friend_sms);
        inviteEmail.setOnClickListener(this);
        inviteSMS.setOnClickListener(this);
        setCountryCodeList(countryCodeEt, getActivity());

        setHeader(v);
        Shadow.editor.putBoolean(Const.FIRST_TIME_LOGGED, false).commit();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        openSoftKeyboard();
    }

    @Override
    public void onPause() {
        super.onPause();
        closeSoftKeyboard(uNameEt);
        closeSoftKeyboard(messageEt);
        closeSoftKeyboard();

    }

    //Header
    @Override
    protected void setHeader(View v) {
        super.setHeader(v);
        TextView title = (TextView) v.findViewById(R.id.title_head);
        title.setText(R.string.add_a_friend);

        TextView connect = (TextView) v.findViewById(R.id.done);
        connect.setText(R.string.connect);

        connect.setOnClickListener(this);
    }

    public static void setCountryCodeList(Spinner s,Context c){
        JSONArray arr = null;
        ArrayList<HashMap<String ,String >> routesList = null;
        try {
            arr = new JSONArray(loadJSONFromAsset(c));
            routesList = new ArrayList<HashMap<String ,String >>();
        JSONObject obj;
        HashMap<String, String> routerMap;
        for (int i = 0; i < arr.length(); i++) {
            obj = arr.getJSONObject(i);
            routerMap = new HashMap<>();
            //routerMap.put(Const._ID, obj.getString(Const._ID));
            routerMap.put(Const.NAME, obj.getString(Const.NAME));
            routerMap.put(Const.DIAL_CODE, obj.getString(Const.DIAL_CODE));
            routesList.add(routerMap);
        }

        CountryCodeListAdapter spinnerArrayAdapter = new CountryCodeListAdapter(c,android.R.layout.simple_spinner_item, routesList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s.setAdapter(spinnerArrayAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public static String loadJSONFromAsset(Context c) {
        String json = null;
        try {
            InputStream is = c.getAssets().open("countries.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done:
                if(checkValidation()) {
                    callApiSearch();
                    goModes();
                }
                break;
            case R.id.invite_friend_email:
                //showPopup(true);
                mCallback.onButtonClickEvent(InviteByEmail.create(true), EventMaker.inviteFriends);
                break;
            case R.id.invite_friend_sms:
                //showPopup(false);
                mCallback.onButtonClickEvent(InviteByEmail.create(false), EventMaker.inviteFriends);
                break;

        }
    }

    private String getShadowName(){
        return Shadow.getAppPreferences().getString(Const.USERNAME,"");
    }

    private void goModes(){
        if(Shadow.getAppPreferences().getBoolean(Const.FIRST_TIME_LOGGED, true)){
            mCallback.goToModes();
        }
    }

    public void showPopup(final boolean isEmail) {

        try {
            LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = layoutInflater.inflate(R.layout.shadow_invite_email_sms, null);
            final PopupWindow popup = new PopupWindow(getActivity());
            popup.setContentView(layout);
            LinearLayout email_invitation_box,sms_invitation_box;
            EditText mobileNumber = null;
            final EditText enter_message;
            Button sendInvitaion;
            final EditText emailId = (EditText) layout.findViewById(R.id.enter_email);
            emailId.setSelectAllOnFocus(true);
            enter_message = (EditText) layout.findViewById(R.id.enter_message);
            enter_message.setSelectAllOnFocus(true);
            sendInvitaion = (Button) layout.findViewById(R.id.sendInvitaion);
           /* if(isEmail){
                email_invitation_box = (LinearLayout)layout.findViewById(R.id.email_invitation_box);
                emailId = (EditText) layout.findViewById(R.id.enter_time);
            }else {
                sms_invitation_box = (LinearLayout) layout.findViewById(R.id.sms_invitation_box);
                mobileNumber = (EditText) layout.findViewById(R.id.enter_time);
            }*/


            if(!isEmail){
                emailId.setHint(R.string.enter_mobile_number);
                emailId.setInputType(InputType.TYPE_CLASS_PHONE |
                        InputType.TYPE_TEXT_VARIATION_PHONETIC);
            }

            enter_message.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_FLAG_MULTI_LINE |
                    InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

            enter_message.setText(getResources().getString(R.string.email_invitation_message,getShadowName()));

            //popup.setWindowLayoutMode(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            ImageView close = (ImageView) layout.findViewById(R.id.close);
            final EditText finalEmailId = emailId;
            final EditText finalMobileNumber = mobileNumber;
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(finalEmailId != null)
                    closeSoftKeyboard(finalEmailId);
                    else
                    closeSoftKeyboard(finalMobileNumber);

                    closeSoftKeyboard(enter_message);
                    popup.dismiss();
                }
            });

            sendInvitaion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                if(emailId != null && Email.isValidEmail(emailId.getText().toString().trim())) {
                    if(finalEmailId != null)
                        closeSoftKeyboard(finalEmailId);
                    else
                        closeSoftKeyboard(finalMobileNumber);

                    closeSoftKeyboard(enter_message);
                    popup.dismiss();
                    if(isEmail) {
                        callApiEmailInvitation(emailId.getText().toString().trim(), enter_message.getText().toString());
                    }
                }else{
                    Toast.makeText(getActivity(),resources.getString(R.string.enter_valid_email),Toast.LENGTH_LONG).show();
                }
                }
            });

            //popup.setAnimationStyle(R.style.MyCustomPopupTheme);
            popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
            popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);

            popup.setFocusable(true);
            //popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            //popup.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
            popup.showAtLocation(layout, Gravity.CENTER, 0, 0);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private boolean checkValidation(){
        boolean uNameBoo = BaseActivity.getText(uNameEt).length() >0;
        int phNumberInt = BaseActivity.getText(phNumberEt).length();
        boolean phNumberBoo = phNumberInt >9;
        boolean emailBoo = BaseActivity.getText(emaiEt).length() >0;
        boolean ShadowIdBoo = BaseActivity.getText(shadowId).length() >0;
        String msg = null;
        if(uNameBoo || emailBoo || ShadowIdBoo){
            return true;
        }else {
            msg = "Please fill any filed";
        }

        if(phNumberBoo) {
            return true;
        }else {
            if(phNumberInt>0) {
                msg = "Please enter valid phoneNo or choose Code";
            }
        }
        new AlertDialogClass(getActivity()).open(msg);
        return false;
        }

    private void callApiEmailInvitation(String email,String message) {
        mTask = new TaskSearch(getActivity());
        mTask.setServiceTaskMapStrParams(APIServiceAsyncTask.emailInvitations,
                getEmailParams(email,message));
        mTask.execute((Void) null);
    }

    private void callApiSearch() {
        mTask = new TaskSearch(getActivity());
        mTask.setServiceTaskMapStrParams(APIServiceAsyncTask.searchContacts, getParams());
        mTask.execute((Void) null);
    }

    class TaskSearch extends APIServiceAsyncTask {
        TaskSearch(Context mContext) {
            super(mContext);
        }

        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            try {
                if(serviceTaskType == APIServiceAsyncTask.INVITAION){
                    String suc = jsonObj.getString(Const.SUCCESS);
                    if (suc.equals("true")) {
                        String info = jsonObj.getString(Const.INFO);
                        if (info.equals("sent")) {
                            /*JSONArray user = jsonObj.getJSONArray(Const.USER);
                            userName = user.getJSONObject(0).getString(Const.USERNAME);*/
                            new AlertDialogClass(getActivity()).openNoAlert(info);

                        } else {
                            Logger.printLog(Logger.ERROR, "success", info);
                            new AlertDialogClass(getActivity()).openNoAlert(info);
                            // new AlertDialogClass(getActivity()).openNoAlert(info);
                        }
                    }
                }else {
                    String suc = jsonObj.getString(Const.SUCCESS);
                    if (suc.equals("true")) {
                        String info = jsonObj.getString(Const.INFO);
                        if (info.equals("sent")) {
                            JSONArray user = jsonObj.getJSONArray(Const.USER);
                            userName = user.getJSONObject(0).getString(Const.USERNAME);
                            callApiSendReq();
                        } else {
                            Logger.printLog(Logger.ERROR, "success", "Your Request has been sent. You will be connected if the user exists and approves you.");
                            new AlertDialogClass(getActivity()).openNoAlert(getActivity().getResources().getString(R.string.msg_on_request_send_success));
                            // new AlertDialogClass(getActivity()).openNoAlert(info);
                            //goModes();
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
            super.failure(jsonObj, serviceTaskType);
            try {
                final String info = jsonObj.getString(Const.INFO);
                new Thread(){
                    public void run(){
                        // Long time consuming operation
                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                new AlertDialogClass(getActivity()).open(info);
                            }
                        });
                    }
                }.start();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Shadow.editor.putBoolean(Const.FIRST_TIME_LOGGED, false).commit();
        }

        @Override
        protected void failure(String message) {
            Shadow.editor.putBoolean(Const.FIRST_TIME_LOGGED, false).commit();
        }
    }

    private Map<String, String> getEmailParams(String email,String message) {
        Map<String, String> params = new HashMap<>();
        params.put(Const.TO, email);
        params.put(Const.MESSAGE, message);
        params.put(Const.APP_NAME, Shadow.getAppCodeName());
        return params;
    }
    private Map<String, String> getParams() {
        Map<String, String> params = new HashMap<>();
        String numSt, emailSt, uNameSt, ShadowIdSt;
        numSt = BaseActivity.getText(phNumberEt);
        emailSt = BaseActivity.getText(emaiEt);
        uNameSt = BaseActivity.getText(uNameEt);
        ShadowIdSt = BaseActivity.getText(shadowId);
        if (numSt.length() > 0) {
            String code = countryCodeEt.getSelectedItem().toString();
            params.put(Const.PHONE, code+numSt);
        } else if (emailSt.length() > 0) {
            params.put(Const.EMAIL, emailSt);
        } else if (uNameSt.length() > 0) {
            params.put(Const.USERNAME, uNameSt);
        } else if (ShadowIdSt.length() > 0) {
            params.put(Const.SHADOW_ID, ShadowIdSt);
        }
        return params;
    }


    private void callApiSendReq() {
        mTask = new TaskSearch(getActivity());
        mTask.setServiceTaskMapStrParams(APIServiceAsyncTask.sendRequesttoFriend, getParams2());
        mTask.execute((Void) null);
    }

    private Map<String, String> getParams2() {
        Map<String, String> params = new HashMap<>();
        String msg;
        msg = BaseActivity.getText(messageEt);
        if (messageEt.length() > 0) {
            params.put(Const.MESSAGE, msg);
        } else {
            params.put(Const.MESSAGE, "");
        }
        params.put(Const.USERNAME, userName);
        try {
            String id = SCProfileHandler.instance().getProfileUserId();
            if(id != null && id.length()>0) {
                Logger.printLog(Logger.ERROR, "current user id is " + id);
                params.put("c2call_friend_id", id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }
}



