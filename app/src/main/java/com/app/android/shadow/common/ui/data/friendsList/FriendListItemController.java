package com.app.android.shadow.common.ui.data.friendsList;

import android.view.View;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemViewHolder;
import com.c2call.sdk.pub.gui.friendlistitem.controller.SCFriendListItemController;
import com.app.android.shadow.R;

/**
 * Custom controller class for friend list items.
 */
public class FriendListItemController extends SCFriendListItemController
{
	public FriendListItemController(final View view, final SCViewDescription viewDescription, final SCFriendData data)
	{
		super(view, viewDescription, data);
	}

	@Override
	protected void onBindViewHolder(final IFriendListItemViewHolder viewHolder)
	{
	    super.onBindViewHolder(viewHolder);

	}


    @Override
	public void onMainViewClick(final View v)
	{
		Ln.d("c2app", "FriendListItemController.onMainViewClick()");

        if (getData().getManager().isTestCall()) {

            C2CallSdk.startControl().openCallbar(getContext(), null, getData().getId(), R.layout.sc_callbar, StartType.Activity);
            new Thread()
            {
                @Override
                public void run()
                {
                    SCCoreFacade.instance().call(getData().getId(), false, false);

                }
            }.start();
        }
        else {
            super.onMainViewClick(v);
        }

	}

    /*@Override
    protected void onPictureLoaded(Bitmap bitmap, String url) {
        Logger.printLog(Logger.ERROR,"working onPictureLoader ");
        Logger.printLog(Logger.ERROR,"working onPictureLoader url"+url);
        if(bitmap != null)
        Logger.printLog(Logger.ERROR,"working onPictureLoader bitmap");
        super.onPictureLoaded(bitmap, url);
    }*/
    /*String _pictureUrl = "";

    @Override
    public void reset() {
        this._pictureUrl = null;
    }

    @Override
    protected void onPictureLoaded(Bitmap bitmap, final String url) {
        Resources resources = Shadow.getAppContext().getResources();
        bitmap = Bitmap.createScaledBitmap(bitmap, ((int)resources.getDimension(R.dimen.image_width)), ((int)resources.getDimension(R.dimen.image_height)), true);
        if(StringExtra.isEqual(this._pictureUrl, url)) {
            Ln.d("fc_tmp", " BasePictureMediator--- no need to u.pdate picture - ", new Object[]{url});
        } else {
            final Bitmap finalBitmap = bitmap;
            this.getHandler().post(new Runnable() {
                public void run() {
                    if(FriendListItemController.this.getViewHolder() != null) {
                        ImageView picture = ((IPictureViewHolder)FriendListItemController.this.getViewHolder()).getItemPicture();
                        if(picture != null) {
                            picture.setVisibility(View.VISIBLE);
                            picture.setImageBitmap(finalBitmap);
                            FriendListItemController.this._pictureUrl = url;
                        }

                    }
                }
            });
        }
    }*/
}
