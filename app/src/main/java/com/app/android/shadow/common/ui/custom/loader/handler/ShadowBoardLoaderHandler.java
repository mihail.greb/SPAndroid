package com.app.android.shadow.common.ui.custom.loader.handler;

import android.database.Cursor;
import android.widget.CursorAdapter;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemControllerFactory;
import com.c2call.sdk.pub.gui.core.adapter.SCBaseControllerCursorAdapter;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;
import com.c2call.sdk.pub.gui.core.loaderhandler.ILoaderHandlerContextProvider;
import com.c2call.sdk.pub.gui.core.loaderhandler.SCBoardLoaderHandler;
import com.c2call.sdk.pub.util.IListViewProvider;

/**
 * Created by rails-dev on 31/12/15.
 */
public class ShadowBoardLoaderHandler extends SCBoardLoaderHandler {

    public ShadowBoardLoaderHandler(ILoaderHandlerContextProvider controller, IListViewProvider listViewProvider, String filterUserid, SCViewDescriptionMap viewDescriptions, IBoardListItemControllerFactory controllerFactory) {
        super(controller, listViewProvider, filterUserid, viewDescriptions, controllerFactory);
    }

    @Override
    protected void onMessagesLoadFinished(Cursor cursor) {
        //super.onMessagesLoadFinished(cursor);
        Ln.d("fc_tmp", "SCBoardLoaderHandler.onMessagesLoadFinished() - this: %s", new Object[]{this});
        if(cursor != null && !cursor.isClosed()) {
            if(cursor.getString(cursor.getColumnIndex("type")) == "CallOut" || cursor.getString(cursor.getColumnIndex("type")) == "CallIn" ){
                return;
            }else {
                CursorAdapter adapter = this.getListAdapter();
                if (adapter == null) {
                    Ln.d("fc_tmp", "SCBoardLoaderHandler.onMessagesLoadFinished() - create new Adapter...", new Object[0]);
                    SCBaseControllerCursorAdapter adapter1 = this.onCreateAapder(cursor, this._itemControllerFactory, this._viewDescriptions);
                    this.getListView().setAdapter(adapter1);
                    Ln.d("fc_tmp", "SCBoardLoaderHandler.onMessagesLoadFinished() - create new Adapter... - done - items: %d", new Object[]{Integer.valueOf(this._listViewProvider.getListView().getCount())});
                } else {
                    Ln.d("fc_tmp", "SCBoardLoaderHandler.onMessagesLoadFinished() - use existing adapter: %s, cursor: %s", new Object[]{adapter, adapter.getCursor()});
                    boolean isNewContent = adapter.getCursor() == null || adapter.getCursor().getCount() < cursor.getCount();
                    Ln.d("fc_tmp", "SCBoardLoaderHandler.onMessagesLoadFinished() - swapping cursor... - size: %d, new content: %b", new Object[]{Integer.valueOf(cursor.getCount()), Boolean.valueOf(isNewContent)});
                    adapter.changeCursor(cursor);
                    Ln.d("fc_tmp", "SCBoardLoaderHandler.onMessagesLoadFinished() - swapping cursor... - done", new Object[0]);
                }

                Ln.d("fc_tmp", "SCBoardLoaderHandler.onMessagesLoadFinished() - list count: %d, child count: %d", new Object[]{Integer.valueOf(this.getListView().getCount()), Integer.valueOf(this.getListView().getChildCount())});
            }
        } else {
            Ln.d("fc_tmp", "SCBoardLoaderHandler.onMessagesLoadFinished() - cursor is closed -> ignore this", new Object[0]);
            this.restart();
        }
    }

  /*  @Override
    protected CursorAdapter getListAdapter() {
        return super.getListAdapter();
        //return this._listViewProvider == null?null:(CursorAdapter)this._listViewProvider.getListView().getAdapter();
    }

    @Override
    protected SCBaseControllerCursorAdapter<SCBoardEventData, IBoardListItemBaseController<? extends IBoardListItemBaseViewHolder>, IBoardListItemControllerFactory, IDecorator<IBoardListItemBaseController<?>>> onCreateAapder(Cursor c, IBoardListItemControllerFactory controllerFactory, SCViewDescriptionMap viewDescriptions) {
        return super.onCreateAapder(c, controllerFactory, viewDescriptions);
    }*/
}
