package com.app.android.shadow.main.adapters;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.adapters.model.ShadowFriendsModel;
import com.app.android.shadow.main.provider.CustomC2CallContentProvider;
import com.app.android.shadow.main.tapmanager.extra.ShadowFriendDataFromTable;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.main.util.SdCardManager;
import com.app.android.shadow.samplechat.SimpleChatFriendsFragment;
import com.app.android.shadow.R;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.SCDownloadManager;
import com.c2call.sdk.pub.core.SCDownloadType;
import com.c2call.sdk.pub.db.data.SCDirtyFriendData;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.richmessage.IDownloadListener;
import com.c2call.sdk.pub.richmessage.SCDownloadState;

import java.util.ArrayList;
import java.util.List;

public class ShadowFriendsListAdapter extends ArrayAdapter {

    Context mcontext;
    ArrayList<ShadowFriendsModel> lst = new ArrayList<ShadowFriendsModel>();
    Resources resources;
    ListView c2CallListView;
    Fragment fragment;


    public ShadowFriendsListAdapter(Context mcontext, List<ShadowFriendsModel> objects, ListView c2CallListView, Fragment fragment) {
        super(mcontext, R.layout.shadow_more_row, objects);
        this.mcontext = mcontext;
        this.lst = (ArrayList<ShadowFriendsModel>) objects;
        this.resources = mcontext.getResources();
        this.c2CallListView = c2CallListView;
        this.fragment = fragment;
    }

    @Override
    public int getCount() {
        return lst.size();
    }

    @Override
    public String getItem(int position) {
        return lst.get(position).getUsername();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ShadowFriendsModel item = lst.get(position);

        ViewHolderItem viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.contact_list_row, parent, false);
            viewHolder = new ViewHolderItem();

            viewHolder.deleteUser = (ImageView) convertView.findViewById(R.id.deleteUser);
            viewHolder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
            viewHolder.rad_dot = (ImageView) convertView.findViewById(R.id.rad_dot);
            viewHolder.shadow_call = (ImageView) convertView.findViewById(R.id.shadow_call);
            viewHolder.shadow_video_call = (ImageView) convertView.findViewById(R.id.shadow_video_call);
            viewHolder.shadow_message = (ImageView) convertView.findViewById(R.id.shadow_message);
            viewHolder.shadow_user_name = (TextView) convertView.findViewById(R.id.shadow_user_name);
            viewHolder.shadow_user_last_msg = (TextView) convertView.findViewById(R.id.shadow_user_last_msg);
            convertView.setTag(viewHolder);
            //setImage(viewHolder.userImage, item, true);
        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
            //setImage(viewHolder.userImage, item);
        }

        viewHolder.shadow_user_name.setText(item.getUsername());

        setImage(viewHolder.userImage, item);

        viewHolder.shadow_call.setImageResource((item.isAudio_call_missed()) ? R.drawable.phone_red : R.drawable.phone);
        viewHolder.shadow_video_call.setImageResource((item.isVideo_call_missed()) ? R.drawable.video_red : R.drawable.video);
        viewHolder.shadow_message.setImageResource((item.isChat_missed()) ? R.drawable.chat_red : R.drawable.chat);
        if (item.isChat_missed()) {
            String lastChat = item.getLastChat();
            if (lastChat.contains("image://")) {
                lastChat = "New Image";
            } else if (lastChat.contains("video://")) {
                lastChat = "New Video";
            } else if (lastChat.contains("audio://")) {
                lastChat = "New Audio";
            } else if (lastChat.contains("vcard://")) {
                lastChat = "New Contact";
            }
            viewHolder.shadow_user_last_msg.setText(lastChat);
            viewHolder.shadow_user_last_msg.setVisibility(View.VISIBLE);
            viewHolder.rad_dot.setVisibility(View.VISIBLE);
        } else {
            viewHolder.shadow_user_last_msg.setText("");
            viewHolder.shadow_user_last_msg.setVisibility(View.GONE);
            viewHolder.rad_dot.setVisibility(View.GONE);
        }

        viewHolder.deleteUser.setVisibility((item.isShowDelete()) ? View.VISIBLE : View.GONE);
        viewHolder.deleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (item.getFriendTableData().get_id().toString().length() > 0 &&
                            item.getFriendTableData().getDipslay_name().toString().length() > 0) {
                        if (fragment instanceof SimpleChatFriendsFragment)
                            ((SimpleChatFriendsFragment) fragment).callDeleteUser(item.getFriendTableData().getDipslay_name(), item.getFriendTableData().get_id(), position);

                    } else {
                        Shadow.alertToast("you are offline");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Shadow.alertToast("you are offline");
                }
            }
        });

       /* viewHolder.shadow_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SCFriendData data = item.getScData();
                if (data != null) {
                    call(data, false);
                } else {
                    Shadow.alertToast(mcontext.getResources().getString(R.string.no_friend));
                }
            }
        });
        viewHolder.shadow_video_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SCFriendData data = item.getScData();
                if (data != null) {
                    call(data, true);
                    Shadow.alertToast("item id " + data.getId());
                    Shadow.alertToast("item manager " + data.getManager().getDisplayName());
                } else {
                    Shadow.alertToast(mcontext.getResources().getString(R.string.no_friend));
                }
            }
        });
        viewHolder.shadow_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SCFriendData data = item.getScData();
                if (data != null) {
                    SCNewMessageCache.instance().getNewMessages(data.getId()).clear();
                    SCCoreFacade.instance().postEvent(new RequestChatHistoryUpdateEvent(), false);
                    openChat(data);
                } else {
                    Shadow.alertToast(mcontext.getResources().getString(R.string.no_friend));
                }
            }
        });*/

        return convertView;
    }

    private class ViewHolderItem {
        ImageView deleteUser;
        ImageView userImage;
        ImageView rad_dot;
        ImageView shadow_call;
        ImageView shadow_video_call;
        ImageView shadow_message;
        TextView shadow_user_name;
        TextView shadow_user_last_msg;
    }

    private void setImage(ImageView userImage, ShadowFriendsModel item) {
        setImage(userImage, item, false);
    }

    private void setImage(ImageView userImage, ShadowFriendsModel item,boolean isOnes) {
        try {
            // String img =(item.getScData().getImageLarge());
            //Logger.printLog(Logger.ERROR,"getImageSmall "+item.getFriendTableData().getImage_small());
            //Logger.printLog(Logger.ERROR, "getImageLarge "+item.getFriendTableData().getImage_large());

            ShadowFriendDataFromTable temData = item.getFriendTableData();
            if (temData != null) {
                Bitmap bmImg = SdCardManager.getUserImgBitmap(temData.get_id());
                Logger.printLog(Logger.ERROR,"Data2 value is 1");
                if (bmImg != null) {
                    Logger.printLog(Logger.ERROR,"Data2 value is 2");
                    userImage.setImageBitmap(bmImg);
                    checkToUpdate(userImage,item,isOnes,bmImg,temData);
                } else { Logger.printLog(Logger.ERROR,"Data2 value is 7");
                    setDefaultAndStartDownload(userImage, item,isOnes);
                }
            } else { Logger.printLog(Logger.ERROR,"Data2 value is 8");
                setDefaultAndStartDownload(userImage, item,isOnes);
            }} catch (Exception e) {
            e.printStackTrace();
            setDefaultAndStartDownload(userImage, item,isOnes);
        }
    }

    private void checkToUpdate(ImageView userImage, ShadowFriendsModel item,boolean isOnes,Bitmap bmImg,ShadowFriendDataFromTable temData) {
        //long var2 = 0;
        boolean var2 = false;
        try {
            //var2 = item.getData2().getManager().getImageModTime();
            //var2= SCUserImageStore.instance().isUpdateNeeded((SCFriendData)item.getData2());
            var2= isUpdateNeeded((SCFriendData) item.getData2());
            Logger.printLog(Logger.ERROR,"Data2 value is 3");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Logger.printLog(Logger.ERROR,"Data2 value is 4");
        Logger.printLog(Logger.ERROR,"value of var2 "+var2);
        //if (var2 <= 0L) {
        //if (var2 && isOnes) {
        if (var2) {
            Logger.printLog(Logger.ERROR, "Data2 value is 6");
            //SCBitmapManager.instance().remove(var2);//String var2 I think this is path
            setDefaultAndStartDownload(userImage, item,false);
        }else {
            userImage.setImageBitmap(bmImg);
        }
    }

    public boolean isUpdateNeeded(SCFriendData var1) {
        if(var1 == null) {
            return false;
        } else {
            long var2 = var1.getManager().getImageModTime();
            if(var2 <= 0L) {
                return false;
            } else {
                try {
                    SCDirtyFriendData var4 = (SCDirtyFriendData)SCDirtyFriendData.dao().queryForId(var1.getId());
                    if(var4 == null) {
                        Ln.d("fc_tmp", "* * * Warning: UserImageStore.isUpdateNeeded() - contact has no entry in dirty-table: %s", new Object[]{var1.getId()});
                        return false;
                    } else {
                        boolean var5 = var2 != var4.getModTime();
                        Logger.printLog(Logger.ERROR,"Friend time"+var2 +"  drity "+var4.getModTime()+ " result is "+var5);
                        Ln.d("fc_tmp", "UserImageStore.isUpdateNeeded() -  %s - %d / %d - %b", new Object[]{var1.getId(), Long.valueOf(var2), Long.valueOf(var4.getModTime()), Boolean.valueOf(var5)});
                        if(var5){
                            SdCardManager.removeOldImage(var1.getId());
                            CustomC2CallContentProvider.customeC2callDB.UpdateDrityFriend(var1.getId(),var2);
                        }
                        return var5;
                    }
                } catch (Exception var6) {
                    var6.printStackTrace();
                    return false;
                }
            }
        }
    }

    private void setDefaultAndStartDownload(final ImageView userImage, final ShadowFriendsModel item,boolean isOnes) {
        setDefaultImage(userImage);
        //if(isOnes) {
            try {
                if (item.getFriendTableData().getImage_large() != null && item.getFriendTableData().getImage_large().length() > 0) {
                    downloadImages(userImage, item.getFriendTableData());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        //}
    }

    private void setDefaultImage(ImageView userImage) {
        userImage.setImageResource(R.drawable.ic_sc_callbar_user_pic_placeholder);
    }

    private void downloadImages(final ImageView userImage, final ShadowFriendDataFromTable info) {
        //SCProfile info=__facade.getProfile();

        String imgUrl = info.getImage_large();
        if (imgUrl != null) {
            Logger.printLog(Logger.ERROR, "image url download is " + imgUrl);
            String filename = Uri.parse(imgUrl).getHost();
            //String filename = imgUrl.substring(imgUrl.indexOf("userimage://") + 1);
            //String filename = info.get_id().toString();
            Logger.printLog(Logger.ERROR, "filename download is " + filename);
/* filepath must be the full path including the filename itself */
            //String filepath = Environment.getExternalStorageDirectory().toString() + "/" + filename;
            String filepath = SdCardManager.getUserImgDir() + "/" + filename;
            Logger.printLog(Logger.ERROR, "filepath download is  " + filepath);
//
// To make use of internal caching routines you can store the picture to a
// defined path
//
//File userPictureFile = SCMediaFacade.instance().getProfileMediaFile(info.getId(), filename);
//String filepath = userPictureFile.getAbsolutePath();

            boolean threaded = true;
            IDownloadListener listener = new IDownloadListener() {
                @Override
                public void onDownloadStatus(String s, String s1, SCDownloadState status, SCDownloadType scDownloadType, Object... objects) {
                    Log.d("tmp", "SCProfileDecorator.test() - status: " + status);
                    switch (status) {

                        case None:
                            Logger.printLog(Logger.ERROR, "run None");
                            break;
                        case Started:
                            Logger.printLog(Logger.ERROR, "run Start");
                            break;
                        case Pending:
                            Logger.printLog(Logger.ERROR, "run padding");
                            break;
                        case Stopped:
                            Logger.printLog(Logger.ERROR, "run Stop");
                            break;
                        case Finished:
                            Logger.printLog(Logger.ERROR, "run Finished");

                            break;
                        case Error:
                            Logger.printLog(Logger.ERROR, "run Error");
                            break;
                    }
                }

                public void onDownloadProgress(String key, int progress, SCDownloadType downloadType) {
                    Log.d("tmp", String.format("SCProfileDecorator.test() - key: %s, progress: %d", key, progress));
                    Logger.printLog(Logger.ERROR, "run progress is " + progress);
                }
            };
            String is = SCDownloadManager.instance().startDownload(imgUrl, filepath, SCDownloadType.UserImage, listener, threaded, null);
            Logger.printLog(Logger.ERROR, "Download status " + is);
            if (is != null) {
                ShadowFriendsListAdapter.this.callNotify();
            }
        }
    }

    private void callNotify() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(2000);

                    ((Activity) mcontext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ShadowFriendsListAdapter.this.notifyDataSetChanged();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

   /* private void openChat(SCFriendData data){
        final Intent intent = new Intent(mcontext, BoardActivity.class);
        intent.putExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, R.layout.sc_board);
        intent.putExtra(SCExtraData.Board.EXTRA_DATA_USERID, data.getId());
        mcontext.startActivity(intent);
    }

    private void call(final SCFriendData data, final boolean isVideo)
    {
        new SimpleAsyncTask<Boolean>((Activity)mcontext, 0)
        {
            @Override
            protected Boolean doInBackground(Void... voids)
            {
                boolean isGroup = data.getManager().isGroup();

                return SCCoreFacade.instance().call(data.getId(), isVideo, isGroup);
            }

            @Override
            protected void onSuccess(Boolean result)
            {
                C2CallSdk.startControl().openCallbar(mcontext, null, data.getId(), R.layout.sc_callbar, StartType.Activity);
            }
        }.execute();
    }*/
}


/* private void checkToUpdate(final ImageView userImage, final ShadowFriendsModel item, final boolean isOnes, Bitmap bmImg, final ShadowFriendDataFromTable temData) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                //long var2 = 0;
                boolean var2 = false;
                try {
                    //var2 = item.getData2().getManager().getImageModTime();
                    var2 = SCUserImageStore.instance().isUpdateNeeded((SCFriendData) item.getData2());
                    Logger.printLog(Logger.ERROR, "Data2 value is 3");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Logger.printLog(Logger.ERROR, "Data2 value is 4");
                Logger.printLog(Logger.ERROR, "value of var2 " + var2);
                //if (var2 <= 0L) {

                final boolean finalVar = var2;
                if (finalVar && isOnes) {
                    fragment.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                            Logger.printLog(Logger.ERROR, "Data2 value is 6");
                            SdCardManager.removeOldImage(temData.get_id());
                            //SCBitmapManager.instance().remove(var2);
                            setDefaultAndStartDownload(userImage, item, isOnes);
                        }
                    });
                }
            }
        }).start();
    }*/