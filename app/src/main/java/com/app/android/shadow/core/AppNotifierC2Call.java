package com.app.android.shadow.core;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.app.android.shadow.common.ui.activities.CallActivity;
import com.app.android.shadow.common.ui.activities.LoginActivity;
import com.app.android.shadow.R;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.notifictaions.NotificationType;
import com.c2call.sdk.pub.notifictaions.SCBaseNotification;
import com.c2call.sdk.pub.notifictaions.SCMessageNotification;
import com.c2call.sdk.pub.notifictaions.SCMissedCallNotification;
import com.c2call.sdk.pub.notifictaions.SCNotifier;

//import com.c2call.sdk.lib.util.extra.PreferenceExtra;
//import com.c2call.sdk.lib.util.extra.ResourcesExtra;
//import com.c2call.sdk.lib.n.g.ac;
//import com.c2call.sdk.lib.n.g;


/**
 * Custom Notifier
 */
public class AppNotifierC2Call extends SCNotifier
{
	public static final String CALL_NOTIFICATION_ID = "c2app_call_notification";

	@Override
	public int getNotifcationIcon(NotificationType notificationType)
	{
		return R.drawable.app_ic_notification;
	}

	@Override
	public void onMessageNotification(final Context context, final SCMessageNotification n)
	{
		super.onMessageNotification(context, n);
	}

	@Override
	public PendingIntent onCreateMissedCallIntent(final Context context, final SCMissedCallNotification n)
	{
		return null;
	}

	@Override
	public PendingIntent onCreateMessageIntent(final Context context, final SCMessageNotification n)
	{
		final Intent intent = new Intent(context, LoginActivity.class);
		return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}

	@Override
	public void onMissedCallNotification(final Context context, final SCMissedCallNotification n)
	{

	}

	@Override
	public String getTitle(final Context context, final SCBaseNotification notification)
	{
		return context.getString(R.string.app_name);
	}

	public void callNotification(final Context context, final String userid, final String incomingCaller)
	{

		final Intent intent = new Intent(context, CallActivity.class);
		intent.putExtra(SCExtraData.Callbar.EXTRA_DATA_ID, userid);
		intent.putExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, R.layout.sc_callbar);
		intent.setAction(Intent.ACTION_VIEW);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		PendingIntent pintent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

		String name = C2CallSdk.contactResolver().getDisplayNameByUserid(userid, true);

		Notification notification;
		if ( Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			notification =(new Notification.Builder(context))
					.setContentTitle(context.getString(R.string.sc_notification_active_call))
					.setSmallIcon(R.drawable.sc_notification_icon_call)
					.setContentText(name)
					.setContentIntent(pintent)
					.setAutoCancel(true)
					.getNotification();
		}else {
			notification = new Notification.Builder(context)
					.setContentTitle(context.getString(R.string.sc_notification_active_call))
					.setSmallIcon(R.drawable.sc_notification_icon_call)
					.setContentText(name)
					.setContentIntent(pintent)
					.setAutoCancel(true)
					.build();
		}

		notification.flags = Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;

		final int notificationId = getNotificationId(CALL_NOTIFICATION_ID);

		NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

		nm.notify(notificationId, notification);
	}
}
