package com.app.android.shadow.utils;

import com.app.android.shadow.events.LogEvent;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.facade.SCCoreFacade;

public class LogUtil
{
    public static void logLargeMessage(final String tag, final String message)
    {
        final int maxLogSize = 2048;
        for(int i = 0; i <= message.length() / maxLogSize; i++) {
            final int start = i * maxLogSize;
            int end = (i+1) * maxLogSize;

            end = end > message.length()
                  ? message.length()
                  : end;

            Ln.d(tag, message.substring(start, end));
        }
    }

    public static void postLog(String msg, Object... args)
    {
        if (args != null && args.length != 0)
        {
            msg = String.format(msg, args);
        }

        SCCoreFacade.instance().postEvent(new LogEvent(msg), false);
    }

    public static void postError(String msg, Object... args)
    {
        if (args != null && args.length != 0)
        {
            msg = String.format(msg, args);
        }

        SCCoreFacade.instance().postEvent(new LogEvent("* * * Error: " + msg), false);
    }
}
