package com.app.android.shadow.main.IntroScreens;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.common.ui.core.ExtraData;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.R;
import com.app.android.shadow.common.ui.activities.LoginActivity;
import com.app.android.shadow.samplechat.SimpleChatMainActivity;

public class IntroActivityFree extends BaseActivity implements View.OnClickListener{

    Button introLogin,introRegistration;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shadow_intro_screen_free);
        LinearLayout yp_apndroid = (LinearLayout) findViewById(R.id.yp_android);
        LinearLayout ac_apndroid = (LinearLayout) findViewById(R.id.ac_android);
        LinearLayout cal_apndroid = (LinearLayout) findViewById(R.id.cal_android);

        yp_apndroid.setOnClickListener(this);
        ac_apndroid.setOnClickListener(this);
        cal_apndroid.setOnClickListener(this);

        introLogin = (Button) findViewById(R.id.introLogin);
        introRegistration = (Button) findViewById(R.id.introRegistration);
        introLogin.setOnClickListener(this);
        introRegistration.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.introLogin:
                goToLoginRegi(true);
                break;
            case R.id.introRegistration:
                goToLoginRegi(false);
                break;
            case R.id.yp_android:
                ShowDialog(getString(R.string.forwarded_to_decoy,"Yellow Puzzle"));
                break;
            case R.id.ac_android:
                ShowDialog(getString(R.string.forwarded_to_decoy,"Alarm Clock"));
                break;
            case R.id.cal_android:
                ShowDialog(getString(R.string.forwarded_to_decoy,"Calculator"));
                break;
        }
    }

    private void goToLoginRegi(boolean isLogin){
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(ExtraData.Login.MAIN_ACTIVITY, SimpleChatMainActivity.class);
        if(isLogin)
                intent.putExtra(ExtraData.Login.PASS_LOGIN, false);
        else
                intent.putExtra(ExtraData.Login.PASS_LOGIN, true);
        startActivity(intent);
        finish();
    }

    private void ShowDialog(String message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        //alertDialogBuilder.setTitle("Alert");
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(R.string.ok,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                goToPlayStore();
                arg0.dismiss();
            }
        });

        alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int which) {
                arg0.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void goToPlayStore(){
        BaseActivity.setSaveTime0();
        BaseSampleActivity.isAppNotClose = true;
        final String appPackageName = Shadow.getProPackageName(); // getPackageName() from Context or Activity object
        try {
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.whatsapp")));
            Logger.printLog(Logger.ERROR,"package name "+appPackageName);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:"+devName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.whatsapp")));
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
            String uri = "https://play.google.com/store/apps/details?id="+appPackageName;
            Logger.printLog(Logger.ERROR,"uri name "+uri);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/search?q=pub:"+devName)));
        }
        BaseSampleActivity.isAppNotClose = false;
    }

}
