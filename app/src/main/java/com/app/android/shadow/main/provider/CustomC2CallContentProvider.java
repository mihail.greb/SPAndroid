package com.app.android.shadow.main.provider;

import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.dbhelper.CustomeC2callDB;
import com.app.android.shadow.main.util.Const;
import com.c2call.lib.androidlog.Ln;
import com.c2call.lib.xml.StringExtra;
//import com.c2call.sdk.lib.l.a;
//import com.c2call.sdk.lib.preferences.GlobalPrefs;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.db.data.SCBoardEventExtraData;
import com.c2call.sdk.pub.db.data.SCDidExt;
import com.c2call.sdk.pub.db.data.SCDirtyFriendData;
import com.c2call.sdk.pub.db.data.SCFavoriteData;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.db.data.SCFriendExtraData;
import com.c2call.sdk.pub.db.data.SCGroupInfoData;
import com.c2call.sdk.pub.db.data.SCMediaData;
import com.c2call.sdk.pub.db.data.SCMissedCallData;
import com.c2call.sdk.pub.db.data.SCMissedCallLookupData;
import com.c2call.sdk.pub.db.data.SCMultiPartData;
import com.c2call.sdk.pub.db.data.SCOpenIdData;
import com.c2call.sdk.pub.db.data.SCPhoneData;
import com.c2call.sdk.pub.db.data.SCSecureMessageData;
import com.c2call.sdk.pub.db.data.SCUserData;
import com.c2call.sdk.pub.db.provider.C2CallContentProvider;

public class CustomC2CallContentProvider extends C2CallContentProvider {

    private static int VERSION = 38;
    private static final Class[] CLASSES = new Class[]{SCFriendData.class, SCPhoneData.class, SCFriendExtraData.class, SCOpenIdData.class, SCBoardEventData.class, SCDirtyFriendData.class, SCMediaData.class, SCBoardEventExtraData.class,
            SCMissedCallData.class, SCMissedCallLookupData.class, SCGroupInfoData.class, SCFavoriteData.class, SCUserData.class, SCDidExt.class,
            SCSecureMessageData.class, SCMultiPartData.class};

    public static CustomeC2callDB customeC2callDB;

    public CustomC2CallContentProvider() {
    }

    @Override
    protected boolean lazyInitialize() {
        Ln.d("fc_tmp", "ContactContentProvider.onCreate()", new Object[0]);
        //String lastAccount = GlobalPrefs.instance().getString("sc_login_email", (String)null);
        //String lastAccount = a.a().a("sc_login_email", (String)null);
        String lastAccount = Shadow.getAppPreferences().getString(Const.EMAIL, "");
        if(StringExtra.isNullOrEmpty(lastAccount)) {
            throw new IllegalStateException("No user is logged in. Database access is per user -> you have to log in first!");
        } else {
            super.setHelper(customeC2callDB = new CustomeC2callDB(CLASSES, "c2call_" + lastAccount + ".sqlite", VERSION, this.getContext()));
            super.setAuthority(AUTHORITY);
            super.initialize(CLASSES);
            return true;
        }
    }
}
