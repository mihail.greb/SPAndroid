package com.app.android.shadow.samplechat;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.ImageView;

//import com.anjlab.android.iab.v3.BillingProcessor;
//import com.anjlab.android.iab.v3.TransactionDetails;
import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.game.alarm.preferences.AlarmPreferencesFragment;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.LauncherActivity;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.apicalls.ApiRequest;
import com.app.android.shadow.main.dbhelper.ShadowDB;
import com.app.android.shadow.main.tapmanager.extra.MoreController;
import com.app.android.shadow.main.tapmanager.settings.Modes;
import com.app.android.shadow.main.tapmanager.settings.MoreSignature;
import com.app.android.shadow.main.tapmanager.settings.ProApp;
import com.app.android.shadow.main.tapmanager.settings.SettingsFragment;
import com.app.android.shadow.main.util.CallTimeLeft;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.utils.Str;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.R;
import com.app.android.shadow.main.tapmanager.settings.Request;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.common.SCProfile;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.SCThreadMode;
import com.c2call.sdk.pub.eventbus.events.SCProfileUpdateEvent;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.facade.SCSecurityFacade;
import com.c2call.sdk.pub.util.SimpleAsyncTask;

import org.json.JSONObject;

import java.security.KeyPair;

public class SimpleChatMainActivity extends BaseSampleActivity
        implements Request.Callbacks,ProApp.OnProAppClickListner,
        MoreController,SensorEventListener
        //,BillingProcessor.IBillingHandler //Billing process methods
{

    /*Check shake*/
    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 1500;
    private static SimpleChatMainActivity _instance = null;

    /*Billing process methods*/
    //BillingProcessor bp;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
       // Flurry.initID();
        super.onCreate(savedInstanceState);
        //bp = new BillingProcessor(this, "YOUR LICENSE KEY FROM GOOGLE PLAY CONSOLE HERE", this);
        //bp.getSubscriptionListingDetails();
        Shadow.editor.putBoolean(Const.FIRST_TIME_LOGGED, false).commit();
        closeSoftKeyboard();
        SCCoreFacade.instance().subscribe(this);
        Shadow.editor.putBoolean(Const.IS_LOGIN, true).commit();
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            from = bundle.getString("from", "");
            if(from.equals("chat")){
                detailPagePosi = bundle.getInt("position");
            }
        }
        initSuperHeader();
        setSuperHeader();
        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        _instance = new SimpleChatMainActivity();
        Shadow.setCurrentActivity(SimpleChatMainActivity.this);
        //initializeReceiver();
        //autoExit();

        /*if(Shadow.getAppPreferences().getBoolean(Const.FIRST_TIME_LOGGED, true) && Shadow.isProApp()){
                goToModes();
        }*/
        callApi(APIServiceAsyncTask.setUserOnlien);
        //TODO comment time left feature
        //new ApiTask2(SimpleChatMainActivity.this).execute((Void) null);
        closeSoftKeyboard();
    }

    /*SuperHeader start*/
    ImageView lockImg;

    private void initSuperHeader(){
        lockImg = (ImageView)findViewById(R.id.lock);
        final View parent = (View) lockImg.getParent();
        lockImg.post(new Runnable() {
            // Post in the parent's message queue to make sure the parent
            // lays out its children before we call getHitRect()
            public void run() {
                final Rect r = new Rect();
                lockImg.getHitRect(r);
                r.top -= 30;
                r.bottom += 20;
                r.left -= 30;
                r.right += 20;
                parent.setTouchDelegate(new TouchDelegate(r, lockImg));
            }
        });
    }

    private void setSuperHeader(){
        setSuperHeader((Shadow.shadowDB.getModeData(ShadowDB.IS_SAFE_MODE)<=0)?true:false);
    }
    private void setSuperHeader(boolean isSafe){
            lockImg.setImageResource((isSafe) ? R.drawable.lock : R.drawable.unlock_white);
            lockImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   goToModes();
                }
            });
    }


    @Override
    public void goToModes(){
        closeSoftKeyboard();
        if (!(mCurrentFragment instanceof Modes)) {
            mStacks.get(Const.SETTINGS).clear();
            mTabHost.setCurrentTab(2);
            pushFragments(Const.SETTINGS, new SettingsFragment(), false, true);
            onButtonClickEvent(new Modes(), EventMaker.SettingsFragment);
        } else {
            Logger.printLog(Logger.ERROR, "currently on modes");
        }
    }
/*SuperHeader end*/

   // Error in Samsung Device
    /*private void initializeReceiver(){
        ShadowReceiver BR_lock_receiver = null;
        BR_lock_receiver = new ShadowReceiver();
        BR_lock_receiver.setMainActivityHandler(this);
        IntentFilter fltr_received = new IntentFilter("android.intent.action.USER_PRESENT");
         fltr_received.addAction("android.intent.action.SCREEN_OFF");
        registerReceiver(BR_lock_receiver, fltr_received);
    }
*/
    public static SimpleChatMainActivity instance() {
        return _instance;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
       //setLogoutTimer();
        initAutoLogoutTimer();
        return super.dispatchTouchEvent(ev);
    }

    /*@Override
    protected void onUserLeaveHint() {
        if(!isFinishing()) {
            Logger.printLog(Logger.ERROR, "onUserLeaveHint exit baseSample");
            exittoApp();
        }
        super.onUserLeaveHint();
    }*/

   /* public void setLogoutTimer(){
        long currentTime = System.currentTimeMillis();
        if((lastTime+1000)<currentTime) {
            // Toast.makeText(SimpleChatMainActivity.this, "touch", Toast.LENGTH_LONG).show();
            if (timer != null) {
                timer.cancel();
            }
            autoExit();
            lastTime = currentTime;
        }
    }

    public void autoExit(){
            boolean isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
            int time = (Modes.getLogoutTime((isSafeMode) ? ShadowDB.LOGOUT_TIME : ShadowDB.UN_LOGOUT_TIME));
            if(time == (-1)){time = 2;}
            timer = new Timer();
            Logger.printLog(Logger.ERROR,"check timer", "time is " + time);
            timer.schedule(new TimerTask() {
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Logger.printLog(Logger.ERROR,"check timer", "run");
                            //logout(false);
                            exittoApp();

                        }
                    });
                }
            }, ((long) time * 60 * 1000));
    }*/

    @Override
    protected void onPause() {
        super.onPause();
        senSensorManager.unregisterListener(this);
        removeTimer();

    }

    @Override
    protected void onRestart() {
        mainLayout.setVisibility(View.INVISIBLE);
        super.onRestart();
        checkLogout();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SimpleChatMainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mainLayout.setVisibility(View.VISIBLE);
                    }
                });
            }
        }, 1000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLogout();
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        initAutoLogoutTimer();
    }


    @Override
    protected void onStop() {
        super.onStop();
       /* if(!isAppNotClose) {
            Logger.printLog(Logger.ERROR, "onStop exit baseSample");
            exittoApp();
        }*/
    }


    @SCEventCallback(threadMode = SCThreadMode.MainThread, isSticky = true)
    private void onEvent(SCProfileUpdateEvent evt)
    {
        Ln.d("c2appa", "SimpleChatMainActivity.onEvent() - evt");

        SCProfile p = evt.getValue();
        if (p == null){
            return;
        }

        if (Str.isEmpty(p.getPublicKeyString())){
            String successMessage = getString(R.string.sc_msg_generated_keypair_success);
            String failedMessage = getString(R.string.msg_generated_keypair_failed);
            new SimpleAsyncTask<KeyPair>(this, 0, successMessage, failedMessage)
            {
                @Override
                protected KeyPair doInBackground(Void... params)
                {
                    return SCSecurityFacade.instance().generateKeypair(SimpleChatMainActivity.this, false);
                }
            }.execute();
        }
    }

    @Override
    public void onProAppButtonClicked(Fragment f) {
        pushFragments(Const.PRO_APP, f, false, true);
    }

    @Override
    public void onButtonClickEvent(Fragment f, EventMaker maker) {
        switch (maker){
            case SettingsFragment:
                pushFragments(Const.SETTINGS, f, false, true);
                break;
            case Friends:
                pushFragments(Const.FRIENDS,f, false, true);
                break;
            case inviteFriends:
                pushFragments(Const.FRIENDS,f, false, true);
                break;
            case Gallery:
                pushFragments(Const.GALLERY,f, false, true);
                break;
            case Logout:
                logout(false);
                break;
            case AutoLogout:
               recallOnTouch();
                break;
            case GoToShadow:
                goToShadowScreen();
                break;
            case CloseApp:
                exittoApp();
                break;
            case GoToMode:
                goToModes();
                break;
            case RefreshLockButton:
                setSuperHeader();
                break;

        }
    }

    @Override
    public void onModeChangeEvent(boolean isSafe) {
        setSuperHeader(isSafe);
    }

    private void goToShadowScreen(){
        gotoReenterScreen();
    }

    private void recallOnTouch(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
     /*   Logger.printLog(Logger.ERROR,"recall","working");
        // Obtain MotionEvent object
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis() + 100;
        float x = 0.0f;
        float y = 0.0f;
// List of meta states found here:     developer.android.com/reference/android/view/KeyEvent.html#getMetaState()
        int metaState = 0;
        MotionEvent motionEvent = MotionEvent.obtain(
                downTime,
                eventTime,
                MotionEvent.ACTION_UP,
                x,
                y,
                metaState
        );
// Dispatch touch event to view
        dispatchTouchEvent(motionEvent);*/
        //setLogoutTimer();
        initAutoLogoutTimer();
    }

    @Override
    public void onButtonClickEvent(MoreSignature name, EventMaker maker) {

        switch (maker){
            case SoundSettings:

                break;
        }
    }

    @Override
    public void addToIncressClickArea(View v) {
        BaseActivity.incresClickArea(v);
    }

    public void logout(boolean isShadowLogout)
    {
        if(!(SimpleChatMainActivity.this).isFinishing()) {
            if (!isShadowLogout) {
                Logger.printLog(Logger.ERROR,"Logout", "working1");
                callApi(APIServiceAsyncTask.logout);
            } else {
                ClearAllData();
                Logger.printLog(Logger.ERROR,"Logout", "working2");
                new SimpleAsyncTask<Boolean>(SimpleChatMainActivity.this, 0) {
                    @Override
                    protected Boolean doInBackground(Void... voids) {
                        return SCCoreFacade.instance().logout();
                    }

                    @Override
                    protected void onFinished() {
                        super.onFinished();
                        goToLoginByLogout();
                    }
                }.execute();
            }
        }
    }

    private void ClearAllData() {
        try {
            Modes.setSavedTabs(Const.DEFAULT_TABS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Modes.setSavedCalculation("0*0");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void goToLoginByLogout() {
        BaseActivity.setSaveTime0();
        AlarmPreferencesFragment.saveAlarmSaveTime(0);
        BaseSampleActivity.isAppNotClose = true;
        Shadow.editor.putBoolean(Const.IS_LOGIN, false).commit();
        SCCoreFacade.instance().unsubscribe(this);
        Intent intent = new Intent(this, LauncherActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


    public void callApi(ApiRequest req){
        ApiTask mTask = new ApiTask(SimpleChatMainActivity.this);
        mTask.setServiceTaskMapStrParams(req, null);
        mTask.execute((Void) null);
    }

    public class ApiTask extends APIServiceAsyncTask {
        ApiTask(Context mContext){
            super(mContext);
        }

        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            if(APIServiceAsyncTask.LOGOUT == serviceTaskType){
                logout(true);
            } else if(APIServiceAsyncTask.ONLINE_USER == serviceTaskType){
                Logger.printLog(Logger.ERROR,"online response "+jsonObj.toString());
            }
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
            super.failure(jsonObj, serviceTaskType);
            failure(""+serviceTaskType);
        }

        @Override
        protected void failure(String message) {
            //super.failure(message);
            if(Integer.parseInt(message) == APIServiceAsyncTask.LOGOUT) {
                goToLoginByLogout();
            } else if(APIServiceAsyncTask.ONLINE_USER == Integer.parseInt(message)){

            }
        }
    }

    class ApiTask2 extends CallTimeLeft {
        ApiTask2(Context mContext){
            super(mContext);
        }
        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            super.success(jsonObj, serviceTaskType);

            if(isShowDialog()){
                // TODO comment time left dialog
                //new AlertDialogClass(SimpleChatMainActivity.this).showPopupWindow(SimpleChatMainActivity.this, getDateStringToDialog(),null);
            }
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
           // Shadow.alertToast("Failed");
        }
    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdate) > 100) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                float speed = Math.abs(x + y + z - last_x - last_y - last_z)/ diffTime * 10000;

                if (speed > SHAKE_THRESHOLD) {
                   // if(Shadow.isProApp()) {
                     boolean isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
                        if(Modes.getStatus((isSafeMode)?ShadowDB.SHAKE_LOGOUT:ShadowDB.UN_SHAKE_LOGOUT)) {
                            //logout(false);
                            exittoApp();
                        }
                   // }
                }
                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        initAutoLogoutTimer();
    }

    /*Billing process methods*/
    /*@Override
    public void onProductPurchased(String productId, TransactionDetails details) {

    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

    }*/



    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }
*/

    /*@Override
    protected void onDestroy() {
        super.onDestroy();
        if (bp != null)
            bp.release();

      *//*  try {
            timer.cancel();
            timer = null;
        } catch (Exception e) {
            e.printStackTrace();
        }*//*
    }*/
    /*/Billing process methods*/

}
   /* @Override
    public void onControllerEvent(SCBaseControllerEvent scBaseControllerEvent) {
        super.onControllerEvent(scBaseControllerEvent);
    }*/


/*

 boolean isSafeMode;
    Timer timer = null;
    long lastTime = 0;
    int time;
    TimerTask timerTask;
    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();

   void initAutoLogoutTimer(){
        isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
        time = (Modes.getLogoutTime((isSafeMode) ? ShadowDB.LOGOUT_TIME : ShadowDB.UN_LOGOUT_TIME));
        if(time == (-1)){
            time = 2;
        }
        resetAutoLogoutTimer();
    }

    void resetLastTouchTime(){
        lastTime = System.currentTimeMillis();
        saveLogoutTimeByClock();
    }


    void saveLogoutTimeByClock(){
        long saveTime = lastTime + (time * Const.ONE_MINUTE_IN_MILLIS);
        Logger.printLog(Logger.ERROR,"saveTime "+saveTime);
        Shadow.getAppPreferencesEditor().putLong(Const.SAVE_TIME_LOGOUT,saveTime).commit();
    }


    boolean isAutoLogout(){
        boolean isNeedToLogout = (lastTime+1000) < System.currentTimeMillis();
        return isNeedToLogout;
    }

    void resetAutoLogoutTimer(){
        if(isAutoLogout()) {
            resetLastTouchTime();
            if (timer == null) {
                resetTimer();
            } else {
                try {
                    timer.cancel();
                    timer = null;
                    resetTimer();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        //exittoApp();
                        gotoReenterScreen();
                    }
                });
            }
        };
    }

    public void gotoReenterScreen(){
        BaseActivity.setSaveTime0();
        BaseSampleActivity.isAppNotClose = true;
        Intent intent = new Intent(SimpleChatMainActivity.this, (Shadow.isProApp() ? Shadow.getProActivity() : FreeAppWayToAccess.class));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


    void resetTimer(){
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, ((long) time * 60 * 1000));
    }

    private void checkLogout(){
        long checkTime = Shadow.getAppPreferences().getLong(Const.SAVE_TIME_LOGOUT, 0);
        long sysCurTime = System.currentTimeMillis();
        Logger.printLog(Logger.ERROR, "checkTime " + checkTime);
        Logger.printLog(Logger.ERROR, "checkTime2  " + sysCurTime);
        if(checkTime != 0 && checkTime < sysCurTime ){
            gotoReenterScreen();
        }
    }
* */