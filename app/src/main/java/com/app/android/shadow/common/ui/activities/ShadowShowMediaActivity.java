package com.app.android.shadow.common.ui.activities;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.R;

public class ShadowShowMediaActivity extends BaseActivity implements MediaPlayer.OnCompletionListener{

    ImageView fullImage ;
    public VideoView videoView;
    MediaController mc;
    ImageView playVideo;
    MediaPlayer mplayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shadow_gallery_fullview);
        setHeader();
        fullImage = (ImageView) findViewById(R.id.full_view);
        mc= new MediaController(this);
        videoView = (VideoView)findViewById(R.id.play_video);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            String uri = bundle.getString("URI");
            Logger.printLog(Logger.ERROR, "uri now is " + uri);
            String value= bundle.getString("flag");
            if(value.equals("image")){
                fullImage.setVisibility(View.VISIBLE);
                videoView.setVisibility(View.GONE);
                fullImage.setImageURI(Uri.parse(uri));
            }else if(value.equals("video") || value.equals("audio")){
                fullImage.setVisibility(View.GONE);
                videoView.setVisibility(View.VISIBLE);
                initVidoeCode(uri);
            }else {
                Toast.makeText(this,getResources().getString(R.string.file_format_not_support),Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    //Header
    TextView title,closeScreen;

    protected void setHeader() {
        title = (TextView) findViewById(R.id.title_head);
        title.setText(getResources().getString(R.string.shadow_gallery));
        findViewById(R.id.back).setVisibility(View.INVISIBLE);
        closeScreen = (TextView) findViewById(R.id.close_screen);
        closeScreen.setVisibility(View.VISIBLE);
        closeScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        incresClickArea(closeScreen);
    }

    private void initVidoeCode (String uri){
        videoView.setKeepScreenOn(true);
        videoView.setVideoURI(Uri.parse(uri));
        videoView.setMediaController(mc);
        videoView.setOnCompletionListener(this);
        videoView.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }
}

/*
if(savedInstanceState == null){
            Bundle bundle = getIntent().getExtras();
            if(bundle != null) {
                try {
                    String value = bundle.getString("flag");
                    Bundle bundle1 = new Bundle();
                    bundle1.putBoolean("isVideo", (value.equals("video") || value.equals("audio")));
                    bundle1.putString("URI", bundle.getString("URI"));

                    //bundle1.putInt("Position", position);
                    // bundle.putSerializable("HashMap",listImage.get(position));
                    //Fragment fragment = new GalleryFullViewFragment();

                    Fragment fragment = new GalleryFliperFragment();
                    fragment.setArguments(bundle1);

                    FragmentTransaction fTransaction = getFragmentManager().beginTransaction();
                    fTransaction.add(R.id.fragment_containers, fragment);
                    fTransaction.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                finish();
            }
        }

* */