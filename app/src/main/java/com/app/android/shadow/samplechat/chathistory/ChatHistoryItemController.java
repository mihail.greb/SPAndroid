package com.app.android.shadow.samplechat.chathistory;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.app.android.shadow.events.RequestChatHistoryUpdateEvent;
import com.app.android.shadow.samplechat.board.BoardActivity;
import com.app.android.shadow.R;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.core.SCNewMessageCache;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemTextViewHolder;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemTextController;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;


/**
 * Custom implementation of a board item controller which controls the items of a chat history.
 * This chat history shows one item which for each chat. And clicking this item should open the chat itself.
 * This behavior differs from the standard one so we have to override #onMainViewClick().
 * We will also override #onPictureViewClick() to show how to open the contact's detail view, this is also not standard
 */
public class ChatHistoryItemController extends SCBoardListItemTextController
{

    /**
     * Additional view to show a badge of new messages in the chat
     */
    private TextView _textUnreadMessages;

    public ChatHistoryItemController(final View view, final SCViewDescription viewDescription, final SCBoardEventData data)
    {
        super(view, viewDescription, data);
    }

    @Override
    protected void onBindViewHolder(IBoardListItemTextViewHolder vh)
    {
        super.onBindViewHolder(vh);

        _textUnreadMessages = (TextView)getView().findViewById(R.id.app_boarditem_unread);

    }


    public TextView getTextUnreadMessages()
    {
        return _textUnreadMessages;
    }


    @Override
    public void onMainViewClick(final View v)
    {
        Ln.d("c2app", "ChatHistoryItemController.onMainViewClick() - userid: %s", getData().getUserid());

        if (getData().getUserid() == null){
            Ln.d("c2app", "* * * Warning: ChatHistoryItemController.onMainViewClick() - userid is null!");
            return;
        }

        /*
        Clear the new message caches for this user
         */
        SCNewMessageCache.instance().getNewMessages(getData().getUserid()).clear();

        /*
        Notify other interested components that the they may should reload their lists.
        In this sample the event will be caught by the ChatHistoryController
        */
        SCCoreFacade.instance().postEvent(new RequestChatHistoryUpdateEvent(), false);

        openChat();
    }

    /**
     * Open the full chat for this user
     */
    private void openChat()
    {

        final Intent intent = new Intent(getContext(), BoardActivity.class);
        intent.putExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, R.layout.sc_board);
        intent.putExtra(SCExtraData.Board.EXTRA_DATA_USERID, getData().getUserid());
        getContext().startActivity(intent);
    }


    /**
     * When the picture of a user is clicked, then we want to open the contact detail view for him
     * @param v
     */
    @Override
    public void onPictureViewClick(final View v)
    {
        final String userid = getData().getUserid();


        try{
            final SCFriendData friend = SCFriendData.dao().queryForId(userid);
            if (friend != null){
                //C2CallSdk.startControl().openContactDetail(getContext(), null, R.layout.sc_contact_detail, friend, StartType.Activity);
                C2CallSdk.startControl().openContactDetail(getContext(), null, R.layout.sc_contact_detail, friend, StartType.Activity);
            }
            else{
                /*
                We have no friend in our list with that id. Maybe it is a phone number or a message of a deleted friend
                In this sample We do nothing in that case.
                */
            }
        }
        catch(final Exception e){
            e.printStackTrace();
        }



    }

    @Override
    public void onHandleReadReport()
    {
        /*
        Don't send read reports in chat history
         */
    }
}
