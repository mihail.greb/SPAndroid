package com.app.android.shadow.common.ui.activities;

import android.app.Fragment;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;

import com.app.android.shadow.common.ui.fragments.ShadowCallbarFragment;
import com.app.android.shadow.core.AppNotifier;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.R;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.activities.SCCallbarFragmentActivity;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.SCThreadMode;
import com.c2call.sdk.pub.eventbus.events.SCCallEvent;
import com.c2call.sdk.pub.facade.SCCoreFacade;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CallActivity extends SCCallbarFragmentActivity
{
    private boolean _callTerminated = false;
    ShadowCallbarFragment fragment;
    public static boolean isVideo;
    public static boolean isCalling = false;
    public static long timeleft;
    android.os.Handler handler;
    Runnable r;
    public static String userIdCurrent = "";

    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        BaseActivity.setSaveTime0();
        BaseSampleActivity.isAppNotClose = false;
       // getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        SCCoreFacade.instance().subscribe(this);
        Ln.d("c2app", "CallActivity.onEvent(SCCallEvent) - userid: %s", getUserid());
        //preventToLock();
        userIdCurrent = getUserid();
    }

    @Override
    protected void onDestroy()
    {
        SCCoreFacade.instance().unsubscribe(this);
        super.onDestroy();
        BaseSampleActivity.isAppNotClose = false;
    }

    @SCEventCallback(threadMode = SCThreadMode.MainThread)
    private void onEvent(SCCallEvent evt)
    {
        Ln.d("c2app", "CallActivity.onEvent(SCCallEvent) - evt: %s", evt);
        _callTerminated = evt.getStatus().isTerminationStatus();

        Logger.printLog(Logger.ERROR, "print call state  " + _callTerminated);
        String statusString = evt.getStatus().toString();
        Logger.printLog(Logger.ERROR,"print call String  "+statusString);
        String durationString = fragment.getDurationLable();
        String userNameString = fragment.getUserName();
        Logger.printLog(Logger.ERROR,"Total call Durations "+durationString +" and username "+userNameString);

        //TODO comment time left feature
       /* if(timeleft == 0 && !Shadow.isProApp()){
            fragment.getController().onButtonHangupClick(fragment.getView());
        }*/

        if(statusString.equals("Connected")){
            if(!Shadow.isProApp()) {
                Logger.printLog(Logger.ERROR, "Call connected");
                //TODO comment time left feature
                //startHandler();
            }
        }

        if(_callTerminated && durationString.length()>0){
            Logger.printLog(Logger.ERROR, "print call state working" );
            try {
                handler.removeCallbacks(r);
            } catch (Exception e) {
                e.printStackTrace();
            }
            callApi(durationString,statusString,userNameString);
        }
        if (_callTerminated){
            showCallNotification(false);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        showCallNotification(false);
       // this.mWakeLock.acquire();
    }

    //ToPravent
    protected PowerManager.WakeLock mWakeLock;
    private void preventToLock() {
        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
    }

    @Override
    protected void onPause()
    {
        Ln.d("c2app", "CallActivity.onPause() - _callTerminated: %b, callRunning: %b", _callTerminated, SCCoreFacade.instance().isCallRunning());

        boolean callRunning = SCCoreFacade.instance().isCallRunning();

        boolean showNotification = !_callTerminated && callRunning;

        showCallNotification(showNotification);

        super.onPause();
      //  this.mWakeLock.release();
    }

    /*@Override
    protected int getLayout(boolean isVideo) {
        int layout = super.getLayout(isVideo);
        Logger.printLog(Logger.ERROR,"is video is "+isVideo);
        this.isVideo = isVideo;
        return layout;
    }*/

    @Override
    protected Fragment onCreateFragment() {
         //super.onCreateFragment();
        Logger.printLog(Logger.ERROR,"onCreateFragment working");
        int layout = this.getIntent().getIntExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, R.layout.sc_callbar);
        String id = this.getIntent().getStringExtra(SCExtraData.Callbar.EXTRA_DATA_ID);
        fragment = ShadowCallbarFragment.create(id, layout);
        return fragment;
    }
    private void startHandler() {

        handler = new android.os.Handler();
        r = new Runnable() {
            @Override
            public void run() {
                Logger.printLog(Logger.ERROR,"fire after time set work");
                fragment.getController().onButtonHangupClick(fragment.getView());
            }
        };
        handler.postDelayed(r, timeleft);
        //handler.postDelayed(r, 5000);
    }

    private void showCallNotification(boolean show)
    {
        Ln.d("c2app", "CallActivity.showCallNotification() - show: %b", show);
        if (show){
            ((AppNotifier) C2CallSdk.notifier()).callNotification(C2CallSdk.context(), getUserid(), null);
        }
        else {
            NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            nm.cancel(C2CallSdk.notifier().getNotificationId(AppNotifier.CALL_NOTIFICATION_ID));
        }
    }

    private void callApi(String duration,String status,String userNameString) {
        UserCallDetail mTaks = new UserCallDetail(CallActivity.this);
        mTaks.setServiceTaskMapStrParams(APIServiceAsyncTask.callHistorysPost, getLoginParams(duration, status,userNameString));
        mTaks.execute((Void) null);
    }

    class UserCallDetail extends APIServiceAsyncTask {
        UserCallDetail(Context mContext){super(mContext);}

        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            //super.success(jsonObj, serviceTaskType);
            Logger.printLog(Logger.ERROR,"Json as String "+jsonObj.toString());
        }
    }

    private Map<String, String> getLoginParams(String duration,String status,String userNameString){
        Map<String, String> params = new HashMap<>();
        if (status.equals("Decline")) { //reject
            params.put("status", "rejected");
        } else if (status.equals("Hung up") && duration.equals("00:00:00")) { //missed
            params.put("status", "missed");
        } else { //connected
            params.put("status", "connected");
        }
        params.put("call_type", (isVideo)?"Video":"Audio");
        params.put("call_duration", duration);
        params.put("username", userNameString);
        return params;
    }

}


/*

(void)sendApiOfCallStatus {
   NSDictionary *aParam;
   NSLog(@"%@", self.labelConnectionState.text);
   if([self.labelConnectionState.text isEqualToString:@"Decline"] ) { //reject
       aParam = @{@"username":[self.labelName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],
                   @"status":@"rejected",
                   @"call_type":sharedDelegate.strCallType,
                   @"call_duration":@"00:00:00"};
   } else if ([self.labelConnectionState.text isEqualToString:@"Ringing"] || ([self.labelConnectionState.text isEqualToString:@"Hang Up"] && [self.labelDuration.text isEqualToString:@"00:00:00"])) { // misscall
       aParam = @{@"username":[self.labelName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],
                  @"status":@"missed",
                  @"call_type":sharedDelegate.strCallType,
                  @"call_duration":@"00:00:00"};
   } else if ([self.labelConnectionState.text isEqualToString:@"Connected"] || [self.labelConnectionState.text isEqualToString:@"Hang Up"]) { //done connection
       aParam = @{@"username":[self.labelName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],
                  @"status":@"connected",
                  @"call_type":sharedDelegate.strCallType,
                  @"call_duration":self.labelDuration.text};
   }
   [sharedDelegate.apiOperation callPostUrlWithHeader:aParam method:@"api/v1/call_histories" success:^(AFHTTPRequestOperation *task, id responseObject) {
       NSLog(@"%@",responseObject);
       self.labelConnectionState.text = @"DismissCall";
   } failure:^(AFHTTPRequestOperation *task, NSError *error) {

   }];
}
* */