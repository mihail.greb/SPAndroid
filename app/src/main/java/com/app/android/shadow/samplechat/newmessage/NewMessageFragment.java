package com.app.android.shadow.samplechat.newmessage;

import android.os.Bundle;
import android.view.View;

import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.fragments.SCNewMessageFragment;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.newmessage.controller.INewMessageController;
import com.c2call.sdk.pub.gui.picknumber.controller.IPickNumberController;

public class NewMessageFragment extends SCNewMessageFragment
{

    private IPickNumberController _pickNumberController;

    public static NewMessageFragment create(final SCFriendData friend, final int layout)
    {
        final Bundle args = new Bundle();
        args.putSerializable(SCExtraData.NewMessage.EXTRA_DATA_CONTACT, friend);
        args.putSerializable(SCExtraData.NewMessage.EXTRA_DATA_SHOW_IDLE_WHILE_SENDING, true);
        args.putInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);

        final NewMessageFragment fragment = new NewMessageFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected INewMessageController onCreateController(View v, SCViewDescription vd)
    {
        initChildController(v);

        return new NewMessageController(v, vd, this, null, true);
    }


    private void initChildController(View v)
    {
        SCViewDescription vd = C2CallSdk.vd().pickNumber();

        _pickNumberController = new NewMessagePickNumberController(v, vd, this);

    }

    @Override
    protected void onControllerPostCreate(INewMessageController controller)
    {
        _pickNumberController.onCreate(getActivity(), null);
        super.onControllerPostCreate(controller);
    }

}

