package com.app.android.shadow.samplechat.chathistory;

import android.view.View;

import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemBaseController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemBaseViewHolder;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemTextController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemControllerFactory;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemType;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;



public class ChatHistoryListItemControllerFactory extends SCBoardListItemControllerFactory
{
    public ChatHistoryListItemControllerFactory(final SCActivityResultDispatcher resultDispatcher)
    {
        super(resultDispatcher);
    }

    @Override
    public IBoardListItemBaseController<? extends IBoardListItemBaseViewHolder> create(final SCBoardListItemType type, final View v, final SCViewDescription vd, final SCBoardEventData data)
    {
        if (type == null
            || v == null
            || vd == null
            || data == null)
        {
            throw new IllegalArgumentException();
        }

        switch(type){
            default:
                return onCreateChatItemController(v, vd, data);

        }
    }

    protected IBoardListItemTextController onCreateChatItemController(final View v, final SCViewDescription vd, final SCBoardEventData data)
    {
        return new ChatHistoryItemController(v, vd, data);
    }


    @Override
    public IBoardListItemBaseController<? extends IBoardListItemBaseViewHolder> onCreateController(final View v, final SCViewDescription vd, final SCBoardEventData data)
    {
        return create(data.getManager().getBoardListItemType(), v, vd, data);
    }
}
