/* Copyright 2014 Sheldon Neilson www.neilson.co.za
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package com.app.android.shadow.game.alarm;

import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.shadow.game.alarm.database.AlarmDatabase;
import com.app.android.shadow.game.alarm.preferences.AlarmPreferencesFragment;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.R;

import java.util.List;

public class AlarmFragment extends BaseAlarmFragment {


	View v;
	//Header
	TextView title;
	@Override
	protected void setHeader(View v) {
		//super.setHeader(v);
		v.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				/*Intent newAlarmIntent = new Intent(getActivity(), larmPreferencesActivity.class);
				startActivity(newAlarmIntent);*/
				mCallback.onButtonClickEvent(new AlarmPreferencesFragment(), EventMaker.SettingsFragment);
			}
		});
	}

	ImageButton newButton;
	ListView mathAlarmListView;
	AlarmListAdapter alarmListAdapter;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		v =inflater.inflate(R.layout.shadow_alarm_activity, container, false);
		setHeader(v);
		mathAlarmListView = (ListView) v.findViewById(android.R.id.list);
		mathAlarmListView.setLongClickable(true);
		mathAlarmListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
				view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
				final Alarm alarm = (Alarm) alarmListAdapter.getItem(position);
				Builder dialog = new Builder(getActivity());
				dialog.setTitle("Delete");
				dialog.setMessage("Delete this alarm?");
				dialog.setPositiveButton("Ok", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

						AlarmDatabase.init(getActivity());
						AlarmDatabase.deleteEntry(alarm);
						AlarmFragment.this.callMathAlarmScheduleService();

						updateAlarmList();
					}
				});
				dialog.setNegativeButton("Cancel", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});

				dialog.show();

				return true;
			}
		});



		callMathAlarmScheduleService();

		alarmListAdapter = new AlarmListAdapter(AlarmFragment.this,getActivity());
		this.mathAlarmListView.setAdapter(alarmListAdapter);
		mathAlarmListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {
				v.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
				Alarm alarm = (Alarm) alarmListAdapter.getItem(position);
				/*Intent intent = new Intent(this, larmPreferencesActivity.class);
				intent.putExtra("alarm", alarm);
				startActivity(intent);*/
				Bundle bundle = new Bundle();
				Fragment fragment = new AlarmPreferencesFragment();
				bundle.putSerializable("alarm", alarm);
				fragment.setArguments(bundle);
				mCallback.onButtonClickEvent(fragment,EventMaker.SettingsFragment);
			}
		});

		return v;
	}
/*
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// requestWindowFeature(Window.FEATURE_NO_TITLE);


	}*/



	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);		
		menu.findItem(R.id.menu_item_save).setVisible(false);
		menu.findItem(R.id.menu_item_delete).setVisible(false);
	    return result;
	}*/
		
	@Override
	public void onPause() {
		// setListAdapter(null);
		AlarmDatabase.deactivateAlram();
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		updateAlarmList();
	}
	
	public void updateAlarmList(){
		//AlarmDatabase.init(larmActivity.this);
		final List<Alarm> alarms = AlarmDatabase.getAllAlarm();
		alarmListAdapter.setMathAlarms(alarms);
		
		getActivity().runOnUiThread(new Runnable() {
			public void run() {
				// reload content			
				AlarmFragment.this.alarmListAdapter.notifyDataSetChanged();
				if (alarms.size() > 0) {
					v.findViewById(android.R.id.empty).setVisibility(View.INVISIBLE);
					v.findViewById(R.id.titleMsg).setVisibility(View.VISIBLE);
				} else {
					v.findViewById(android.R.id.empty).setVisibility(View.VISIBLE);
					v.findViewById(R.id.titleMsg).setVisibility(View.INVISIBLE);
				}
			}
		});
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.checkBox_alarm_active) {
			CheckBox checkBox = (CheckBox) v;
			//Switch checkBox = (Switch) v;
			Alarm alarm = (Alarm) alarmListAdapter.getItem((Integer) checkBox.getTag());
			alarm.setAlarmActive(checkBox.isChecked());
			AlarmDatabase.updateAlarm(alarm);
			AlarmFragment.this.callMathAlarmScheduleService();
			if (checkBox.isChecked()) {
				Toast.makeText(getActivity(), alarm.getTimeUntilNextAlarmMessage(), Toast.LENGTH_LONG).show();
			}
		}

	}

}