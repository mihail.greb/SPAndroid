package com.app.android.shadow.common.ui.data.core;

/**
 * This class defines the loader IDs used by this app. To avoid conflicts all IDs are negative,
 * because all IDs used by the  C2Call SDK  are always positive.
 */
public class LoaderHandlers
{
    public static final int CHATHISTORY = -10;
}
