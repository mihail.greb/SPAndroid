package com.app.android.shadow.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.android.shadow.common.ui.core.ExtraData;
import com.app.android.shadow.main.dbhelper.ShadowDB;
import com.app.android.shadow.main.login.UserLoginTask;
import com.app.android.shadow.main.tapmanager.settings.Modes;
import com.app.android.shadow.main.util.AlertDialogClass;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.R;
import com.app.android.shadow.common.ui.activities.LoginActivity;
import com.app.android.shadow.samplechat.SimpleChatMainActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FreeAppWayToAccess extends BaseActivity{

    int i = -1;
    Handler handler;
    Runnable r;
    String uName, uPassword;

    boolean isSafeMode = true;
    boolean isxTimes =false;
    boolean isPassRequired = false;

    public static boolean isFreeDecoyActive = false;

    EditText shadowName,password;
    TextView loginTxt;
    ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.free_app_waytoaccess);
        isFreeDecoyActive = true;
        uName = Shadow.pref.getString(Const.USERNAME, "");
        uPassword = Shadow.pref.getString(Const.PASSWORD, "");

        shadowName = (EditText)findViewById(R.id.shadowName);
        password = (EditText)findViewById(R.id.password);
        loginTxt = (TextView)findViewById(R.id.done);
        logo = (ImageView)findViewById(R.id.logo);
        loginTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginClicked();
            }
        });

        /*if(Shadow.isTestingOn) {
            shadowName.setText(uName);
            password.setText(uPassword);
        }*/

        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.action_login || id == EditorInfo.IME_NULL) {
                    loginClicked();
                    return true;
                }
                return false;
            }
        });

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shadowClick();
            }
        });

        isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
        if (!isSafeMode) {
            isPassRequired = Modes.getStatus(ShadowDB.UN_IS_ENTER_NAME_PASS);
            isxTimes = Modes.getStatus(ShadowDB.UN_IS_PASS_ON_X_TABS_FREE);

            if(!isPassRequired){
                shadowName.setVisibility(View.GONE);
                password.setVisibility(View.GONE);
                loginTxt.setVisibility(View.GONE);
            }
            if(isxTimes) {
                handler = new Handler();
                r = new Runnable() {
                    @Override
                    public void run() {
                        i = -1;
                    }
                };
            }
        }
    }

    protected void countClick() {
        if (i == (Integer.parseInt(Modes.getSavedTabs())-2)) {
            i = -1;
            handler.removeCallbacks(r);
              goToNextScreen();
        } else {
            i++;
            Logger.printLog(Logger.ERROR,"working", "" + i);
            if (i == 0) {
                handler.removeCallbacks(r);
                handler.postDelayed(r, 10000);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isFreeDecoyActive = false;
    }

    public static boolean isIsFreeDecoyActivity(){
        return isFreeDecoyActive;
    }

    private void shadowClick() {
        Logger.printLog(Logger.ERROR, "Shadow clicked");
        if(isxTimes) {
            Logger.printLog(Logger.ERROR,"login clicked now");
            countClick();
        }
    }

    private void loginClicked() {
        Logger.printLog(Logger.ERROR, "login clicked");
        if(isValid()){
            Logger.printLog(Logger.ERROR, "login clicked now");

            callApi(shadowName.getText().toString().trim().toLowerCase(),
                    password.getText().toString());
        }else {
            new AlertDialogClass(this).open(getResources().getString(R.string.wrong_user_password));
        }
    }

    private void callApi(String nameSt, String ageSt){
        UserLogin mSignUpTask = new UserLogin(this,nameSt,ageSt);
        mSignUpTask.setServiceTaskMapStrParams(APIServiceAsyncTask.login, getLoginParams(nameSt, ageSt));
        mSignUpTask.execute((Void) null);
    }

    class UserLogin extends UserLoginTask {
        String nameSt;
        String ageSt;
        AlertDialogClass adClass = null;
        protected UserLogin(Context mContext,String nameSt, String ageSt) {
            super(mContext, ageSt);
            this.nameSt = nameSt;
            this.ageSt = ageSt;
        }

        @Override
        protected void fireEvent() {
            goToNextScreen();
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
            //super.failure(jsonObj, serviceTaskType);
            failure("");
        }

        @Override
        protected void failure(String message) {
            //super.failure(message);
            if(adClass == null) {
                adClass = new AlertDialogClass(FreeAppWayToAccess.this);
                adClass.open(getResources().getString(R.string.wrong_user_password));
            }
        }
    }


    private Map<String, String> getLoginParams(String nameSt, String ageSt){
        Map<String, String> params = new HashMap<>();
        params.put("user[username]", nameSt.toLowerCase());
        params.put("user[password]", ageSt);
        params.put("app_name", Shadow.getAppCodeName().toString());
        return params;
    }

    private boolean isValid(){
      /*  Logger.printLog(Logger.ERROR,"user Name is "+ uName + "user pass is "+ uPassword);
        Logger.printLog(Logger.ERROR,"enter Name is "+ shadowName.getText().toString().trim()
                + "enter pass is "+ password.getText().toString().trim());
        if(shadowName.getText().toString().trim().toLowerCase().equalsIgnoreCase(uName)
                && password.getText().toString().trim().toLowerCase().equalsIgnoreCase(uPassword)
                ){
            return true;
        }*/
        if(shadowName != null && password != null && shadowName.getText().toString().trim().length()>0
                && password.getText().toString().length() > 7
                ){
            return true;
        }
        return false;
    }

    protected void goToNextScreen(){
        //SipMediator.initInstantPresenceSubsribeThread();
        try {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.putExtra(ExtraData.Login.MAIN_ACTIVITY, SimpleChatMainActivity.class);
            intent.putExtra(ExtraData.Login.PASS_LOGIN, false);
            intent.putExtra(ExtraData.Login.BY_GAME, true);
            startActivity(intent);
    /*    int a =SCCoreFacade.instance().autoLoginIfPossible(FreeAppWayToAccess.this,true,true);
        Logger.printLog(Logger.ERROR,"print result is "+a);
        startActivity(new Intent(this, SimpleChatMainActivity.class));*/
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        exittoApp();
    }
}
