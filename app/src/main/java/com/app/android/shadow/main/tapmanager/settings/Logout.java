package com.app.android.shadow.main.tapmanager.settings;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.R;


public class Logout extends BaseFragment
{

    //Header
    TextView title;
    @Override
    protected void setHeader(View v) {
        super.setHeader(v);
        title = (TextView) v.findViewById(R.id.title_head);
        title.setText(resources.getString(R.string.logout));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view =inflater.inflate(R.layout.shadow_close, container, false);
        TextView logout =(TextView)_view.findViewById(R.id.closeApp);
        logout.setText(resources.getString(R.string.logout));
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onButtonClickEvent((Fragment) null, EventMaker.Logout);
            }
        });
        setHeader(_view);
        return _view;
    }



}
