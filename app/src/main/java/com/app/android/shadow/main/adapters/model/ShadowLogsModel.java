package com.app.android.shadow.main.adapters.model;

/**
 * Created by rails-dev on 28/10/15.
 */
public class ShadowLogsModel {

    int id,user_id,to_user_id;
    String call_type,status,created_at,get_to_username,get_duration;

    public ShadowLogsModel(int id, int user_id, String call_type, String status, int to_user_id, String created_at, String get_to_username, String get_duration) {
        this.id = id;
        this.user_id = user_id;
        this.call_type = call_type;
        this.status = status;
        this.to_user_id = to_user_id;
        this.created_at = created_at;
        this.get_to_username = get_to_username;
        this.get_duration = get_duration;
    }

    public int getId() {
        return id;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getCall_type() {
        return call_type;
    }

    public String getStatus() {
        return status;
    }

    public int getTo_user_id() {
        return to_user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getGet_to_username() {
        return get_to_username;
    }

    public String getGet_duration() {
        return get_duration;
    }
}
