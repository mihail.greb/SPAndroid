package com.app.android.shadow.samplechat.chathistory;

import android.app.Fragment;

import com.app.android.shadow.common.ui.fragments.ChatHistoryFragment;
import com.app.android.shadow.R;
import com.c2call.sdk.pub.activities.SCBoardFragmentActivity;

/**
 * This Activity shows a simple chat overview, i.e. a Board where the message list is grouped by chat partners
 */
public class ChatHistoryActivity extends SCBoardFragmentActivity
{
    @Override
    protected Fragment onCreateBoardFragment(String userid)
    {
        return ChatHistoryFragment.create(R.layout.app_chathistory);
    }
}
