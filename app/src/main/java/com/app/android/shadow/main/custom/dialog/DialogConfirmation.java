package com.app.android.shadow.main.custom.dialog;

        import com.c2call.sdk.R.string;
        import com.c2call.sdk.pub.gui.core.common.SCListModus;
        import com.c2call.sdk.pub.gui.core.events.SCListModusChangedEvent;
        import com.c2call.sdk.pub.gui.dialog.BaseItemRunnable;
        import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
        import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog.Builder;
        import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemController;

public class DialogConfirmation {
    public DialogConfirmation() {
    }

    public static SCChoiceDialog build(IFriendListItemController controller) {
        Builder builder = new Builder(controller.getContext());
        builder.addItem(string.sc_dlg_ctx_contact_delete_title, string.sc_dlg_ctx_contact_delete_summary, com.c2call.sdk.R.drawable.ic_sc_content_discard_grey, new DialogConfirmation.DeleteModusRunnable(controller));
        return builder.build();
    }

    public static class DeleteModusRunnable extends BaseItemRunnable<IFriendListItemController> {
        public DeleteModusRunnable(IFriendListItemController controller) {
            super(controller);
        }

        public void run() {
            ((IFriendListItemController)this.getController()).fireEvent(new SCListModusChangedEvent(SCListModus.Delete));
        }
    }

}

