package com.app.android.shadow.main;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import com.app.android.shadow.game.calculator.Calculator;
import com.app.android.shadow.game.puzzle.GamePlay;
import com.app.android.shadow.main.dbhelper.ShadowDB;
import com.app.android.shadow.R;
import com.app.android.shadow.game.alarm.AlarmMainActivity;
import com.app.android.shadow.main.util.Const;

/*
import org.acra.*;
import org.acra.annotation.*;

@ReportsCrashes(
        formUri = "https://collector.tracepot.com/5ccc9fea"
)*/

public class Shadow extends Application {
    public static final boolean isLogcatAllow = true;
    public static boolean isFirstRun;
    public static ShadowDB shadowDB;
    public static final String TAG = Shadow.class.getSimpleName();

    private static Context context;
    private static Activity activity;

   // private RequestQueue mRequestQueue;
   // private ImageLoader mImageLoader;

    private static Shadow mInstance;

    /*Shared Prefrence*/
    public static final String AppPref = TAG;
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;

    public static final String [] appsNames={
            "spandroid","ypandroid","acandroid","caandroid","tiandroid"
    };
    private static String proPackageName =  "com.pro.android.shadow";

    public static String getProPackageName() {
        return proPackageName;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Shadow.context = getApplicationContext();
        // ACRA.init(this);

        isFirstRun = true;
        mInstance = this;
        // initialized in application onCreate

        getAppPreferences();
        getAppPreferencesEditor();
        shadowDB = new ShadowDB(context);
        shadowDB.callReadable();
        //SdCardManager.createDirectory();
    }

    public static ShadowDB getDBInstance(){
        if(shadowDB == null){
            shadowDB = new ShadowDB(context);
        }
        return shadowDB;
    }

    public static String getpackageNameAsStr(){
        return Shadow.context.getPackageName();
    }

    public static String getAppNameAsStr(){
        return context.getString(context.getApplicationInfo().labelRes);
    }

    public static SharedPreferences getAppPreferences() {
        pref = Shadow.context.getSharedPreferences(TAG, MODE_PRIVATE);
        return pref;
    }

    public static SharedPreferences.Editor getAppPreferencesEditor() {
        SharedPreferences pref = Shadow.getAppPreferences();
        editor = pref.edit();
        return editor;
    }

    public static Context getAppContext() {
        return Shadow.context;
    }

    public static Activity getCurrentActivity() {
        return Shadow.activity;
    }

    public static Activity setCurrentActivity(Activity _activity) {
        return Shadow.activity =_activity;
    }

    public static void alertToast(String massage){
        Toast.makeText(Shadow.getAppContext(), massage,
                Toast.LENGTH_SHORT).show();
    }

    public static Class getProActivity(){
        String appName= Shadow.getAppPreferences().getString(Const.APP_NAME,"");
        if(appName.equals(appsNames[1])){
            return GamePlay.class;
        }else if(appName.equals(appsNames[2])){
            return AlarmMainActivity.class;
        }else if(appName.equals(appsNames[3])){
            return Calculator.class;
        }/*else if(appName.equals(appsNames[4])){
            return TicMainActivity.class;
        }*/else {
            Shadow.getAppPreferencesEditor().putString(Const.APP_NAME,appsNames[1]).commit();
            return GamePlay.class;
        }
    }

    public static boolean isYellowPuzzle(){
        if(isProApp() && getProActivity() == GamePlay.class){
            return false;
        }
        return false;
    }

    public static boolean isCalculator(){
        if(isProApp() && getProActivity() == Calculator.class){
            return true;
        }
        return false;
    }

    public static boolean isAlarmGame(){
        if(isProApp() && getProActivity() == AlarmMainActivity.class){
            return true;
        }
        return false;
    }

    public static Class getWayToAccessCls(){
        if(isProApp()){
            return getProActivity();
        }
        return FreeAppWayToAccess.class;
    }

    public static String getAppName(){
        if(isProApp()){
            if(getProActivity() == GamePlay.class){
                return getAppContext().getResources().getString(R.string.yellow_puzzle);
            }else if(getProActivity() == AlarmMainActivity.class){
                return getAppContext().getResources().getString(R.string.alarm_clock);
            }else if(getProActivity() == Calculator.class){
                return getAppContext().getResources().getString(R.string.calculator);
            }/*else if(getProActivity() == TicMainActivity.class){
                return getAppContext().getResources().getString(R.string.tic_tac_toe);
            }*/
            return getAppContext().getResources().getString(R.string.yellow_puzzle);
        }else {
            return getAppContext().getResources().getString(R.string.shadow_phone);
        }
    }

    public static String getAppCodeName(){
        if(isProApp()){
            if(getProActivity() == GamePlay.class){
                return appsNames[1];
            }else if(getProActivity() == AlarmMainActivity.class){
                return appsNames[2];
            }else if(getProActivity() == Calculator.class){
                return appsNames[3];
            }/*else if(getProActivity() == TicMainActivity.class){
                return appsNames[4];
            }*/else {
                Shadow.getAppPreferencesEditor().putString(Const.APP_NAME, appsNames[1]).commit();
                return appsNames[1];
            }
        }else {
            return appsNames[0];
        }
    }

    public static String getAppVersionName(){
        if(isProApp()){
            if(getProActivity() == GamePlay.class){
                return getAppContext().getResources().getString(R.string.yellow_puzzle_version);
            }else if(getProActivity() == AlarmMainActivity.class){
                return getAppContext().getResources().getString(R.string.alarm_clock_version);
            }else if(getProActivity() == Calculator.class){
                return getAppContext().getResources().getString(R.string.calculator_version);
            }/*else if(getProActivity() == TicMainActivity.class){
                return getAppContext().getResources().getString(R.string.tic_tac_toe_version);
            }*/
            return getAppContext().getResources().getString(R.string.yellow_puzzle_version);
        }else {
            return getAppContext().getResources().getString(R.string.shadow_phone_version);
        }
    }

    public static String getGamePackageName(String appName){
        if(isProApp()) {
            if (appName.equals(appsNames[1])) {
                return Shadow.getpackageNameAsStr()+".main.SetFlowActivity-yp";
            } else if (appName.equals(appsNames[2])) {
                return Shadow.getpackageNameAsStr()+".main.SetFlowActivity-ac";
            } else if (appName.equals(appsNames[3])) {
                return Shadow.getpackageNameAsStr()+".main.SetFlowActivity-cal";
            } else if (appName.equals(appsNames[4])) {
                return Shadow.getpackageNameAsStr()+".main.SetFlowActivity-ttt";
            } else {
                Shadow.getAppPreferencesEditor().putString(Const.APP_NAME, appsNames[1]).commit();
                return Shadow.getpackageNameAsStr()+".main.SetFlowActivity-yp";
            }
        }else {
            return Shadow.getpackageNameAsStr()+".main.SetFlowActivity-sp";
        }
    }

    public static Bitmap getAppImage(){
        String appName =getCurrentAppRun();
        if(isProApp()) {
            if (appName.equals(appsNames[1])) {
                return getDrawabletoBitmap(R.drawable.yellow_puzzle);
            } else if (appName.equals(appsNames[2])) {
                return getDrawabletoBitmap(R.drawable.alarm_logo2);
            } else if (appName.equals(appsNames[3])) {
                return getDrawabletoBitmap(R.drawable.calculator_logo);
            } else if (appName.equals(appsNames[4])) {
                return getDrawabletoBitmap(R.drawable.tic_bola);
            } else {
                Shadow.getAppPreferencesEditor().putString(Const.APP_NAME, appsNames[1]).commit();
                return getDrawabletoBitmap(R.drawable.ic_launcher);
            }
        }else {
            return getDrawabletoBitmap(R.drawable.ic_launcher);
        }
    }

    public static int getFirstTimeMessage() {
        int firstTimeMessage = 0;
        String appName =getCurrentAppRun();
        if(Shadow.isProApp()){
            if (appName.equals(appsNames[1])) {
                firstTimeMessage = R.string.first_time_login_dialog_yp;
            } else if (appName.equals(appsNames[2])) {
                firstTimeMessage = R.string.first_time_login_dialog_ac;
            } else if (appName.equals(appsNames[3])) {
                firstTimeMessage = R.string.first_time_login_dialog_cal;
            } else if (appName.equals(appsNames[4])) {
                firstTimeMessage = R.string.first_time_login_dialog_ttt;
            } else {
                firstTimeMessage = R.string.first_time_login_dialog_default;
            }
        }else {
            firstTimeMessage = R.string.first_time_login_dialog_free;
        }
        return firstTimeMessage;
    }

    public static Bitmap getDrawabletoBitmap(int drawableId){
        try {
            return BitmapFactory.decodeResource(context.getResources(),drawableId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //public static boolean isTestingOn = false;
    public static boolean isProApp(){
        return false;
    }

    public static String getCurrentAppRun(){
        //return appsNames[1];
        return Shadow.getAppPreferences().getString(Const.APP_NAME, "");
    }

    /*
    To changes
    1. isProApp false/ture
    2. Change Icon
    3. change App name
    4. change app_short_name
    5. change app_short_name2
    6. In String change Project number by this string sc_google_api_project_id
    7. Change receiver package name in manifest file
    * */
    /*<string name="app_short_name2"></string>
    <!--<string name="app_short_name2">spandroid</string>-->*/

    /*
Change Package Doc.
Note :- Don't use clean project, Rebuild project or run app.
0.  ProApp false/true (in Shadow.java)
1.  Create a new package
2.  Create a dummy R file in current package in new package name (Click Do refector)
3.  Drag&Drop all packages in new package with (every thing option)
4.  Change package name in build.gradle files
5.  Change the productFlavors in build.gradle files
5.  Change package name in manifest at multiple places by find replace
6.  Change API key of GCM in manifest.
7.  Delete build folder
8.  Delete new R file from folder not in IDE
9.  In String change Project number by this string sc_google_api_project_id
10. Change api key and secret.
11. Restart studio with invalidate and restart option.
    * */

}
