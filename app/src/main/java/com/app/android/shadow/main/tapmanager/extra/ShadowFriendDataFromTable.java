package com.app.android.shadow.main.tapmanager.extra;


public class ShadowFriendDataFromTable {

    String city, zip,country, custom_status,street,did, dipslay_name, email, favorite, firstname,group_info,
            _id, image_large, image_small, language, lastname, own_number,  custom_status_time, mod_time, status, usertype,confirmed;


    public ShadowFriendDataFromTable(String city, String zip, String country, String custom_status, String street, String did, String dipslay_name, String email, String favorite, String firstname, String group_info, String _id, String image_large, String image_small, String language, String lastname, String own_number, String custom_status_time, String mod_time, String status, String usertype, String confirmed) {

        this.city = city;
        this.zip = zip;
        this.country = country;
        this.custom_status = custom_status;
        this.street = street;
        this.did = did;
        this.dipslay_name = dipslay_name;
        this.email = email;
        this.favorite = favorite;
        this.firstname = firstname;
        this.group_info = group_info;
        this._id = _id;
        this.image_large = image_large;
        this.image_small = image_small;
        this.language = language;
        this.lastname = lastname;
        this.own_number = own_number;
        this.custom_status_time = custom_status_time;
        this.mod_time = mod_time;
        this.status = status;
        this.usertype = usertype;
        this.confirmed = confirmed;
    }

    public String getCity() {
        return city;
    }

    public String getZip() {
        return zip;
    }

    public String getCountry() {
        return country;
    }

    public String getCustom_status() {
        return custom_status;
    }

    public String getStreet() {
        return street;
    }

    public String getDid() {
        return did;
    }

    public String getDipslay_name() {
        return dipslay_name;
    }

    public String getEmail() {
        return email;
    }

    public String getFavorite() {
        return favorite;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getGroup_info() {
        return group_info;
    }

    public String get_id() {
        return _id;
    }

    public String getImage_large() {
        return image_large;
    }

    public String getImage_small() {
        return image_small;
    }

    public String getLanguage() {
        return language;
    }

    public String getLastname() {
        return lastname;
    }

    public String getOwn_number() {
        return own_number;
    }

    public String getCustom_status_time() {
        return custom_status_time;
    }

    public String getMod_time() {
        return mod_time;
    }

    public String getStatus() {
        return status;
    }

    public String getUsertype() {
        return usertype;
    }

    public String getConfirmed() {
        return confirmed;
    }
}
