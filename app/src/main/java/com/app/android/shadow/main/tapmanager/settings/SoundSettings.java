package com.app.android.shadow.main.tapmanager.settings;

import android.content.Intent;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.R;
import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.main.util.Logger;


/**
 * Custom Board Fragment to show a chat history grouped by users
 */
public class SoundSettings extends BaseFragment implements View.OnClickListener
{
    MoreSignature controller;
    String users;
    String tapTag;
    Ringtone _ringtone = null;
    ImageView ringPlay;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view =inflater.inflate(R.layout.shadow_more_sound_settings, container, false);
        setHeader(_view);
        Bundle bundle = getArguments();
        if(bundle != null) {
            controller = (MoreSignature) bundle.get(Const.VALUE);
            users = bundle.getString(Const.USER, Const.DEFAULT_USER_ID);
            tapTag = bundle.getString(Const.TAGS);
        }
        _view.findViewById(R.id.choose).setOnClickListener(this);
        _view.findViewById(R.id.selected).setOnClickListener(this);
        _view.findViewById(R.id.reset).setOnClickListener(this);
        ringPlay = (ImageView) _view.findViewById(R.id.play_selected);
        ringPlay.setOnClickListener(this);
        mCallback.addToIncressClickArea(ringPlay);
        return _view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.choose:
                BaseActivity.setSaveTime0();
                BaseSampleActivity.isAppNotClose = true;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("audio/*");
                startActivityForResult(intent, 200);
                break;
            case R.id.selected:

                break;
            case R.id.play_selected:
                if(_ringtone != null && _ringtone.isPlaying()){
                    _ringtone.stop();
                    ringPlay.setImageResource(R.drawable.arror_white);
                }else {
                    String ring = Shadow.shadowDB.getRingTone(users, controller, (users.equals(Const.DEFAULT_USER_ID)) ? true : false);
                    Logger.printLog(Logger.ERROR,"ring to play", "is " + ring);
                    if (ring != null) {
                        _ringtone = RingtoneManager.getRingtone(getActivity(), Uri.parse(ring));
                        if (_ringtone != null) {
                            _ringtone.play();
                            ringPlay.setImageResource(R.drawable.play_ring);
                        }
                        //new MediaPlayer().create(getActivity(), Uri.parse(ring));
                    } else
                        Toast.makeText(getActivity(), "No ring for this user", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.reset:
                Shadow.shadowDB.resetRingtone(users, controller,(users.equals(Const.DEFAULT_USER_ID))?true:false);
                Shadow.alertToast("Reset Successfully");
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        BaseSampleActivity.isAppNotClose = false;
        Logger.printLog(Logger.ERROR,"Result code", "" + resultCode);
        Logger.printLog(Logger.ERROR,"Request code",""+requestCode);
        if(requestCode == 200 && data != null){
            Uri selectedAudio = data.getData();
            String[] filePathColumn = { MediaStore.Audio.Media.DATA };
            Cursor cursor = getActivity().getContentResolver().query(selectedAudio, filePathColumn, null, null, null);
            if(cursor.getCount()>0) {
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();
                //String filePath = data.getData().getPath();
                Logger.printLog(Logger.ERROR,"path", "" + filePath);
                Shadow.shadowDB.addRingtone(users, filePath, controller, (users.equals(Const.DEFAULT_USER_ID)) ? true : false);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(_ringtone != null && _ringtone.isPlaying())
            _ringtone.stop();
    }

    //Header
    TextView title;
    @Override
    protected void setHeader(View v) {
        super.setHeader(v);
        title = (TextView) v.findViewById(R.id.title_head);
        title.setText(resources.getString(R.string.ringtone));
        v.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }
}
