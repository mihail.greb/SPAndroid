package com.app.android.shadow.main.IntroScreens;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.R;

public class IntroRegistration extends BaseFragment {

    public static IntroRegistration create(){
        IntroRegistration frag = new IntroRegistration();
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.shadow_intro_registration,container,false);

        return view;
    }
}
