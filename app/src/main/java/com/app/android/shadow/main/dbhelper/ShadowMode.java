package com.app.android.shadow.main.dbhelper;

import com.c2call.sdk.pub.db.data.SCBaseData;
import com.c2call.sdk.pub.db.util.core.DatabaseHelper;
import com.c2call.sdk.pub.db.util.core.DefaultSortOrder;
import com.c2call.sdk.pub.db.util.core.ObservableDao;
import com.c2call.sdk.pub.db.util.core.UriPaths;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.simpleframework.xml.Root;

@DatabaseTable(
        tableName = "shadow_mode"
)
@UriPaths({"modes", "modes/#"})
@DefaultSortOrder("user ASC")
@Root(
        strict = false
)


public class ShadowMode extends SCBaseData<Long> {
    public static final String ENTITY = "mode";
    public static final String ENTITY_PL = "modes";
    public static final String USER="user";
    public static final String CURRENT_MODE="current_mode";
    public static final String VIBRATE_FOR_CALL="vibrate_for_call";
    public static final String VIBRATE_FOR_CHAT="vibrate_for_chat";
    public static final String SHAKE_LOGOUT="shake_logout";
    public static final String LOGOUT_TIME="logout_time";
    public static final String ONLINE_STATUS="online_status";
    public static final String UN_RINGTONE_FOR_CALL="un_ringtone_for_call";
    public static final String UN_VIBRATE_ON_CALL="un_vibrate_for_call";
    public static final String UN_SOUND_ON_CHAT="un_sound_for_chat";
    public static final String UN_VIBRATE_ON_CHAT="un_vibrate_for_chat";
    public static final String UN_SHAKE_LOGOUT="un_shake_logout";
    public static final String UN_LOGOUT_TIME="un_logout_time";
    public static final String UN_ONLINE_STATUS="un_online_status";
    public static final String[] PROJECTION_ALL = new String[]{"_id","user", "current_mode", "vibrate_for_call","vibrate_for_chat","shake_logout","logout_time","online_status","un_ringtone_for_call","un_vibrate_for_call","un_sound_for_chat","un_vibrate_for_chat","un_shake_logout","un_logout_time","un_online_status"};

    @DatabaseField(
            columnName = "_id",
            generatedId = true
    )
    private Long _id;

    @DatabaseField(
            columnName = "user",
            unique = true
    )
    private String _user;

    @DatabaseField(
            columnName = "current_mode",
            canBeNull = false
    )
    private String _current_mode;

    @DatabaseField(
            columnName = "vibrate_for_call",
            canBeNull = true
    )
    private String _vibrate_for_call;

    @DatabaseField(
            columnName = "vibrate_for_chat",
            canBeNull = true
    )
    private String _vibrate_for_chat;

    @DatabaseField(
            columnName = "shake_logout",
            canBeNull = true
    )
    private String _shake_logout;

    @DatabaseField(
            columnName = "logout_time",
            canBeNull = true
    )
    private String _logout_time;

    @DatabaseField(
            columnName = "online_status",
            canBeNull = true
    )
    private String _online_status;

    @DatabaseField(
                columnName = "un_ringtone_for_call",
                canBeNull = true
        )
        private String _un_ringtone_for_call;

    @DatabaseField(
                columnName = "un_vibrate_for_call",
                canBeNull = true
        )
        private String _un_vibrate_for_call;

    @DatabaseField(
                    columnName = "un_sound_for_chat",
                    canBeNull = true
            )
            private String _un_sound_for_chat;


    @DatabaseField(
                        columnName = "un_vibrate_for_chat",
                        canBeNull = true
                )
                private String _un_vibrate_for_chat;


    @DatabaseField(
                            columnName = "un_shake_logout",
                            canBeNull = true
                    )
                    private String _un_shake_logout;


    @DatabaseField(
                                columnName = "un_logout_time",
                                canBeNull = true
                        )
                        private String _un_logout_time;

     @DatabaseField(
                                columnName = "un_online_status",
                                canBeNull = true
                        )
                        private String _un_online_status;


    public ShadowMode() {
    }


    public ShadowMode(Long _id, String _user, String _current_mode, String _vibrate_for_call, String _vibrate_for_chat, String _shake_logout, String _logout_time, String _online_status, String _un_ringtone_for_call, String _un_vibrate_for_call, String _un_sound_for_chat, String _un_vibrate_for_chat, String _un_shake_logout, String _un_logout_time, String _un_online_status) {
        this._id = _id;
        this._user = _user;
        this._current_mode = _current_mode;
        this._vibrate_for_call = _vibrate_for_call;
        this._vibrate_for_chat = _vibrate_for_chat;
        this._shake_logout = _shake_logout;
        this._logout_time = _logout_time;
        this._online_status = _online_status;
        this._un_ringtone_for_call = _un_ringtone_for_call;
        this._un_vibrate_for_call = _un_vibrate_for_call;
        this._un_sound_for_chat = _un_sound_for_chat;
        this._un_vibrate_for_chat = _un_vibrate_for_chat;
        this._un_shake_logout = _un_shake_logout;
        this._un_logout_time = _un_logout_time;
        this._un_online_status = _un_online_status;
    }

    public static ObservableDao<ShadowMode, Long> dao() {
        return DatabaseHelper.getDefaultDao(ShadowMode.class);
    }

    public Long getId() {
        return this._id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    public String get_user() {
        return _user;
    }

    public void set_user(String _user) {
        this._user = _user;
    }

    public String get_current_mode() {
        return _current_mode;
    }

    public void set_current_mode(String _current_mode) {
        this._current_mode = _current_mode;
    }

    public String get_vibrate_for_call() {
        return _vibrate_for_call;
    }

    public void set_vibrate_for_call(String _vibrate_for_call) {
        this._vibrate_for_call = _vibrate_for_call;
    }

    public String get_vibrate_for_chat() {
        return _vibrate_for_chat;
    }

    public void set_vibrate_for_chat(String _vibrate_for_chat) {
        this._vibrate_for_chat = _vibrate_for_chat;
    }

    public String get_shake_logout() {
        return _shake_logout;
    }

    public void set_shake_logout(String _shake_logout) {
        this._shake_logout = _shake_logout;
    }

    public String get_logout_time() {
        return _logout_time;
    }

    public void set_logout_time(String _logout_time) {
        this._logout_time = _logout_time;
    }

    public String get_online_status() {
        return _online_status;
    }

    public void set_online_status(String _online_status) {
        this._online_status = _online_status;
    }

    public String get_un_ringtone_for_call() {
        return _un_ringtone_for_call;
    }

    public void set_un_ringtone_for_call(String _un_ringtone_for_call) {
        this._un_ringtone_for_call = _un_ringtone_for_call;
    }

    public String get_un_vibrate_for_call() {
        return _un_vibrate_for_call;
    }

    public void set_un_vibrate_for_call(String _un_vibrate_for_call) {
        this._un_vibrate_for_call = _un_vibrate_for_call;
    }

    public String get_un_sound_for_chat() {
        return _un_sound_for_chat;
    }

    public void set_un_sound_for_chat(String _un_sound_for_chat) {
        this._un_sound_for_chat = _un_sound_for_chat;
    }

    public String get_un_vibrate_for_chat() {
        return _un_vibrate_for_chat;
    }

    public void set_un_vibrate_for_chat(String _un_vibrate_for_chat) {
        this._un_vibrate_for_chat = _un_vibrate_for_chat;
    }

    public String get_un_shake_logout() {
        return _un_shake_logout;
    }

    public void set_un_shake_logout(String _un_shake_logout) {
        this._un_shake_logout = _un_shake_logout;
    }

    public String get_un_logout_time() {
        return _un_logout_time;
    }

    public void set_un_logout_time(String _un_logout_time) {
        this._un_logout_time = _un_logout_time;
    }

    public String get_un_online_status() {
        return _un_online_status;
    }

    public void set_un_online_status(String _un_online_status) {
        this._un_online_status = _un_online_status;
    }
}
