package com.app.android.shadow.main.apicalls;



import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.main.util.CheckNetwork;
import com.app.android.shadow.main.util.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class APIServiceAsyncTask extends AsyncTask<Void, Integer, Object> {

    public static final int LOGIN =1,REQUEST_LIST=2,ACCEPT_FRIEND_REQUEST=3,
            DECLINE_FRIENDS_REQUEST=4, CALL_HISTORY_GET =5,GET_FRIENDS_LIST=6,SEARCH_CONTACTS=7,
            REGISTRATION =8,LOGOUT=9,FETCH_USER_INFO_NUM=10,SEND_REQ_TO_FRIENDS=11,BLOCK_USER=12,
            BLOCK_USER_LIST=13, BLOCK_USER_UNBLOCK =14,REMOVE_FRND_TO_FRND_LIST=15,PRO_APP_LINK=16,
            CALL_TIME_LEFT=17,CALL_HISTORY_POST =18,DELETE_ALL_DATA =19,CALL_HISTORY_SEEN =20,CALL_HISTORY_SEEN_USER =24,
            ONLINE_USER=21,OFFLINE_USER=22,ONLINE_STATUS_OF_USER=23,AWAIT_REQUEST=24,APP_NAME=25,UNREGISTRATION=26,INVITAION=27;

    public static final ApiRequest login = new ApiRequest("sessions/sign_in", ServiceHandler.POST, false,false,false,LOGIN);
    public static final ApiRequest registration = new ApiRequest("registrations", ServiceHandler.POST, false,false,false,REGISTRATION);
    public static final ApiRequest unregistration = new ApiRequest("registrations", ServiceHandler.DELETE, false,false,true,UNREGISTRATION);
    public static final ApiRequest logout = new ApiRequest("sessions/sign_out", ServiceHandler.DELETE, true,false,false,LOGOUT);
    public static final ApiRequest fetchUserInfoNum = new ApiRequest("search_contacts", ServiceHandler.POST, false,false,false,FETCH_USER_INFO_NUM);
    public static final ApiRequest sendRequesttoFriend = new ApiRequest("send_friend_request", ServiceHandler.POST, true,false,false,SEND_REQ_TO_FRIENDS);
    public static final ApiRequest getFrndList = new ApiRequest("friends", ServiceHandler.GET, true,false,true,GET_FRIENDS_LIST);
    public static final ApiRequest searchContacts = new ApiRequest("search_contacts", ServiceHandler.POST, true,false,false,SEARCH_CONTACTS);
    public static final ApiRequest requestList = new ApiRequest("requested_friends", ServiceHandler.GET, true,false,false,REQUEST_LIST);
    public static final ApiRequest acceptFrndReq = new ApiRequest("accept_friend_request", ServiceHandler.POST, true,false,false,ACCEPT_FRIEND_REQUEST);
    public static final ApiRequest declineFrndReq = new ApiRequest("decline_friend_request", ServiceHandler.POST, true,false,false,DECLINE_FRIENDS_REQUEST);
    public static final ApiRequest callHistorysGet = new ApiRequest("call_histories", ServiceHandler.GET, true,false,false, CALL_HISTORY_GET);
    public static final ApiRequest callHistorysPost = new ApiRequest("call_histories", ServiceHandler.POST, true,false,false, CALL_HISTORY_POST);
    public static final ApiRequest blockUser = new ApiRequest("block_users", ServiceHandler.POST, true,false,false,BLOCK_USER);
    public static final ApiRequest blockUserlIST = new ApiRequest("block_users", ServiceHandler.GET, true,false,false,BLOCK_USER_LIST);
    public static final ApiRequest blockUserUnblock = new ApiRequest("block_users/unblock", ServiceHandler.DELETE, true,false,false, BLOCK_USER_UNBLOCK);
    public static final ApiRequest removeFrndtoFrndList = new ApiRequest("remove_friend", ServiceHandler.POST, true,false,false,REMOVE_FRND_TO_FRND_LIST);
    public static final ApiRequest proAppLink = new ApiRequest("pro_url", ServiceHandler.GET, true,false,false,PRO_APP_LINK);
    public static final ApiRequest callTimeLeft = new ApiRequest("time_left", ServiceHandler.GET, true,false,false,CALL_TIME_LEFT);
    public static final ApiRequest deleteAllData = new ApiRequest("users", ServiceHandler.DELETE, true,false,false,DELETE_ALL_DATA);
    public static final ApiRequest callHistorysSeen = new ApiRequest("call_histories/seen", ServiceHandler.PUT, true,false,true,CALL_HISTORY_SEEN);
    public static final ApiRequest setUserOnlien = new ApiRequest("users/online", ServiceHandler.PUT, true,false,true,ONLINE_USER);
    public static final ApiRequest setUserOfflien= new ApiRequest("users/offline", ServiceHandler.PUT, true,false,true,OFFLINE_USER);
    public static final ApiRequest callAwaitRequest = new ApiRequest("awaiting_request", ServiceHandler.GET, true,false,true,AWAIT_REQUEST);
    public static final ApiRequest getAppName = new ApiRequest("app_name", ServiceHandler.GET, true,false,true,APP_NAME);
    //public static final ApiRequest getUserOnlineStatus = new ApiRequest("users/show_status/", ServiceHandler.GET, true,false,false,ONLINE_STATUS_OF_USER);
    public static final ApiRequest emailInvitations = new ApiRequest("invitations", ServiceHandler.POST, true,false,true,INVITAION);

    private Map<String, String> serviceParamsMap = null;
    private Map<String, Object> serviceParamsObjectMap = null;

    private ApiRequest apiRequest = null;
    private ProgressDialog mDialog = null;
    Context mContext;
    private int mTries = 0;

    public APIServiceAsyncTask(Context mContext) {
        this.mContext = mContext;
    }

    public APIServiceAsyncTask(Context mContext,ApiRequest apiRequest,Map<String, String> serviceParamsMap) {
        this.mContext = mContext;
        this.apiRequest = apiRequest;
        this.serviceParamsMap = serviceParamsMap;
    }

    public void setServiceTaskMapStrParams(ApiRequest apiRequest,Map<String, String> serviceParamsMap){
        this.apiRequest = apiRequest;
        this.serviceParamsMap = serviceParamsMap;
    }

    public void setServiceTaskMapObjParams(ApiRequest apiRequest,Map<String, Object> serviceParamsObjectMap){
        this.apiRequest = apiRequest;
        this.serviceParamsObjectMap = serviceParamsObjectMap;
    }

    public void showProgress(boolean isShow) {
            if (isShow) {
                mDialog = new ProgressDialog(mContext);
                mDialog.setMessage("Please Wait...");
                mDialog.setCancelable(false);
                if(!((Activity) mContext).isFinishing()) {
                    if (mDialog != null && (!mDialog.isShowing())) {
                        try {
                            mDialog.show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                if(mDialog != null && mDialog.isShowing()) {
                    mDialog.hide();
                }
            }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(CheckNetwork.isNetworkAvailable(mContext)) {
            /*if(mContext instanceof BaseActivity) {
                mTries = 0 ;
                ((BaseActivity)mContext).mDeviceBandwidthSampler.startSampling();
            }*/
            if(!apiRequest.noProgressBar)
                try {
                    showProgress(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }else {
            CheckNetwork.ShowDialog(mContext);
            APIServiceAsyncTask.this.cancel(true);
        }
    }

    @Override
    protected Object doInBackground(Void... params) {

            try {
                if (apiRequest != null) {
                    ServiceHandler sh = new ServiceHandler();
                    JSONObject jsonObj = null;
                    // Making a request to url and getting response
                    if (apiRequest.isDownload) {
                        //jsonStr = sh.makeDownloadCall(MediaDownloadURLKey, MediaSavePathKey,serviceParamsMap).toString();
                    //} else if (apiRequest.isUpload) {
                        //jsonStr = sh.makeUploadCall();
                    } else {
                        jsonObj = sh.makeServiceCall(apiRequest, serviceParamsMap);
                    }
                    if (jsonObj != null) {
                        Logger.printLog(Logger.ERROR,"Working", "Json Object not null");
                        return jsonObj;
                    } else {
                        //Shadow.alertToast("Exception : Couldn't get any data from the url");
                        Logger.printLog(Logger.ERROR,"Exception", "Couldn't get any data from the url");
                        failure("Couldn't get any data from the url");
                        return null;
                    }
                } else {
                    //Shadow.alertToast("Exception : Something went wrong");
                    Logger.printLog(Logger.ERROR,"Exception", "Something went wrong");
                    failure("Something went wrong");
                    return null;
                }

            } catch (Exception e) {
                //Shadow.alertToast("Exception" + e.getMessage());
                //Logger.printLog(Logger.ERROR,"Exception", e.getMessage());
                e.printStackTrace();
                return null;
            }

    }

    @Override
    protected void onPostExecute(Object o) {
        //super.onPostExecute(o);
        showProgress(false);
        /*if(mContext instanceof BaseActivity) {
            ((BaseActivity)mContext).mDeviceBandwidthSampler.stopSampling();
        }*/
        if(o instanceof JSONObject){
            Logger.printLog(Logger.ERROR,"print instanceof ","json");
            JSONObject jsonObj = (JSONObject)o;
            try {
                Logger.printLog(Logger.ERROR,"print jsonobject ","is "+jsonObj.toString());
                if(jsonObj.getBoolean("success")){
                    this.success(jsonObj,apiRequest.reqID);
                    Logger.printLog(Logger.ERROR,"Success", "Goto success block");
                }else{
                    Log.i("Failure", "Goto failure block");
                    String msg = "";
                    if(jsonObj.has(Const.INFO)){
                        msg=jsonObj.getString(Const.INFO);
                    }else if(jsonObj.has(Const.ERRORS)){
                        msg=jsonObj.getString(Const.ERRORS);
                    }
                    this.failure(msg);
                    this.failure(jsonObj, apiRequest.reqID);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(o == null){

        }

        onFinished(apiRequest.reqID);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        showProgress(false);
        Shadow.alertToast("Please retry");
    }

    protected void success(JSONObject jsonObj,int serviceTaskType) {

    }

    protected void failure(String message) {
        Toast.makeText(mContext,message,Toast.LENGTH_LONG).show();
    }

    protected void failure(JSONObject jsonObj,int serviceTaskType) {

    }

    protected void onFinished(int serviceTaskType) {

    }


}