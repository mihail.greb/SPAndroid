package com.app.android.shadow.common.ui.dialogs;

import android.content.Intent;

import com.c2call.sdk.pub.core.SCDownloadType;
import com.c2call.sdk.pub.gui.core.controller.IController;
import com.c2call.sdk.pub.gui.dialog.BaseItemRunnable;
import com.c2call.sdk.pub.gui.dialog.DialogBuilderVideo;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.util.MediaUtil;

import java.util.UUID;

/**
 * Created by rails-dev on 31/12/15.
 */
public class ShadowDialogBuilderVideo extends DialogBuilderVideo {

    public static SCChoiceDialog build(IController<?> controller, String filename) {
        SCChoiceDialog.Builder builder = new SCChoiceDialog.Builder(controller.getContext());
        builder.addItem(com.c2call.sdk.R.string.sc_dlg_rich_message_take_video_title,
                com.c2call.sdk.R.string.sc_dlg_rich_message_take_video_summary,
                com.c2call.sdk.R.drawable.sc_device_access_video,
                new ShadowDialogBuilderVideo.CamRunnable(controller, filename))
                .addItem(com.c2call.sdk.R.string.sc_dlg_rich_message_existing_video_title, com.c2call.sdk.R.string.sc_dlg_rich_message_existing_video_summary,
                        com.c2call.sdk.R.drawable.sc_content_picture,
                        new ShadowDialogBuilderVideo.ExistingRunnable(controller));
        return builder.build();
    }

    public static class CamRunnable extends BaseItemRunnable<IController<?>> {
        private final String _filename;

        public CamRunnable(IController<?> controller, String filename) {
            super(controller);
            this._filename = filename;
        }

        public void run() {
            try {
                String e = UUID.randomUUID().toString().replace("-", "") + ".3gp";
                String output = MediaUtil.getMediaPath(SCDownloadType.Video, e, true);
                ShadowDialogBuilderPhoto.setCloseFalse();
                Intent intent = new Intent("android.media.action.VIDEO_CAPTURE");
                intent.putExtra("output", output);
                this.startActivityForResult(intent, 7);
            } catch (Exception var4) {
                var4.printStackTrace();
            }

        }
    }

    public static class ExistingRunnable extends BaseItemRunnable<IController<?>> {
        public ExistingRunnable(IController<?> controller) {
            super(controller);
        }

        public void run() {
            ShadowDialogBuilderPhoto.setCloseFalse();
            Intent intent = new Intent("android.intent.action.PICK");
            intent.setType("video/*");
            this.startActivityForResult(intent, 5);
        }
    }
}
