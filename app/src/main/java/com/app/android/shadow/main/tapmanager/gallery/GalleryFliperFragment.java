package com.app.android.shadow.main.tapmanager.gallery;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.dbhelper.CustomeC2callDB;
import com.app.android.shadow.main.provider.CustomC2CallContentProvider;
import com.app.android.shadow.R;
import com.app.android.shadow.main.adapters.GalleryFliperAdapter;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Custom Board Fragment to show a chat history grouped by users
 */
public class GalleryFliperFragment extends BaseFragment implements MediaPlayer.OnCompletionListener{

    int position;
    boolean isVideo;
    String url;
    VideoView videoView;
    MediaController mc;
    ArrayList<HashMap<String,String >> list = new ArrayList<HashMap<String,String >>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shadow_gallery_filper, container, false);
        setHeader(view);

        Bundle bundle = getArguments();
        if(bundle != null){
            isVideo= bundle.getBoolean("isVideo");
            position = bundle.getInt("Position");
            //url = bundle.getString("URI");
            list = CustomC2CallContentProvider.customeC2callDB.getMediaData(isVideo);
            if(isVideo){
                videoView = (VideoView)view.findViewById(R.id.play_video);
                videoView.setVisibility(View.VISIBLE);
                mc= new MediaController(getActivity());
                videoView.setVisibility(View.VISIBLE);
                videoView.setKeepScreenOn(true);
                videoView.setVideoURI(Uri.parse(list.get(position).get(CustomeC2callDB.LOC)));
                videoView.setMediaController(mc);
                videoView.setOnCompletionListener(this);
                videoView.start();
            }else {
                ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
                viewPager.setVisibility(View.VISIBLE);
                GalleryFliperAdapter adapter = new GalleryFliperAdapter(getActivity(),list);
                viewPager.setAdapter(adapter);
                viewPager.setCurrentItem(position);
            }
        }

        return view;
    }

    //Header
    TextView title,closeScreen,deleteItem;

    protected void setHeader(View v) {
        title = (TextView) v.findViewById(R.id.title_head);
        title.setText(resources.getString(R.string.shadow_gallery));
        v.findViewById(R.id.back).setVisibility(View.INVISIBLE);
        closeScreen = (TextView) v.findViewById(R.id.close_screen);
        closeScreen.setVisibility(View.VISIBLE);
        closeScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

/*        deleteItem = (TextView) v.findViewById(R.id.deleteItem);
        deleteItem.setVisibility(View.VISIBLE);
        deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCurrentItem();
            }
        });
        mCallback.addToIncressClickArea(closeScreen);*/
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }
}

/* private void deleteCurrentItem() {

        if(map != null){
            String id = map.get(Const._ID);
            if(id != null && id.length()>0){
                if(CustomC2CallContentProvider.customeC2callDB.deleteMediaData(id)){
                    try {
                        new File(map.get(CustomeC2callDB.LOC)).delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    onBackPressed();
                }
            }
        }else {
            Logger.printLog(Logger.ERROR,"Shadow map","is null");
        }
    }*/