package com.app.android.shadow.common.ui.shadow.custom;

import android.view.View;

import com.c2call.sdk.pub.gui.core.controller.IControllerRequestListener;
import com.c2call.sdk.pub.gui.core.controller.SCBaseController;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;


public class SCChatBoardBaseController extends SCBaseController<IChatBoardBaseViewHolder> implements IChatBoardBaseController {

    public SCChatBoardBaseController(View view, SCViewDescription viewDescription) {
        super(view, viewDescription);
    }

    public SCChatBoardBaseController(View view, SCViewDescription viewDescription, IControllerRequestListener requestListener) {
        super(view, viewDescription, requestListener);
    }

    @Override
    protected IChatBoardBaseViewHolder onCreateViewHolder(View view, SCViewDescription scViewDescription) {
        return new SCChatBoardBaseViewHolder(scViewDescription);
    }

    @Override
    protected void onBindViewHolder(IChatBoardBaseViewHolder iChatBoardBaseViewHolder) {

    }
}
