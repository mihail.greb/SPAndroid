package com.app.android.shadow.common.ui.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;

import com.app.android.shadow.common.ui.core.ExtraData;
import com.app.android.shadow.utils.BitmapUtil;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.storage.SCBitmapManager;
import com.app.android.shadow.R;

import java.io.IOException;

/**
 * This Activity is used to show a full screen image. The file path of the image must be
 * set by the extra data com.android.shadow.FULLSCREENIMAGE.PATH.
 */
public class FullScreenImageActiviy extends Activity
{
    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.app_full_image);

        final ImageView imageView = (ImageView)findViewById(R.id.br_image);

        final String path = getIntent().getStringExtra(ExtraData.FullScreenImage.IMAGE_PATH);

        Ln.d("c2app", "FullScreenImageActiviy.onCreate() - path: %s", path);
        if (path == null){
            finish();
            return;
        }

        Bitmap b = null;
        try {
            b = BitmapUtil.fromFile(path);
        }
        catch (final IOException e) {
            e.printStackTrace();
        }

        if (b == null){
            Ln.d("c2app", "FullScreenImageActiviy.onCreate() - file not found: %s -> try image cache...", path);
            b = SCBitmapManager.instance().get(path);
            Ln.d("c2app", "FullScreenImageActiviy.onCreate() - from cache: %s", b);
            if (b == null){
                finish();
                return;
            }
        }

        imageView.setImageBitmap(b);

    }
}
