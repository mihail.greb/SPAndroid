package com.app.android.shadow.samplechat.board;

import android.view.View;

import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemCallController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemRichController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemTextController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemControllerFactory;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemType;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;

/**
 * A factory to create controllers for Board-/Chat list items. Ths factory
 * is used by the BoardController's adapter whenever a new controller is needed for a certain list item.
 * There are 8 different item types and each can by controlled by different controllers. However,
 * in this sample all the media-/rich messages should use the same controller class {@link BoardListItemRichController}.
 * So here we only use the controller classes for Text-, Call- and Rich message items.
 */
public class BoardListItemControllerFactory extends SCBoardListItemControllerFactory
{

    public BoardListItemControllerFactory(SCActivityResultDispatcher resultDispatcher)
    {
        super(resultDispatcher);
    }



   /* @Override
    public IBoardListItemBaseController<? extends IBoardListItemBaseViewHolder> create(SCBoardListItemType type, View v, SCViewDescription vd, SCBoardEventData data) {

        IBoardListItemBaseController con  = super.create(type, v, vd, data);
       *//* if(con instanceof IBoardListItemCallController){
            return null;
        }*//*
        return con;
       *//* if(type != null && v != null && vd != null && data != null) {
            switch(BoardListItemControllerFactory.SyntheticClass_1.$SwitchMap$com$c2call$sdk$pub$gui$boardlistitem$controller$SCBoardListItemType[type.ordinal()]) {
                case 1:
                    return this.onCreateCallItemController(v, vd, data);
                case 2:
                    return this.onCreateTextItemController(v, vd, data);
                case 3:
                    return this.onCreateImageItemController(v, vd, data);
                case 4:
                    return this.onCreateAudioItemController(v, vd, data);
                case 5:
                    return this.onCreateVideoItemController(v, vd, data);
                case 6:
                    return this.onCreateLocationItemController(v, vd, data);
                case 7:
                    return this.onCreateFriendItemController(v, vd, data);
                case 8:
                    return this.onCreatFileItemController(v, vd, data);
                default:
                    throw new IllegalStateException("Unhandled item type: " + type);
            }
        } else {
            throw new IllegalArgumentException();
        }*//*
    }*/

    /**
     * Creates a custom controller for list items showing call
     * @param v     the item's view used by the returned controller
     * @param vd    the item's view description used by the returned controller
     * @param data  the item's data used by the returned controller
     * @return the new controller
     */
    @Override
    protected IBoardListItemCallController onCreateCallItemController(View v, SCViewDescription vd, SCBoardEventData data)
    {
        //return null;
        return new BoardListItemCallController(v, vd, data);
    }

    /**
     * Creates a custom controller for list items showing calls
     * @param v     the item's view,that should be used by the returned controller
     * @param vd    the item's view description, that should be used by the returned controller
     * @param data  the item's data, that should be used by the returned controller
     * @return the new controller
     */
    @Override
    protected IBoardListItemTextController onCreateTextItemController(View v, SCViewDescription vd, SCBoardEventData data)
    {
        return new BoardListItemTextController(v, vd, data);
    }

    /**
     * Creates a custom controller for list items showing audio messages
     * @param v     the item's view,that should be used by the returned controller
     * @param vd    the item's view description, that should be used by the returned controller
     * @param data  the item's data, that should be used by the returned controller
     * @return the new controller
     */
    @Override
    protected IBoardListItemRichController onCreateAudioItemController(View v, SCViewDescription vd, SCBoardEventData data)
    {
        return new BoardListItemRichController(SCBoardListItemType.Audio, v, vd, data);
    }

    /**
     * Creates a custom controller for list items showing shared friend messages
     * @param v     the item's view,that should be used by the returned controller
     * @param vd    the item's view description, that should be used by the returned controller
     * @param data  the item's data, that should be used by the returned controller
     * @return the new controller
     */
    @Override
    protected IBoardListItemRichController onCreateFriendItemController(View v, SCViewDescription vd, SCBoardEventData data)
    {
        return new BoardListItemRichController(SCBoardListItemType.Friend, v, vd, data);
    }

    /**
     * Creates a custom controller for list items showing image massages
     * @param v     the item's view,that should be used by the returned controller
     * @param vd    the item's view description, that should be used by the returned controller
     * @param data  the item's data, that should be used by the returned controller
     * @return the new controller
     */
    @Override
    protected IBoardListItemRichController onCreateImageItemController(View v, SCViewDescription vd, SCBoardEventData data)
    {
        return new BoardListItemRichController(SCBoardListItemType.Image, v, vd, data);
    }

    /**
     * Creates a custom controller for list items showing shared location messages
     * @param v     the item's view,that should be used by the returned controller
     * @param vd    the item's view description, that should be used by the returned controller
     * @param data  the item's data, that should be used by the returned controller
     * @return the new controller
     */
    @Override
    protected IBoardListItemRichController onCreateLocationItemController(View v, SCViewDescription vd, SCBoardEventData data)
    {
        return new BoardListItemRichController(SCBoardListItemType.Location, v, vd, data);
    }

    /**
     * Creates a custom controller for list items showing video messages
     * @param v     the item's view,that should be used by the returned controller
     * @param vd    the item's view description, that should be used by the returned controller
     * @param data  the item's data, that should be used by the returned controller
     * @return the new controller
     */
    @Override
    protected IBoardListItemRichController onCreateVideoItemController(View v, SCViewDescription vd, SCBoardEventData data)
    {
        return new BoardListItemRichController(SCBoardListItemType.Video, v, vd, data);
    }

    /**
     * Creates a custom controller for list items showing file messages
     * @param v     the item's view,that should be used by the returned controller
     * @param vd    the item's view description, that should be used by the returned controller
     * @param data  the item's data, that should be used by the returned controller
     * @return the new controller
     */
    @Override
    protected IBoardListItemRichController onCreatFileItemController(View v, SCViewDescription vd, SCBoardEventData data)
    {
        return new BoardListItemRichController(SCBoardListItemType.File, v, vd, data);
    }

}
