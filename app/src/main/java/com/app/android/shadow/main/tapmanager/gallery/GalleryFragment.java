package com.app.android.shadow.main.tapmanager.gallery;

import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.dbhelper.CustomeC2callDB;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.R;
import com.app.android.shadow.main.adapters.ImageAdapter;
import com.app.android.shadow.main.provider.CustomC2CallContentProvider;
import com.app.android.shadow.main.util.Logger;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Custom Board Fragment to show a chat history grouped by users
 */
public class GalleryFragment extends BaseFragment implements View.OnClickListener
{
    private View _view;
    GridView gridViewImage,gridViewVideo;
    ArrayList<HashMap<String,String >> listImage = new ArrayList<HashMap<String,String >>();
    ArrayList<HashMap<String,String >> listVideo = new ArrayList<HashMap<String,String >>();

    TextView videoBtn,imageBtn;
    boolean isVideo = false;
    ImageAdapter adapterImage;
    ImageAdapter adapterVideo;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        _view =inflater.inflate(R.layout.shadow_gallery, container, false);
        setHeader(_view);

        imageBtn= (TextView) _view.findViewById(R.id.imageBtn);
        videoBtn = (TextView) _view.findViewById(R.id.videoBtn);
        imageBtn.setOnClickListener(this);
        videoBtn.setOnClickListener(this);

        gridViewImage = (GridView) _view.findViewById(R.id.gridView_image);
        gridViewVideo = (GridView) _view.findViewById(R.id.gridView_video);

        listImage = CustomC2CallContentProvider.customeC2callDB.getMediaData(false);
        listVideo = CustomC2CallContentProvider.customeC2callDB.getMediaData(true);

        //setListData();

        adapterImage = new ImageAdapter(getActivity(), listImage, false,GalleryFragment.this);
        adapterVideo = new ImageAdapter(getActivity(), listVideo, true,GalleryFragment.this);
        gridViewImage.setAdapter(adapterImage);
        gridViewVideo.setAdapter(adapterVideo);
        makefilearray(isVideo);
        gridViewImage.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //setonItemClick(position);
                Bundle bundle = new Bundle();
                bundle.putBoolean("isVideo",false);
                Logger.printLog(Logger.ERROR,"uri is "+listImage.get(position).get(CustomeC2callDB.LOC));
                bundle.putString("URI", listImage.get(position).get(CustomeC2callDB.LOC));
                bundle.putInt("Position", position);
                // bundle.putSerializable("HashMap",listImage.get(position));
                //Fragment fragment = new GalleryFullViewFragment();
                Fragment fragment = new GalleryFliperFragment();
                fragment.setArguments(bundle);
                mCallback.onButtonClickEvent(fragment, EventMaker.Gallery);
            }
        });

        gridViewVideo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //setonItemClick(position);
                Bundle bundle = new Bundle();
                bundle.putBoolean("isVideo",true);
                Logger.printLog(Logger.ERROR, "uri is " + listVideo.get(position).get(CustomeC2callDB.LOC));
                bundle.putString("URI", listVideo.get(position).get(CustomeC2callDB.LOC));
                bundle.putInt("Position", position);
                // bundle.putSerializable("HashMap",listImage.get(position));
                //Fragment fragment = new GalleryFullViewFragment();
                Fragment fragment = new GalleryFliperFragment();
                fragment.setArguments(bundle);
                mCallback.onButtonClickEvent(fragment, EventMaker.Gallery);
            }
        });

        return _view;
    }

    @Override
    public void onResume() {
        super.onResume();
        isVideo = false;
        setBtnLayout(isVideo);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageBtn:
                setBtnLayout(false);
                break;
            case R.id.videoBtn:
                setBtnLayout(true);
                break;
        }
    }

    public void setBtnLayout(boolean isVideo) {
        this.isVideo = isVideo;
        makefilearray(isVideo);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            imageBtn.setBackground((isVideo)?getnewDrawable(R.drawable.button_image_unselected):getnewDrawable(R.drawable.button_image_selected));
            videoBtn.setBackground((isVideo) ? getnewDrawable(R.drawable.button_video_selected) : getnewDrawable(R.drawable.button_video_unselected));
        }else {
            imageBtn.setBackgroundDrawable((isVideo)?getnewDrawable(R.drawable.button_image_unselected):getnewDrawable(R.drawable.button_image_selected));
            videoBtn.setBackgroundDrawable((isVideo)?getnewDrawable(R.drawable.button_video_selected):getnewDrawable(R.drawable.button_video_unselected));
        }
        imageBtn.setTextColor(resources.getColor(isVideo?R.color.white:R.color.black));
        videoBtn.setTextColor(resources.getColor(isVideo?R.color.black:R.color.white));
    }


    private void makefilearray(boolean isVideo){
        if(isVideo) {
            gridViewImage.setVisibility(View.GONE);
            gridViewVideo.setVisibility(View.VISIBLE);
        }else {
            gridViewImage.setVisibility(View.VISIBLE);
            gridViewVideo.setVisibility(View.GONE);
        }
    }

    //Header
    TextView title;
    protected void setHeader(View v) {
        title = (TextView) v.findViewById(R.id.title_head);
        title.setText(resources.getString(R.string.shadow_gallery));
        v.findViewById(R.id.back).setVisibility(View.INVISIBLE);
    }

    /* List<SCMediaData> listImageL;
    List<SCMediaData> listVideoL;

    public List<SCMediaData> getMediaData(boolean isVideo) throws SQLException {

        List<SCMediaData> data = SCMediaData.dao().queryForAll();
        return data;
    }*/

}



/*
    public boolean compareDrawable(Drawable d1, Drawable d2){
        try{
            Bitmap bitmap1 = ((BitmapDrawable)d1).getBitmap();
            ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
            bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, stream1);
            stream1.flush();
            byte[] bitmapdata1 = stream1.toByteArray();
            stream1.close();

            Bitmap bitmap2 = ((BitmapDrawable)d2).getBitmap();
            ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
            bitmap2.compress(Bitmap.CompressFormat.JPEG, 100, stream2);
            stream2.flush();
            byte[] bitmapdata2 = stream2.toByteArray();
            stream2.close();

            return bitmapdata1.equals(bitmapdata2);
        }
        catch (Exception e) {
            // TODO: handle exception
        }
        return false;
    }*/

    /*private void setListData(){
               lst =new ArrayList<File>();
       File[] fArray = SdCardManager.getFilesList();
        for (File f:fArray){
            Logger.printLog(Logger.ERROR,"Files path is " + f.getAbsolutePath());
            String type = ImageAdapter.getObjectFileType(f);
            if(type.equals(IMAGE) && !isVideo){
                lst.add(f);
            }else if(type.equals(VIDEO) && isVideo){
                lst.add(f);
            }
        }
    }*/


    /*private void setonItemClick(int position){
        HashMap<String, String> item = (isVideo)?listVideo.get(position):listImage.get(position);
        String type = ImageAdapter.getMediaType(item);
        try {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            if (type.equals(UNKNOW)) {
                type = (isVideo) ? VIDEO : IMAGE;
            }
            if (type.equals(IMAGE))
                intent.setDataAndType(Uri.parse("file://" + item.get(CustomeC2callDB.LOC)), "image*//*");
            else if (type.equals(VIDEO))
                intent.setDataAndType(Uri.parse("file://" + item.get(CustomeC2callDB.LOC)), "video*//*");
            else
                return;
            BaseSampleActivity.isAppNotClose =true;
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        BaseSampleActivity.isAppNotClose =false;
    }*/