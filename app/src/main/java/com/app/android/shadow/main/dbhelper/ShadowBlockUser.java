package com.app.android.shadow.main.dbhelper;

import com.c2call.sdk.pub.db.data.SCBaseData;
import com.c2call.sdk.pub.db.util.core.DatabaseHelper;
import com.c2call.sdk.pub.db.util.core.DefaultSortOrder;
import com.c2call.sdk.pub.db.util.core.ObservableDao;
import com.c2call.sdk.pub.db.util.core.UriPaths;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.simpleframework.xml.Root;

@DatabaseTable(
        tableName = "shadow_block_user"
)
@UriPaths({"block_users", "block_users/#"})
@DefaultSortOrder("block_to ASC")
@Root(
        strict = false
)


public class ShadowBlockUser extends SCBaseData<Long> {
    public static final String ENTITY = "block_user";
    public static final String ENTITY_PL = "block_users";
    public static final String BLOCK_BY="block_by";
    public static final String BLOCK_TO="block_to";
    public static final String[] PROJECTION_ALL = new String[]{"_id","block_to", "block_by"};

    @DatabaseField(
            columnName = "_id",
            generatedId = true
    )
    private Long _id;

    @DatabaseField(
            columnName = "block_to",
            unique = true
    )
    private String _block_to;

    @DatabaseField(
            columnName = "block_by",
            canBeNull = false
    )
    private String _block_by;


    public ShadowBlockUser() {

    }

    public ShadowBlockUser(Long _id, String _block_to, String _block_by) {
        this._id = _id;
        this._block_to = _block_to;
        this._block_by = _block_by;
    }

    public static ObservableDao<ShadowBlockUser, Long> dao() {
        return DatabaseHelper.getDefaultDao(ShadowBlockUser.class);
    }

    public Long getId() {
        return this._id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    public String get_block_to() {
        return _block_to;
    }

    public void set_block_to(String _block_to) {
        this._block_to = _block_to;
    }

    public String get_block_by() {
        return _block_by;
    }

    public void set_block_by(String _block_by) {
        this._block_by = _block_by;
    }
}
