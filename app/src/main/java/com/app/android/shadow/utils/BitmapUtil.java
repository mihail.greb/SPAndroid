package com.app.android.shadow.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class BitmapUtil
{
    public static Bitmap fromFile(final String path) throws IOException
    {
         final InputStream is = new FileInputStream(path);
         final BitmapFactory.Options options = new BitmapFactory.Options();
         final Bitmap b = BitmapFactory.decodeStream(is,null,options);

         is.close();

         return b;
    }
}
