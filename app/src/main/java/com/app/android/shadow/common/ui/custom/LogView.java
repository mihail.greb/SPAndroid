
package com.app.android.shadow.common.ui.custom;
 
import android.app.Activity;
import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.util.*;
import android.widget.TextView;

import com.app.android.shadow.utils.Str;

public class LogView extends TextView{

    public LogView(Context context) {
        super(context);
        init();
    }
 
    public LogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
 
    public LogView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init()
    {
        setMovementMethod(new ScrollingMovementMethod());
        setHorizontallyScrolling(true);
    }

    public void println(String msg) {
        println(null, msg, null);
    }



    public void println(String tag, String msg, Throwable tr) {
        String exceptionStr = null;
        if (tr != null) {
            exceptionStr = android.util.Log.getStackTraceString(tr);
        }
 
        final StringBuilder outputBuilder = new StringBuilder();
 
        String delimiter = " \t";
        append(outputBuilder, tag, delimiter);
        append(outputBuilder, msg, delimiter);
        append(outputBuilder, exceptionStr, delimiter);
 
        ((Activity) getContext()).runOnUiThread( (new Thread(new Runnable() {
            @Override
            public void run() {
                appendToLog(outputBuilder.toString());
            }
        })));
    }

    private StringBuilder append(StringBuilder builder, String s, String delimiter) {
        if (!Str.isEmpty(s)) {
            return builder.append(s).append(delimiter);
        }
        return builder;
    }
 
    public void appendToLog(String s) {
        append("\n" + s);
    }
 
 
}