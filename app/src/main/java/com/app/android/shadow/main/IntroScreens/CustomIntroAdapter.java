package com.app.android.shadow.main.IntroScreens;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.R;

public class CustomIntroAdapter extends PagerAdapter {

    private  int[] image_resources;

    private Context ctx;
    private LayoutInflater layoutInflator;

    public CustomIntroAdapter(Context ctx){
        this.ctx =ctx;
        this.image_resources = getImageResources();
    }
    @Override
    public int getCount() {
        return image_resources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==(LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflator=(LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item_view = layoutInflator.inflate(R.layout.shadow_intro_screen_page,container,false);
        ImageView introImage=(ImageView)item_view.findViewById(R.id.introImage);
        introImage.setImageResource(image_resources[position]);
        container.addView(item_view);
        return item_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }

    private int[] image_resources_yp = {R.drawable.intro_yp1,R.drawable.intro_add_friend2,R.drawable.intor_modes3};
    private int[] image_resources_ac = {R.drawable.intro_ac1,R.drawable.intro_add_friend2,R.drawable.intor_modes3};
    private int[] image_resources_ca = {R.drawable.intro_ac1,R.drawable.intro_add_friend2,R.drawable.intor_modes3};
    private int[] image_resources_free = {R.drawable.screen1,R.drawable.screen2,R.drawable.screen3,R.drawable.screen7,R.drawable.screen10};

    private int[] getImageResources(){
        if(Shadow.isProApp()) {
            if(Shadow.isAlarmGame()){
                return image_resources_ac;
            }else if(Shadow.isCalculator()){
                return image_resources_ca;
            }else {
                return image_resources_yp;
            }
        }else {
            return image_resources_free;
        }
    }

}

