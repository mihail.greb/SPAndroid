package com.app.android.shadow.common.ui.data.friendsList;

import android.content.Context;
import android.database.Cursor;
import android.view.View;

import com.c2call.sdk.pub.db.adapter.SCFriendCursorAdapter;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.core.decorator.IDecorator;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemController;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemControllerFactory;


/**
 * This Adapter is used by the {@link FriendsLoaderHandler}. It only overrides the method
 * {@link #onCreateDecorator(IFriendListItemController)} to customize the display of dynamic
 * elements of friend list items.
 */
public class FriendsAdapter extends SCFriendCursorAdapter
{
    public FriendsAdapter(final Context context,
                                 final Cursor c,
                                 final int layout,
                                 final IFriendListItemControllerFactory mediatorFactory,
                                 final SCViewDescription vd,
                                 final int flags)
    {
        super(context, c, layout,mediatorFactory, vd, flags);
    }


    /**
     * Creates a decorator which is used by the given Controller to customize the display of dynamic
     * elements of friend list items.
     * @param controller the controller which will use the created decorator
     * @return the new decorator instance.
     */
    @Override
    protected IDecorator<IFriendListItemController> onCreateDecorator(final IFriendListItemController controller)
    {
        return new FriendListItemDecorator();
    }


    @Override
    protected SCFriendData onCreateData(Cursor cursor) {
        return super.onCreateData(cursor);
    }

    public SCFriendData getSCFriendData(Cursor cursor){
        return onCreateData(cursor);
    }

    @Override
    public void bindItemView(View view, Context context, Cursor cursor) {
        super.bindItemView(view, context, cursor);
    }

    @Override
    protected void onUpdateView(IFriendListItemController controller, View v, SCFriendData data) {
        super.onUpdateView(controller, v, data);
    }

}
