package com.app.android.shadow.main.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.android.shadow.main.dbhelper.CustomeC2callDB;
import com.app.android.shadow.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rails-dev on 30/5/16.
 */
public class GalleryFliperAdapter extends PagerAdapter{

    private Context context;
    ArrayList<HashMap<String,String >> list;
    LayoutInflater inflater;

    public GalleryFliperAdapter(Context context,ArrayList<HashMap<String,String >> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        //return super.instantiateItem(container, position);
        View view = null;

        ViewHolder holder;
        if(view == null){
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //view = inflater.inflate(R.layout.shadow_fragments_container2, container,false);
            view = inflater.inflate(R.layout.shadow_gallery_fullview, container,false);
            holder = new ViewHolder();
            holder.fullImage = (ImageView) view.findViewById(R.id.full_view);
            ((ViewPager) container).addView(view);
            view.setTag(holder);
        }else{
            holder = (ViewHolder) view.getTag();
        }
        //holder.fullImage.setVisibility(View.VISIBLE);
        holder.fullImage.setImageURI(Uri.parse(list.get(position).get(CustomeC2callDB.LOC)));
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //super.destroyItem(container, position, object);
        ((ViewPager) container).removeView((LinearLayout) object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout)object);
    }


    class ViewHolder{
        ImageView fullImage ;
    }

}
