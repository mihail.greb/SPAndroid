package com.app.android.shadow.common.ui.data.friendsList;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.gui.core.adapter.SCBaseControllerCursorAdapter;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.core.decorator.IDecorator;
import com.c2call.sdk.pub.gui.core.loaderhandler.ILoaderHandlerContextProvider;
import com.c2call.sdk.pub.gui.core.loaderhandler.SCFriendLoaderHandler;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemController;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemControllerFactory;
import com.c2call.sdk.pub.util.IListViewProvider;
import com.app.android.shadow.R;

public class FriendsLoaderHandler extends SCFriendLoaderHandler
{

    private int _itemLayout;

    public FriendsLoaderHandler(final ILoaderHandlerContextProvider contextProvider,
                                final IListViewProvider listViewProvider,
                                final IFriendListItemControllerFactory itemControllerFactory,
                                final SCViewDescription itemviewDescription,
                                final int loaderId)
    {
        this(contextProvider, listViewProvider, itemControllerFactory, itemviewDescription, loaderId, R.layout.sc_friend_listitem);
    }

    public FriendsLoaderHandler(final ILoaderHandlerContextProvider contextProvider,
                                final IListViewProvider listViewProvider,
                                final IFriendListItemControllerFactory itemControllerFactory,
                                final SCViewDescription itemviewDescription,
                                final int loaderId,
                                final int itemLayout)
    {
        super(contextProvider, listViewProvider, itemControllerFactory, itemviewDescription, loaderId);

        _itemLayout = itemLayout;
        Ln.d("c2app", "FriendListLoaderHandler() - contextProvider: %s", contextProvider);
    }


    protected SCBaseControllerCursorAdapter<  SCFriendData,
            IFriendListItemController,
            IFriendListItemControllerFactory,
            IDecorator<IFriendListItemController>>
    onCreateAapder()
    {
        return new FriendsAdapter(getContext(),
                                  null,
                                  _itemLayout,
                                  getItemControllerFactory(),
                                  getItemViewDescription(),
                                  0);
    }
}
