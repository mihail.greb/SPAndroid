package com.app.android.shadow.main.util;

import android.content.Context;
import android.content.res.Resources;

import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class CallTimeLeft extends APIServiceAsyncTask {

    protected long totalTimeInMS = 0;
    protected String time;
    protected String datas;
    int freeTime = 30;
    Context mContext;

    boolean isProApp;
    SimpleDateFormat format2 = new SimpleDateFormat("MMMM dd,yyyy",Locale.ENGLISH);

    public CallTimeLeft(Context mContext) {
        super(mContext, callTimeLeft, null);
        this.mContext = mContext;
        // resources = mContext.getResources();
        isProApp = Shadow.isProApp();
    }

    @Override
    protected void success(JSONObject jsonObj, int serviceTaskType) {
        super.success(jsonObj, serviceTaskType);
        if(serviceTaskType == APIServiceAsyncTask.CALL_TIME_LEFT) {
            try {
                JSONObject timeObj = jsonObj.getJSONObject(Const.DATA);
                if(Shadow.isProApp()){
                    if(isDayAvailible(timeObj.getString("call_expiry_date"))){
                    //if(isDayAvailible("2016-02-31T09:27:14.889Z")){
                        time = "99:00:00";
                    }else {
                        time = "00:00:00";//timeObj.getString("time_left");
                    }
                }else {
                    time = timeObj.getString("time_left");
                }

                totalTimeInMS = CheckLeftTime.convertTime(time);
                if(!Shadow.isProApp()) {
                    datas = timeObj.getString("call_expiry_date");
                    //datas = "2016-02-31T09:27:14.889Z";
                }else {
                    try {
                        datas = timeObj.getString("subscription_expiry_date");
                        Logger.printLog(Logger.ERROR,"date is "+datas);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if(datas == null || datas.equals("null")){
                        datas = timeObj.getString("call_expiry_date");
                        //datas = "2016-02-31T09:27:14.889Z";
                    }
                }
               /* time = "00:30:01";
                totalTimeInMS = CheckLeftTime.convertTime(time);
                datas = "2016-01-11T07:12:08.879Z";//timeObj.getString("call_expiry_date");*/


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    protected boolean isShowDialog() {
        //Date date7DaysBefore = sabtractDaysFromCurrentDate(7);
        Date curDate = new Date();
        Logger.printLog(Logger.ERROR, "curDate " + curDate);
        Logger.printLog(Logger.ERROR, "getDate() " +  getDate());
        String minutes = time.substring(time.indexOf(":") + 1, time.lastIndexOf(":"));
        Logger.printLog(Logger.ERROR, "minits " + minutes);
        Logger.printLog(Logger.ERROR, "Time in ms and compare time " + totalTimeInMS + "  " + freeTime * 60 * 1000L);
        String date = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH).format(new Date());
        if (!Shadow.getAppPreferences().getString(Const.DATE, "").equals(date)) {
            if (isProApp) {
                //if (curDate.before(getDate())) {
                //if (getDateInMS(curDate) > (getDateInMS(sabtractDaysFromCurrentDate(7, getDate())))) {
                Logger.printLog(Logger.ERROR,"show left days "+daysLeft());
                if (daysLeft() <= 7) {
                    Shadow.getAppPreferencesEditor().putString(Const.DATE,date).commit();
                    return true;
                }
                return false;
            } else {
                if ((totalTimeInMS) <= (freeTime * 60 * 1000L)) {
                    Shadow.getAppPreferencesEditor().putString(Const.DATE,date).commit();
                    return true;
                }
                return false;
            }

        }else
            return false;
    }

    public static boolean isDayAvailible(String dateStr){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",Locale.ENGLISH);
        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date strDate = null;
        try {
            strDate = sdf.parse(dateStr);
        //return (!(new Date().after(strDate))) ;
            //return (new Date().before(strDate)) ;
            if (System.currentTimeMillis() <= strDate.getTime()) {
                return true;
            }
            return false;
        } catch (ParseException e) {
            e.printStackTrace();
            Logger.printLog(Logger.ERROR,"date not parsed");
        }
        return false;
    }

    protected long getDateInMS(Date d){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        return calendar.getTimeInMillis();
    }

    protected Date sabtractDaysFromCurrentDate(int days){
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -days);
        return calendar.getTime();
    }

    protected Date sabtractDaysFromCurrentDate(int days,Date d){
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.DATE, -days);
        return cal.getTime();
    }

    protected Date addDaysFromCurrentDate(int days){
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, days);
        return calendar.getTime();
    }

    protected String getDateStringToProfile(){
        Resources resources = mContext.getResources();
        String date =  getDateStr();
        if(date != null && date.length()>0) {
            if (Shadow.isProApp()) {
                return (resources.getString(R.string.proapp_credit, date));
            } else {
                return (resources.getString(R.string.free_credit, getTimeString(), date));
            }
        }
        return "";
    }

    protected String getDateStringToDialog(){
        Resources resources = mContext.getResources();
        String date =  getDateStr();
        if(date != null && date.length()>0) {
            if (Shadow.isProApp()) {
                long daysLeft = daysLeft();
                if(daysLeft <= 0){
                    return (resources.getString(R.string.proapp_message_0days));
                }else {
                    return (resources.getString(R.string.proapp_message, (daysLeft)));
                }
            } else {
                return (resources.getString(R.string.free_credit, getTimeString(), date));
            }
        }
        return "";
    }

    protected long daysLeft(){
        long diff = ((getDate().getTime() - new Date().getTime())+(2*24*60*60*1000));// User Date - Current Date
        Logger.printLog(Logger.ERROR,"diff "+diff);
        if(diff >= (24*60*60*1000)) {
            long secondsLong = diff / 1000;
            long minutesLong = secondsLong / 60;
            long hoursLong = minutesLong / 60;
            long days = hoursLong / 24;
            return days;
        }else
            return 0L;
    }


    protected String getDateStr(){

        try {
            Date d = getDate();
            if(d != null) {
                String date = format2.format(d);
                Logger.printLog(Logger.ERROR, "new date is " + date);//+","+format3.format(d)
                return date;
            }else
                return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    protected Date getDate(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        // 2016-01-20T10:44:25.197Z
        //SimpleDateFormat format3 = new SimpleDateFormat("yyyy");
        try {
            Logger.printLog(Logger.ERROR,"dates is "+datas);
            Date d = format.parse(datas);
            return d;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }


    protected String getTimeString(){
        if(time.contains(":")){
            return time.substring(0,(time.lastIndexOf(":")));
        }
        return "";
    }

}
