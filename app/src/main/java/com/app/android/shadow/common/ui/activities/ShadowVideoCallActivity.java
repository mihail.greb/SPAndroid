package com.app.android.shadow.common.ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.R;
import com.c2call.sdk.pub.activities.SCVideoCallActivity;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.videocall.controller.IVideoCallController;
import com.c2call.sdk.pub.gui.videocall.controller.IVideoCallViewHolder;
import com.c2call.sdk.pub.gui.videocall.controller.SCVideoCallController;
import com.c2call.sdk.pub.gui.videocall.controller.SCVideoCallFactory;
import com.c2call.sdk.pub.video.IVideoSlave;

import java.util.Timer;
import java.util.TimerTask;

public class ShadowVideoCallActivity extends SCVideoCallActivity {

    String currentUserId ="";
    String userName ="";
    TextView callerName,durationTime;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setUserNameOnTop();
        try {
            Logger.printLog(Logger.ERROR,"ShadowVideoCallActivity working");
            durationTime = (TextView) findViewById(R.id.sc_callbar_label_duration);
            Logger.printLog(Logger.ERROR,"ShadowVideoCallActivity working2");
            durationTime.setVisibility(View.VISIBLE);
            try {
                startTimerAndShow();
            } catch (Exception e) {
                e.printStackTrace();
                durationTime.setVisibility(View.GONE);
            }
            Logger.printLog(Logger.ERROR, "ShadowVideoCallActivity working3");
        } catch (Exception e) {
            Logger.printLog(Logger.ERROR, "ShadowVideoCallActivity exception");
            e.printStackTrace();
        }
        try {
            SCVideoCallController controller = (SCVideoCallController) getController();
            controller.onButtonFillSrceenClicked(getView());
            controller.onButtonFillSrceenClicked(getView());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUserNameOnTop(){
        try {
            currentUserId = SCCoreFacade.instance().getInterlocutor();
            Logger.printLog(Logger.ERROR, "print current user id 2");
            userName = C2CallSdk.contactResolver().getDisplayNameByUserid(currentUserId, true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(userName == null || userName.length() ==0){
            try {
                currentUserId = CallActivity.userIdCurrent;
                Logger.printLog(Logger.ERROR,"print current user id callActivity "+currentUserId);
                userName = C2CallSdk.contactResolver().getDisplayNameByUserid(currentUserId, true);
                CallActivity.userIdCurrent = "";
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        if (userName != null && userName.length() > 0) {
            callerName = (TextView) findViewById(R.id.sc_callbar_label_name);
            //SCFriendData friend = SCFriendData.dao().queryForId(currentUserId);
            Logger.printLog(Logger.ERROR, "print current user id " + currentUserId);
            Logger.printLog(Logger.ERROR, "print current user id name" + userName);
            callerName.setText(userName);
            callerName.setVisibility(View.VISIBLE);
        }
    }


    public int hours = 00;
    public int seconds = 00;
    public int minutes = 00;
    private void startTimerAndShow() {
        //Declare the timer
        Timer t = new Timer();
        //Set the schedule function and rate
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        if(seconds == 60)
                        {
                            seconds=00;
                            minutes=minutes+1;
                        }
                        if(minutes == 60)
                        {
                            minutes=00;
                            hours=hours+1;
                        }
                        durationTime.setText(getIntToStr(hours)+":"+getIntToStr(minutes)+":"+getIntToStr(seconds));
                        ++seconds;
                    }

                });
            }

        }, 0, 1000);
}

    private String getIntToStr(int value){
        if(value<10){
            return ""+0+value;
        }
        return ""+value;
    }

    @Override
    protected void onDestroy() {
        BaseSampleActivity.isAppNotClose = false;
        super.onDestroy();
    }

    @Override
    protected IVideoCallController onCreateController(View view, SCViewDescription scViewDescription, IVideoSlave iVideoSlave) {
        //return super.onCreateController(view, scViewDescription, iVideoSlave);
        SCVideoCallFactory var4 = this.getVideoCallFactory(iVideoSlave);
        if(var4 != null) {
            return (IVideoCallController)var4.createController(this, view);
        } else {
            SCVideoCallController var5 = new SCVideoCallController(view, scViewDescription){
                @Override
                public void showControls(boolean var1) {
                    //super.showControls(b);
                    if(this.getViewHolder() != null) {
                        View var2 = ((IVideoCallViewHolder)this.getViewHolder()).getItemContainerControls();
                        RelativeLayout var3 = ((IVideoCallViewHolder)this.getViewHolder()).getItemContainerSlots();
                        View var4 = ((IVideoCallViewHolder)this.getViewHolder()).getItemContainerInfo();
                        if(var2 != null) {
                            setVisibility(var2, var1);
                            setVisibility(((IVideoCallViewHolder)this.getViewHolder()).getItemButtonRotatePreview(), var1);
                            if(var3 != null) {
                                setVisibility(var3, !var1);
                            }
                            if(var4 != null) {
                                setVisibility(var4, var1);
                            }
                            if(callerName != null){
                                setVisibility(callerName, var1);
                            }
                            if(durationTime != null){
                                setVisibility(durationTime, var1);
                            }
                        }
                    }
                }

                /*@Override
                public void onUpdateVideoSlots(ArrayList<SCVideoCallRegion> arrayList) {
                    super.onUpdateVideoSlots(arrayList);
                    RelativeLayout var2 = ((IVideoCallViewHolder)this.getViewHolder()).getItemContainerSlots();
                    GradientDrawable border = new GradientDrawable();
                    border.setColor(0x00000000); //Transparent background
                    border.setStroke(1, 0xFFFFFFFF); //white border with full opacity
                    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        var2.setBackgroundDrawable(border);
                    } else {
                        var2.setBackground(border);
                    }
                }*/
            };
            return var5;
        }
    }
}
