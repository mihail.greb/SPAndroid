package com.app.android.shadow.events;

public class LogEvent
{
    public static String DEFAULT_TAG = "c2capp";

    private String _tag;
    private String _msg;
    private Throwable _throwable;

    public LogEvent(String msg)
    {
        this(DEFAULT_TAG, msg, null);
    }

    public LogEvent(String tag, String msg, Throwable throwable)
    {
        _msg = msg;
        _tag = tag;
        _throwable = throwable;
    }

    public String getMsg()
    {
        return _msg;
    }

    public void setMsg(String msg)
    {
        _msg = msg;
    }

    public Throwable getThrowable()
    {
        return _throwable;
    }

    public void setThrowable(Throwable throwable)
    {
        _throwable = throwable;
    }


    public String getTag()
    {
        return _tag;
    }

    public void setTag(String tag)
    {
        _tag = tag;
    }

    @Override
    public String toString()
    {
        final StringBuffer sb = new StringBuffer("LogEvent{");
        sb.append(", _tag='").append(_tag).append('\'');
        sb.append(", _msg='").append(_msg).append('\'');
        sb.append(", _throwable=").append(_throwable);
        sb.append('}');
        return sb.toString();
    }
}
