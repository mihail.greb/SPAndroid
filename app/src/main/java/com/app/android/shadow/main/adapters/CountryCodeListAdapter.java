package com.app.android.shadow.main.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by abc on 3/24/2015.
 */
public class CountryCodeListAdapter extends ArrayAdapter {

    Context mcontext;
    ArrayList<HashMap<String,String>> lst= new ArrayList<HashMap<String,String>>();
    Resources resources;


    public CountryCodeListAdapter(Context mcontext, int resource, List<HashMap<String, String>> objects) {
        super(mcontext, resource, objects );
        this.mcontext =mcontext;
        this.lst= (ArrayList<HashMap<String,String>>) objects;
        this.resources = mcontext.getResources();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        //return super.getDropDownView(position, convertView, parent);
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final HashMap<String,String> item = lst.get(position);
        TextView row;
        LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.shadow_singal_textview, parent, false);
        row = (TextView) convertView.findViewById(R.id.row);
        row.setText(item.get(Const.DIAL_CODE));
        return convertView;
    }

    @Override
    public String getItem(int position) {
        return (lst.get(position).get(Const.DIAL_CODE)).toString();
    }


    public View getCustomView(int position, View convertView, ViewGroup parent) {

        final HashMap<String,String> item = lst.get(position);
        TextView row;
        LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.shadow_singal_textview, parent, false);
        row = (TextView) convertView.findViewById(R.id.row);
        row.setText("" + item.get(Const.DIAL_CODE) + " |  " + item.get(Const.NAME));
        row.setPadding(5,3,3,0);
      /*  if(item.get(Const.DIAL_CODE).toString().length()==2){
            row.setText(" " + item.get(Const.DIAL_CODE) + "  |  " + item.get(Const.NAME));
        }else {

        }*/
        return convertView;
    }
}
