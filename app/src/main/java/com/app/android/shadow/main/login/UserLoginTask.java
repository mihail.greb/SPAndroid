package com.app.android.shadow.main.login;

import android.content.Context;

import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.main.util.Logger;

import org.json.JSONException;
import org.json.JSONObject;

public class UserLoginTask extends APIServiceAsyncTask {

    private String pass;

    protected UserLoginTask(Context mContext,String pass){
        super(mContext);
        this.pass = pass;
    }

    @Override
    protected void success(JSONObject jsonObj, int serviceTaskType) {
        //super.success(jsonObj, serviceTaskType);
        try {

            String userName;
            JSONObject data = jsonObj.getJSONObject(Const.DATA);
            JSONObject user = data.getJSONObject(Const.USER);

            try{
                //TODO uncommit to maintain first time logs by API
                //Shadow.editor.putBoolean(Const.FIRST_TIME_LOGGED, data.getBoolean(Const.FIRST_TIME_LOGGED)).commit();
            }catch (Exception e){
                e.printStackTrace();
            }

            Shadow.editor.putString(Const.AUTH_TOKEN,data.getString(Const.AUTH_TOKEN));
            Shadow.editor.putString(Const.ID, user.getString(Const.ID));
            Shadow.editor.commit();
            userName = user.getString(Const.USERNAME);
            Logger.printLog(Logger.ERROR,"User is2", "is " + userName);
            Shadow.editor.putString(Const.USERNAME,userName).commit();
            Shadow.editor.putString(Const.PASSWORD,pass).commit();

            try {
                String phone = user.getString(Const.PHONE);
                if(phone != null)
                    Shadow.editor.putString(Const.PHONE,phone).commit();
                else
                    Shadow.editor.putString(Const.PHONE,"").commit();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Shadow.editor.putString(Const.EMAIL, user.getString(Const.EMAIL)).commit();
            Shadow.editor.putString(Const.DISPLAY_EMAIL,user.getString(Const.DISPLAY_EMAIL)).commit();
            Shadow.editor.putString(Const.SHADOW_ID,user.getString(Const.SHADOW_ID)).commit();

            fireEvent();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    protected void fireEvent(){

    }


}
