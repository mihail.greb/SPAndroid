package com.app.android.shadow.main.tapmanager.extra;

/**
 * Created by rails-dev on 19/10/15.
 */
public enum EventMaker {

    SoundSettings,
    Sound,
    SettingsFragment,
    Friends,
    inviteFriends,
    ProApp,
    Logout,
    GoToShadow,
    CloseApp,
    AutoLogout,
    GoToMode,
    Gallery,
    OpenDialog,
    ClickCount,
    ClickSwipe,
    NoDialogRequired,
    RefreshLockButton;

}
