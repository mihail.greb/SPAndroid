package com.app.android.shadow.main.tapmanager.settings;

/**
 * Created by rails-dev on 19/10/15.
 */
public enum MoreSignature {
    AudioRingtone,
    VideoRingtone,
    MessageRingtone;
}
