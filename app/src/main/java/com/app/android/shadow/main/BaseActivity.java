package com.app.android.shadow.main;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.TouchDelegate;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.app.android.shadow.main.dbhelper.ShadowDB;
import com.app.android.shadow.main.tapmanager.settings.Modes;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.apicalls.ApiRequest;
import com.app.android.shadow.main.apicalls.connectionclass.ConnectionClassManager;
import com.app.android.shadow.main.apicalls.connectionclass.ConnectionQuality;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.util.CheckNetwork;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.common.ui.activities.ExitActivity;
import com.app.android.shadow.main.util.Logger;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BaseActivity extends AppCompatActivity{ //SCBaseActivity {

    Resources resources;

    /*private ConnectionClassManager mConnectionClassManager;
    public DeviceBandwidthSampler mDeviceBandwidthSampler;
    private ConnectionChangedListener mListener;
    public ConnectionQuality mConnectionClass = ConnectionQuality.UNKNOWN;*/

    private class ConnectionChangedListener
            implements ConnectionClassManager.ConnectionClassStateChangeListener {
        @Override
        public void onBandwidthStateChange(ConnectionQuality bandwidthState) {
           // mConnectionClass = bandwidthState;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Logger.printLog(Logger.ERROR, " no error " );
                    //Logger.printLog(Logger.ERROR, " no error " + mConnectionClass.toString());
                  //  Shadow.alertToast(mConnectionClass.toString());
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resources = getResources();
        //minimize no image
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        Bitmap recentsIcon = Shadow.getAppImage(); // Initialize this to whatever you want
        String title  = Shadow.getAppName(); // You can either set the title to whatever you want or just use null and it will default to your app/activity name
        int color = Color.TRANSPARENT; // Set the color you want to set the title to, it's a good idea to use the colorPrimary attribute

        ActivityManager.TaskDescription description = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            description = new ActivityManager.TaskDescription(title, recentsIcon,color);
            setTaskDescription(description);
        }

      /*  mConnectionClassManager = ConnectionClassManager.getInstance();
        mDeviceBandwidthSampler = DeviceBandwidthSampler.getInstance();
        Shadow.alertToast(mConnectionClassManager.getCurrentBandwidthQuality().toString());
        mListener = new ConnectionChangedListener();*/
    }

    public int colorGet(int id){
        return colorGet(id,this);
    }
    public static int colorGet(int id,Context context){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return context.getResources().getColor(id, null);
            }else {
                return context.getResources().getColor(id);
            }

    }


    @Override
    protected void onPause() {
        super.onPause();
        //mConnectionClassManager.remove(mListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        CheckNetwork.isNetworkAvailable(BaseActivity.this);
       // mConnectionClassManager.register(mListener);
    }

    public static String getText(EditText e){
        return e.getText().toString();
    }

    public static boolean isValid(String email)
    {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return matcher.matches();
    }


   /* @Override
    protected void unRegisterBaseActivityReceiver() {
        super.unRegisterBaseActivityReceiver();
    }*/

    @Override
    public void onBackPressed() {
        handleBackPressed();
    }

    public void handleBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        //exittoApp();
    }


    public Drawable drawableGet(int drawable){
        return drawableGet(drawable,BaseActivity.this);
    }
    public static Drawable drawableGet(int drawable,Context tempCon){
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)?
                tempCon.getResources().getDrawable(drawable, null)
                :tempCon.getResources().getDrawable(drawable);
    }

    public void exittoApp(){
        callApi(APIServiceAsyncTask.setUserOfflien);
       /* Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(intent);
        Logger.printLog(Logger.ERROR, "onDestroy exit2 base");
        if(android.os.Build.VERSION.SDK_INT >= 21)
        {
            finishAndRemoveTask();
        }
        else
        {
            finish();
        }*/
        ExitActivity.exitApplicationAnRemoveFromRecent(this);

    }

    public void callApi(ApiRequest req){
        ApiTaskBase mTask = new ApiTaskBase(BaseActivity.this);
        mTask.setServiceTaskMapStrParams(req, null);
        mTask.execute((Void) null);
    }

    public class ApiTaskBase extends APIServiceAsyncTask {
        ApiTaskBase(Context mContext){
            super(mContext);
        }

        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            if(APIServiceAsyncTask.OFFLINE_USER == serviceTaskType){
                Logger.printLog(Logger.ERROR, "success to offline " + jsonObj.toString());
            }
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
            super.failure(jsonObj, serviceTaskType);
            failure("" + serviceTaskType);
            Logger.printLog(Logger.ERROR, "success to offline " + jsonObj.toString());
        }

        @Override
        protected void failure(String message) {
            //super.failure(message);
            Logger.printLog(Logger.ERROR, "failed to offline null");
            Logger.printLog(Logger.ERROR, message);
            try {
                if(APIServiceAsyncTask.OFFLINE_USER == Integer.parseInt(message)) {
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }

    public void callFragment(Fragment fragment, boolean isBack, int id,Bundle bundle) {

        if(bundle != null)
            fragment.setArguments(bundle);
        FragmentTransaction fTransaction = getFragmentManager().beginTransaction();

        fTransaction.replace(id, fragment);
        if (isBack){fTransaction.addToBackStack(null);}
        fTransaction.commit();
    }



    public static void incresClickArea(final View viewName){
        final View parent = (View) viewName.getParent();
        viewName.post(new Runnable() {
            // Post in the parent's message queue to make sure the parent
            // lays out its children before we call getHitRect()
            public void run() {
                final Rect r = new Rect();
                viewName.getHitRect(r);
                r.top -= 30;
                r.bottom += 20;
                r.left -= 30;
                r.right += 20;
                parent.setTouchDelegate(new TouchDelegate(r, viewName));
            }
        });
    }

/*Timer settings*/
    boolean isSafeMode;
    Timer timer = null;
    long lastTime = 0;
    int time;
    TimerTask timerTask;
    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();

    public static void setSaveTime0(){
        Shadow.getAppPreferencesEditor().putLong(Const.SAVE_TIME_LOGOUT,0).commit();
    }

    protected void initAutoLogoutTimer(){
        isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
        time = (Modes.getLogoutTime((isSafeMode) ? ShadowDB.LOGOUT_TIME : ShadowDB.UN_LOGOUT_TIME));
        if(time == (-1)){
            time = 2;
        }
        resetAutoLogoutTimer();
    }

    protected void resetLastTouchTime(){
        lastTime = System.currentTimeMillis();
        saveLogoutTimeByClock();
    }

    protected void saveLogoutTimeByClock(){
        long saveTime = lastTime + (time * Const.ONE_MINUTE_IN_MILLIS);
        Logger.printLog(Logger.ERROR,"saveTime "+saveTime);
        Shadow.getAppPreferencesEditor().putLong(Const.SAVE_TIME_LOGOUT,saveTime).commit();
    }

   protected boolean isAutoLogout(){
        boolean isNeedToLogout = (lastTime+1000) < System.currentTimeMillis();
        return isNeedToLogout;
    }

    protected void resetAutoLogoutTimer(){
        if(isAutoLogout()) {
            resetLastTouchTime();
            if (timer == null) {
                resetTimer();
            } else {
                try {
                    timer.cancel();
                    timer = null;
                    resetTimer();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        gotoReenterScreen();
                    }
                });
            }
        };
    }

    protected void gotoReenterScreen(){
        Logger.printLog(Logger.ERROR,"logout gotoReenterScreen");
        BaseActivity.setSaveTime0();
        BaseSampleActivity.isAppNotClose = true;
        Intent intent = new Intent(this, (Shadow.isProApp() ? Shadow.getProActivity() : FreeAppWayToAccess.class));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    protected void resetTimer(){
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, ((long) time * 60 * 1000));
    }

    protected synchronized void checkLogout(){
        long checkTime = Shadow.getAppPreferences().getLong(Const.SAVE_TIME_LOGOUT, 0);
        long sysCurTime = System.currentTimeMillis();
        Logger.printLog(Logger.ERROR, "checkTime " + checkTime);
        Logger.printLog(Logger.ERROR, "checkTime2  " + sysCurTime);
        if(checkTime != 0 && checkTime < sysCurTime ){
            gotoReenterScreen();
        }
    }

    protected void removeTimer() {
        try {
            if(timer != null){
                timer.cancel();
                timer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeSoftKeyboard(){
      /*  try {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            //imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            imm.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            // imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        BaseFragment.closeSoftKeyboard(this);
    }

    public void closeSoftKeyboard(View et){
        /*try {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        BaseFragment.closeSoftKeyboard(et,this);
    }

    /*Timer settings*/

    /*  @Override
    public boolean onCreateThumbnail(Bitmap outBitmap, Canvas canvas) {
        Bitmap myBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.yp_screenshot);
        canvas.drawBitmap(myBitmap, 0, 0, null);
        return true;
    }*/

    public void callIntent(Class cls) {
        callIntent(cls,null);
    }

    public void callIntent(Class cls, Bundle bundle) {
        callIntent(cls,bundle,false,this);
    }

    public void callIntent(Class cls, boolean isFinish) {
        callIntent(cls,null,isFinish,this);
    }

    public void callIntent(Class cls, Bundle bundle, boolean isFinish) {
        callIntent(cls,bundle,isFinish,this);
    }
    public static void callIntent(Class cls, Bundle bundle, boolean isFinish, Activity activity) {
        Intent intent = new Intent(activity, cls);
        if(bundle != null)
            intent.putExtras(bundle);
        activity.startActivity(intent);
        if (isFinish)
            activity.finish();
    }
}
