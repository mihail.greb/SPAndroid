package com.app.android.shadow.main.custom.call;

import android.app.Activity;
import android.view.View;

//import com.c2call.sdk.lib.data.core.Providers;
//import com.c2call.sdk.lib.util.eventindication.CallIndication;
import com.app.android.shadow.main.util.Logger;
import com.c2call.sdk.pub.common.SCCallInData;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.incomingcall.controller.SCIncomingCallController;
//import com.c2call.sdk.lib.n.f.a;


public class ShadowSCIncomingCallController extends SCIncomingCallController {

    Activity context;
    public ShadowSCIncomingCallController(View view, SCViewDescription viewDescription) {
        super(view, viewDescription);
    }

    @Override
    public void onCreate(Activity context, SCActivityResultDispatcher resultDispatcher) {
        this.context = context;
        super.onCreate(context, resultDispatcher);
        // This seems old code
        //CallIndication.instance().stopIndication();
    }

   /* @Override
    protected void onIndicateCall() {
        //super.onIndicateCall();
        // TODO CHECK Add Custom Ringtone
        CustomCallIndication.instance().indicateCall(context,ShadowSCIncomingCallHandler.data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // TODO CHECK Stop Custom Ringtone
        CustomCallIndication.instance().stopIndication();
    }*/

    @Override
    public void setData(SCCallInData data) {
        super.setData(data);
        Logger.printLog(Logger.ERROR,"User con data id", data.getCallid());
     /*   if (Shadow.shadowDB.isUserBlocked(data.getCallid())) {
            Logger.printLog(Logger.ERROR,"User data id", "finished");
            onButtonRejectClick(getView());
        } else {
            super.setData(data);
        }*/
    }
}
