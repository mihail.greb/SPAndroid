package com.app.android.shadow.core;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.app.android.shadow.common.ui.activities.AddFriendActivity;
import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.common.ui.activities.ShadowContactDetailFragmentActivity;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.samplechat.board.BoardActivity;
import com.app.android.shadow.R;
import com.app.android.shadow.common.ui.activities.CallActivity;
import com.app.android.shadow.common.ui.activities.ProfileActivity;
import com.app.android.shadow.common.ui.activities.RegisterActivity;
import com.app.android.shadow.common.ui.activities.ShadowVideoCallActivity;
import com.app.android.shadow.main.util.Logger;
import com.c2call.sdk.pub.activities.SCFriendsFragmentActivity;
import com.c2call.sdk.pub.common.SCRegistrationData;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.core.SCStartControl;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.gui.videocall.controller.SCVideoCallFactory;
import com.c2call.sdk.pub.video.IVideoSlave;


/**
 * Custom StartControl which will be used by the SDK during this sample app.
 */
public class StartControl extends SCStartControl
{
    @Override
    public boolean openCallbar(final Context activity,
                               final View anchor,
                               final String userid,
                               final int layout,
                               final StartType startType)
    {
        BaseActivity.setSaveTime0();
        BaseSampleActivity.isAppNotClose = true;
        final Intent intent = new Intent(activity, CallActivity.class);
        intent.putExtra(SCExtraData.Callbar.EXTRA_DATA_ID, userid);
        intent.putExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
        return true;
    }

    @Override
    public boolean openBoard(Activity activity, View view, int layout, String userid, StartType startType)
    {
        final Intent intent = new Intent(activity, BoardActivity.class);
        intent.putExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, R.layout.sc_board);
        intent.putExtra(SCExtraData.Board.EXTRA_DATA_USERID, userid);
        activity.startActivity(intent);
        return true;
    }

    @Override
    public boolean openMain(Activity activity) {
        Intent intent = new Intent(activity, SCFriendsFragmentActivity.class);
        intent.putExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, com.c2call.sdk.R.layout.sc_friends);
        activity.startActivity(intent);
        activity.finish();
        return true;
    }

     @Override
    public boolean openVideoCall(Context context, View anchor, int layout, IVideoSlave slave, StartType startType) {
         BaseActivity.setSaveTime0();
         BaseSampleActivity.isAppNotClose = true;
        return this.open(context, anchor, layout, new SCVideoCallFactory(slave), ShadowVideoCallActivity.class, startType);
    }

    @Override
    public boolean openProfile(final Activity activity, final View view, final int layout, final StartType startType)
    {
        final Intent intent = new Intent(activity, ProfileActivity.class);
        intent.putExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, R.layout.sc_edit_profile);
        activity.startActivity(intent);
        return true;
    }

    @Override
    public boolean openRegister(Activity activity, View anchor, int layout, SCRegistrationData data, StartType startType)
    {
        final Intent intent = new Intent(activity, RegisterActivity.class);
        intent.putExtra(SCExtraData.Register.EXTRA_DATA_LAYOUT, layout);
        intent.putExtra(SCExtraData.Register.EXTRA_DATA_REGISTRATION, data);
        activity.startActivity(intent);
        //activity.finish();
        return true;
    }

    @Override
    public boolean openAddFriend(Activity activity, View anchor, int layout, StartType startType) {
        final Intent intent = new Intent(activity, AddFriendActivity.class);
        intent.putExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);
        activity.startActivity(intent);
        //return super.openAddFriend(activity, anchor, layout, startType);
        return true;
    }

    @Override
    public boolean openContactDetail(Activity activity, View anchor, int layout, SCFriendData contact, StartType startType) {
        Logger.printLog(Logger.ERROR,"print details1",""+layout);
        //R.layout.sc_contact_detail;
        //return super.openContactDetail(activity, anchor, layout, contact, startType);
        Intent intent = new Intent(activity, ShadowContactDetailFragmentActivity.class);
        intent.putExtra(SCExtraData.ContactDetail.EXTRA_DATA_USERID, contact.getId());
        intent.putExtra(SCExtraData.ContactDetail.EXTRA_DATA_USER_TYPE, contact.getUserType());
        intent.putExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);
        activity.startActivity(intent);

        return true;
    }
}
