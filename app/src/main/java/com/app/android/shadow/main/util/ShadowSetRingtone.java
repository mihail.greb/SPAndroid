package com.app.android.shadow.main.util;

import android.app.Activity;
import android.content.ContentValues;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.File;

/**
 * Created by rails-dev on 28/9/15.
 */
public class ShadowSetRingtone {

    Uri uriDefault;

    public void changeRingTone(Activity mActivity) {
        File k = new File("//sdcard/", "mysong.mp3"); // path is a file to /sdcard/media/ringtone

        ContentValues values = new ContentValues();
        values.put(MediaStore.MediaColumns.DATA, k.getAbsolutePath());
        values.put(MediaStore.MediaColumns.TITLE, "My Song title");
        values.put(MediaStore.MediaColumns.SIZE, 215454);
        values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
        values.put(MediaStore.Audio.Media.ARTIST, "Madonna");
        values.put(MediaStore.Audio.Media.DURATION, 230);
        values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
        values.put(MediaStore.Audio.Media.IS_NOTIFICATION, false);
        values.put(MediaStore.Audio.Media.IS_ALARM, false);
        values.put(MediaStore.Audio.Media.IS_MUSIC, false);

//Insert it into the database
        Uri uri = MediaStore.Audio.Media.getContentUriForPath(k.getAbsolutePath());
        Uri newUri = mActivity.getContentResolver().insert(uri, values);

        uriDefault = RingtoneManager.getActualDefaultRingtoneUri(mActivity.getApplicationContext(), RingtoneManager.TYPE_RINGTONE);

        RingtoneManager.setActualDefaultRingtoneUri(
                mActivity,
                RingtoneManager.TYPE_RINGTONE,
                newUri
        );

    }

    public void reSetDefautlRingtone(Activity mActivity){
        RingtoneManager.setActualDefaultRingtoneUri(
                mActivity.getApplicationContext(),
                RingtoneManager.TYPE_RINGTONE, uriDefault);
    }
}
