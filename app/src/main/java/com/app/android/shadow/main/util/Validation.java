package com.app.android.shadow.main.util;



import android.text.TextUtils;
import android.widget.EditText;


import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.R;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class Validation {
    private final static String NONE = "None";
    private final static String PRESENCE = "Presence";
    private final static String FORMAT = "Format";
    private final static String LENGTH = "Length";
    private final static String MATCH = "Match";

    public final static String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public final static String PASSWORD_PATTERN =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";


    private EditText validationField;
    private Map<String, String>  validations;
    private String[] errorMessages;

    Validation(){
        //this.validations = new ArrayList<NameValuePair>();
        this.validations = new HashMap<>();
        this.errorMessages = new String[]{};
    }

    public Validation(EditText field){
        this.validationField = field;
        //this.validations = new ArrayList<NameValuePair>();
        this.validations = new HashMap<>();
        this.errorMessages = new String[]{};

    }

    public void setValidationField(EditText field){
        this.validationField = field;
        //this.validations = new ArrayList<NameValuePair>();
        this.validations = new HashMap<>();
        this.errorMessages = new String[]{};
    }

    public EditText getValidationField(){
        return this.validationField;
    }

    public void addPresenceOf(){
        //this.validations.add(new BasicNameValuePair(PRESENCE,PRESENCE));
        this.validations.put(PRESENCE, PRESENCE);
    }

    public void addLengthOf(int length){
        //this.validations.add(new BasicNameValuePair(LENGTH, Integer.toString(length)));
        this.validations.put(LENGTH, Integer.toString(length));
    }

    public void addFormatOf(String format){
        //this.validations.add(new BasicNameValuePair(FORMAT, format));
        this.validations.put(FORMAT, format);
    }

    public void addMatchOf(String value1,String value2){
        //this.validations.add(new BasicNameValuePair(FORMAT, format));
        if(value1.equals(value2))
            this.validations.put(MATCH, MATCH);
        else
            this.validations.put(MATCH, "noMatch");
    }

    private String getValue(){
        return this.validationField.getText().toString();
    }

    private boolean setError(boolean valid, String message){
        if(!valid){
            this.validationField.setError(message);
        }
        return valid;
    }

    public Boolean validate(){
        boolean validAll = true;
        boolean  validField = true;
        this.errorMessages = new String[]{};
        Iterator entries = this.validations.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry element = (Map.Entry) entries.next();
            validField = true;
            if(element.getKey().equals(PRESENCE) && element.getValue().equals(PRESENCE)){
                validField = this.setError(!TextUtils.isEmpty(this.getValue()) , Shadow.getAppContext().getResources().getString(R.string.error_presence_error_msg));
            } else if (element.getKey().equals(FORMAT) && element.getValue().equals(EMAIL_PATTERN)) {
                validField = this.setError(this.getValue().matches((String) element.getValue()), Shadow.getAppContext().getResources().getString(R.string.error_email_error_msg));
            } else if (element.getKey().equals(FORMAT) && element.getValue().equals(PASSWORD_PATTERN)) {
                validField = this.setError(this.getValue().matches((String) element.getValue()), Shadow.getAppContext().getResources().getString(R.string.error_password_error_msg));
            } else if (element.getKey().equals(FORMAT)) {
                validField = this.setError(this.getValue().matches((String) element.getValue()), Shadow.getAppContext().getResources().getString(R.string.error_format_error_msg));
            } else if (element.getKey().equals(LENGTH)) {
                validField = this.setError(this.getValue().length() >= Integer.parseInt((String) element.getValue()), Shadow.getAppContext().getResources().getString(R.string.error_length_error_msg));
            }else if (element.getKey().equals(MATCH) ) {
                validField = this.setError((element.getValue().equals(MATCH)), Shadow.getAppContext().getResources().getString(R.string.error_password_match_msg));
            }
            if(!validField){
                validAll = false;
            }
        }
        return validAll;
    }

    protected boolean validAll(){
        return false;
    }

}
