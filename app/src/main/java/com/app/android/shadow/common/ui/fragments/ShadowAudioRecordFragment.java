package com.app.android.shadow.common.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.fragments.SCAudioRecordFragment;
import com.app.android.shadow.R;


public class ShadowAudioRecordFragment extends SCAudioRecordFragment {

    public static ShadowAudioRecordFragment create(int layout, String output) {
        ShadowAudioRecordFragment f = new ShadowAudioRecordFragment();
        Bundle args = new Bundle();
        args.putString(SCExtraData.AudioRecord.EXTRA_DATA_OUTPUT_PATH, output);
        args.putInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view = super.onCreateView(inflater, container, savedInstanceState);
        setHeader(_view);
        return _view;
    }

    //Header
    TextView title;

    private void setHeader(View v) {
        title = (TextView) v.findViewById(R.id.title_head);
        title.setText(getActivity().getResources().getString(R.string.audio_recording));
        v.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

}
