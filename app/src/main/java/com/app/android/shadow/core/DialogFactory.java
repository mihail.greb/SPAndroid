package com.app.android.shadow.core;

import com.app.android.shadow.common.ui.dialogs.ShadowSCDialogFactory;
import com.app.android.shadow.common.ui.dialogs.ShadowDialogBuilderPhoto;
import com.c2call.sdk.pub.gui.core.controller.IController;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.app.android.shadow.common.ui.dialogs.DialogBuilderInvite;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemController;


/**
 * Custom DialogFactory class which overrides some methods of the default SCDialoFactory
 * to generate certain dialogs.
 */
public class DialogFactory extends ShadowSCDialogFactory
{
    @Override
    public SCChoiceDialog createInviteDialog(IController<?> controller)
    {
        return DialogBuilderInvite.build(controller);
    }

    @Override
    public SCChoiceDialog createSpreadDialog(IController<?> controller)
    {
        return createInviteDialog(controller);
    }

    @Override
    public SCChoiceDialog createAddFriendDialog(IController<?> controller)
    {
        return createInviteDialog(controller);
    }

    @Override
    public SCChoiceDialog createFriendListItemDialog(IFriendListItemController controller) {
         return super.createFriendListItemDialog(controller);
    }

    public SCChoiceDialog createDeleteConformation(IFriendListItemController controller) {
        return super.createConformationDialog(controller);
    }

    @Override
    public SCChoiceDialog createPhotoDialog(IController<?> controller, String filename) {
        return ShadowDialogBuilderPhoto.build(controller, filename);
    }
}
