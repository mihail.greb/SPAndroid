package com.app.android.shadow.main.base;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.tapmanager.extra.MoreController;
import com.app.android.shadow.R;

public class BaseFragment extends Fragment {

    public BaseActivity mActivity;

    public MoreController mCallback;
    public Resources resources;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity		=	(BaseActivity) this.getActivity();
        resources= getResources();
    }

    public boolean onBackPressed(){
        return false;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){

    }

    protected void setHeader(View v) {
        try {
            ImageView backBtn = (ImageView) v.findViewById(R.id.back);
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().onBackPressed();
                }
            });
            mCallback.addToIncressClickArea(backBtn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (MoreController) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement Interface");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (MoreController) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement Interface");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        BaseSampleActivity.mCurrentFragment = BaseFragment.this;
        closeSoftKeyboard();
    }

    /* public void Success(String response){
    }

    public void Failier(String response){
        Toast.makeText(getActivity(), "" + response, Toast.LENGTH_LONG).show();
    }

    public void Failier(){
        Toast.makeText(getActivity(), "" + "Please try again", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            InputMethodManager imm = (InputMethodManager)
                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
           // imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    }*/

    @Override
    public void onPause() {
        super.onPause();
       closeSoftKeyboard();
    }

    public void openSoftKeyboard(){
        try {
            InputMethodManager imm = (InputMethodManager)
                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openSoftKeyboard(View v){
        try {
            InputMethodManager imm = (InputMethodManager)
                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInputFromInputMethod(v.getWindowToken(),0);
            //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
           // imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

  /*  public void openSoftKeyboard(View v){
        try {
            InputMethodManager imm = (InputMethodManager)
                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(v,0);
            //imm.showSoftInputFromInputMethod(v.getWindowToken(),0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public int getColorId(int id){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return resources.getColor(id,null);
        }else {
            return resources.getColor(id);
        }
    }

    public void closeSoftKeyboard(){
        closeSoftKeyboard(getActivity());
    }

    public static void closeSoftKeyboard(Activity mCon){
       /* try {
            InputMethodManager imm = (InputMethodManager)
                    mCon.getSystemService(Context.INPUT_METHOD_SERVICE);
            try {
                imm.hideSoftInputFromWindow(mCon.getCurrentFocus().getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            } catch (Exception e) {
                e.printStackTrace();
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }*/
        closeSoftKeyboard(mCon.getCurrentFocus(),mCon);
    }

    public void closeSoftKeyboard(View v){
        closeSoftKeyboard(v,getActivity());
    }

    public static void closeSoftKeyboard(View v,Activity mCon){
        try {
            InputMethodManager imm = (InputMethodManager)
                    mCon.getSystemService(Context.INPUT_METHOD_SERVICE);
            try {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
          /*  if (imm.isAcceptingText()) {
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Drawable getnewDrawable(int id){
       return BaseFragment.getnewDrawable(id,resources);
    }
    public static Drawable getnewDrawable(int id, Resources resources){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return resources.getDrawable(id,null);
        }else {
            return resources.getDrawable(id);
        }
    }

}
