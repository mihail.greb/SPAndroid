package com.app.android.shadow.common.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.android.shadow.common.ui.activities.CallActivity;
import com.app.android.shadow.R;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.fragments.SCCallbarFragment;

public class ShadowCallbarFragment extends SCCallbarFragment {

    TextView durationLable,userName;

    public static ShadowCallbarFragment create(String id, int layout) {
        Bundle args = new Bundle();
        args.putString(SCExtraData.Callbar.EXTRA_DATA_ID, id);
        args.putInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);
        ShadowCallbarFragment fragment = new ShadowCallbarFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        durationLable = (TextView) v.findViewById(R.id.sc_callbar_label_duration);
        userName = (TextView) v.findViewById(R.id.sc_callbar_label_name);
        TextView incomingCall= (TextView) v.findViewById(R.id.incoming_call);
        if(CallActivity.isCalling) {
            incomingCall.setText(getResources().getString(R.string.outgoing_call));
            CallActivity.isCalling = false;
        }
        return v;
    }

    public String getDurationLable() {
        try {
            return durationLable.getText().toString();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getUserName() {
        try {
            return userName.getText().toString().trim();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return "";
        }
    }


}
