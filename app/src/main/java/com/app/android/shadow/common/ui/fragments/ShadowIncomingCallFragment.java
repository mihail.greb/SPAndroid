package com.app.android.shadow.common.ui.fragments;

import android.os.Bundle;
import android.view.View;

import com.app.android.shadow.main.custom.call.ShadowSCIncomingCallController;
import com.app.android.shadow.main.util.Logger;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.common.SCIncomingCallData;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.fragments.SCIncomingCallFragment;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.incomingcall.controller.IIncomingCallController;

public class ShadowIncomingCallFragment extends SCIncomingCallFragment {


    public static ShadowIncomingCallFragment create(SCIncomingCallData data, int layout) {
        Ln.d("fc_tmp", "SCIncomingCallFragment.onCreate() - %s", new Object[]{data});
        Bundle args = new Bundle();
        args.putParcelable(SCExtraData.IncomingCall.EXATR_DATA_INCOMING_CALL, data);
        args.putInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);
        ShadowIncomingCallFragment fragment = new ShadowIncomingCallFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected IIncomingCallController onCreateController(View v, SCViewDescription vd) {
        Logger.printLog(Logger.ERROR,"working ", "onCreateController");
        return new ShadowSCIncomingCallController(v, vd);
    }
}
