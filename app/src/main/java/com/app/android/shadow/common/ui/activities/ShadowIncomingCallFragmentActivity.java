package com.app.android.shadow.common.ui.activities;

import android.app.Fragment;

import com.app.android.shadow.common.ui.fragments.ShadowIncomingCallFragment;
import com.app.android.shadow.main.util.Logger;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.activities.SCIncomingCallFragmentActivity;
import com.c2call.sdk.pub.common.SCIncomingCallData;
import com.c2call.sdk.pub.core.SCExtraData;

public class ShadowIncomingCallFragmentActivity extends SCIncomingCallFragmentActivity {

       protected Fragment onCreateFragment() {
        Logger.printLog(Logger.ERROR,"working ShadowIncomingCallFragmentActivity");
        Ln.d("fc_tmp", "SCIncomingCallFragmentActivity.onCreate()", new Object[0]);
        SCIncomingCallData data = (SCIncomingCallData)this.getIntent().getParcelableExtra(SCExtraData.IncomingCall.EXATR_DATA_INCOMING_CALL);
        if(data == null) {
            throw new IllegalStateException("IncomingCallData must not be null and must be passed by intent with key: ExtraData.IncomingCall.EXATR_DATA_INCOMING_CALL");
        } else {
                int layout = this.getLayoutFromIntent();
                if (layout == 0) {
                    layout = this.getLayout(data.isVideoCall);
                }
                ShadowIncomingCallFragment fragment = ShadowIncomingCallFragment.create(data, layout);
                return fragment;
        }
    }
}
