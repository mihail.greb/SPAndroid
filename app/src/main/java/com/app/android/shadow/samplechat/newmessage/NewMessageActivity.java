package com.app.android.shadow.samplechat.newmessage;

import android.app.Fragment;
import android.os.Bundle;

import com.c2call.sdk.pub.activities.SCNewMessageActivity;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.db.data.SCFriendData;

/**
 * Simple custom activity to compose new messages or start a new chat
 */
public class NewMessageActivity extends SCNewMessageActivity
{
    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        getActionBar().show();
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected Fragment onCreateFragment()
    {
        /*
        Create our custom NewMessageFragment which will then be used by this activity
         */
        int layout = getIntent().getIntExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, 0);
        final SCFriendData contact = (SCFriendData)getIntent().getSerializableExtra(SCExtraData.NewMessage.EXTRA_DATA_CONTACT);
        return NewMessageFragment.create(contact, layout);
    }
}
