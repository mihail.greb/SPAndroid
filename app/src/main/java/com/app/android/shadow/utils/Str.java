package com.app.android.shadow.utils;

import org.apache.http.util.ByteArrayBuffer;

import java.io.IOException;
import java.io.InputStream;

public class Str
{

    public static String toSafeString(final String s)
    {
        return (s == null)
                    ? ""
                    : s;
    }

    public static String toString(final InputStream stream) throws IOException
    {
        final ByteArrayBuffer baf = new ByteArrayBuffer(50);
        int read = 0;
        final int bufSize = 512;
        final byte[] buffer = new byte[bufSize];
        while((read = stream.read(buffer)) >= 0)
        {
             baf.append(buffer, 0, read);
        }

        return new String(baf.toByteArray(), "utf-8");
    }


    public static boolean isLonger(final String s, final int len)
    {
        return isLonger(s, len, false);
    }

    public static boolean isLonger(final String s, final int len, final boolean acceptEqual)
    {
        if (s == null){
            return false;
        }

        return acceptEqual
                ? s.length() >= len
                : s.length() > len;
    }



    public static String toEclipseString(final String s, final int maxLength)
    {
        if (s.length() <= maxLength
            || maxLength < 4)
        {
            return s;
        }

        final StringBuilder builder = new StringBuilder(maxLength);
        builder.append(s.substring(0, maxLength - 3)).append("...");

        return builder.toString();
    }

    public static boolean isEmpty(final String s)
    {
        return (s == null
                || s.length() == 0);
    }

    public static boolean isEmpty(final String s, final String... others)
    {
        if (others == null){
            return isEmpty(s);
        }
        else{
            for (final String cur : others){
                if (isEmpty(cur)){
                    return true;
                }
            }
        }

        return false;
    }

    public static boolean isPhonenumber(final String s)
    {
        return !isEmpty(s)
                && s.charAt(0) == '+';
    }




}
