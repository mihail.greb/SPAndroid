package com.app.android.shadow.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.app.android.shadow.common.ui.activities.LoginActivity;
import com.app.android.shadow.common.ui.core.ExtraData;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.dbhelper.ShadowDB;
import com.app.android.shadow.main.login.UserLoginTask;
import com.app.android.shadow.main.tapmanager.settings.Modes;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.samplechat.SimpleChatMainActivity;
import com.app.android.shadow.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class CountClickBase extends BaseActivity {

    /*Shadow handler*/
    protected int i = -1;
    protected int s = -1;
    protected Handler handler;
    protected Handler handlerSwipe;
    protected Runnable r;
    protected Runnable rSwipe;
    String uNameText,uAgeText;
    EditText name,age;

    public static boolean isDecoyActive = false;

    protected boolean isSafeMode = true;
    protected boolean isxTimes =false;
    protected boolean isxTimesSwipe =false;
    protected boolean isPassRequired = true;
    protected boolean isPassRequiredSwipe = true;

    private TextView enterNameAge;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uNameText = Shadow.pref.getString(Const.USERNAME, "");
        uAgeText = Shadow.pref.getString(Const.PASSWORD, "");
        Logger.printLog(Logger.ERROR,"userName ", "is " + uNameText);
        Logger.printLog(Logger.ERROR,"userAge ", "is " + uAgeText);
        isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
        Logger.printLog(Logger.ERROR,"isSafeMode ", "is " + isSafeMode);
         /*Shadow*/
        initHandler();
        isDecoyActive = true;
    }

    protected void initHandler(){
        handler = new Handler();
        r = new Runnable() {
            @Override
            public void run() {
                i = -1;
            }
        };


      /*  handlerSwipe = new Handler();
        rSwipe = new Runnable() {
            @Override
            public void run() {
                s = -1;
            }
        };*/
    }

    protected void initCustomeBox(){
        Logger.printLog(Logger.ERROR,"initCustomeBox ", "is working");
        try {
            enterNameAge = getClickableTextView();
            if(enterNameAge != null) {
                if (!Modes.getStatus((isSafeMode) ? ShadowDB.IS_ENTER_NAME_PASS : ShadowDB.UN_IS_ENTER_NAME_PASS)) {
                    enterNameAge.setVisibility(View.GONE);
                    Logger.printLog(Logger.ERROR,"initCustomeBox ", "if working");
                } else {
                    Logger.printLog(Logger.ERROR,"initCustomeBox ", "else working");
                    enterNameAge.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onClickOfEnterNameAge();
                        }
                    });
                }
                incresClickArea(enterNameAge);
            }
            name = (EditText) findViewById(R.id.name);
            age = (EditText) findViewById(R.id.age);
        }catch (Exception e){
            e.printStackTrace();
        }

        if (isSafeMode) {
            isxTimes = Modes.getStatus(ShadowDB.IS_TAB_X_TIMES);
            isxTimesSwipe = Modes.getStatus(ShadowDB.IS_SWIPE_X_TIMES);
        } else {
            isPassRequired = Modes.getStatus(ShadowDB.UN_IS_PASS_ON_X_TABS);
            isPassRequiredSwipe = Modes.getStatus(ShadowDB.UN_IS_PASS_ON_X_SWIPE);
        }

    }

    protected void checkToGoNextBySwipe(){
        Logger.printLog(Logger.ERROR, "goTonextcount ");
        if(Shadow.isCalculator()){
            if(!isSafeMode && Modes.getStatus(ShadowDB.UN_IS_PASS_ON_X_SWIPE)){
                goToNextScreen();
            }
        }else {
            if (isSafeMode) {
                if (isxTimesSwipe) {
                    Logger.printLog(Logger.ERROR, "isSafe " + isSafeMode + "  isxTimes " + isxTimesSwipe);
                    countSwipe();
                } else {
                    Logger.printLog(Logger.ERROR, "if else isSafe " + isSafeMode + "  isxTimes " + isxTimesSwipe);
                }
            } else {
                Logger.printLog(Logger.ERROR, "else isSafe " + isSafeMode + "  isPasswordReq " + isPassRequiredSwipe);
                countSwipe();
            }
        }
    }
    protected void checkToGoNext(){
        Logger.printLog(Logger.ERROR, "goTonextcount ");
        if(isSafeMode){
            if(isxTimes) {
                Logger.printLog(Logger.ERROR, "isSafe "+isSafeMode+"  isxTimes "+isxTimes);
                countClick();
            }else {
                Logger.printLog(Logger.ERROR, "if else isSafe "+isSafeMode+"  isxTimes "+isxTimes);
            }
        }else {
            Logger.printLog(Logger.ERROR, "else isSafe "+isSafeMode+"  isPasswordReq "+isPassRequired);
            countClick();
        }
    }

    protected void onClickOfEnterNameAge() {
        showPopupWindow();
    }

    public void showPopupWindow(){
        try {
            LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
            final View layout = layoutInflater.inflate(R.layout.custom_popup, null);
            LinearLayout.LayoutParams parm1=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            parm1.setMargins(20, 0, 20, 0);
            layout.setLayoutParams(parm1);
            final PopupWindow popup = new PopupWindow(this);

            final EditText userNamePopup = (EditText) layout.findViewById(R.id.name);
            final EditText userAgePopup = (EditText) layout.findViewById(R.id.age);
            if(Shadow.isProApp() && (Shadow.isAlarmGame() || Shadow.isCalculator())){
                userNamePopup.setHint(resources.getString(R.string.shadow_name));
                userAgePopup.setHint(resources.getString(R.string.password));
                userAgePopup.setInputType(InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_PASSWORD);
                //
                // userAgePopup.setSelection(userAgePopup.getText().length());
            }
            TextView cancel = (TextView) layout.findViewById(R.id.cancel);
            final TextView submit = (TextView) layout.findViewById(R.id.submit);

            userAgePopup.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {

                    if (id == R.id.action_submit || id == EditorInfo.IME_NULL) {
                        submit.performLongClick();
                        return true;
                    }
                    return false;
                }

            });

             /* if(Shadow.isTestingOn){
                  userNamePopup.setText(uNameText);
                  userAgePopup.setText(uAgeText);
            }*/

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*if ((userNamePopup.getText().toString().toLowerCase().trim()).equals(uNameText)
                            && (userAgePopup.getText().toString()).equals(uAgeText)) {


                    } else {*/
                    if(userNamePopup != null && userAgePopup != null &&
                            userNamePopup.getText().toString().length() > 0 && userAgePopup.getText().toString().length() > 7) {
                        //Logger.printLog(Logger.ERROR, "is " + uNameText + " and pass " + uAgeText);
                        Logger.printLog(Logger.ERROR, "is " + userNamePopup.getText() + " and pass " + userAgePopup.getText());
                        callApi(userNamePopup.getText().toString().toLowerCase(), userAgePopup.getText().toString());
                    }else {
                        setNameAge(userNamePopup.getText().toString(),userAgePopup.getText().toString());
                    }
                    BaseFragment.closeSoftKeyboard(name, CountClickBase.this);
                    BaseFragment.closeSoftKeyboard(age, CountClickBase.this);
                    popup.dismiss();
                    //}
                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popup.dismiss();
                }
            });

            //popup.setAnimationStyle(R.style.MyCustomDialogTheme);
            popup.setContentView(layout);
            popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
            popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
            popup.setFocusable(true);
            //popup.setBackgroundDrawable(new BitmapDrawable(getResources()));
            // popup.showAtLocation(layout, Gravity.LEFT | Gravity.TOP, rect.left -
            // v.getWidth(), getDeviceHeight() - rect.top);
            //popup.showAtLocation(layout, Gravity.CENTER_VERTICAL,layout.getWidth()/2, layout.getHeight()/2);
            popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void callApi(String nameSt, String ageSt){
        UserLogin mSignUpTask = new UserLogin(this,nameSt,ageSt);
        mSignUpTask.setServiceTaskMapStrParams(APIServiceAsyncTask.login, getLoginParams(nameSt,ageSt));
        mSignUpTask.execute((Void) null);
    }

    class UserLogin extends UserLoginTask {
        String nameSt;
        String ageSt;
        protected UserLogin(Context mContext,String nameSt, String ageSt) {
            super(mContext, ageSt);
            this.nameSt = nameSt;
            this.ageSt = ageSt;
        }

        @Override
        protected void fireEvent() {
            //Shadow.getAppPreferencesEditor().putString(Const.USER, nameSt);
            //Shadow.getAppPreferencesEditor().putString(Const.PASSWORD, ageSt);
            goToNextScreen();
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
            //super.failure(jsonObj, serviceTaskType);
            failure("");
        }

        @Override
        protected void failure(String message) {
            //super.failure(message);
            setNameAge(nameSt,ageSt);
        }
    }

    private void setNameAge(String nameSt,String ageSt){
        try {
            name.setText(nameSt);
            age.setText(ageSt);
            name.setVisibility(View.VISIBLE);
            age.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
            Shadow.alertToast("Data Saved");
        }
    }

    private Map<String, String> getLoginParams(String nameSt, String ageSt){
        Map<String, String> params = new HashMap<>();
        params.put("user[username]", nameSt.toLowerCase());
        params.put("user[password]", ageSt);
        params.put("app_name", Shadow.getAppCodeName().toString());
        return params;
    }

    public TextView getClickableTextView() {
        return null;
    }

    protected void countSwipe() {
       /* Init handlerSwipe when uncommit this code
        Logger.printLog(Logger.ERROR,"countClick wrokgin");;
        if (s == 6) {
            s = -1;
            handlerSwipe.removeCallbacks(rSwipe);
            if(!isSafeMode && !isPassRequiredSwipe){
                goToNextScreen();
            }else {
                showPopupWindow();
            }
        } else {
            s++;
            Logger.printLog(Logger.ERROR,"working", "" + s);
            if (s == 0) {
                handlerSwipe.removeCallbacks(rSwipe);
                handlerSwipe.postDelayed(rSwipe, 8000);
            }
        }*/

        if(!isSafeMode && !isPassRequiredSwipe){
            goToNextScreen();
        }else {
            showPopupWindow();
        }
    }
    protected void countClick() {
        Logger.printLog(Logger.ERROR,"countClick wrokgin");;
        if (i == (Integer.parseInt(Modes.getSavedTabs())-2)) {
            i = -1;
            handler.removeCallbacks(r);
            if(!isSafeMode && !isPassRequired){
                goToNextScreen();
            }else {
                showPopupWindow();
            }
        } else {
            i++;
            Logger.printLog(Logger.ERROR,"working", "" + i);
            if (i == 0) {
                handler.removeCallbacks(r);
                handler.postDelayed(r, 8000);
            }
        }
    }

    public void goToNextScreen(){
        //SipMediator.initInstantPresenceSubsribeThread();
        try {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.putExtra(ExtraData.Login.MAIN_ACTIVITY, SimpleChatMainActivity.class);
            intent.putExtra(ExtraData.Login.PASS_LOGIN, false);
            intent.putExtra(ExtraData.Login.BY_GAME, true);
            startActivity(intent);
            //startActivity(new Intent(this, SimpleChatMainActivity.class));
            finish();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        // SCCoreFacade.instance().unsubscribe(this);
        super.onDestroy();
        isDecoyActive = false;
    }



    @Override
    protected void onStart() {
        super.onStart();
        //SCCoreFacade.instance().subscribe(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        exittoApp();
    }

    public static boolean isDecoyActivity(){
        return isDecoyActive;
    }
}
