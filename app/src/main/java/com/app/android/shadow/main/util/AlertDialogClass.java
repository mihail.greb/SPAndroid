package com.app.android.shadow.main.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;


import com.app.android.shadow.main.tapmanager.settings.Modes;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.R;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.samplechat.SimpleChatMainActivity;

/**
 * Created by abc on 4/7/2015.
 */
public class AlertDialogClass {

    Context mContext;

    public AlertDialogClass(Context mContext){
        this.mContext = mContext;
    }

    public void openNoAlert(String message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext, AlertDialog.THEME_TRADITIONAL);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK",new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

        public void open(String message){
            open("Alert",message);
        }

        public void open(String title,String message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext, AlertDialog.THEME_TRADITIONAL);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK",new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void open2(String massage){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext, AlertDialog.THEME_TRADITIONAL);
        alertDialogBuilder.setTitle("Alert");
        alertDialogBuilder.setMessage(massage);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK",new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void open(String massage,final Fragment f){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext, AlertDialog.THEME_TRADITIONAL);
        alertDialogBuilder.setTitle("Alert");
        alertDialogBuilder.setMessage(massage);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK",new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
               // ((BaseActivity)mContext).CallFragment(f);
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static void showPopupWindow(Activity mContext, String message, final android.app.Fragment currentFragment){
        try{
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = layoutInflater.inflate(R.layout.shadow_first_time_popup, null);

            LinearLayout.LayoutParams parm1=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            parm1.setMargins(20, 0, 20, 0);
            layout.setLayoutParams(parm1);
            final PopupWindow popup = new PopupWindow(mContext);

            if(mContext instanceof SimpleChatMainActivity && currentFragment == null) {
                TextView titleMsg = (TextView) layout.findViewById(R.id.titleMsg);
                //titleMsg.setText(mContext.getResources().getString(R.string.first_time_time_msg));
                titleMsg.setText("");
            }
            TextView close = (TextView) layout.findViewById(R.id.ok_popup);
            TextView msg = (TextView) layout.findViewById(R.id.msg_first_dialog);
            if(Shadow.isCalculator()) {
                String emailText = mContext.getResources().getString(R.string.first_time_login_dialog_cal_clickable);
                SpannableString ss = new SpannableString(message);
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View textView) {
                        if(currentFragment instanceof Modes)
                        ((Modes)currentFragment).callSaveCalculation();

                        popup.dismiss();
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };
                ss.setSpan(clickableSpan, message.indexOf(emailText.toString()), (message.indexOf(emailText.toString())+emailText.toString().length()), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                msg.setText(ss);
                msg.setMovementMethod(LinkMovementMethod.getInstance());
                msg.setHighlightColor(Color.TRANSPARENT);
            }else {
                msg.setText(message);
            }

            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popup.dismiss();
                }
            });

            //popup.setAnimationStyle(R.style.MyCustomDialogTheme);
            popup.setContentView(layout);
            popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
            popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);

            //popup.setFocusable(true);
            BaseFragment.closeSoftKeyboard(popup.getContentView(), mContext);
            BaseFragment.closeSoftKeyboard(mContext);
            //popup.setBackgroundDrawable(new BitmapDrawable(getResources()));

            // popup.showAtLocation(layout, Gravity.LEFT | Gravity.TOP, rect.left -
            // v.getWidth(), getDeviceHeight() - rect.top);
            //popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

            new Handler().postDelayed(new Runnable(){

                public void run() {
                    popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
                }

            }, 500L);

        }
        catch (Exception e){e.printStackTrace();
        }
    }


   /* public static void showCustomPopupMenu(Context mContext,String message)
    {
        WindowManager windowManager2 = (WindowManager)mContext.getSystemService(mContext.WINDOW_SERVICE);
        LayoutInflater layoutInflater=(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=layoutInflater.inflate(R.layout.shadow_first_time_popup, null);
        TextView close = (TextView) view.findViewById(R.id.ok_popup);
        TextView msg = (TextView) view.findViewById(R.id.msg_first_dialog);
        msg.setText(message);

        *//*close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });*//*
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity=Gravity.CENTER|Gravity.CENTER;
        params.x=0;
        params.y=0;
        windowManager2.addView(view, params);
    }*/

}
 /* public void customeDialog(ArrayList<String > list){

        // custom dialog
        //final Dialog dialog = new Dialog(mContext,R.style.MyCustomDialogTheme);
        final Dialog dialog = new Dialog(mContext);
        *//*dialog.getWindow();*//*
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.getWindow().getAttributes().windowAnimations = R.style.MyCustomDialogTheme;
        //dialog.setTitle("Title...");
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.total_contacts);
        text.setText(mContext.getResources().getString(R.string.total_cap) + ":  " + list.size() + " " + mContext.getResources().getString(R.string.contacts));
        ListView listView = (ListView) dialog.findViewById(R.id.dia_contact_list);
        listView.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, list));

        Button dialogButton = (Button) dialog.findViewById(R.id.accept);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
                dialog.cancel();
            }
        });
        dialog.show();
    }

    private PopupWindow showOptions(Context mcon){
        try{
            LayoutInflater inflater = (LayoutInflater) mcon.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.custom_dialog,null);
            layout.setAnimation(AnimationUtils.loadAnimation(mContext, R.anim.popup_in));

            PopupWindow optionspu = new PopupWindow(layout, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            optionspu.setFocusable(true);
            optionspu.showAtLocation(layout, Gravity.TOP, 0, 0);
            //optionspu.u.pdate(0, 0, LinearLayout.LayoutParams.WRAP_CONTENT, (int)(hei/5));
            optionspu.setAnimationStyle(R.anim.popup_out);
            return optionspu;
        }
        catch (Exception e){e.printStackTrace();
            return null;}
    }*/
