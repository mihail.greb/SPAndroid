package com.app.android.shadow.game.alarm;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import com.app.android.shadow.game.alarm.alert.AlarmAlertActivity;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.R;
import com.app.android.shadow.main.util.Logger;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Custom Board Fragment to show a chat history grouped by users
 */
public class AlarmTimerFragment extends BaseFragment implements View.OnClickListener {
    //Header
    @Override
    protected void setHeader(View v) {
        //super.setHeader(v);
        TextView title = (TextView) v.findViewById(R.id.title_head);
        title.setText(resources.getString(R.string.alarm_timer));
    }

    Button btnStart, btnReset, btnSetTimer;
    int hrTime = 0,minTime = 0;
    TextView time;
    long starttime = 0L;
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedtime = 0L;
    int t = 1;
    int secs = -1;
    int mins = 0;
    int hrs = 0;
    int milliseconds = 0;
    Handler handler = new Handler();



    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view =inflater.inflate(R.layout.shadow_alarm_timer, container, false);

        btnStart = (Button) _view.findViewById(R.id.start);
        btnReset = (Button) _view.findViewById(R.id.reset);
        btnSetTimer = (Button) _view.findViewById(R.id.set_timer);
        btnStart.setOnClickListener(this);
        btnReset.setOnClickListener(this);
        btnSetTimer.setOnClickListener(this);

        time = (TextView) _view.findViewById(R.id.timer);

        setHeader(_view);

    return _view;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.start:
                onStartClick();
                break;

            case R.id.reset:
                onRestClick();
                break;

            case R.id.set_timer:
                callSaveTime();
                break;
        }

    }

    private void onStartClick(){
        if(hrs != 0 || mins != 0 || secs != -1) {
            if (t == 1) {
//timer will start
                btnStart.setText(resources.getString(R.string.pause));
                btnStart.setTextColor(Color.GREEN);
                //starttime = SystemClock.uptimeMillis();
                time.setTextColor(Color.BLACK);
                startTimerAndShow();
                //handler.postDelayed(updateTimer, 0);
                t = 0;
            } else {
//timer will pause
                btnStart.setText(resources.getString(R.string.start));
                btnStart.setTextColor(Color.BLACK);
                time.setTextColor(Color.BLUE);
                removeTimer();
                //timeSwapBuff += timeInMilliseconds;
               // handler.removeCallbacks(updateTimer);
                t = 1;
            }
        }else {
            Shadow.alertToast(resources.getString(R.string.set_timer_first));
        }
    }

    private void removeTimer(){
        try {
            tsk.cancel();
            tsk = null;
            t2.purge();
            t2.cancel();
            t2 = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onRestClick(){
       /* starttime = 0L;
        timeInMilliseconds = 0L;
        timeSwapBuff = 0L;
        updatedtime = 0L;*/
        //milliseconds = 0;
        //handler.removeCallbacks(updateTimer);
        removeTimer();
        t = 1;
        secs = -1;
        mins = 0;
        hrs = 0;
        btnStart.setText("Start");
        btnStart.setTextColor(Color.BLACK);
        time.setText("00:00:00");
        time.setTextColor(Color.BLACK);

    }

    private void callSaveTime() {
        if(hrs == 0 && mins == 0 && secs == -1) {
            final int hour;
            final int minute;
            Calendar mcurrentTime = Calendar.getInstance();
            hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
                    Calendar newAlarmTime = GregorianCalendar.getInstance();
                    newAlarmTime.setTime(new Date());
                    newAlarmTime.set(Calendar.HOUR_OF_DAY, hours);
                    newAlarmTime.set(Calendar.MINUTE, minutes);
                    newAlarmTime.set(Calendar.SECOND, 0);
                    Logger.printLog(Logger.ERROR, "set Time in ms hour min" + hours + "  " + minutes);
                    //AlarmPreferencesFragment.saveAlarmSaveTime(newAlarmTime.getTimeInMillis());
                    //starttime = newAlarmTime.getTimeInMillis();
                    Logger.printLog(Logger.ERROR, "set Time in ms " + newAlarmTime.getTimeInMillis());
                    //Logger.printLog(Logger.ERROR,"set Time in ms hour"+msToHour(newAlarmTime.getTimeInMillis()));
                    //Logger.printLog(Logger.ERROR,"set Time in ms hour"+msToMin(newAlarmTime.getTimeInMillis()));
                    setTimerTimeText(hours, minutes);
                }
            }, hour, minute, true);
            timePickerDialog.setTitle(resources.getString(R.string.choose_timer_time));
            timePickerDialog.show();
        }else {
            Shadow.alertToast(resources.getString(R.string.use_reset));
        }
    }

    private void setTimerTimeText(int hours, int minutes) {
        time.setText("" + String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":"
                + String.format("%02d", 0));
        this.hrs = hours;
        this.mins= minutes;
        this.secs = 0;
    }



    Timer t2;
    TimerTask tsk;
    private void startTimerAndShow() {

       tsk = new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if(hrs == 0 && mins == 0 && secs == -1){
                            Shadow.alertToast("Working");
                            fireAlarm();
                            onRestClick();
                        }else {
                            if (secs == -1) {
                                secs = 59;
                                if((mins > -1))
                                    --mins;
                            }
                            if (mins == -1) {
                                mins = 59;
                                if(hrs >-1)
                                    --hrs;
                            }
                            time.setText("" + String.format("%02d", hrs) + ":"
                                    + String.format("%02d", mins)+ ":"
                                    + String.format("%02d", secs));
                            secs--;
                        }
                    }

                });
            }

        };
        //Declare the timer
        t2 = new Timer();
        //Set the schedule function and rate
        t2.scheduleAtFixedRate(tsk, 0, 1000);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        onRestClick();
    }

    private void fireAlarm(){
        Intent mathAlarmAlertActivityIntent;

        mathAlarmAlertActivityIntent = new Intent(getActivity(), AlarmAlertActivity.class);
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        Alarm alarm = new Alarm(cal);
        mathAlarmAlertActivityIntent.putExtra("alarm", alarm);

        mathAlarmAlertActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mathAlarmAlertActivityIntent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

        getActivity().startActivity(mathAlarmAlertActivityIntent);

    }

}
  /*  public Runnable updateTimer = new Runnable() {

        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - starttime;

            updatedtime = timeSwapBuff + timeInMilliseconds;

           *//* secs = (int) (updatedtime / 1000);
            hrs = (secs > (60*60))?(secs / (60*60)):0;
            mins = (secs > (60*60))?(secs / 60):0;
            secs = secs % 60;*//*

            Calendar newAlarmTime = GregorianCalendar.getInstance();
            newAlarmTime.setTimeInMillis(updatedtime);
            hrs = newAlarmTime.get(Calendar.HOUR_OF_DAY);
            mins = newAlarmTime.get(Calendar.MINUTE);
            secs = newAlarmTime.get(Calendar.SECOND);

            Logger.printLog(Logger.ERROR, "timer time " + hrs + "  " + mins+ "  " + secs);
            time.setText("" + String.format("%02d", hrs) + ":" + String.format("%02d", mins) + ":"
                    + String.format("%02d", secs));
            time.setTextColor(Color.GRAY);
            handler.postDelayed(this, 0);
        }

    };*/

