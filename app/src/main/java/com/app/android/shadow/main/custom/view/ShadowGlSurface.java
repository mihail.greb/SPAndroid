package com.app.android.shadow.main.custom.view;

import android.content.Context;
import android.util.AttributeSet;

import com.c2call.sdk.pub.gui.custom.ViewVideoCallSlotContext;

/**
 * Created by rails-dev on 25/2/16.
 */
public class ShadowGlSurface extends ViewVideoCallSlotContext {


    public ShadowGlSurface(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ShadowGlSurface(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
