package com.app.android.shadow.utils.custom;


import android.graphics.Bitmap;
import com.c2call.lib.androidlog.Ln;
import java.lang.reflect.Method;

public class __ThumbnailUtils extends BackwardCompatibleClass {
    private static Class __thumbnailUtilsClass = null;
    private static Method __createVideoThumbnail = null;

    public __ThumbnailUtils() {
    }

    public static Bitmap createVideoThumbnail(String filePath, int kind) {
        try {
            return (Bitmap)invokeMethod(__createVideoThumbnail, (Object)null, new Object[]{filePath, Integer.valueOf(kind)});
        } catch (Exception var3) {
            var3.printStackTrace();
            return null;
        }
    }

    static {
        try {
            __thumbnailUtilsClass = Class.forName("android.media.ThumbnailUtils");
        } catch (Exception var1) {
            Ln.e("fc_error", "* * * Class __profileClass not found", new Object[0]);
        }

        if(__thumbnailUtilsClass != null) {
            __createVideoThumbnail = initMethod(__thumbnailUtilsClass, "createVideoThumbnail", new Class[]{String.class, Integer.TYPE});
        }

    }
}

