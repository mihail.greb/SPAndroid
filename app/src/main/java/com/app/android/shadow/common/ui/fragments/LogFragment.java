package com.app.android.shadow.common.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.android.shadow.common.ui.custom.LogView;
import com.app.android.shadow.events.LogEvent;
import com.app.android.shadow.utils.LogUtil;
import com.app.android.shadow.R;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.SCThreadMode;
import com.c2call.sdk.pub.facade.SCCoreFacade;

/**
 * A simple frgament that shows a {@link LogView} and automatically listens
 * for {@link LogEvent} to display them
 */
public class LogFragment extends Fragment
{
    private LogView _logView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.app_log, null);

        onInitChildren(v);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        /*
        Subscribe to the C2Call EventBus to listen for log events
         */
        SCCoreFacade.instance().subscribe(this);
    }

    @Override
    public void onDestroy()
    {
        /*
        Unsubscribe to avoid memory leaks
         */
        SCCoreFacade.instance().unsubscribe(this);
        super.onDestroy();
    }


    private void onInitChildren(View v)
    {
        _logView = (LogView)v.findViewById(R.id.app_logview);
    }

    /**
     * Convenient method to directly post LogEvents in subclasses
     * @param msg the message to post
     */
    protected void logd(String msg)
    {
        LogUtil.postLog(msg);
    }

    @SCEventCallback(threadMode = SCThreadMode.MainThread)
    private void onEvent(LogEvent evt)
    {
        Ln.d("c2app", "LogFragment.onEvent() - this: %s, evt: %s", this, evt);
        _logView.println(evt.getMsg());
    }

}
