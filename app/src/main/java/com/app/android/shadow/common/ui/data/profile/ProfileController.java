package com.app.android.shadow.common.ui.data.profile;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.common.ui.fragments.ProfileFragment;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.dialog.SCDialogCountrySelection;
import com.c2call.sdk.pub.gui.profile.controller.IProfileViewHolder;
import com.c2call.sdk.pub.gui.profile.controller.SCProfileController;
import com.c2call.sdk.pub.util.SCCountry;

/**
 * A Custom Profile Controller class which extends the basic SCProfileController.
 * While in {@link ProfileFragment}
 * is shown how additional elements can be handled directly in the fragment,
 * here you see how to extend the internally used ViewHolder and let
 * the Controller manage the new view (in this case a Country field).
 */
public class ProfileController extends SCProfileController
{
    /**
     * The current country of this profile
     */
    private SCCountry _country;

	public ProfileController(final View view, final SCViewDescription viewDescription)
	{
		super(view, viewDescription);
	}


	@Override
	protected IProfileViewHolder onCreateViewHolder(final View v, final SCViewDescription vd)
	{
        /*
        use our custom ProfileViewHolder with additional elements
         */
		return new ProfileViewHolder(v, vd);
	}

	@Override
	protected void onBindViewHolder(final IProfileViewHolder vh)
	{
		super.onBindViewHolder(vh);
		onBindEditCountry(vh);
	}

    /**
     * Bind the "Country" field here.
     * @param vh
     */
	private void onBindEditCountry(final IProfileViewHolder vh)
	{
        /*
        Add a touch listener to the field, which will open a conutry-selection dialog when touched.
         */
		bindTouchListener(((ProfileViewHolder) vh).getItemEditCountry(), new View.OnTouchListener()
		{

			@Override
			public boolean onTouch(final View v, final MotionEvent event)
			{
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					((ProfileViewHolder) vh).getItemEditCountry().setInputType(InputType.TYPE_NULL);
					openDialog(((ProfileViewHolder) vh).getItemEditCountry());
				}
				return true;
			}
		});

	}

    /**
     * Open the conutry-selection dialog
     * @param editCountry the field which displays the current country
     * @return
     */
	public boolean openDialog(final EditText editCountry)
	{
		final SCDialogCountrySelection dlg = new SCDialogCountrySelection(getContext());
		if (dlg != null) {
			dlg.setOnDismissListener(new OnDismissListener()
			{

				@Override
				public void onDismiss(final DialogInterface dialog)
				{
				    _country = dlg.getSelection();
					if (dlg.getSelection() != null) {

                        /*
                        Show the selected country
                         */
						editCountry.setText(dlg.getSelection().getName());

                        /*
                        Update the profile. Note that this will only update the local object.
                        It does not automatically upload the new value to the sserver
                         */
						getData().setCountry(dlg.getSelection().getName());
					}
				}
			});
			dlg.show();
		}
		return true;
	}

	@Override
	public void onPictureViewClick(View v) {
		BaseActivity.setSaveTime0();
		BaseSampleActivity.isAppNotClose = true;
		super.onPictureViewClick(v);
	}

	/*public void onPictureViewClickMange(View v,boolean isValueSet){
		if(isValueSet){

		}else {

			onPictureViewClickMange(v,true);
		}
	}*/

	@Override
	public synchronized void onDestroy() {
		super.onDestroy();
		BaseSampleActivity.isAppNotClose = false;
	}

	@Override
	public void onStop() {
		super.onStop();

	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == -1) {
			try {
				switch (requestCode) {
					case 11:
					//	SdCardManager.deleteUserImage();
						BaseSampleActivity.isAppNotClose = false;
						break;
				}
			} catch (Exception e) {
				e.printStackTrace();
				//BaseSampleActivity.isAppNotClose = false;
			}
		}else {
			BaseSampleActivity.isAppNotClose = false;
		}
	}

	@Override
	public void save(final boolean finishOnSuccess)
	{
	    if (_country != null){
            /*
            Set the newly selected country as the default country (code) for outgoing calls.
             */
	        SCCoreFacade.instance().setDefaultCountryCode("" + _country.getCode());
	    }
	    super.save(finishOnSuccess);
	}

}
