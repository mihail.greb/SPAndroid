package com.app.android.shadow.main.IntroScreens;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.LauncherActivity;
import com.app.android.shadow.R;
import com.ravindu1024.indicatorlib.ViewPagerIndicator;

public class IntroActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shadow_intro_screen);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        if(Shadow.isProApp()) {
            CustomIntroAdapter adapter = new CustomIntroAdapter(this);
            viewPager.setAdapter(adapter);
        }else {
            CustomIntroFragmentAdapter adapter = new CustomIntroFragmentAdapter(getFragmentManager());
            viewPager.setAdapter(adapter);
        }

        final ViewPagerIndicator indicator = (ViewPagerIndicator) findViewById(R.id.pager_indicator);
        indicator.setPager(viewPager);  //pager - the target ViewPager
        indicator.setOnIndicatorItemClickListener(new ViewPagerIndicator.OnIndicatorItemClickListener() {
            @Override
            public void onItemClick(View view) {
                viewPager.setCurrentItem(Integer.parseInt(view.getTag().toString()));
            }
        });

        final ImageView previous = (ImageView)findViewById(R.id.previous);
        ImageView next = (ImageView)findViewById(R.id.next);

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int item = viewPager.getCurrentItem();
                if(!(item <= 0)) {
                    viewPager.setCurrentItem(--item);
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int item = viewPager.getCurrentItem();
                int size = viewPager.getAdapter().getCount();
                if(!(item >= (size-1))) {
                    viewPager.setCurrentItem(++item);
                }else {
                    callIntent(LauncherActivity.class,true);
                }
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if(position==0){
                    previous.setVisibility(View.INVISIBLE);
                }else {
                    previous.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /*public static void showPopupWindow(Activity mContext, String message, final android.app.Fragment currentFragment){
        try{
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = layoutInflater.inflate(R.layout.shadow_intro_screen, null);

            LinearLayout.LayoutParams parm1=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            parm1.setMargins(20, 0, 20, 0);
            layout.setLayoutParams(parm1);
            final PopupWindow popup = new PopupWindow(mContext);


            popup.setContentView(layout);
            popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
            popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);

            BaseFragment.closeSoftKeyboard(popup.getContentView(), mContext);
            BaseFragment.closeSoftKeyboard(mContext);

            new Handler().postDelayed(new Runnable(){

                public void run() {
                    popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
                }

            }, 500L);

        }
        catch (Exception e){e.printStackTrace();
        }
    }*/


}
