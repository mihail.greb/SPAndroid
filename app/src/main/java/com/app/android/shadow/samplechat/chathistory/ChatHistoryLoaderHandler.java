package com.app.android.shadow.samplechat.chathistory;

import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;

import com.app.android.shadow.common.ui.data.core.LoaderHandlers;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemBaseController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemBaseViewHolder;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemControllerFactory;
import com.c2call.sdk.pub.gui.core.adapter.SCBaseControllerCursorAdapter;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;
import com.c2call.sdk.pub.gui.core.decorator.IDecorator;
import com.c2call.sdk.pub.gui.core.filter.IBaseFilter;
import com.c2call.sdk.pub.gui.core.filter.SCBoardFilterFactory;
import com.c2call.sdk.pub.gui.core.loader.SCBoardLoader;
import com.c2call.sdk.pub.gui.core.loaderhandler.ILoaderHandlerContextProvider;
import com.c2call.sdk.pub.gui.core.loaderhandler.SCBoardLoaderHandler;
import com.c2call.sdk.pub.util.IListViewProvider;
import com.c2call.sdk.pub.util.Validate;
import com.j256.ormlite.dao.Dao;

/**
 * The lodaer handler that is used by the {@link ChatHistoryController} to load the list in background
 */
public class ChatHistoryLoaderHandler extends SCBoardLoaderHandler
{

    @Override
    public void onCreate()
    {
        super.onCreate();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }


    public ChatHistoryLoaderHandler(final ILoaderHandlerContextProvider controller,
                                    final IListViewProvider listViewProvider,
                                    final String filterUserid,
                                    final SCViewDescriptionMap viewDescriptions,
                                    final IBoardListItemControllerFactory controllerFactory)
    {
        super(controller, listViewProvider, filterUserid, viewDescriptions, controllerFactory);

        /*
        We only want to show messages here, no calls
         */
        setFilterMask(SCBoardFilterFactory.FLAG_FILTER_MESSAGES);

        /*
        Set our own loader id to avoid possible conflicts with other default loaders from the C2Call SDK.
        In this sample this is actually not needed because in the ChatHistoryActivity there aren't any other lists (loaders) used.
        But if you want to show multiple list in one Activity you have to set unique IDs for the corresponding loaders
        */
        setLoaderId(LoaderHandlers.CHATHISTORY);
    }



    @Override
    protected SCBaseControllerCursorAdapter<SCBoardEventData,
                                                IBoardListItemBaseController<? extends IBoardListItemBaseViewHolder>,
                                                IBoardListItemControllerFactory,
                                                IDecorator<IBoardListItemBaseController<?>>>
    onCreateAapder(final Cursor c,
                   final IBoardListItemControllerFactory controllerFactory,
                   final SCViewDescriptionMap viewDescriptions)
    {
        /*
        Here we have a lot of generic arguments which are needed to describe the underlying adapter. In most cases you can
        just copy/paste them from the method's return type or documentation.
         */
        SCBaseControllerCursorAdapter<SCBoardEventData,
                        IBoardListItemBaseController<? extends IBoardListItemBaseViewHolder>,
                        IBoardListItemControllerFactory,
                        IDecorator<IBoardListItemBaseController<?>>>
        adapter = new ChatHistoryCursorAdapter(getContext(),
                                                c,
                                                controllerFactory,
                                                viewDescriptions,
                                                ChatHistoryCursorAdapter.DECORATOR_MAP,
                                                0);


        return adapter;
    }


    /**
     * Create the actual loader that will be used to load the list data in background and populate the result on the main thhread
     * @param id the loader's id
     * @param arg
     * @return
     */
    @Override
    public Loader<Cursor> onCreateLoader(final int id, final Bundle arg)
    {

        switch(id){
            case LoaderHandlers.CHATHISTORY:

                final Dao<SCBoardEventData, String> dao = SCBoardEventData.dao();

                /*
                The SCBoardLoader is a convenient standard loader each time  when you want to show a list of SCBoardEvents.
                 Here we want to order the list descending by the timestamp (most recent at top)
                 */
                final SCBoardLoader loader =  new SCBoardLoader(getContext(), dao, SCBoardEventData.PROJECTION_ALL);
                loader.addOrderBy(SCBoardEventData.TIMEVALUE, false);
                loader.setReverse(true);

                /*
                Use the dafault filter for this loader.
                 */
                IBaseFilter<SCBoardEventData, String> filter = onCreateFilter(getFilterUserid(), getFilterQuery(), getFilterMask());

                loader.setFilter(filter);

                /*
                Finally we have to group the list by users. I.e. we get only one item for each user. Clicking that item will then
                open the full chat with that user.
                 */
                loader.setGroupBy(SCBoardEventData.USERID);
                return loader;
            default:
                Validate.isTrue(false, "unhandled loader id: " + id);
                return null;
        }
    }


}
