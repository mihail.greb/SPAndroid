package com.app.android.shadow.samplechat.chathistory;

import android.app.Activity;
import android.view.View;

import com.app.android.shadow.events.RequestChatHistoryUpdateEvent;
import com.app.android.shadow.R;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.SCThreadMode;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.board.controller.IBoardViewHolder;
import com.c2call.sdk.pub.gui.board.controller.SCBoardController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemControllerFactory;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.controller.IControllerRequestListener;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;
import com.c2call.sdk.pub.gui.core.loaderhandler.ILoaderHandlerContextProvider;
import com.c2call.sdk.pub.gui.core.loaderhandler.SCBoardLoaderHandler;
import com.c2call.sdk.pub.util.IListViewProvider;

public class ChatHistoryController extends SCBoardController
{

    public ChatHistoryController(final View view,
                                 final SCViewDescription viewDescription,
                                 final SCViewDescriptionMap itemViewDescriptions,
                                 final IBoardListItemControllerFactory itemControllerFactory,
                                 final IControllerRequestListener requestListener)
    {
        super(view, viewDescription, itemViewDescriptions, itemControllerFactory, requestListener);

    }

    @Override
    public SCBoardLoaderHandler onCreateLoaderHandler(final IListViewProvider listViewProvider,
                                                      final ILoaderHandlerContextProvider loaderHandlerMasterProvider,
                                                      final IBoardListItemControllerFactory itemMediatorFactory,
                                                      final SCViewDescriptionMap itemDescriptions)
    {
        return new ChatHistoryLoaderHandler(loaderHandlerMasterProvider,
                                            listViewProvider,
                                            getUserId(),
                                            itemDescriptions,
                                            itemMediatorFactory);
    }


    @Override
    public void onCreate(Activity activity, SCActivityResultDispatcher scActivityResultDispatcher)
    {
        super.onCreate(activity, scActivityResultDispatcher);

        SCCoreFacade.instance().subscribe(this);
    }


    @Override
    public synchronized void onDestroy()
    {

        SCCoreFacade.instance().unsubscribe(this);

        super.onDestroy();
    }

    @Override
    protected void onBindViewHolder(final IBoardViewHolder vh)
    {
        super.onBindViewHolder(vh);


        View emptyListView = getContext().getLayoutInflater().inflate(R.layout.app_emptylist, null);
        getViewHolder().getItemList().setEmptyView(emptyListView);

        super.showFilterInput(false);

    }




    @SCEventCallback(threadMode = SCThreadMode.MainThread, isSticky = false)
    private void onEvent(RequestChatHistoryUpdateEvent evt)
    {
        Ln.d("c2app", "ChatHistoryController.onEvent() - evt: %s", evt);
        getLoaderHandler().restart(false, null);
    }


}
