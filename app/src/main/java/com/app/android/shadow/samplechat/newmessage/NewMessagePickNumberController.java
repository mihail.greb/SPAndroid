package com.app.android.shadow.samplechat.newmessage;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.view.View;

import com.app.android.shadow.utils.Str;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.common.SCPhoneNumberType;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.eventbus.events.SCPickNumberEvent;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.core.controller.IControllerRequestListener;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.picknumber.controller.IPickNumberViewHolder;
import com.c2call.sdk.pub.gui.picknumber.controller.SCPickNumberController;
import com.c2call.sdk.thirdparty.eventbus.EventBus;

import java.util.HashSet;
import java.util.Set;

public class NewMessagePickNumberController extends SCPickNumberController
{
    


    private static class NumberData
    {
        public String name;
        public String number;
        public SCPhoneNumberType numberType;

        private NumberData(String name, String number, SCPhoneNumberType numberType)
        {
            this.name = name;
            this.number = number;
            this.numberType = numberType;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            NumberData that = (NumberData) o;

            if (name != null ? !name.equals(that.name) : that.name != null) {
                return false;
            }
            if (number != null ? !number.equals(that.number) : that.number != null) {
                return false;
            }
            if (numberType != that.numberType) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode()
        {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (number != null ? number.hashCode() : 0);
            result = 31 * result + (numberType != null ? numberType.hashCode() : 0);
            return result;
        }

        @Override
        public String toString()
        {
            return "NumberData{" +
                   "name='" + name + '\'' +
                   ", number='" + number + '\'' +
                   ", numberType=" + numberType +
                   '}';
        }
    }

    private boolean _forceShowList = false;
    private Set<NumberData> _numberData = new HashSet<NumberData>();

    
    public NewMessagePickNumberController(View v, SCViewDescription vd, IControllerRequestListener requstListener)
    {

        super(v, vd, requstListener);

//        setFlags(0);
    }

    @Override
    protected void onBindViewHolder(final IPickNumberViewHolder vh)
    {
        super.onBindViewHolder(vh);

        vh.getItemEditSearch().setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View view, boolean hasFocus)
            {
                Ln.d("c2app", "PickNumberController.onFocusChange() - hasFocus: %b", hasFocus);
                if (!hasFocus) {
                    String input = vh.getItemEditSearch().getText().toString();

                    String number = null;
                    String name = null;
                    if (Str.isPhonenumber(input)) {
                        number = input;
                    }
                    else {
                        SCFriendData friend = SCCoreFacade.getFriendByEmail(input);
                        if (friend != null) {
                            number = friend.getId();
                            name = friend.getManager().getDisplayName();
                        }
                    }

                    if (number != null) {
                        acceptInput(number, name);
                    }

                    setVisibility(getViewHolder().getItemList(), false);
                }

            }
        });

        vh.getItemEditSearch().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                boolean isVisible = (getListView().getVisibility() == View.VISIBLE);

                Ln.d("c2app", "PickNumberController.OnClickListener() - visible: %b", isVisible);
                if (getListView().getVisibility() == View.VISIBLE){
                    setVisibility(getListView(), false);
                    return;
                }

                _forceShowList = true;
                String filter = getViewHolder().getItemEditSearch().getText().toString();
                onFilter(filter);
            }
        });

    }


    @Override
    protected synchronized void onInitAppContacts()
    {
        Ln.d("c2app", "PickNumberController.onInitAppContacts()");
        super.onInitAppContacts();
    }

    protected void onInitAddressbookData()
    {
        Ln.d("c2app", "PickNumberController.onInitAddressbookData()");
        _numberData.clear();;
        Cursor peopleCursor = null;
        try {
            final ContentResolver cr = getContext().getContentResolver();

            final String where = ContactsContract.Contacts.HAS_PHONE_NUMBER + "<>'0'";
            peopleCursor = cr
                    .query(ContactsContract.Contacts.CONTENT_URI, null, where, null, ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC");

            if (peopleCursor == null)
            {
                Ln.w("c2app", "* * * Warning: PickNumberController.onInitAddressbookData() - cursor is null!");
                return;
            }
            // final Cursor peopleCursor =
            // cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null,
            // null);

            while (peopleCursor.moveToNext()) {
                final String contactName = peopleCursor.getString(peopleCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                final String contactId = peopleCursor.getString(peopleCursor.getColumnIndex(ContactsContract.Contacts._ID));
                final String hasPhone = peopleCursor.getString(peopleCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                if ((Integer.parseInt(hasPhone) > 0)) {
                    Ln.d("c2app", "PickNumberController.onInitAddressbookData() - contactId: %s, name: %s", contactId, contactName);
                    onAddPhoneNumbers(contactId, contactName);
                }

            }
            peopleCursor.close();

            onAddEmailAddresses();
        }
        catch (final Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (peopleCursor != null) {
                    peopleCursor.close();
                }
            }
            catch (final Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void onAddPhoneNumber(String contactName, String phoneNumber, SCPhoneNumberType numberType)
    {
        NumberData data = new NumberData(contactName, phoneNumber, numberType);
        if (_numberData.contains(data)){
            Ln.d("c2app", "PickNumberController.onAddPhoneNumber() - number already added: %s", data);
            return;
        }
        else {
            _numberData.add(data);
            super.onAddPhoneNumber(contactName, phoneNumber, numberType);
        }
    }


    @Override
    protected void onItemClicked(final View view, final int pos, final String number, String name)
    {
        Ln.d("c2app", "PickNumberController.onItemClicked() - %s / %s / %s", number, name, getRequestListener());

        String dname = (Str.isEmpty(name))
                       ? number
                       : name;

        getViewHolder().getItemEditSearch().setText(dname);
        acceptInput(number, name);

    }

    protected void onPostFilterTextChanged(final String s)
    {
        Ln.d("c2app", "PickNumberController.onPostFilterTextChanged() - forceShow: %b", _forceShowList);


        getHandler().post(new Runnable()
        {
            @Override
            public void run()
            {

                boolean show = _forceShowList
                               || !Str.isEmpty(s);

                setVisibility(getListView(), show);
                _forceShowList = false;
            }
        });
    }

    private void acceptInput(String number, String name)
    {
        EventBus.getDefault().post(new SCPickNumberEvent(getTag(), number, name));
        onFilter("");
    }

    @Override
    protected int getTypeIcon(String name, String number, SCPhoneNumberType numberType, String email, int userType )
    {
        return 0;
    }
}
