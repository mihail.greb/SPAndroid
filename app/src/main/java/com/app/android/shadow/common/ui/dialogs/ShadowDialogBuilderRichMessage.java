package com.app.android.shadow.common.ui.dialogs;

import android.content.Intent;
import android.provider.ContactsContract;

import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.common.ui.activities.ShadowAudioRecordFragmentActivity;
import com.app.android.shadow.samplechat.board.BoardActivity;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.activities.SCShareLocationActivity;
import com.c2call.sdk.pub.core.SCDownloadType;
import com.c2call.sdk.pub.gui.core.controller.IController;
import com.c2call.sdk.pub.gui.dialog.BaseItemRunnable;
import com.c2call.sdk.pub.gui.dialog.DialogBuilderRichMessage;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.util.MediaUtil;

import java.util.UUID;

/**
 * Created by rails-dev on 31/12/15.
 */
public class ShadowDialogBuilderRichMessage extends DialogBuilderRichMessage {


    public static SCChoiceDialog build(IController<?> controller, boolean isSms) {
        SCChoiceDialog.Builder builder = new SCChoiceDialog.Builder(controller.getContext());
        builder.addItem(com.c2call.sdk.R.string.sc_dlg_rich_message_photo_title, com.c2call.sdk.R.string.sc_dlg_rich_message_photo_summary, com.c2call.sdk.R.drawable.ic_sc_device_access_camera_grey, new ShadowDialogBuilderRichMessage.PhotoRunnable(controller));
        builder.addItem(com.c2call.sdk.R.string.sc_dlg_rich_message_video_title, com.c2call.sdk.R.string.sc_dlg_rich_message_video_summary, com.c2call.sdk.R.drawable.ic_sc_device_access_video_grey, new ShadowDialogBuilderRichMessage.VideoRunnable(controller));
        if(isMapsApiAvailable(controller.getContext())) {
          //  builder.addItem(com.c2call.sdk.R.string.sc_dlg_rich_message_location_title, com.c2call.sdk.R.string.sc_dlg_rich_message_location_summary, com.c2call.sdk.R.drawable.sc_location_place, new ShadowDialogBuilderRichMessage.LocationRunnable(controller));
        }

        builder.addItem(com.c2call.sdk.R.string.sc_dlg_rich_message_voice_title, com.c2call.sdk.R.string.sc_dlg_rich_message_voice_summary, com.c2call.sdk.R.drawable.sc_device_access_mic, new ShadowDialogBuilderRichMessage.VoiceRunnable(controller));
        //builder.addItem(com.c2call.sdk.R.string.sc_dlg_rich_message_contact_title, com.c2call.sdk.R.string.sc_dlg_rich_message_contact_summary, com.c2call.sdk.R.drawable.ic_sc_profile_grey, new ShadowDialogBuilderRichMessage.ContactRunnable(controller));
        return builder.build();
    }

    /*public static class PhotoRunnable extends BaseItemRunnable<IController<?>> {
        public PhotoRunnable(IController<?> controller) {
            super(controller);
        }
        public void run() {
            SCChoiceDialog dlg = C2CallSdk.dialogFactory().createPhotoDialog(this.getController(), (String)null);
            if(dlg != null) {
                dlg.show();
            }

        }
    }*/

    public static class LocationRunnable extends BaseItemRunnable<IController<?>> {
        public LocationRunnable(IController<?> controller) {
            super(controller);
        }

        public void run() {
            Ln.d("fc_tmp", "DialogBuilderRichMessage.LocationRunnable.run()", new Object[0]);
            try {
                BaseActivity.setSaveTime0();
                BaseSampleActivity.isAppNotClose = true;
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            try {
                BaseActivity.setSaveTime0();
                BoardActivity.isAppNotClose = true;
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            Intent intent = new Intent(this.getContext(), SCShareLocationActivity.class);
            this.startActivityForResult(intent, 9);
        }
    }

    public static class VoiceRunnable extends BaseItemRunnable<IController<?>> {
        public VoiceRunnable(IController<?> controller) {
            super(controller);
        }

        public void run() {
            String filename = UUID.randomUUID().toString().replace("-", "");
            String output = MediaUtil.getMediaPath(SCDownloadType.Image, filename, true);
            try {
                BaseActivity.setSaveTime0();
                BaseSampleActivity.isAppNotClose = true;
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            try {
                BaseActivity.setSaveTime0();
                BoardActivity.isAppNotClose = true;
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            Intent intent = new Intent(this.getContext(), ShadowAudioRecordFragmentActivity.class);
            intent.putExtra("sc_etxra_data_audio_record_output_path", output);
            this.startActivityForResult(intent, 8);
        }
    }

    public static class ContactRunnable extends BaseItemRunnable<IController<?>> {
        public ContactRunnable(IController<?> controller) {
            super(controller);
        }

        public void run() {
            try {
                try {
                    BaseActivity.setSaveTime0();
                    BaseSampleActivity.isAppNotClose = true;
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

                try {
                    BaseActivity.setSaveTime0();
                    BoardActivity.isAppNotClose = true;
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                Intent e = new Intent("android.intent.action.PICK", ContactsContract.Contacts.CONTENT_URI);
                this.startActivityForResult(e, 10);
            } catch (Exception var2) {
                var2.printStackTrace();
            }

        }
    }

}
