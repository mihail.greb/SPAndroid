package com.app.android.shadow.common.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.app.android.shadow.samplechat.board.BoardController;
import com.app.android.shadow.samplechat.board.BoardListItemControllerFactory;
import com.app.android.shadow.utils.Str;
import com.app.android.shadow.R;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.facade.SCSecurityFacade;
import com.c2call.sdk.pub.fragments.SCBoardFragment;
import com.c2call.sdk.pub.gui.board.controller.IBoardController;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.util.SimpleAsyncTask;

import java.security.KeyPair;

/**
 * Custom board fragment that shows the chat with the user.
 */
public class BoardFragment extends SCBoardFragment
{
    private final SCActivityResultDispatcher _resultDispatcher = new SCActivityResultDispatcher();

    public static BoardFragment create(final String userid, final int layout)
    {
        final Bundle args = new Bundle();

        return create(userid, layout, args);
    }

    public static BoardFragment create(final String userid, int layout, Bundle forwardArgs)
    {
        Ln.d("c2app", "BoardFragment.create() - userid: %s", userid);

        final BoardFragment f = new BoardFragment();
        Bundle args = new Bundle();
        args.putAll(forwardArgs);
        args.putString(SCExtraData.Board.EXTRA_DATA_USERID, userid);
        //args.putInt(SCExtraData.Board.EXTRA_DATA_FILTER_MASK, layout);
        args.putInt(SCExtraData.Board.EXTRA_DATA_LAYOUT, layout);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState)
    {
        final int layout = R.layout.sc_board_part_list;
        final View v = inflater.inflate(layout, null);
        return v;
    }


    @Override
    protected IBoardController onCreateController(final View v, final SCViewDescription vd)
    {
        /*
        Create our custom Board Controller
         */
        return new BoardController(v,
                                   vd,
                                   C2CallSdk.instance().getVD().boardListItem(),
                                   new BoardListItemControllerFactory(getResultDispatcher()),
                                   null)
        {
            @Override
            protected boolean enableOverscroll()
            {
                return true;
            }
        };
    }


    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater)
    {
        if (!Str.isEmpty(getUserid())) {
            inflater.inflate(R.menu.app_menu_chat, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item)
    {
        switch(item.getItemId()) {
            case R.id.menu_clear:
                getController().clearHistory();
                return true;
            case R.id.menu_search:
                getController().toggleFilterInput();
                View root = getActivity().getWindow().getDecorView().findViewById(android.R.id.content);
                root.invalidate();
                root.requestLayout();
                return true;
            case R.id.menu_genkey:
                generateSecurityKey();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This will generate a new certificate for secure messaging.
     */
    private void generateSecurityKey()
    {
        String successMessage = getString(R.string.sc_msg_generated_keypair_success);
        String failedMessage = getString(R.string.msg_generated_keypair_failed);
        new SimpleAsyncTask<KeyPair>(getActivity(), 0, successMessage, failedMessage)
        {
            @Override
            protected KeyPair doInBackground(Void... params)
            {
                return SCSecurityFacade.instance().generateKeypair(getActivity(), true);
            }
        }.execute();
    }

    @Override
    public SCActivityResultDispatcher getResultDispatcher()
    {
        return _resultDispatcher;
    }


}
