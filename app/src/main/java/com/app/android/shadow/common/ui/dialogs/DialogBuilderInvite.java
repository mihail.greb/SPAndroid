package com.app.android.shadow.common.ui.dialogs;

import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.gui.core.controller.IController;
import com.c2call.sdk.pub.gui.dialog.BaseItemRunnable;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.app.android.shadow.R;

/**
 * A convenient method to create a choice dialog which shows some options for adding users to the friend list.
 */
public class DialogBuilderInvite
{

    public static SCChoiceDialog build(final IController<?> controller)
    {
        final SCChoiceDialog.Builder builder = new SCChoiceDialog.Builder(controller.getContext());

        builder.addItem(R.string.sc_dlg_invite_add_friend_title,
                R.string.sc_dlg_invite_add_friend_summary,
                R.drawable.sc_social_add_person,
                new AddFriendRunnable(controller));
        return builder.build();


        /*builder.addItem(com.c2call.sdk.R.string.sc_dlg_spread_create_group_title,
                        com.c2call.sdk.R.string.sc_dlg_spread_create_group_title_summary,
                        com.c2call.sdk.R.drawable.sc_social_add_group,
                        new CreateGroupRunnable(controller));
*/


    }

    public static class AddFriendRunnable extends BaseItemRunnable<IController<?>>
    {
        public AddFriendRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            C2CallSdk.startControl().openAddFriend(getController().getContext(), null, R.layout.sc_addfriend, StartType.Activity);
        }
    }


   /* public static class CreateGroupRunnable extends BaseItemRunnable<IController<?>>
    {
        public CreateGroupRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            C2CallSdk.startControl().openAddGroup(getController().getContext(), null, com.c2call.sdk.R.layout.sc_select_contacts,  StartType.Auto);
        }
    }*/
}
