package com.app.android.shadow.main.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.apicalls.ApiRequest;

import org.json.JSONObject;

public class ToClearActivityTop extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callApi(APIServiceAsyncTask.setUserOfflien);
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        Logger.printLog(Logger.ERROR, "onDestroy exit2 base");
        finish();
        System.exit(0);
    }

    public void callApi(ApiRequest req){
        ApiTaskBase mTask = new ApiTaskBase(this);
        mTask.setServiceTaskMapStrParams(req, null);
        mTask.execute((Void) null);
    }

    public class ApiTaskBase extends APIServiceAsyncTask {
        ApiTaskBase(Context mContext) {
            super(mContext);
        }

        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            if (APIServiceAsyncTask.OFFLINE_USER == serviceTaskType) {
                Logger.printLog(Logger.ERROR, "success to offline " + jsonObj.toString());
            }
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
            super.failure(jsonObj, serviceTaskType);
            failure("" + serviceTaskType);
            Logger.printLog(Logger.ERROR, "failed to offline " + jsonObj.toString());
        }

        @Override
        protected void failure(String message) {
            //super.failure(message);
            Logger.printLog(Logger.ERROR, "failed to offline null");
            if (APIServiceAsyncTask.OFFLINE_USER == Integer.parseInt(message)) {
            }
        }
    }
}
