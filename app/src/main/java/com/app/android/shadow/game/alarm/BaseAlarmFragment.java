/* Copyright 2014 Sheldon Neilson www.neilson.co.za
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package com.app.android.shadow.game.alarm;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewConfiguration;

import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.game.alarm.service.AlarmServiceBroadcastReciever;

import java.lang.reflect.Field;

public abstract class BaseAlarmFragment extends BaseFragment implements android.view.View.OnClickListener{

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		try {
	        ViewConfiguration config = ViewConfiguration.get(getActivity());
	        Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
	        if(menuKeyField != null) {
	            menuKeyField.setAccessible(true);
	            menuKeyField.setBoolean(config, false);
	        }
	    } catch (Exception ex) {
	        // Ignore
	    }
	}



	protected void callMathAlarmScheduleService() {
		Intent mathAlarmServiceIntent = new Intent(getActivity(), AlarmServiceBroadcastReciever.class);
		getActivity().sendBroadcast(mathAlarmServiceIntent, null);
		//mCallback.onButtonClickEvent(new AlarmServiceBroadcastReciever(), EventMaker.SettingsFragment);
	}
}
