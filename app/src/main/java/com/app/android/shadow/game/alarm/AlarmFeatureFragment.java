package com.app.android.shadow.game.alarm;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.R;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.samplechat.SimpleChatMainActivity;


/**
 * Custom Board Fragment to show a chat history grouped by users
 */
public class AlarmFeatureFragment extends BaseFragment implements View.OnClickListener
{
    //Header
    TextView title;
    ImageView alarmLogo;
    protected void setHeader(View v) {
        super.setHeader(v);
        title = (TextView) v.findViewById(R.id.title_head);
        title.setText(resources.getString(R.string.alarm));
        v.findViewById(R.id.back).setVisibility(View.GONE);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view =inflater.inflate(R.layout.shadow_alarm_feature_list, container, false);
        SimpleChatMainActivity.mCurrentTab = Const.SETTINGS;
        _view.findViewById(R.id.alarmBtn).setOnClickListener(this);
        _view.findViewById(R.id.stopWatchBtn).setOnClickListener(this);
        _view.findViewById(R.id.alarmworldwatch).setOnClickListener(this);
        _view.findViewById(R.id.alarmTimerBtn).setOnClickListener(this);
        alarmLogo = (ImageView) _view.findViewById(R.id.alarmLogo);
        alarmLogo.setOnClickListener(this);
        setHeader(_view);
        return _view;
    }

    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        switch (v.getId()) {
            case R.id.alarmBtn:
                fragment = new AlarmFragment();
                break;
            case R.id.stopWatchBtn:
                fragment = new StopWatchFragment();
                break;

            case R.id.alarmTimerBtn:
                fragment = new AlarmTimerFragment();
                break;

            case R.id.alarmLogo:
                mCallback.onButtonClickEvent(fragment, EventMaker.ClickCount);
                break;
            case R.id.alarmworldwatch:
                 fragment = new WorldTimeZone();
                break;
        }
        if(fragment != null) {
            mCallback.onButtonClickEvent(fragment, EventMaker.SettingsFragment);
        }
    }

    private void callEvent(){

    }

}
