package com.app.android.shadow.common.ui.activities;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import com.app.android.shadow.common.ui.fragments.LoginFragment;
import com.app.android.shadow.main.LauncherActivity;
import com.c2call.sdk.pub.activities.SCLoginFragmentActivity;

public class LoginActivity extends SCLoginFragmentActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        /*ToTrnsActiveapp*/
        try {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                    WindowManager.LayoutParams.FLAG_SECURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
    }


    @Override
    protected Fragment onCreateFragment()
    {
        return LoginFragment.create(getIntent().getExtras());
    }

    @Override
    protected void onResume() {
        super.onResume();
       /* if(Shadow.getAppPreferences().getBoolean(Const.IS_LOGIN,false)){
            Intent intent = new Intent(this, Shadow.getProActivity());
            startActivity(intent);
            finish();
        }*/
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this, LauncherActivity.class);
        startActivity(intent);
        finish();
    }

}



/*
public class LoginActivity extends BaseActivity implements com.c2call.sdk.pub.fragments.SCLoginFragment.Callbacks
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shadow_fragments_container);
        callFragment(onCreateFragment(),true,R.id.fragment_containers,null);
    }

    //@Override
    protected Fragment onCreateFragment()
    {
        return LoginFragment.create(getIntent().getExtras());
    }


    @Override
    protected void onResume() {
        super.onResume();

}

@Override
public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this, LauncherActivity.class);
        startActivity(intent);
        finish();
        }

@Override
public void onControllerEvent(SCBaseControllerEvent scBaseControllerEvent) {

        }
        }
*/