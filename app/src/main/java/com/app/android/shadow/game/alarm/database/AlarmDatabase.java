package com.app.android.shadow.game.alarm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.app.android.shadow.game.alarm.Alarm;
import com.app.android.shadow.main.Shadow;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.List;

/* 
 * usage:  
 * DatabaseSetup.init(egActivityOrContext); 
 * DatabaseSetup.createEntry() or DatabaseSetup.getContactNames() or DatabaseSetup.getDb() 
 * DatabaseSetup.d.eactivate() then job done
 */

public class AlarmDatabase extends SQLiteOpenHelper {
	static AlarmDatabase instance = null;
	static SQLiteDatabase database = null;

	public AlarmDatabase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	public static final String ALARM_TABLE = "alarm";
	public static final String COLUMN_ALARM_ID = "_id";
	public static final String COLUMN_ALARM_ACTIVE = "alarm_active";
	public static final String COLUMN_ALARM_TIME = "alarm_time";
	public static final String COLUMN_ALARM_DAYS = "alarm_days";
	public static final String COLUMN_ALARM_DIFFICULTY = "alarm_difficulty";
	public static final String COLUMN_ALARM_TONE = "alarm_tone";
	public static final String COLUMN_ALARM_VIBRATE = "alarm_vibrate";
	public static final String COLUMN_ALARM_NAME = "alarm_name";
	
	public static void init(Context context) {
		if (null == instance) {
			instance = Shadow.getDBInstance();
		}
	}

	public static SQLiteDatabase getAlarmDatabase() {
		if (null == database) {
			database = Shadow.getDBInstance().getWritableDatabase();
		}
		return database;
	}

	public static void deactivateAlram() {
		if (null != database && database.isOpen()) {
			database.close();
		}
		database = null;
		instance = null;
	}

	public static long createAlarm(Alarm alarm) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_ALARM_ACTIVE, alarm.getAlarmActive());
		cv.put(COLUMN_ALARM_TIME, alarm.getAlarmTimeString());
		
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
		    ObjectOutputStream oos = null;
		    oos = new ObjectOutputStream(bos);
		    oos.writeObject(alarm.getDays());
		    byte[] buff = bos.toByteArray();
		    
		    cv.put(COLUMN_ALARM_DAYS, buff);
		    
		} catch (Exception e){
		}		
		
		cv.put(COLUMN_ALARM_DIFFICULTY, alarm.getDifficulty().ordinal());
		cv.put(COLUMN_ALARM_TONE, alarm.getAlarmTonePath());
		cv.put(COLUMN_ALARM_VIBRATE, alarm.getVibrate());
		cv.put(COLUMN_ALARM_NAME, alarm.getAlarmName());

		long data =getAlarmDatabase().insert(ALARM_TABLE, null, cv);
				deactivateAlram();//if Database already open
		return data;
	}
	public static int updateAlarm(Alarm alarm) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_ALARM_ACTIVE, alarm.getAlarmActive());
		cv.put(COLUMN_ALARM_TIME, alarm.getAlarmTimeString());
		
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
		    ObjectOutputStream oos = null;
		    oos = new ObjectOutputStream(bos);
		    oos.writeObject(alarm.getDays());
		    byte[] buff = bos.toByteArray();
		    
		    cv.put(COLUMN_ALARM_DAYS, buff);
		    
		} catch (Exception e){
		}		
		
		cv.put(COLUMN_ALARM_DIFFICULTY, alarm.getDifficulty().ordinal());
		cv.put(COLUMN_ALARM_TONE, alarm.getAlarmTonePath());
		cv.put(COLUMN_ALARM_VIBRATE, alarm.getVibrate());
		cv.put(COLUMN_ALARM_NAME, alarm.getAlarmName());

		int data =getAlarmDatabase().update(ALARM_TABLE, cv, "_id=" + alarm.getId(), null);
		deactivateAlram();//if Database already open

		return data;
	}
	public static int deleteEntry(Alarm alarm){
		int data = deleteEntry(alarm.getId());
				deactivateAlram();//if Database already open
		return data;
	}
	
	public static int deleteEntry(int id){
		int data = getAlarmDatabase().delete(ALARM_TABLE, COLUMN_ALARM_ID + "=" + id, null);
		deactivateAlram();//if Database already open
		return data;
	}
	
	public static int deleteAll(){
		int data = getAlarmDatabase().delete(ALARM_TABLE, "1", null);
		deactivateAlram();//if Database already open
		return data;
	}
	
	public static Alarm getAlarm(int id) {
		// TODO Auto-generated method stub
		String[] columns = new String[] {
				COLUMN_ALARM_ID, 
				COLUMN_ALARM_ACTIVE,
				COLUMN_ALARM_TIME,
				COLUMN_ALARM_DAYS,
				COLUMN_ALARM_DIFFICULTY,
				COLUMN_ALARM_TONE,
				COLUMN_ALARM_VIBRATE,
				COLUMN_ALARM_NAME
				};
		Cursor c = getAlarmDatabase().query(ALARM_TABLE, columns, COLUMN_ALARM_ID + "=" + id, null, null, null,
				null);
		Alarm alarm = null;
		
		if(c.moveToFirst()){
			
			alarm =  new Alarm();
			alarm.setId(c.getInt(1));
			alarm.setAlarmActive(c.getInt(2)==1);
			alarm.setAlarmTime(c.getString(3));
			byte[] repeatDaysBytes = c.getBlob(4);
			
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(repeatDaysBytes);
			try {
				ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
				Alarm.Day[] repeatDays;
				Object object = objectInputStream.readObject();
				if(object instanceof Alarm.Day[]){
					repeatDays = (Alarm.Day[]) object;
					alarm.setDays(repeatDays);
				}								
			} catch (StreamCorruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
						
			alarm.setDifficulty(Alarm.Difficulty.values()[c.getInt(5)]);
			alarm.setAlarmTonePath(c.getString(6));
			alarm.setVibrate(c.getInt(7)==1);
			alarm.setAlarmName(c.getString(8));
		}
		c.close();
		deactivateAlram();//if Database already open
		return alarm;
	}
	
	public static Cursor getCursorAlarm() {
		// TODO Auto-generated method stub
		String[] columns = new String[] {
				COLUMN_ALARM_ID, 
				COLUMN_ALARM_ACTIVE,
				COLUMN_ALARM_TIME,
				COLUMN_ALARM_DAYS,
				COLUMN_ALARM_DIFFICULTY,
				COLUMN_ALARM_TONE,
				COLUMN_ALARM_VIBRATE,
				COLUMN_ALARM_NAME
				};
		Cursor cursor = getAlarmDatabase().query(ALARM_TABLE, columns, null, null, null, null, null);
		return cursor;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	public static List<Alarm> getAllAlarm() {
		List<Alarm> alarms = new ArrayList<Alarm>();
		Cursor cursor = AlarmDatabase.getCursorAlarm();
		if (cursor.moveToFirst()) {

			do {
				// COLUMN_ALARM_ID,
				// COLUMN_ALARM_ACTIVE,
				// COLUMN_ALARM_TIME,
				// COLUMN_ALARM_DAYS,
				// COLUMN_ALARM_DIFFICULTY,
				// COLUMN_ALARM_TONE,
				// COLUMN_ALARM_VIBRATE,
				// COLUMN_ALARM_NAME

				Alarm alarm = new Alarm();
				alarm.setId(cursor.getInt(0));
				alarm.setAlarmActive(cursor.getInt(1) == 1);
				alarm.setAlarmTime(cursor.getString(2));
				byte[] repeatDaysBytes = cursor.getBlob(3);

				ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
						repeatDaysBytes);
				try {
					ObjectInputStream objectInputStream = new ObjectInputStream(
							byteArrayInputStream);
					Alarm.Day[] repeatDays;
					Object object = objectInputStream.readObject();
					if (object instanceof Alarm.Day[]) {
						repeatDays = (Alarm.Day[]) object;
						alarm.setDays(repeatDays);
					}
				} catch (StreamCorruptedException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}

				alarm.setDifficulty(Alarm.Difficulty.values()[cursor.getInt(4)]);
				alarm.setAlarmTonePath(cursor.getString(5));
				alarm.setVibrate(cursor.getInt(6) == 1);
				alarm.setAlarmName(cursor.getString(7));
				
				alarms.add(alarm);

			} while (cursor.moveToNext());			
		}
		cursor.close();
		deactivateAlram();//if Database already open
		return alarms;
	}
}