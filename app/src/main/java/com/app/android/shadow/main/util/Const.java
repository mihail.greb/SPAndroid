package com.app.android.shadow.main.util;


import com.app.android.shadow.main.Shadow;

public class Const {

	/*DefautlRingUser*/
	public static final String DEFAULT_USER_ID  = Shadow.getpackageNameAsStr();

	public static final String SHADOW_TEST_LINK = "http://www.shadow-phone.com/test";
	public static final String FRIEND  = "Friends";
	public static final String FRIENDS= "friends";//tab
	public static final String PENDING_FRIENDS = "pending_friends";//tab
	public static final String REQUESTS  = "Requests";
	public static final String GALLERY  = "Gallery";
	public static final String PRO_APP  = "ProApp";
	public static final String SETTINGS = "Settings";
	public static final String CLOSE  = "Close";
	public static final String APP_NAME = "app_name";

	public static final int NO_TIME= -1;
	public static final String STATUS= "status";
	public static final String MESSAGE= "message";
	public static final String ID= "id";
	public static final String _ID= "_id";
	public static final String NAME= "name";
	public static final String DIAL_CODE= "dial_code";
	public static final String CODE= "code";
	public static final String TAGS= "tags";
	public static final String IMAGES= "image";


	public static final String VALUE= "value";
	public static final String ANDROID_PRO_URL = "android_pro_url";
	public static final String USEREMAIL= "user_email";
	public static final String AUTH_TOKEN= "auth_token";
	public static final String SUCCESS= "success";
	public static final String INFO= "info";
	public static final String USER= "user";

	/*Shadow Logs*/
	public static final String USER_ID= "user_id";
	public static final String CALL_TYPE= "call_type";
	public static final String TO_USER_ID= "to_user_id";
	public static final String GET_TO_USERNAME= "get_to_username";
	public static final String GET_DURATION= "get_duration";


	public static final String PLEASE_UNBLOCK= "Please Unblock";
	public static final String TIME_LIMIT_OVER= "Time limit over";
	public static final String DATE= "date";
	public static final String ERRORS= "errors";
	public static final String FIRST_TIME_LOGGED= "first_time_logged";
	public static final String SAVE_TIME_LOGOUT= "save_time_logout";
	public static final String ALARM_SAVE_TIME_LOGIN_SAFE = "save_time_login"; //Alarm Feature //_safe
	public static final String ALARM_SAVE_TIME_LOGIN_UNSAFE = "save_time_login"; //Alarm Feature // _unsafe
	public static final String CAL_SAVE_LOGIN_SAFE = "cal_save_time_login"; //Cal Feature //_safe
	public static final String CAL_SAVE_LOGIN_UNSAFE = "cal_save_time_login"; //Cal Feature // _unsafe
	public static final String YP_SAVE_TAB_SAFE = "yp_save_tab_safe"; //YP Feature //_safe
	public static final String YP_SAVE_TAB_UNSAFE = "yp_save_tab_safe"; //YP Feature //_unsafe
	public static final String SP_SAVE_TAB_UNSAFE = "sp_save_tab_safe"; //YP Feature //_unsafe
	public static final String DEFAULT_TABS = "8"; //YP Feature //_unsafe


	public static final long ONE_MINUTE_IN_MILLIS=60000;

	//for Shadow
	public static final String DATA= "data";
	public static final String EMAIL= "email";
	public static final String DISPLAY_EMAIL= "display_email";
	public static final String SHADOW_ID= "shadow_id";
	public static final String CREATED_AT= "created_at";
	public static final String USERNAME = "username";
	public static final String PHONE = "phone";
	public static final String GET_MESSAGE = "get_message";
	public static final String GET_STATUS = "get_status";
	public static final String AUDIO_CALL_MISSED = "audio_call_missed";
	public static final String VIDEO_CALL_MISSED= "video_call_missed";


	public static final String KEY = "key";
	public static final String UPDATED_AT = "updated_at";

	public static final String EMAIL_ADDRESS= "email_address";




	/*GetUserProfile*/
	public static final String FIRST_NAME = "first_name";
	public static final String LAST_NAME = "last_name";
	public static final String LANGUAGE = "language";
	public static final String CITY_ID = "city_id";

	public static final String LANGUAGE_NAME = "language_name";
	public static final String IS_REGISTRATION = "is_registration";


	public static final String FILE = "file";
	public static final String PASSWORD = "password";
	public static final String ALARM = "alarm";
	public static final String MAP_FILENAME = "map_filename";
	public static final String EXTENSION = "extension";


	public static final String IS_LOGIN= "isLogin";


	public static final String STOP_WATCH_TAB= "Stop Watch";
	public static final String TIMER_TAB = "Timer";
	public static final String ALARM_TAB = "Alarm";
	/*public static final String WORLD_CLOCK_TAB  = "World Clock";*/


	/*Calculator*/
	public static final String IS_SET_CALCULATION = "com.pro.android.shadow.isSetCalculation";


	/*Email Invitation*/
	public static final String TO = "to";

}
