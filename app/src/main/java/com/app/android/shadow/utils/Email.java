package com.app.android.shadow.utils;

import android.text.TextUtils;


public class Email {
    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target) && target==null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
