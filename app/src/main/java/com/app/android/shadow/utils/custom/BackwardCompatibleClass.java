package com.app.android.shadow.utils.custom;


import com.c2call.lib.androidlog.Ln;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class BackwardCompatibleClass {
    public BackwardCompatibleClass() {
    }

    protected static Method initMethod(Class c, String name, Class... params) {
        try {
            return c.getMethod(name, params);
        } catch (Exception var4) {
            log(new Object[]{"Invalid value in initMethod"});
            var4.printStackTrace();
            return null;
        }
    }

    protected static Field initField(Class c, String name) {
        try {
            return c.getField(name);
        } catch (Exception var3) {
            log(new Object[]{"Invalid value in initField"});
            var3.printStackTrace();
            return null;
        }
    }

    protected static Object getFieldValue(Object o, Field f) {
        if(f == null) {
            log(new Object[]{"Invalid value in invokeMethodString"});
            return null;
        } else {
            try {
                return f.get(o);
            } catch (Exception var3) {
                log(new Object[]{"Invalid value in getFieldValue"});
                var3.printStackTrace();
                return null;
            }
        }
    }

    protected static int getFieldValueInt(Object o, Field f) {
        if(f == null) {
            log(new Object[]{"Invalid value in getFieldValueInt"});
            return 0;
        } else {
            try {
                return f.getInt(o);
            } catch (Exception var3) {
                var3.printStackTrace();
                return 0;
            }
        }
    }

    protected static Object invokeMethod(Method m, Object o, Object... args) {
        if(m == null) {
            log(new Object[]{"Invalid value in invokeMethod"});
            return null;
        } else {
            try {
                return m.invoke(o, args);
            } catch (Exception var4) {
                var4.printStackTrace();
                return null;
            }
        }
    }

    protected static int invokeMethodInt(Method m, Object o, Object... args) {
        if(m == null) {
            log(new Object[]{"Invalid value in invokeMethodInt"});
            return 0;
        } else {
            try {
                Object e = invokeMethod(m, o, args);
                return e != null?((Integer)e).intValue():0;
            } catch (Exception var4) {
                var4.printStackTrace();
                return 0;
            }
        }
    }

    protected static boolean invokeMethodBoolean(Method m, Object o, Object... args) {
        if(m == null) {
            return false;
        } else {
            try {
                Object e = invokeMethod(m, o, args);
                return e != null?((Boolean)e).booleanValue():false;
            } catch (Exception var4) {
                var4.printStackTrace();
                return false;
            }
        }
    }

    protected static String invokeMethodString(Method m, Object o, Object... args) {
        if(m == null) {
            log(new Object[]{"Invalid value in invokeMethodString"});
            return null;
        } else {
            try {
                Object e = invokeMethod(m, o, args);
                return e != null?(String)e:null;
            } catch (Exception var4) {
                var4.printStackTrace();
                return null;
            }
        }
    }

    protected static float invokeMethodFloat(Method m, Object o, Object... args) {
        if(m == null) {
            log(new Object[]{"Invalid value in invokeMethodFloat()"});
            return 0.0F;
        } else {
            try {
                Object e = invokeMethod(m, o, args);
                return e != null?((Float)e).floatValue():0.0F;
            } catch (Exception var4) {
                var4.printStackTrace();
                return 0.0F;
            }
        }
    }

    private static void log(Object... args) {
        Ln.d("fc_backward", args, new Object[0]);
    }
}
