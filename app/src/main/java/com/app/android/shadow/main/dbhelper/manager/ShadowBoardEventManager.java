package com.app.android.shadow.main.dbhelper.manager;

import com.app.android.shadow.samplechat.board.ShadowNewMessageCache;
import com.app.android.shadow.utils.custom.StringExtra;
import com.c2call.lib.androidlog.Ln;
//import com.c2call.sdk.lib.util.extra.StringExtra;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.db.data.SCBoardEventExtraData;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.db.data.SCMediaData;
import com.c2call.sdk.pub.db.data.SCSecureMessageData;
import com.c2call.sdk.pub.db.datamanager.SCBoardEventManager;

/**
 * Created by rails-dev on 16/10/15.
 */
public class ShadowBoardEventManager extends SCBoardEventManager {

    private final SCBoardEventData _data;

    public ShadowBoardEventManager(SCBoardEventData data) {
        super(data);
        this._data = data;
    }

    @Override
    public SCMediaData createOrGetMediaData(boolean refresh) {
        return super.createOrGetMediaData(refresh);
    }

    @Override
    public SCSecureMessageData createOrGetSecureMessageData(boolean refresh) {
        return super.createOrGetSecureMessageData(refresh);
    }

    public synchronized boolean storeExtra(boolean notifyChange) {
        Ln.d("fc_tmp", "SCBoardEventManager.storeExtra(%b) - id: %s, secureMessage: %s", new Object[]{this._data.getId(), Boolean.valueOf(notifyChange), this._data.getSecureMessage()});

        try {
            this.customUpdateNewMessageCache();
            if(!StringExtra.isNullOrEmpty(this._data.getUserid())) {
                if(this._data.getFriend() == null) {
                    this._data.setFriend(new SCFriendData());
                }

                this._data.getFriend().setId(this._data.getUserid());
            }

            SCBoardEventData.dao().enableNotification(notifyChange);
            SCBoardEventExtraData.dao().createOrUpdate(this._data.getExtra());
            SCBoardEventData.dao().createOrUpdate(this._data);
            Ln.d("fc_tmp", "boardEventManager.storeExtra() - id: %s, userid: %s, content: %s", new Object[]{this._data.getId(), this._data.getUserid(), this._data.getDescription()});
            SCBoardEventData.dao().enableNotification(true);
            return true;
        } catch (Exception var3) {
            var3.printStackTrace();
            return false;
        }
    }

    public synchronized boolean storeSelf(boolean notifyChange) {
        Ln.d("fc_tmp", "SCBoardEventManager.storeSelf() - id: %s, secureMessage: %s", new Object[]{this._data.getId(), this._data.getSecureMessage()});

        try {
            this.customUpdateNewMessageCache();
            if(!StringExtra.isNullOrEmpty(this._data.getUserid())) {
                if(this._data.getFriend() == null) {
                    this._data.setFriend(new SCFriendData());
                }

                this._data.getFriend().setId(this._data.getUserid());
            }

            SCBoardEventData.dao().enableNotification(notifyChange);
            SCBoardEventData.dao().createOrUpdate(this._data);
            SCBoardEventData.dao().enableNotification(true);
            return true;
        } catch (Exception var3) {
            var3.printStackTrace();
            return false;
        }
    }

    public synchronized boolean storeAll(boolean notifyChange) {
        Ln.d("fc_tmp", "SCBoardEventManager.storeAll() - id: %s, secureMessage: %s", new Object[]{this._data.getId(), this._data.getSecureMessage()});

        try {
            this.customUpdateNewMessageCache();
            if(!StringExtra.isNullOrEmpty(this._data.getUserid())) {
                if(this._data.getFriend() == null) {
                    this._data.setFriend(new SCFriendData());
                }

                this._data.getFriend().setId(this._data.getUserid());
            }

            SCBoardEventData.dao().enableNotification(notifyChange);
            SCFriendData.dao().enableNotification(false);
            SCMediaData.dao().enableNotification(false);
            SCFriendData.dao().createOrUpdate(this._data.getFriend());
            SCBoardEventExtraData.dao().createOrUpdate(this._data.getExtra());
            SCMediaData.dao().createOrUpdate(this._data.getMediaData());
            SCSecureMessageData.dao().createOrUpdate(this._data.getSecureMessage());
            SCBoardEventData.dao().createOrUpdate(this._data);
            SCBoardEventData.dao().enableNotification(true);
            SCFriendData.dao().enableNotification(true);
            SCMediaData.dao().enableNotification(true);
            return true;
        } catch (Exception var3) {
            var3.printStackTrace();
            return false;
        }
    }

    private synchronized void customUpdateNewMessageCache() {
        ShadowNewMessageCache.instance().onHandleMessage(this._data);
    }
}
