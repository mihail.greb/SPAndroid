package com.app.android.shadow.main.tapmanager.settings;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.app.android.shadow.main.apicalls.ApiRequest;
import com.app.android.shadow.main.util.AlertDialogClass;
import com.app.android.shadow.main.adapters.ShadowLogsAdapter;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.R;
import com.app.android.shadow.main.adapters.model.ShadowLogsModel;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.util.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Custom Board Fragment to show a chat history grouped by users
 */
public class ShadowLogs extends BaseFragment implements View.OnClickListener
{

    ListView reqList;
    ArrayList<ShadowLogsModel> lst;
    ShadowLogsAdapter fReqAdapter = null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view =inflater.inflate(R.layout.shadow_logs, container, false);

        setHeader(_view);
        reqList = (ListView)_view.findViewById(R.id.shadow_list);
        callApi(getActivity(), APIServiceAsyncTask.callHistorysGet);

        return _view;
    }


    //Header
    @Override
    protected void setHeader(View v) {
        super.setHeader(v);
        TextView title =(TextView)v.findViewById(R.id.title_head);
        title.setText("Shadow Logs");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.audio_ringtone:

                break;
        }
    }

    public void callApi(Context mContext,ApiRequest apiReq){
        ApiCall mCall = new ApiCall(mContext);
        mCall.setServiceTaskMapStrParams(apiReq, null);
        mCall.execute((Void) null);
    }

    public class ApiCall extends APIServiceAsyncTask {
        ApiCall(Context mContext){
            super(mContext);
        }

        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            try {
                boolean suc = jsonObj.getBoolean(Const.SUCCESS);
                if (suc) {

                    if (serviceTaskType == APIServiceAsyncTask.CALL_HISTORY_GET) {
                        final String info = jsonObj.getString(Const.INFO);
                        JSONObject dataObj = jsonObj.getJSONObject(Const.DATA);
                        JSONArray friendsList = dataObj.getJSONArray("call_histories");
                        lst = new ArrayList<ShadowLogsModel>();
                        ShadowLogsModel model;
                        for(int i=0;i<friendsList.length();i++){
                            JSONObject fReqObj = friendsList.getJSONObject(i);
                            model = new ShadowLogsModel(
                                    fReqObj.getInt(Const.ID),
                                    fReqObj.getInt(Const.USER_ID),
                                    fReqObj.getString(Const.CALL_TYPE),
                                    fReqObj.getString(Const.STATUS),
                                    fReqObj.getInt(Const.TO_USER_ID),
                                    fReqObj.getString(Const.CREATED_AT),
                                    fReqObj.getString(Const.GET_TO_USERNAME),
                                    fReqObj.getString(Const.GET_DURATION)
                            );
                            lst.add(model);
                        }
                        fReqAdapter = new ShadowLogsAdapter(getActivity(),lst);
                        reqList.setAdapter(fReqAdapter);
                        callApi(getActivity(),APIServiceAsyncTask.callHistorysSeen);
                    }else {

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
            super.failure(jsonObj, serviceTaskType);
            try {
                final String info = jsonObj.getString(Const.INFO);
                new Thread(){
                    public void run(){
                        // Long time consuming operation
                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                new AlertDialogClass(getActivity()).open(info);
                            }
                        });
                    }
                }.start();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void failure(String message) {

        }
    }

}
