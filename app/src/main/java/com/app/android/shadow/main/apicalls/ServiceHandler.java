package com.app.android.shadow.main.apicalls;

import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.main.util.Logger;
/*import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;*/

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;


public class ServiceHandler {

    static String response = null;

    public final static int GET = 1;
    public final static int POST = 2;
    public final static int PUT = 3;
    public final static int PATCH = 4;
    public final static int DELETE = 5;

    public static String[] baseUrl  = {"http://callshadow.herokuapp.com/api/v1/"
                                        ,"http://callshadow.herokuapp.com/api/v2/"};

    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * */

    private String getURlFormPath(String path) {
        return this.baseUrl[0] + path;
    }
/*
    private String getURlFormPath(String path,List<NameValuePair> params) {
        if (params != null) {
            String paramString = URLEncodedUtils
                    .format(params, "utf-8");
            return this.baseUrl + path + "?" + paramString;
        }
        return this.baseUrl + path;
    }

    private HttpPost postRequest(String path,List<NameValuePair> params) {
        HttpPost httpPost = new HttpPost(this.getURlFormPath(path));
        if (params != null) {
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(params));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return httpPost;
    }

    private HttpPatch patchRequest(String path,List<NameValuePair> params) {
        HttpPatch httpPatch = new HttpPatch(this.getURlFormPath(path));
        if (params != null) {
            try {
                httpPatch.setEntity(new UrlEncodedFormEntity(params));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return httpPatch;
    }

    private HttpGet getRequest(String path,List<NameValuePair> params) {
        return new HttpGet(this.getURlFormPath(path,params));
    }
    private HttpPut putRequest(String path,List<NameValuePair> params) {
        return new HttpPut(this.getURlFormPath(path,params));
    }
    private HttpDelete deleteRequest(String path,List<NameValuePair> params) {
        return new HttpDelete(this.getURlFormPath(path,params));
    }

    */
    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     * */
    /*
    public String makeServiceCall1(String path, int method,
                                  List<NameValuePair> params) {
        try {
            // http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity = null;
            HttpResponse httpResponse = null;

            // Checking http request method type
            if (method == POST) {
                HttpPost httpPost = this.postRequest(path,params);
                httpResponse = httpClient.execute(httpPost);
            } else if (method == GET) {
                HttpGet httpGet = this.getRequest(path,params);
                httpResponse = httpClient.execute(httpGet);
            } else if (method == PUT) {
                HttpPut httpPut = this.putRequest(path,params);
                httpResponse = httpClient.execute(httpPut);
            } else if (method == DELETE) {
                HttpDelete httpDelete = this.deleteRequest(path,params);
                httpResponse = httpClient.execute(httpDelete);
            }

            httpEntity = httpResponse.getEntity();
            response = EntityUtils.toString(httpEntity);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;

    }

    */

    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     * */

    /*
    public JSONObject makeServiceCall(String path, int method,
                                      List<NameValuePair> params) {
        String paramsStr = null;
        try {
            paramsStr = getQuery(params);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return makeServiceCallBase(path, method,paramsStr);

    }
    */

    private String getUserToker(){
       String token = Shadow.getAppPreferences().getString(Const.AUTH_TOKEN,"");
        Logger.printLog(Logger.ERROR,"token ","is "+token);
        return token;
    }

    public JSONObject makeServiceCall(ApiRequest apiRequest,
                                      Map<String, String> params) {
            String paramsStr = null;
            try {
                if(params != null)
                paramsStr = getQuery(params);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return makeServiceCallBase(apiRequest, paramsStr);
    }

    public JSONObject makeServiceCallBase(ApiRequest apiRequest,
                                  String paramsStr) {
        HttpURLConnection urlConnection = null;

        URL url = null;
        JSONObject object = null;
        InputStream inStream = null;
        String temp, response="";
        try {
            String urlStr = this.getURlFormPath(apiRequest.path);
            if(apiRequest.method == DELETE && paramsStr!= null){
                urlStr=urlStr+"?"+paramsStr;
            }
            url = new URL(urlStr);

            Logger.printLog(Logger.ERROR,"URL is ",""+this.getURlFormPath(apiRequest.path));
            Logger.printLog(Logger.ERROR,"Method is ",""+apiRequest.method);
            Logger.printLog(Logger.ERROR,"peramers is ",""+paramsStr);

            urlConnection = (HttpURLConnection) url.openConnection();

            if (apiRequest.method == POST) {
                urlConnection.setRequestMethod("POST");
            } else if (apiRequest.method == GET) {
                urlConnection.setRequestMethod("GET");
            } else if (apiRequest.method == PUT) {
                urlConnection.setRequestMethod("PUT");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Accept", "application/json");
            } else if (apiRequest.method == PATCH) {
                urlConnection.setRequestMethod("PATCH");
            } else if (apiRequest.method == DELETE) {
                urlConnection.setDoInput(true);
                urlConnection.setInstanceFollowRedirects(false);
                if(apiRequest.authunticate) {
                    urlConnection.setRequestProperty("auth-token", getUserToker());
                }
                urlConnection.setRequestProperty("Connection", "Keep-Alive");
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setRequestMethod("DELETE");
                urlConnection.setRequestProperty("charset", "utf-8");
                urlConnection.setUseCaches(false);
                urlConnection.connect();
            }

            if(apiRequest.authunticate && apiRequest.method != DELETE) {
                urlConnection.setRequestProperty("auth_token", getUserToker());
            }
            if(apiRequest.method != DELETE && apiRequest.method != GET) {
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                if(apiRequest.method == PUT){
                    OutputStreamWriter osw = new OutputStreamWriter(urlConnection.getOutputStream());
                    if(paramsStr != null &&  paramsStr.length()>0)
                    osw.write(paramsStr);
                    osw.flush();
                    osw.close();
                    System.err.println(urlConnection.getResponseCode());
                }else {
                    OutputStream os = urlConnection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(paramsStr);
                    Logger.printLog(Logger.ERROR,"paramsStr", "is " + paramsStr);
                    writer.flush();
                    writer.close();
                    os.close();
                    urlConnection.connect();
                }
            }
            try {
                int status = urlConnection.getResponseCode();
                Logger.printLog(Logger.ERROR,"Print Response Code","" +status);
                //if((status%100) != HttpStatus.SC_BAD_REQUEST)
                if((status/100) != 2)
                    inStream = urlConnection.getErrorStream();
                else
                    inStream = urlConnection.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
                inStream = urlConnection.getErrorStream();
                if(inStream == null){
                    inStream = urlConnection.getInputStream();
                }
            }

            BufferedReader bReader = new BufferedReader(new InputStreamReader(inStream));
            while ((temp = bReader.readLine()) != null) {
                response += temp;
            }

            Logger.printLog(Logger.ERROR,"Response is ", "" + response);
            object = (JSONObject) new JSONTokener(response).nextValue();

            //object= new JSONObject(getStringFromInputStream(inStream));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inStream != null) {
                try {
                    // this will close the bReader as well
                    inStream.close();
                } catch (IOException ignored) {
                }
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        try {
            Logger.printLog(Logger.ERROR,"JsonObject", "" + object.toString());
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        return object;
    }

   /* public JSONObject makeServicePatchCallBase(String path, int method,
                                               Map<String, String> params) throws IOException, JSONException {

         OkHttpClient client = new OkHttpClient();

        ArrayList<String > keyList = new ArrayList<String >();
       // public void run() throws Exception {
        for (Map.Entry<String,String> entry : params.entrySet()) {
            //System.out.printf("%s -> %s%n", entry.getKey(), entry.getValue());
            keyList.add(entry.getKey());
        }
            RequestBody formBody = new FormEncodingBuilder()
                .add(keyList.get(0), params.get(keyList.get(0)))
                .add(keyList.get(1), params.get(keyList.get(1)))
                .add(keyList.get(2), params.get(keyList.get(2)))
                .add(keyList.get(3), params.get(keyList.get(3)))
                .add(keyList.get(4), params.get(keyList.get(4)))
                .add(keyList.get(5), params.get(keyList.get(5)))
                .add(keyList.get(6), params.get(keyList.get(6)))
                    .build();

            Request request = new Request.Builder()
                    .url(this.getURlFormPath(path))
                    .patch(formBody)
                    .header("auth_token", "" + getUserToker())
                    .build();

            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        String res = response.body().string();

            Logger.printLog(Logger.ERROR,"okHttp Response", res);
       // }

        try {
            return  new JSONObject(res);
        } catch (Throwable t) {
            Logger.printLog(Logger.ERROR,"My App", "Could not parse malformed JSON");
            return  null;
        }
    }*/

    public JSONObject makeDownloadCall(String downloadURLKey, String savePathKey,Map<String, String> params){
        String downloadUrl = params.get(downloadURLKey);
        String saveFilePath = params.get(savePathKey);
        if(downloadUrl == null || saveFilePath == null){
            return null;
        }
        JSONObject object = null;
        final int  BYTE = 1024;
        final int  MEGABYTE = BYTE * 1024;
        HttpURLConnection urlConnection = null;
        URL url = null;
        try {
            url = new URL(downloadUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            //urlConnection.setRequestMethod("GET");
            //urlConnection.setDoInput(true);
            //urlConnection.setDoOutput(true);

            String afterSaveFilePath = saveFilePath;
            String lastStr = saveFilePath.substring(saveFilePath.lastIndexOf('/') + 1);
            saveFilePath = saveFilePath.replace(lastStr,"tempData__"+ lastStr);
            Logger.printLog(Logger.ERROR,"FileName: ", afterSaveFilePath);
            Logger.printLog(Logger.ERROR,"FileName: ", saveFilePath);


            urlConnection.connect();
            File tmpFile = new File(saveFilePath);
            FileOutputStream fileOutput = new FileOutputStream(tmpFile);
            InputStream inputStream = urlConnection.getInputStream();
            int totalSize = urlConnection.getContentLength();
            int downloadedSize = 0;
            float presentSize = totalSize/100;
            byte[] buffer = new byte[MEGABYTE];
            int bufferSize = 0;
            while ( (bufferSize = inputStream.read(buffer)) > 0 ) {
                fileOutput.write(buffer, 0, bufferSize);
                downloadedSize += bufferSize;
                //Logger.printLog(Logger.ERROR,"Progress", "Downloaded : "+  Math.round(downloadedSize/presentSize)+" %");
            }
            fileOutput.close();

            File file = new File(afterSaveFilePath);
            if(tmpFile.exists())
                tmpFile.renameTo(file);

            object = (JSONObject) new JSONTokener("{\"success\":true}").nextValue();
        } catch (Exception e) {
            //Logger.printLog(Logger.ERROR,"Progress", "Error : " + e.getMessage());
            e.printStackTrace();
            return null;
        }
        return object;
    }

  /*  public JSONObject makeUploadCall(String path, int method,Map<String, Object> params){
        String fileName = params.get("path").toString();
        JSONObject object = null;
        int serverResponseCode = 0;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(fileName);

        if (sourceFile.isFile()) {
            try {

                // open a URL connection to the Servlet
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(""+urls[APIBASEURL]+path);

                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + fileName +"\"" + lineEnd);
                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                     int progress = (int)((bytesRead / (float) bufferSize) * 100);
                     //publishProgress(progress);

                }


               // publishProgress(100);

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if(serverResponseCode == 200){
                    RondogoApp.alertToast("Done");
                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();
                object = (JSONObject) new JSONTokener("{\"success\":true}").nextValue();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } // End else block
        return object;
    }*/
    /*
    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }
    */
    public static String getQuery(Map<String, String> params) throws UnsupportedEncodingException {
        StringBuffer requestParams = new StringBuffer();
        if (params != null && params.size() > 0) {
            Iterator<String> paramIterator = params.keySet().iterator();
            while (paramIterator.hasNext()) {
                String key = paramIterator.next();
                String value = params.get(key);
                requestParams.append(URLEncoder.encode(key, "UTF-8"));
                requestParams.append("=").append(URLEncoder.encode(value, "UTF-8"));
                requestParams.append("&");
            }
        }
        int lenght =requestParams.toString().length();
        if(lenght>0)
            return requestParams.toString().substring(0,(lenght-1));
        else
            return requestParams.toString();
    }

    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }
}
