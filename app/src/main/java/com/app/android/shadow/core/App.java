package com.app.android.shadow.core;

import android.content.Context;
import android.support.multidex.MultiDex;

import com.actai.logger.SipLogger;
import com.app.android.shadow.common.ui.custom.eventhandler.ShodowCustomEventHandler;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.custom.call.ShadowSCIncomingCallHandler;
import com.app.android.shadow.main.custom.handler.ShadowPushHandler;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.main.util.SdCardManager;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.affiliate.AffiliateCredentials;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.facade.SCMediaFacade;

import java.io.File;
import java.io.IOException;

/**
 * Application class of this C2Call SDK
 */

public class App extends Shadow
{
    @Override
    public void onCreate()
    {
        super.onCreate();

        /*
          The affiliate ID and Secret from the C2Call SDK DevArea
          Note: In a custom App you will have to change these to your own credentials
         */

        String affiliateID = "B5566D2614FB7074076";
        String secret = (getpackageNameAsStr().equals("com.pro.android.shadow"))?"56a191340c26e039724b03e5e8494547":"4f026657a50835161d01b3cd0240a806";
        //String secret = "4f026657a50835161d01b3cd0240a806"; // Free App
        //String secret = "56a191340c26e039724b03e5e8494547"; //Pro App

      final AffiliateCredentials credentials = new AffiliateCredentials(affiliateID,
                                                                          this.getPackageName(),
                                                                          secret);

        Ln.d("c2app", "App.onCreate() - affiliate: %s", credentials);

       /*
         Initialise the C2CallSdk instance. This must be called before
         using any C2Call functionality
        */

        /*
        Initialise the C2CallSdk instance. This must be called before using any C2Call functionality
         */
        C2CallSdk.instance().init(this, credentials);
        try {
            File directory = SdCardManager.getMainDirectory(getApplicationContext());
            Logger.printLog(Logger.ERROR,"file path is "+directory.getAbsolutePath());
            SCMediaFacade.instance().setMediaRootDirectory(directory);
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*
        Whether the SDK should produce verbose logging. This will produce a lot of logs which may be helpful
        for debugging. Beside that you should disable it.
         */
        boolean enableDebug = true;

        Ln.enableLogging(enableDebug);
        SipLogger.enableDebug(enableDebug);


        /*
        Set our custom notifier for the SDK. This will be used for all SDK notifications like
        "new messages", "missed calles", etc.
         */
        //ShadowC2callSdk.instance().setNotifier(new AppNotifier());
        C2CallSdk.instance().setNotifier(new AppNotifier());

        /*
        Set our custom StartControl. The SDK will use this instance to open certain Activities.
         */
        //ShadowC2callSdk.instance().setStartControl(new StartControl());
        C2CallSdk.instance().setStartControl(new StartControl());

        /*
        Set our custom DialoFactory. The SDK will use this to generate certain dialogs.
         */
        //ShadowC2callSdk.instance().setDialogFactory(new DialogFactory());
        C2CallSdk.instance().setDialogFactory(new DialogFactory());

        /*
         Set our custom IncomingCallHandler for Shadow
        * */
        C2CallSdk.instance().setIncomingCallHandler(new ShadowSCIncomingCallHandler());

         /*
         Set our custom MessageHandler for Shadow
        * */
        C2CallSdk.instance().setPushHandler(new ShadowPushHandler());
        ShodowCustomEventHandler.instance().subscribe();

       /* String version = C2CallSdk.instance().getVersionString();
        Log.e("Print Version ",version);*/

        init();
    }

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);

        MultiDex.install(this);
    }

    /**
     * Do some application specific initialisations
     */
    private void init()
    {
        try {
            Ln.d("c2app", "App.init() - init billing helper...");
            SCCoreFacade.instance().setEnableOfferWall(true);
            RingingHandler.instance().subscribe();

            Ln.d("c2app", "App.init() - init billing helper... - done.");
        }
        catch(Throwable e){
            e.printStackTrace();
        }
    }
}
