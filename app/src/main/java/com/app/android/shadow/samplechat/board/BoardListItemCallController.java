package com.app.android.shadow.samplechat.board;

import android.view.View;

import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemCallController;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;

/**
 * Basic custom board item controller. This controls a single board list item that is showing a call.
 * Further customization of the controller's functionality should be done here
 */
public class BoardListItemCallController extends SCBoardListItemCallController
{
    public BoardListItemCallController(View view, SCViewDescription scViewDescription, SCBoardEventData scBoardEventData)
    {
        super(view, scViewDescription, scBoardEventData);
    }

    @Override
    public void onMainViewClick(View v) {
        //super.onMainViewClick(v);
    }

    @Override
    public void onButtonCallClick(View v) {
        //super.onButtonCallClick(v);
    }

    @Override
    public void onButtonVideoClick(View v) {
        //super.onButtonVideoClick(v);
    }

    @Override
    public void onButtonMessageClick(View v) {
        //super.onButtonMessageClick(v);
    }

    @Override
    public void onButtonDetailsClick(View v) {
        //super.onButtonDetailsClick(v);
    }

    @Override
    public void onMainViewLongClick(View view)
    {
        /*
        This super call is needed to fire the SCBoardItemLongClickedEvent that may be caught by other components of the app
         */
       // super.onMainViewLongClick(view);

        /*
        Show a simple context dialog for this item
         */
       // SCChoiceDialog dlg = C2CallSdk.dialogFactory().createBoardItemDialog(this);
       // dlg.show();
    }
}
