package com.app.android.shadow.common.ui.activities;

import android.app.Fragment;

import com.app.android.shadow.common.ui.fragments.ShadowContactDetailFragment;
import com.app.android.shadow.main.util.Logger;
import com.c2call.sdk.pub.activities.core.SCControlledFragmentActivity;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.gui.contactdetail.controller.IContactDetailController;

/**
 * Created by rails-dev on 8/10/15.
 */
public class ShadowContactDetailFragmentActivity extends SCControlledFragmentActivity<IContactDetailController>{

    public ShadowContactDetailFragmentActivity() {
    }

    protected Fragment onCreateFragment() {
        //Toast.makeText(this, "" + getClass().getSimpleName(), Toast.LENGTH_LONG).show();
        Logger.printLog(Logger.ERROR,"print details2" + "activity");
        int layout = this.getIntent().getExtras().getInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT);
        String id = this.getIntent().getStringExtra(SCExtraData.ContactDetail.EXTRA_DATA_USERID);
        int usertype = this.getIntent().getIntExtra(SCExtraData.ContactDetail.EXTRA_DATA_USER_TYPE, 0);
        Fragment fragment = ShadowContactDetailFragment.create(id, usertype, layout);
        Logger.printLog(Logger.ERROR,"fragment name "+fragment.getClass().getSimpleName());
        return fragment;
    }

}
