package com.app.android.shadow.main.util;

import android.app.Activity;
import android.app.Fragment;
import android.os.Handler;

import com.app.android.shadow.samplechat.SimpleChatFriendsFragment;
import com.app.android.shadow.main.Shadow;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by rails-dev on 2/2/16.
 */
public class ScheduleTimer {

    Timer timer = null;
    long lastTime = 0;
    int time;
    TimerTask timerTask;
    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();

    Activity activity = null;
    Fragment fragment = null;

    public ScheduleTimer(Activity activity, Fragment fragment,int time) {
        if (activity != null){
            this.activity = activity;
        }else {
            this.fragment = fragment;
        }
        this.time = time;
        //Shadow.alertToast("working constractor"+time);
    }

    public void setOrResetTimer(){
        Shadow.alertToast("working setOrResetTimer");
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        Shadow.alertToast("working setOrResetTimer2");
                        if(activity != null){

                        }else {
                            if(fragment instanceof SimpleChatFriendsFragment){
                                Shadow.alertToast("working");
                                      //  ((SimpleChatFriendsFragment) fragment).timerComplete();
                            }
                        }
                    }
                });
            }
        };
        timer.schedule(timerTask, ((long) time * 60 * 1000));
    }

    public void cancelTimer(){
        try {
            this.timer.cancel();
            this.timer = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
