package com.app.android.shadow.samplechat.board;

import android.content.Intent;
import android.view.View;

import com.app.android.shadow.common.ui.activities.ShadowShowMediaActivity;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.util.Logger;
import com.c2call.sdk.pub.common.SCRichMessageType;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemRichController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemType;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;

/**
 * Basic custom board item controller. This controls a single board list item that is showing a media message (audio, image, video, ...)
 * Further customization of the controller's functionality should be done here
 */
public class BoardListItemRichController extends SCBoardListItemRichController
{
    private SCRichMessageType _richMessageType = null;
    public BoardListItemRichController(SCBoardListItemType type, View view, SCViewDescription scViewDescription, SCBoardEventData scBoardEventData)
    {
        super(type, view, scViewDescription, scBoardEventData);
    }

    @Override
    public void onMainViewLongClick(View view)
    {

       // This super call is needed to fire the SCBoardItemLongClickedEvent that may be caught by other components of the app

        super.onMainViewLongClick(view);


       // Show a simple context dialog for this item

        SCChoiceDialog dlg = C2CallSdk.dialogFactory().createBoardItemDialog(this);
        dlg.show();
    }

      @Override
    public void onMainViewClick(View v) {
        BaseActivity.setSaveTime0();
        BoardActivity.isAppNotClose = true;
        super.onMainViewClick(v);
    }

  /*  @Override
    public void onMainViewClick(View var1) {
        Ln.d("fc_tmp", "SCBoardListItemRichController.onRichMessageClicked: %s / %s - %s", new Object[]{this._richMessageType, this._richMessagePath, ((SCBoardEventData) this.getData()).getId()});
        BaseActivity.setSaveTime0();
        BoardActivity.isAppNotClose = true;
        if(this._richMessageType != null) {
            if(this.getMessageStatus() == 3) {
                //this.onErrorMessageClicked();
            } else {
                Ln.d("fc_tmp", "SCBoardListItemRichController.onUpdateRichMessagePath...", new Object[0]);
                this.onUpdateRichMessagePath();
                Ln.d("fc_tmp", "SCBoardListItemRichController.onUpdateRichMessagePath... - done", new Object[0]);
                if(this._richMessagePath == null && this._richMessageType.isDownloadable()) {
                    Ln.d("fc_error", "* * * Error: DefaultBoardListItemRichMediator.onRichMessageClicked() - richMessagePath could not be resovled!", new Object[0]);
                } else {
                    switch(this._richMessageType) {
                        case Audio: //
                            Shadow.alertToast("working1");
                           *//* AdManager.instance().setInterstitialMark(AdSpaceId.AUIO_PLAYER_INTERSTITIAL);
                            this.onRichMessageMediaClick("audio");*//*
                            return;
                        case Image: //
                            Shadow.alertToast("working2");
                           *//* AdManager.instance().setInterstitialMark(AdSpaceId.PHOTO_VIEWER_INTERSTITIAL);
                            this.onRichMessageMediaClick("image");*//*
                            return;
                        case Video: //
                            Shadow.alertToast("working3");
                            *//*AdManager.instance().setInterstitialMark(AdSpaceId.VIDEO_PLAYER_INTERSTITIAL);
                            this.onRichMessageMediaClick("video");*//*
                            return;
                        case Location: //
                            Shadow.alertToast("working4");
                            *//*AdManager.instance().setInterstitialMark(AdSpaceId.LOCATION_VIEWER_INTERSTITIAL);
                            this.onRichMessageLocationClick();*//*
                            return;
                        case VCard: //
                            Shadow.alertToast("working5");
                           *//* this.onRichMessageVCardClick();*//*
                            return;
                        case Friend: //
                            Shadow.alertToast("working6");
                            *//*this.onRichMessageFriendClick();*//*
                            return;
                        case File:
                            Shadow.alertToast("working7");
                            //this.openFile();
                            return;
                        default:
                            Shadow.alertToast("working default");
                            //super.onMainViewClick(var1);
                          //  Ln.e("fc_error", "* * * Error: DefaultBoardListItemRichMediator.onRichMessageClicked() - MessageType not implemented yet: %s", new Object[]{this._richMessageType});
                           // this.openFile();
                    }
                }
            }
        }
    }*/

    @Override
    protected void onRichMessageMediaClick(String s) {
        //super.onRichMessageMediaClick(s);
        Intent var2 = new Intent(this.getContext(), ShadowShowMediaActivity.class);
        Logger.printLog(Logger.ERROR, "uri is " + this._richMessagePath);
        if(s.contains("image"))
            var2.putExtra("flag", "image");
        else if(s.contains("video"))
            var2.putExtra("flag", "video");
        else if(s.contains("audio"))
            var2.putExtra("flag", "audio");

        var2.putExtra("URI", this._richMessagePath);
        this.getContext().startActivity(var2);
    }

    @Override
    public void clearRichContent() {
        this._richMessageType = null;
        super.clearRichContent();
    }

    @Override
    public void setRichMessageType(SCRichMessageType scRichMessageType) {
        _richMessageType = scRichMessageType;
        super.setRichMessageType(scRichMessageType);
    }

}
