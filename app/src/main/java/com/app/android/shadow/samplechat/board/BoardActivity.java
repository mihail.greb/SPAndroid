package com.app.android.shadow.samplechat.board;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;

import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.apicalls.ApiRequest;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.utils.Str;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.samplechat.SimpleChatMainActivity;
import com.app.android.shadow.R;
import com.app.android.shadow.common.ui.fragments.BoardFragment;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.fragments.SCBoardFragment;
import com.c2call.sdk.pub.fragments.communication.IBoardFragmentCommunictation;
import com.c2call.sdk.pub.gui.core.events.SCBaseControllerEvent;
import com.c2call.sdk.pub.gui.core.loaderhandler.ILoaderHandlerContext;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONObject;

/**
 * Custom Board Activity that shows a chat history with a given user.
 * This activity contains two fragments. One for the chat list and one for the chat input.
 * In this sample we override both of them.
 *
 */
public class BoardActivity extends BaseActivity implements SCBoardFragment.Callbacks, com.c2call.sdk.pub.fragments.SCNewMessageFragment.Callbacks, ILoaderHandlerContext
{

    public static boolean isAppNotClose = false;
    private String curPosition;
    protected View mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.onInitWithIntent(this.getIntent(), savedInstanceState);
        invalidateOptionsMenu();
        Shadow.setCurrentActivity(BoardActivity.this);
        initHeader();
        curPosition = ""+getIntent().getExtras().getInt("position");
    }



    private void initHeader() {
        ((TextView)findViewById(R.id.title_head)).setText(getResources().getString(R.string.chat));
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        resetAutoLogoutTimer();
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onRestart() {
        mainLayout.setVisibility(View.INVISIBLE);
        super.onRestart();
        checkLogout();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                BoardActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mainLayout.setVisibility(View.VISIBLE);
                    }
                });
            }
        }, 1000);
    }

    @Override
    protected void onResume() {
     //   mainLayout.setVisibility(View.INVISIBLE);
        super.onResume();
      /*  isAppNotClose = false;
        checkLogout();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                BoardActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mainLayout.setVisibility(View.VISIBLE);
                    }
                });
            }
        }, 1000);*/
        initAutoLogoutTimer();
    }


    @Override
    protected void onPause() {
        removeTimer();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
       /* if(!isAppNotClose) {
            Logger.printLog(Logger.ERROR, "onStop exit board");
            exittoApp();
        }*/
    }

    @Override
    protected void onDestroy() {
        removeTimer();
        super.onDestroy();
    }

    public void exittoApp(){
        callApi(APIServiceAsyncTask.setUserOfflien);
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        Logger.printLog(Logger.ERROR, "onDestroy exit2 base");
        finish();
        System.exit(0);
    }

    public void callApi(ApiRequest req){
        ApiTaskBase mTask = new ApiTaskBase(this);
        mTask.setServiceTaskMapStrParams(req, null);
        mTask.execute((Void) null);
    }


    public class ApiTaskBase extends APIServiceAsyncTask {
        ApiTaskBase(Context mContext){
            super(mContext);
        }

        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            if(APIServiceAsyncTask.OFFLINE_USER == serviceTaskType){
                Logger.printLog(Logger.ERROR, "success to offline " + jsonObj.toString());
            }
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
            super.failure(jsonObj, serviceTaskType);
            failure("" + serviceTaskType);
            Logger.printLog(Logger.ERROR, "failed to offline " + jsonObj.toString());
        }

        @Override
        protected void failure(String message) {
            //super.failure(message);
            Logger.printLog(Logger.ERROR,"failed to offline null");
            if(APIServiceAsyncTask.OFFLINE_USER == Integer.parseInt(message)) {
            }
        }
    }

    /*Methods form parent*/
    private String _userid;
    private View _view;

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.onInitWithIntent(intent, (Bundle)null);
    }

    protected void onInitWithIntent(Intent intent, Bundle savedInstanceState) {
        this._userid = intent.getStringExtra(SCExtraData.Board.EXTRA_DATA_USERID);
        Logger.printLog(Logger.ERROR,"Board UserId",_userid);
        String title = C2CallSdk.contactResolver().getDisplayNameByUserid(this._userid, true);
        this.setTitle(title);
        int layout = intent.getIntExtra(SCExtraData.ContactDetail.EXTRA_DATA_USER_TYPE, 0);
        if(layout == 0) {
            layout = R.layout.sc_board;
        }

        this._view = LayoutInflater.from(this).inflate(layout, (ViewGroup)null);
        this.setContentView(this._view);
        mainLayout = findViewById(R.id.mainLayout);
        this.restoreFragment(savedInstanceState);
    }

    public void restoreFragment(Bundle savedInstanceState) {
        this.onInitFragments();
    }

    private void onInitFragments() {
        Fragment boardFragment = this.onCreateBoardFragment(this._userid);
        Fragment messageInputFragment = this.onCreateMessageInputFragment(this._userid);
        FragmentManager fragmentManager = this.getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if(boardFragment != null) {
            //ft.replace(com.c2call.sdk.R.id.view_board_list_container, boardFragment);
            ft.replace(R.id.view_board_list_container, boardFragment);
        }

        if(messageInputFragment != null) {
            //ft.replace(com.c2call.sdk.R.id.view_board_input_container, messageInputFragment);
            ft.replace(R.id.view_board_input_container, messageInputFragment);
        }

        ft.commit();
    }
    /* /Methods form parent*/

    /**
     * Create a custom board fragment to show the chat list
     * @param userid the userid to filter the chat. If this is empty then every message from every friend should be shown
     * @return the new fragment
     */
    protected Fragment onCreateBoardFragment(final String userid)
    {
        Ln.d("c2app", "BoardActivity.onCreateBoardFragment() - userid: %s", userid);
        return BoardFragment.create(userid, R.layout.sc_board_part_list, getIntent().getExtras());
    }

    /**
     * Creates a custom chat input fragment
     * @param userid the userid of the chat partner where the new messages should be send to.
     *               If this <p>userid</p> is null then no chat input fragment is created.
     * @return
     */
    protected Fragment onCreateMessageInputFragment(String userid)
    {
        Ln.d("c2app", "BoardActivity.onCreateMessageInputFragment() - userid: %s", userid);
        try {
            if (userid == null) {
                return null;
            }

            SCFriendData friend = null;
            if (Str.isPhonenumber(userid)) {
                friend = new SCFriendData(userid);
            }
            else {
                friend = SCFriendData.dao().queryForId(userid);
                if (friend == null) {
                    friend = new SCFriendData(userid);
                }
            }

            return BoardChatInputFrgament.create(friend, getIntent().getExtras(), R.layout.sc_board_part_chatinput);
        }
        catch (final Exception e) {
            e.printStackTrace();
            throw new IllegalStateException("No friend found for userid: " + userid);
        }
    }

    @Override
    public void onBackPressed() {
        isAppNotClose = true;
        // super.onBackPressed();
        initAutoLogoutTimer();
        Intent intent = new Intent(this, SimpleChatMainActivity.class);
        intent.putExtra("from","chat");
        intent.putExtra("position",curPosition);
        startActivity(intent);
        finish();
    }


    @Override
    public void onControllerEvent(SCBaseControllerEvent scBaseControllerEvent) {

    }

    @Override
    public Context getLoaderContext() {
        return this;
    }

    @Override
    public void onRegisterBoardCommunictation(IBoardFragmentCommunictation iBoardFragmentCommunictation) {

    }

}


