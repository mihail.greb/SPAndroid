package com.app.android.shadow.common.ui.dialogs;

import com.c2call.sdk.pub.gui.dialog.BaseItemRunnable;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemController;

public class ShadowDialogBuilderContactListItem {

    public ShadowDialogBuilderContactListItem() {
    }

    public static SCChoiceDialog build(IFriendListItemController controller) {

        new ShadowDialogBuilderContactListItem.ViewDetailRunnable(controller).run();
       // SCChoiceDialog.Builder builder = new SCChoiceDialog.Builder(controller.getContext());
        //builder.addItem(com.c2call.sdk.R.string.sc_dlg_ctx_contact_details_title, com.c2call.sdk.R.string.sc_dlg_ctx_contact_details_summary, com.c2call.sdk.R.drawable.ic_sc_profile_grey, new ShadowDialogBuilderContactListItem.ViewDetailRunnable(controller));
        //return builder.build();
        return null;
    }

     public static class ViewDetailRunnable extends BaseItemRunnable<IFriendListItemController> {
        public ViewDetailRunnable(IFriendListItemController controller) {
            super(controller);
        }

        public void run() {
            ((IFriendListItemController)this.getController()).openDetail();
        }
    }
}