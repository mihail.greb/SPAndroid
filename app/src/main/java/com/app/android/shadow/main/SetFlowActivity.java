package com.app.android.shadow.main;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;

import com.app.android.shadow.main.IntroScreens.IntroActivity;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.R;
import com.app.android.shadow.main.IntroScreens.IntroActivityFree;
import com.app.android.shadow.main.util.CheckNetwork;
import com.app.android.shadow.main.util.Const;

public class SetFlowActivity extends Activity implements View.OnClickListener {

    String appName = "";
    LinearLayout splash,splashOff;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //splash = (LinearLayout) findViewById(R.id.splash);
        //splashOff = (LinearLayout) findViewById(R.id.splash_off);
        if(!CheckNetwork.isNetworkAvailable(this)){
            finish();
        }
        String app = Shadow.getCurrentAppRun();
        if((app != null && app.length() > 0)) {
            checkForNext();
        }else {
            showChooseOptions();
        }
    }

    private void showChooseOptions() {
        if(Shadow.isProApp()) {
            //switchVisiblity();
            setContentView(R.layout.shadow_choose_app);
            LinearLayout yp_apndroid = (LinearLayout) findViewById(R.id.yp_android);
            LinearLayout ac_apndroid = (LinearLayout) findViewById(R.id.ac_android);
            LinearLayout cal_apndroid = (LinearLayout) findViewById(R.id.cal_android);
            yp_apndroid.setOnClickListener(this);
            ac_apndroid.setOnClickListener(this);
            cal_apndroid.setOnClickListener(this);
        }else {
            setAppName(Shadow.appsNames[0]);
        }
    }

    /*private void switchVisiblity() {
        splash.setVisibility(View.GONE);
        splashOff.setVisibility(View.VISIBLE);
    }*/

    private void setAppName(String appname) {
        Shadow.getAppPreferencesEditor().putString(Const.APP_NAME,appname).commit();
        appName = appname;
        if(appName!= null && appName.length()>0) {
            //Shadow.getCurrentAppRun();
            setApp();
        }
    }

    private void setApp(){
        //callApi();
        if(Shadow.isProApp()){ //appName.equals(Shadow.appsNames[0])
            getPackageManager().setComponentEnabledSetting(
                    new ComponentName(Shadow.getpackageNameAsStr(), Shadow.getpackageNameAsStr()+".main.SetFlowActivity-sp"),
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);

            getPackageManager().setComponentEnabledSetting(
                    new ComponentName(Shadow.getpackageNameAsStr(), Shadow.getGamePackageName(appName)),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

        }else {

        }

        checkForNext();
    }

   private int getEnableDisabled(String name){
        if(appName.equals(name)) {
            Shadow.getAppPreferencesEditor().putString(Const.APP_NAME,name).commit();
            return PackageManager.COMPONENT_ENABLED_STATE_ENABLED;
        }else {
            return PackageManager.COMPONENT_ENABLED_STATE_DISABLED;
        }
    }

    protected void checkForNext(){
        if (Shadow.getAppPreferences().getBoolean(Const.FIRST_TIME_LOGGED, true)) {
                if(Shadow.isProApp()){
                    goToNextScreen(IntroActivity.class);
                }else {
                    goToNextScreen(IntroActivityFree.class);
                }
        }else {
            if (!Shadow.getAppPreferences().getBoolean(Const.IS_LOGIN, false))
                goToNextScreen(LauncherActivity.class);
            else
                goToNextScreen(Shadow.getWayToAccessCls());
        }
    }

    protected void goToNextScreen(Class lanchClass){
        Logger.printLog(Logger.ERROR,"Working class "+lanchClass.getSimpleName());
        Intent intent = new Intent(this,lanchClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.yp_android:
                setAppName(Shadow.appsNames[1]);
                break;
            case R.id.ac_android:
                setAppName(Shadow.appsNames[2]);
                break;
            case R.id.cal_android:
                Shadow.getAppPreferencesEditor().putString(Const.CAL_SAVE_LOGIN_UNSAFE, "0*0").commit();
                setAppName(Shadow.appsNames[3]);
                break;

        }
    }
}
