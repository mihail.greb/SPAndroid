package com.app.android.shadow.main.tapmanager.gallery;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.R;

import java.util.HashMap;


/**
 * Custom Board Fragment to show a chat history grouped by users
 */
public class GalleryFullViewFragment extends BaseFragment implements MediaPlayer.OnCompletionListener{

    ImageView fullImage ;
    public VideoView videoView;
    ImageView playVideo;
    MediaController mc;
    HashMap<String,String > map;
    String uri;
    boolean isVideo;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shadow_gallery_fullview, container, false);
        //setHeader(view);
       /* fullImage = (ImageView) view.findViewById(R.id.full_view);
        mc= new MediaController(getActivity());
        videoView = (VideoView)view.findViewById(R.id.play_video);
        Bundle bundle = getArguments();
        if(bundle != null){
            uri = bundle.getString("URI");
            Logger.printLog(Logger.ERROR,"uri now is "+uri);
           isVideo= bundle.getBoolean("isVideo");
            //map = (HashMap<String, String>) bundle.getSerializable("HashMap");
            if(!isVideo){
                fullImage.setVisibility(View.VISIBLE);
                fullImage.setImageURI(Uri.parse(uri));
            }else {
                videoView.setVisibility(View.VISIBLE);
                initVidoeCode(uri);
            }
        }*/

        return view;
    }

    //Header
    TextView title,closeScreen,deleteItem;

   /* protected void setHeader(View v) {
        title = (TextView) v.findViewById(R.id.title_head);
        title.setText(resources.getString(R.string.shadow_gallery));
        v.findViewById(R.id.back).setVisibility(View.INVISIBLE);
        closeScreen = (TextView) v.findViewById(R.id.close_screen);
        closeScreen.setVisibility(View.VISIBLE);
        closeScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

*//*        deleteItem = (TextView) v.findViewById(R.id.deleteItem);
        deleteItem.setVisibility(View.VISIBLE);
        deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCurrentItem();
            }
        });
        mCallback.addToIncressClickArea(closeScreen);*//*
    }*/

   /* private void deleteCurrentItem() {

        if(map != null){
            String id = map.get(Const._ID);
            if(id != null && id.length()>0){
                if(CustomC2CallContentProvider.customeC2callDB.deleteMediaData(id)){
                    try {
                        new File(map.get(CustomeC2callDB.LOC)).delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    onBackPressed();
                }
            }
        }else {
            Logger.printLog(Logger.ERROR,"Shadow map","is null");
        }
    }*/


    private void initVidoeCode (String uri){
        videoView.setKeepScreenOn(true);
        videoView.setVideoURI(Uri.parse(uri));
        videoView.setMediaController(mc);
        videoView.setOnCompletionListener(this);
        videoView.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }
}
