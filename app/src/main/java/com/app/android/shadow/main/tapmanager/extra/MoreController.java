package com.app.android.shadow.main.tapmanager.extra;

import android.app.Fragment;
import android.view.View;

import com.app.android.shadow.main.tapmanager.settings.MoreSignature;

/**
 * Created by rails-dev on 19/10/15.
 */
public interface MoreController {

    public void onButtonClickEvent(Fragment f,EventMaker maker);

    public void onModeChangeEvent(boolean isSafe);

    public void onButtonClickEvent(MoreSignature name, EventMaker maker);

    public void addToIncressClickArea(View v);

    public void goToModes();

}
