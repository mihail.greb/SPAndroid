package com.app.android.shadow.game.alarm;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.R;
import com.app.android.shadow.main.base.BaseFragment;


/**
 * Custom Board Fragment to show a chat history grouped by users
 */
public class StopWatchFragment extends BaseFragment {
    //Header
    TextView title;
    @Override
    protected void setHeader(View v) {
        //super.setHeader(v);
        title = (TextView) v.findViewById(R.id.title_head);
        title.setText(resources.getString(R.string.stop_watch));
    }

    Button butnstart, butnreset;
    TextView time;
    LinearLayout swipeArea;
    long starttime = 0L;
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedtime = 0L;
    int t = 1;
    int secs = 0;
    int mins = 0;
    int milliseconds = 0;
    Handler handler = new Handler();


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view =inflater.inflate(R.layout.shadow_alarm_stopwatch, container, false);
        butnstart = (Button) _view.findViewById(R.id.start);
        butnreset = (Button) _view.findViewById(R.id.reset);
        swipeArea = (LinearLayout) _view.findViewById(R.id.swipeArea);
        time = (TextView) _view.findViewById(R.id.timer);
       /* time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onButtonClickEvent((Fragment)null, EventMaker.ClickCount);
            }
        });*/


        swipeArea.setOnTouchListener(new View.OnTouchListener() {
            private float x1,x2;
            static final int MIN_DISTANCE = 120;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //return proferomEvent(v,event);
                switch(event.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                        x1 = event.getX();
                        break;
                    case MotionEvent.ACTION_UP:
                        x2 = event.getX();
                        float deltaX = x1 - x2;
                        Logger.printLog(Logger.ERROR,"Down and Up "+x1 +"  "+x2 +"  "+deltaX);
                        if(deltaX>0) {
                            if (Math.abs(deltaX) > MIN_DISTANCE) {
                                //Toast.makeText(getActivity(), "right2left swipe", Toast.LENGTH_SHORT).show();
                                mCallback.onButtonClickEvent((Fragment)null, EventMaker.ClickSwipe);
                            }
                        }else {
                            //mCallback.onButtonClickEvent((Fragment)null, EventMaker.ClickCount);
                        }
                        break;
                }
                return true;
            }
        });
        /*time.setOnTouchListener(new View.OnTouchListener() {
            private float x1,x2;
            static final int MIN_DISTANCE = 150;
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                        x1 = event.getX();
                        break;
                    case MotionEvent.ACTION_UP:
                        x2 = event.getX();
                        float deltaX = x1 - x2;
                        Logger.printLog(Logger.ERROR,"Down and Up "+x1 +"  "+x2 +"  "+deltaX);
                        if(deltaX>0) {
                            if (Math.abs(deltaX) > MIN_DISTANCE) {
                                //Toast.makeText(getActivity(), "right2left swipe", Toast.LENGTH_SHORT).show();
                                mCallback.onButtonClickEvent((Fragment)null, EventMaker.ClickSwipe);
                            }
                        }else {
                            mCallback.onButtonClickEvent((Fragment)null, EventMaker.ClickCount);
                        }
                        break;
                }
                return true;
            }
        });*/
        setHeader(_view);
        butnstart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
// TODO Auto-generated method stub

                if (t == 1) {
//timer will start
                    butnstart.setText(resources.getString(R.string.pause));
                    butnstart.setTextColor(Color.GREEN);
                    starttime = SystemClock.uptimeMillis();
                    handler.postDelayed(updateTimer, 0);
                    t = 0;
                } else {
//timer will pause
                    butnstart.setText(resources.getString(R.string.start));
                    butnstart.setTextColor(Color.BLACK);
                    time.setTextColor(Color.BLUE);
                    timeSwapBuff += timeInMilliseconds;
                    handler.removeCallbacks(updateTimer);
                    t = 1;
                }

            }
        });

        butnreset.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onResetClicketed();
            }
        });
        return _view;
    }

    /*public boolean proferomEvent(View v, MotionEvent event) {

    }*/


    private void onResetClicketed(){
        starttime = 0L;
        timeInMilliseconds = 0L;
        timeSwapBuff = 0L;
        updatedtime = 0L;
        t = 1;
        secs = 0;
        mins = 0;
        milliseconds = 0;
        butnstart.setText("Start");
        butnstart.setTextColor(Color.BLACK);
        handler.removeCallbacks(updateTimer);
        time.setText("00:00:00");
        time.setTextColor(Color.BLACK);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        onResetClicketed();
    }

    public Runnable updateTimer = new Runnable() {

        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - starttime;

            updatedtime = timeSwapBuff + timeInMilliseconds;
            secs = (int) (updatedtime / 1000);
            mins = secs / 60;
            secs = secs % 60;
            milliseconds = (int) (updatedtime % 1000);
            Logger.printLog(Logger.ERROR, "stop watch timer time " + mins + "  " + secs + "  " + milliseconds);
            time.setText("" + mins + ":" + String.format("%02d", secs) + ":"
                    + String.format("%03d", milliseconds));
            time.setTextColor(Color.RED);
            handler.postDelayed(this, 0);
        }

    };


}
