package com.app.android.shadow.main.tapmanager.settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.tapmanager.extra.MoreController;
import com.app.android.shadow.main.util.AlertDialogClass;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.R;
import com.app.android.shadow.main.util.Logger;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.fragments.SCAddFriendFragment;
import com.c2call.sdk.pub.gui.addfriend.controller.IAddFriendController;
import com.c2call.sdk.pub.gui.addfriend.controller.SCAddFriendController;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Custom Board Fragment to show a chat history grouped by users
 */
public class Request extends SCAddFriendFragment implements View.OnClickListener
{
    ListView reqList,pendingList;
    public MoreController mCallback;
    EditText addFriend;
    FriendsReqAdapter fReqAdapter = null;
    PendingReqAdapter pendingReqAdapter=null;
    ArrayList<HashMap<String ,String >> requestArrayList;
    ArrayList<HashMap<String ,String >> pendingArrayList;
    TaskRequestList mTask;
    View vi;
    int currentPos = -1;
    SCAddFriendController scAddFriendController;
    TextView request,pendingRequest;
    public BaseActivity mActivity;
    private boolean isPending =false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity		=	(BaseActivity) this.getActivity();
    }

    @Override
    public boolean onBackPressed() {
        //return super.onBackPressed();
        return false;
    }

    //Header
    TextView title;
    ImageView refresh,backBtn;

    private void setHeader(View v){
    title =(TextView)v.findViewById(R.id.title_head);
    title.setText(Const.REQUESTS);
        ImageView backBtn = (ImageView) v.findViewById(R.id.back);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        mCallback.addToIncressClickArea(backBtn);
    v.findViewById(R.id.refresh).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callApiReqList();
        }
    });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (MoreController) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement Interface");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view =inflater.inflate(R.layout.sc_addfriend, container, false);
        setHeader(_view);
        addFriend = (EditText)_view.findViewById(R.id.sc_addfriend_input);
        reqList = (ListView)_view.findViewById(R.id.list_menu);
        pendingList = (ListView)_view.findViewById(R.id.pendingList);

        request= (TextView) _view.findViewById(R.id.request);
        pendingRequest = (TextView) _view.findViewById(R.id.pending_request);
        request.setOnClickListener(this);
        pendingRequest.setOnClickListener(this);

        callApiReqList();

       /* reqList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                LinearLayout requestRow = (LinearLayout)view.findViewById(R.id.request_row);
                RelativeLayout rowOne = (RelativeLayout)requestRow.findViewById(R.id.row_one);
                TextView accept = (TextView)rowOne.findViewById(R.id.accept);
                TextView reject = (TextView)rowOne.findViewById(R.id.reject);

                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        currentPos = position;
                        callApiAccept(st.get(position).get(Const.USERNAME));

                    }
                });

                reject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        currentPos = position;
                        callApiReject(st.get(position).get(Const.USERNAME));
                    }
                });

            }
        });*/

        return _view;
    }

    private void callApiReqList() {
        mTask = new TaskRequestList(getActivity());
        mTask.setServiceTaskMapStrParams(APIServiceAsyncTask.requestList, null);
        mTask.execute((Void) null);
    }

    private void callApiAccept(String name) {
        mTask = new TaskRequestList(getActivity());
        mTask.setServiceTaskMapStrParams(APIServiceAsyncTask.acceptFrndReq, getParams(name));
        mTask.execute((Void) null);
    }

    private void callApiReject(String name) {
        mTask = new TaskRequestList(getActivity());
        mTask.setServiceTaskMapStrParams(APIServiceAsyncTask.declineFrndReq, getParams(name));
        mTask.execute((Void) null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.request:
                setBtnLayout(false);
                break;
            case R.id.pending_request:
                setBtnLayout(true);
                break;
        }
    }

    public void setBtnLayout(boolean isPending) {
        this.isPending = isPending;
        Resources resources = getActivity().getResources();
        makefilearray(isPending);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            request.setBackground((isPending)? BaseFragment.getnewDrawable(R.drawable.button_image_unselected,resources):BaseFragment.getnewDrawable(R.drawable.button_image_selected,resources));
            pendingRequest.setBackground((isPending) ? BaseFragment.getnewDrawable(R.drawable.button_video_selected,resources) : BaseFragment.getnewDrawable(R.drawable.button_video_unselected,resources));
        }else {
            request.setBackgroundDrawable((isPending)?BaseFragment.getnewDrawable(R.drawable.button_image_unselected,resources):BaseFragment.getnewDrawable(R.drawable.button_image_selected,resources));
            pendingRequest.setBackgroundDrawable((isPending)?BaseFragment.getnewDrawable(R.drawable.button_video_selected,resources):BaseFragment.getnewDrawable(R.drawable.button_video_unselected,resources));
        }
        request.setTextColor(resources.getColor(isPending?R.color.white:R.color.black));
        pendingRequest.setTextColor(resources.getColor(isPending?R.color.black:R.color.white));
    }

    private void makefilearray(boolean isPending){
        if(isPending) {
            reqList.setVisibility(View.GONE);
            pendingList.setVisibility(View.VISIBLE);
        }else {
            reqList.setVisibility(View.VISIBLE);
            pendingList.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isPending = false;
        setBtnLayout(isPending);
    }

    class TaskRequestList extends APIServiceAsyncTask {

        TaskRequestList(Context mContext) {
            super(mContext);
        }

        @Override
        protected void success(final JSONObject jsonObj, int serviceTaskType) {

            try {
                String suc = jsonObj.getString(Const.SUCCESS);
                if (suc.equals("true")) {
                    final String info = jsonObj.getString(Const.INFO);
                    if (serviceTaskType == REQUEST_LIST) {
                        JSONArray friendsJList = jsonObj.getJSONArray(Const.FRIENDS);
                        if(requestArrayList == null) {
                            requestArrayList = new ArrayList<>();
                        }else {
                            requestArrayList.clear();
                        }
                        HashMap<String ,String > map ;
                        for(int i=0;i<friendsJList.length();i++){
                            JSONObject fReqObj = friendsJList.getJSONObject(i);
                            map = new HashMap<String ,String >();
                            map.put(Const.ID,fReqObj.getString(Const.ID));
                            map.put(Const.USERNAME,fReqObj.getString(Const.USERNAME));
                            map.put(Const.PHONE,fReqObj.getString(Const.PHONE));
                            map.put(Const.FIRST_NAME,fReqObj.getString(Const.FIRST_NAME));
                            map.put(Const.LAST_NAME,fReqObj.getString(Const.LAST_NAME));
                            map.put(Const.EMAIL,fReqObj.getString(Const.EMAIL));
                            map.put(Const.SHADOW_ID,fReqObj.getString(Const.SHADOW_ID));
                            map.put(Const.GET_MESSAGE,fReqObj.getString(Const.GET_MESSAGE));
                            map.put(Const.GET_STATUS,fReqObj.getString(Const.GET_STATUS));

                            requestArrayList.add(map);
                        }
                        fReqAdapter = new FriendsReqAdapter(getActivity(), requestArrayList);
                        reqList.setAdapter(fReqAdapter);

                        JSONArray pendingJList = jsonObj.getJSONArray(Const.PENDING_FRIENDS);
                        if(pendingArrayList == null) {
                            pendingArrayList = new ArrayList<>();
                        }else {
                            pendingArrayList.clear();
                        }

                        for(int i=0;i<pendingJList.length();i++){
                            JSONObject fReqObj = pendingJList.getJSONObject(i);
                            map = new HashMap<String ,String >();
                            map.put(Const.ID,fReqObj.getString(Const.ID));
                            map.put(Const.USERNAME,fReqObj.getString(Const.USERNAME));
                            map.put(Const.PHONE,fReqObj.getString(Const.PHONE));
                            map.put(Const.FIRST_NAME,fReqObj.getString(Const.FIRST_NAME));
                            map.put(Const.LAST_NAME,fReqObj.getString(Const.LAST_NAME));
                            map.put(Const.EMAIL,fReqObj.getString(Const.EMAIL));
                            map.put(Const.SHADOW_ID,fReqObj.getString(Const.SHADOW_ID));
                            map.put(Const.GET_MESSAGE,fReqObj.getString(Const.GET_MESSAGE));
                            map.put(Const.GET_STATUS,fReqObj.getString(Const.GET_STATUS));

                            pendingArrayList.add(map);
                        }

                        pendingReqAdapter = new PendingReqAdapter(getActivity(), pendingArrayList);
                        pendingList.setAdapter(pendingReqAdapter);
                        makefilearray(isPending);
                    } else if(serviceTaskType == ACCEPT_FRIEND_REQUEST){
                                        Logger.printLog(Logger.ERROR,"success", "accepted");
                                        String e_mail = null;
                                        String c2call_friend_id = null;
                                        try {
                                            e_mail = jsonObj.getJSONObject(Const.USER).getString(Const.EMAIL);
                                            c2call_friend_id = jsonObj.getString("c2call_friend_id");
                                            Logger.printLog(Logger.ERROR,"email is ",e_mail);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        if(e_mail != null) {
                                            addFriend.setText(e_mail);
                                            scAddFriendController.onButtonSubmitClick(vi);
                                            requestArrayList.remove(currentPos);
                                            fReqAdapter.notifyDataSetChanged();
                                            currentPos = -1;
                                        }else {
                                            new AlertDialogClass(getActivity()).open(info);
                                        }

                        try {
                            // Test002 f43122a71523e908c78
                            // Test003 218e9f3d15258bca011
                            // Test004 330e3d8015264866d61
                            //         330e3d8015264866d61
                            // mayank1 660ca79c1503cfb6f05
                            //ShadowFriendDataFromTable c2cTable = CustomC2CallContentProvider.customeC2callDB.getSCFriendData(e_mail);
                            if(c2call_friend_id != null && c2call_friend_id.length()>0)
                            SCCoreFacade.instance().sendEvent(c2call_friend_id,"SCEVNT_SREF", (String)null);
                            //Toast.makeText(getActivity(), "Event send ", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            //Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                        }

                    }else if(serviceTaskType == DECLINE_FRIENDS_REQUEST){
                        //new AlertDialogClass(getActivity()).open(info);
                        requestArrayList.remove(currentPos);
                        fReqAdapter.notifyDataSetChanged();
                        currentPos = -1;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
            super.failure(jsonObj, serviceTaskType);
            try {
                final String info = jsonObj.getString(Const.INFO);
                new Thread(){
                    public void run(){
                        // Long time consuming operation
                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                new AlertDialogClass(getActivity()).open(info);
                            }
                        });
                    }
                }.start();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void failure(String message) {

        }
    }

    private Map<String, String> getParams(String name) {
        Map<String, String> params = new HashMap<>();
            params.put(Const.USERNAME, name);
        return params;
    }

     @Override
    protected IAddFriendController onCreateController(View v, SCViewDescription vd) {
         vi = v;
        return scAddFriendController = new SCAddFriendController(v, vd){

            @Override
            public void onButtonSubmitClick(View v) {
                super.onButtonSubmitClick(v);
            }
        };
    }

    public class FriendsReqAdapter extends ArrayAdapter {
        Context mcontext;
        ArrayList<HashMap<String ,String >> lst= new ArrayList<HashMap<String ,String >>();
        Resources resources;
        Fragment fragment;

        public FriendsReqAdapter(Context mcontext, List<HashMap<String ,String >> objects) {
            super(mcontext, R.layout.shadow_more_row, objects );
            this.mcontext =mcontext;
            this.lst= (ArrayList<HashMap<String ,String >>) objects;
            this.resources = mcontext.getResources();
        }

        @Override
        public int getCount() {
            return lst.size();
        }

        @Override
        public String getItem(int position) {
            return lst.get(position).get(Const.USERNAME);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final HashMap<String ,String > item = lst.get(position);
    /*TextView itemName;
            LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.shadow_request_row, parent, false);
            itemName = (TextView) convertView.findViewById(R.id.item_name);
            itemName.setText(item.get(Const.USERNAME));
    return convertView;*/
            ViewHolderItem viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.shadow_request_row, parent, false);

                viewHolder = new ViewHolderItem();
                viewHolder.title = (TextView)convertView.findViewById(R.id.item_name);
                viewHolder.accept = (TextView)convertView.findViewById(R.id.accept);
                viewHolder.reject = (TextView)convertView.findViewById(R.id.reject);
                viewHolder.msg = (TextView)convertView.findViewById(R.id.message);

                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ViewHolderItem) convertView.getTag();
            }
            viewHolder.title.setText(item.get(Const.USERNAME));
            viewHolder.msg.setText(item.get(Const.GET_MESSAGE));

            viewHolder.accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  /*  SimpleChatFriendsFragment.isNewFriendAdd = true;
                    SimpleChatFriendsFragment.newListSize ++;*/
                    currentPos = position;
                    callApiAccept(lst.get(position).get(Const.USERNAME));
                }
            });
            viewHolder.reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentPos = position;
                    callApiReject(lst.get(position).get(Const.USERNAME));
                }
            });
            return convertView;
        }

        private class ViewHolderItem{
            TextView title;
            TextView accept;
            TextView reject;
            TextView msg;
        }



    }
    public class PendingReqAdapter extends ArrayAdapter {
        Context mcontext;
        ArrayList<HashMap<String ,String >> lst= new ArrayList<HashMap<String ,String >>();
        Resources resources;

        public PendingReqAdapter(Context mcontext, List<HashMap<String ,String >> objects) {
            super(mcontext, R.layout.shadow_more_row, objects );
            this.mcontext =mcontext;
            this.lst= (ArrayList<HashMap<String ,String >>) objects;
            this.resources = mcontext.getResources();
        }

        @Override
        public int getCount() {
            return lst.size();
        }

        @Override
        public String getItem(int position) {
            return lst.get(position).get(Const.USERNAME);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final HashMap<String ,String > item = lst.get(position);
            ViewHolderItem viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.shadow_request_row, parent, false);

                viewHolder = new ViewHolderItem();
                viewHolder.title = (TextView)convertView.findViewById(R.id.item_name);
                viewHolder.accept = (TextView)convertView.findViewById(R.id.accept);
                viewHolder.accept.setVisibility(View.GONE);
                viewHolder.reject = (TextView)convertView.findViewById(R.id.reject);
                viewHolder.reject.setVisibility(View.VISIBLE);
                viewHolder.msg = (TextView)convertView.findViewById(R.id.message);
                viewHolder.msg.setVisibility(View.GONE);

                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ViewHolderItem) convertView.getTag();
            }
            viewHolder.title.setText(item.get(Const.USERNAME));
            viewHolder.reject.setText(item.get(Const.GET_STATUS));
            if(item.get(Const.GET_STATUS).equals("decline")){
                viewHolder.reject.setTextColor(Color.RED);
            }else {
                viewHolder.reject.setTextColor(Color.WHITE);
            }
            return convertView;
        }

        private class ViewHolderItem{
            TextView title;
            TextView accept;
            TextView reject;
            TextView msg;
        }



    }

}
