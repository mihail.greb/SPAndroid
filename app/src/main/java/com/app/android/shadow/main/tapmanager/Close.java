package com.app.android.shadow.main.tapmanager;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.R;


/**
 * Custom Board Fragment to show a chat history grouped by users
 */
public class Close extends BaseFragment
{

    //Header
    TextView title;
    @Override
    protected void setHeader(View v) {
        super.setHeader(v);
        title = (TextView) v.findViewById(R.id.title_head);
        title.setText(Const.CLOSE);
        v.findViewById(R.id.back).setVisibility(View.INVISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view =inflater.inflate(R.layout.shadow_close, container, false);
        _view.findViewById(R.id.closeApp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onButtonClickEvent((Fragment)null, EventMaker.CloseApp);
            }
        });
        setHeader(_view);
        return _view;
    }



}
