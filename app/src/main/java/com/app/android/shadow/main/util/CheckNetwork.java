package com.app.android.shadow.main.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class CheckNetwork {

	private static boolean isNetworkAvailable;

	/**
	 * check wheather network available or not either wi-fi of mobile network
	 * 
	 * @param mContext
	 * @return
	 */
	public static boolean isNetworkAvailable(Context mContext) {
		try {
			ConnectivityManager connectionManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo wifiInfo = connectionManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			NetworkInfo mobileInfo = connectionManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        /*WifiManager wifiManager = (WifiManager) ((Activity)mContext).getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo2 = wifiManager.getConnectionInfo();
        if (wifiInfo != null) {
            int linkSpeed = wifiInfo2.getLinkSpeed(); //measured using WifiInfo.LINK_SPEED_UNITS
            Logger.printLog(Logger.ERROR,"WIFI link speed is "+linkSpeed);
            Shadow.alertToast("WIFI link speed is "+linkSpeed);
        }*/

			if ((wifiInfo != null && wifiInfo.isConnected()) || (mobileInfo != null && mobileInfo.isConnected())) {
                isNetworkAvailable = true;
            } else {
                isNetworkAvailable = false;
                ShowDialog(mContext);
            }
			Logger.printLog(Logger.VERBOSE, "isNetworkAvailable: " + isNetworkAvailable);
		} catch (Exception e) {
			e.printStackTrace();
			isNetworkAvailable = false;
			ShowDialog(mContext);
		}
		return isNetworkAvailable;
	}

    public static void ShowDialog(final Context mContext){
        AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(mContext);
        // Setting Dialog Title
        mAlertDialog.setTitle("Networke is not available")
                .setMessage("No internet connectivity found")
                .setPositiveButton("Open WIFI", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
                        mContext.startActivity(intent);
                    }
                })
                        /*.setNeutralButton("Open DataConnection", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Intent.ACTION_MAIN);
                                intent.setClassName("com.android.phone", "com.android.phone.NetworkSetting");
                                context.startActivity(intent);
                            }
                        })*/
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
    }

}
