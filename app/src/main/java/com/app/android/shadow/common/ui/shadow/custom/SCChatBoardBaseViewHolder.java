package com.app.android.shadow.common.ui.shadow.custom;

import com.c2call.sdk.pub.gui.core.controller.SCBaseViewHolder;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;

/**
 * Created by rails-dev on 30/12/15.
 */
public class SCChatBoardBaseViewHolder extends SCBaseViewHolder implements IChatBoardBaseViewHolder {

    public SCChatBoardBaseViewHolder(SCViewDescription vd) {
        super(vd);
    }
}
