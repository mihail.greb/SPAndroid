package com.app.android.shadow.main.adapters;

import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.android.shadow.main.dbhelper.CustomeC2callDB;
import com.app.android.shadow.main.util.AddWaterMark;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.utils.custom.BitmapExtra;
import com.app.android.shadow.utils.custom.__ThumbnailUtils;
import com.app.android.shadow.R;
import com.c2call.lib.androidlog.Ln;
import com.c2call.lib.xml.StringExtra;
//import com.c2call.sdk.lib.backward.media.__ThumbnailUtils;
//import com.c2call.sdk.lib.util.extra.BitmapExtra;
//import com.c2call.sdk.lib.util.extra.DeviceExtra;
//import com.c2call.sdk.lib.util.extra.StringExtra;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    // Keep all Images in array
    ArrayList<HashMap<String,String >> list = new ArrayList<>();
    boolean isVideo;
    Fragment fragment;
    // Constructor
    public ImageAdapter(Context c, ArrayList<HashMap<String,String >> list,boolean isVideo,Fragment fragment){
        mContext = c;
        this.list = list;
        this.isVideo = isVideo;
        this.fragment = fragment;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final HashMap<String,String > item = list.get(position);
        ViewHolderItem viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.shadow_grid_row, parent, false);

            // well set up the ViewHolder
            viewHolder = new ViewHolderItem();
            viewHolder.imgItem = (ImageView)convertView.findViewById(R.id.img_item);
            if(!isVideo){
                viewHolder.imgItem.setImageResource(android.R.drawable.ic_menu_gallery);
            }
            viewHolder.videoBase = (ImageView)convertView.findViewById(R.id.video_base);
            viewHolder.video_time = (TextView)convertView.findViewById(R.id.video_time);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        String thumbUrl = item.get(CustomeC2callDB.LOC);
        if(thumbUrl.length()>0 && isThumFileExist(thumbUrl)) {
            if (isVideo) {
                try {
                    viewHolder.imgItem.setImageBitmap(retriveVideoFrameFromVideo(thumbUrl));
                } catch (Throwable throwable) {
                    viewHolder.imgItem.setImageBitmap(createVideoThumb(thumbUrl,(thumbUrl.substring(thumbUrl.lastIndexOf("-"),thumbUrl.lastIndexOf(".")))));
                    throwable.printStackTrace();
                }
                viewHolder.videoBase.setVisibility(View.VISIBLE);
                viewHolder.video_time.setVisibility(View.VISIBLE);
                viewHolder.video_time.setText(getVideoTime(item.get(CustomeC2callDB.LOC)));
            }else {
                viewHolder.imgItem.setImageURI(Uri.parse(item.get(CustomeC2callDB.THUMB_LOC)));
            }
        }else {
            Logger.printLog(Logger.ERROR,"go to else1 "+ item);
            new convertBitmapTask(item,viewHolder,isVideo).execute();
        }

        return convertView;
    }

    private class convertBitmapTask extends AsyncTask<Void,Void,Bitmap> {
        HashMap<String,String > item;
        String thumbUrl;
        ViewHolderItem viewHolder;
        boolean isVideo;

        public convertBitmapTask(HashMap<String, String> item, ViewHolderItem viewHolder,boolean isVideo) {
            this.item = item;
            this.viewHolder = viewHolder;
            this.isVideo = isVideo;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            thumbUrl = item.get(CustomeC2callDB.THUMB_LOC);
            Bitmap b = null;
          /*  if(thumbUrl.length()>0) {
                b = getBitmap(thumbUrl);
                if(b != null){return b;}
            }*/
            if (thumbUrl == null || thumbUrl.length() == 0 ) {  //|| b == null
                Logger.printLog(Logger.ERROR,"go to ","second ");
                String sdUrl = item.get(CustomeC2callDB.LOC);
                if (sdUrl.length() > 0) {
                    Logger.printLog(Logger.ERROR,"go to ","third" + sdUrl);
                    File f = getFile(sdUrl);
                    String name = getFileName(item.get(Const._ID));
                    if (f.exists()) {
                        Logger.printLog(Logger.ERROR,"go to ","four" );
                        if (!isVideo) {
                            return createImageThumb(sdUrl, (name.length() > 0) ? name : "image");
                        } else if (isVideo) {
                            return createVideoThumb(sdUrl, (name.length() > 0) ? name : "video");
                        }
                    }
                }
            }else {
                Logger.printLog(Logger.ERROR, "go to five");
            }
            return b;
        }
        @Override
        protected void onPostExecute(Bitmap b) {
            super.onPostExecute(b);
            if(b != null) {
                    viewHolder.imgItem.setImageBitmap(b);
                if (isVideo) {
                    viewHolder.videoBase.setVisibility(View.VISIBLE);
                    viewHolder.video_time.setVisibility(View.VISIBLE);
                    viewHolder.video_time.setText(getVideoTime(item.get(CustomeC2callDB.LOC)));
                }
                notifyDataSetChanged();
            }
        }
    }


    private boolean isThumFileExist(String path){
        try {
            File f = new  File(path);
            return f.exists();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private class ViewHolderItem{
        ImageView imgItem;
        ImageView videoBase;
        TextView video_time;
    }

    public String getVideoTime(String path){
        try {
          return AddWaterMark.getTimeDuration(mContext, path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getFileName(String path){
            String name =path.substring(0,(path.lastIndexOf("@")-1));
            return "thumb- "+name;
    }

    public File getFile(String path){
        return new  File(path);
    }

    private Bitmap createImageThumb(String origPath, String filename) {
        if(StringExtra.isNullOrEmpty(origPath)) {
            return null;
        } else {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(origPath, options);
            int imageHeight = options.outHeight;
            int imageWidth = options.outWidth;
            try {
                Bitmap e = BitmapExtra.createScaledBitmap(origPath, (imageWidth * 20 / 100), (imageHeight * 20 / 100));
                return e;
            } catch (Exception var6) {
                var6.printStackTrace();
                return null;
            }
        }
    }

    private Bitmap createVideoThumb(String origPath, String filename) {
        if(!StringExtra.isNullOrEmpty(origPath) && Build.VERSION.SDK_INT >= 8) {
            try {
                Ln.d("fc_error", "* * * Error: AwsUploadWorker.createVideoThumb() - %s / %s", new Object[]{origPath, filename});

                Bitmap e = __ThumbnailUtils.createVideoThumbnail(origPath, 1);
                Ln.d("fc_error", "* * * Error: AwsUploadWorker.createVideoThumb() - %s", new Object[]{e});
                    e = BitmapExtra.fastblur(e, 40);

                //String thumbPath = this.saveThumb(filename, e);
                //return thumbPath;
                return e;
            } catch (Exception var5) {
                var5.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    public static Bitmap retriveVideoFrameFromVideo(String p_videoPath)
            throws Throwable
    {
        Bitmap m_bitmap = null;
        MediaMetadataRetriever m_mediaMetadataRetriever = null;
        try
        {
            m_mediaMetadataRetriever = new MediaMetadataRetriever();
            m_mediaMetadataRetriever.setDataSource(p_videoPath);
            m_bitmap = m_mediaMetadataRetriever.getFrameAtTime();
        }
        catch (Exception m_e)
        {
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String p_videoPath)"
                            + m_e.getMessage());
        }
        finally
        {
            if (m_mediaMetadataRetriever != null)
            {
                m_mediaMetadataRetriever.release();
            }
        }
        return m_bitmap;
    }

    public static Bitmap createScaledBitmap(Uri bitmapUri, ContentResolver resolver, int width, int height) throws IOException {
        InputStream is = resolver.openInputStream(bitmapUri);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, (Rect)null, options);
        is.close();
        int ratio = Math.min(options.outWidth / width, options.outHeight / height);
        int sampleSize = Integer.highestOneBit((int)Math.floor((double)ratio));
        if(sampleSize == 0) {
            sampleSize = 1;
        }

        options = new BitmapFactory.Options();
        options.inSampleSize = sampleSize;
        is = resolver.openInputStream(bitmapUri);
        Bitmap b = BitmapFactory.decodeStream(is, (Rect)null, options);
        is.close();
        return b;
    }

}


  /*

   private String saveThumb(String filename, Bitmap thumb) throws FileNotFoundException, IOException {
        if(thumb == null) {
            Ln.d("fc_error", "* * * Error: AwsUploadWorker.saveThumb() - error creating thumbnail!", new Object[0]);
            return null;
        } else {
            String thumbPath = MediaUtil.getMediaPath(SCDownloadType.Thumb, filename, true);
            BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(thumbPath));
            thumb.compress(Bitmap.CompressFormat.PNG, 90, os);
            os.close();
            thumb.recycle();
            return thumbPath;
        }
    }

  public Bitmap getObjectBitmapImage(String objectPath,String type) {
        Bitmap bitmapImage;

            String[] pathComponants =  objectPath.toString().trim().split("\\.");
            String extension =  pathComponants[pathComponants.length-1].toLowerCase();
            if(this.getObjectFileType(f).equals("IMAGE")){
                bitmapImage = createImageThumbnail(objectPath);

            }else if(this.getObjectFileType(f).equals("VIDEO")){

                String time = AddWaterMark.getTimeDuration(mContext, f.getAbsolutePath());
                Logger.printLog(Logger.ERROR,"getDurationTime", time);
                Point p=new Point();
                p.set(40, 360);
                bitmapImage = AddWaterMark.waterMark(ThumbnailUtils.createVideoThumbnail(objectPath, MediaStore.Video.Thumbnails.MINI_KIND),
                        time, p, Color.WHITE, 200, 50, false);
               *//* bitmapImage = AaddTextWaterMark.waterMark(ThumbnailUtils.createVideoThumbnail(objectPath, MediaStore.Video.Thumbnails.MINI_KIND),
                        "Test text have to very long to check",p, Color.WHITE,200,50,false);*//*
            }else{
                bitmapImage = BitmapFactory.decodeResource(Shadow.getAppContext().getResources(), R.drawable.ic_sc_std_picture_image_src);
            }
        return bitmapImage;
    }

    public Bitmap createImageThumbnail(String path){
        BitmapFactory.Options options=new BitmapFactory.Options();
        options.inSampleSize = 6;
        return BitmapFactory.decodeFile(path,options);
    }

    public static String getObjectFileType(File f) {
        String objectPath = f.getAbsolutePath();
        String[] pathComponants =  objectPath.toString().trim().split("\\.");
        String extension =  pathComponants[pathComponants.length-1].toLowerCase();
        Logger.printLog(Logger.ERROR,"extension","is "+extension);
        if(extension.equals("jpg") || extension.equals("png")|| extension.equals("gif")){
            return GalleryFragment.IMAGE;
        }else if(extension.equals("mp4") || extension.equals("3gp") || extension.equals("wav")){
            return GalleryFragment.VIDEO;
        }else{
            return GalleryFragment.UNKNOW;
        }
    }*/
