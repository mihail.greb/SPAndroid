package com.app.android.shadow.samplechat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.app.android.shadow.common.ui.data.friendsList.FriendsController;
import com.app.android.shadow.main.adapters.model.ShadowFriendsModel;
import com.app.android.shadow.main.dbhelper.ShadowDB;
import com.app.android.shadow.main.tapmanager.extra.MoreController;
import com.app.android.shadow.main.util.AlertDialogClass;
import com.app.android.shadow.main.util.SdCardManager;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.adapters.ShadowFriendsListAdapter;
import com.app.android.shadow.main.provider.CustomC2CallContentProvider;
import com.app.android.shadow.R;
import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.common.ui.data.friendsList.FriendListItemController;
import com.app.android.shadow.common.ui.fragments.AddFriendFragment;
import com.app.android.shadow.common.ui.fragments.FriendsFragment;
import com.app.android.shadow.common.ui.fragments.ShadowContactDetailFragment;
import com.app.android.shadow.main.apicalls.ApiRequest;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.tapmanager.settings.Request;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.main.tapmanager.extra.ShadowFriendDataFromTable;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.main.util.Logger;
//import com.c2call.sdk.lib.n.b;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.db.adapter.SCFriendCursorAdapter;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemController;
import com.c2call.sdk.pub.gui.friendlistitem.controller.SCFriendListItemControllerFactory;
import com.c2call.sdk.pub.notifictaions.SCMessageNotification;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SimpleChatFriendsFragment extends FriendsFragment implements View.OnClickListener
{

    public BaseActivity mActivity;
    public ListView shadowListView;
    ArrayList<ShadowFriendsModel> friendsList;
    public ListView c2callListView;
    private static SimpleChatFriendsFragment __instance = new SimpleChatFriendsFragment();
    public static ArrayList<String> selectedUserList;
    private UserTask mTask;
    public MoreController mCallback;
    ShadowFriendsListAdapter adapter2;
    Resources resources;
    public static int delPosition = -1;
   /* Handler delayHandler = null;
    Runnable refreshListRunnable;
    public static boolean isNewFriendAdd;
    public static int newListSize=0;
    public static int oldListSize=0;
    public int callCount=0;
    int time = 7000;*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resources = getResources();
        mActivity		=	(BaseActivity) this.getActivity();
        selectedUserList = new ArrayList<String>();
      //  if(Shadow.isProApp()) {
            modeData();
      //  }
    }
    @Override
    public boolean onBackPressed() {
        //return super.onBackPressed();
          return false;
    }

    public static SimpleChatFriendsFragment instance() {
        return __instance;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (MoreController) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()+ " must implement Interface");
        }
    }

    private void modeData(){
        if(!Shadow.shadowDB.isModeUserExist()) {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put(Const.USER, ShadowDB.getUser());
            map.put(ShadowDB.IS_SAFE_MODE, "" + 1);
            map.put(ShadowDB.VIBRATE_FOR_CALL, "" + 1);
            map.put(ShadowDB.VIBRATE_FOR_CHAT, "" + 1);
            map.put(ShadowDB.SHAKE_LOGOUT, "" + 0);
            map.put(ShadowDB.LOGOUT_TIME, "" + 2);
            //map.put(ShadowDB.ONLINE_STATUS,""+-1);
            map.put(ShadowDB.IS_ENTER_NAME_PASS, "" + 0);
            if (Shadow.isAlarmGame()){
                map.put(ShadowDB.IS_SWIPE_X_TIMES, "" + 0);
                map.put(ShadowDB.IS_TAB_X_TIMES, "" + 1);
            }else {
                map.put(ShadowDB.IS_SWIPE_X_TIMES, "" + 1);
                map.put(ShadowDB.IS_TAB_X_TIMES, "" + 0);
            }
            map.put(ShadowDB.UN_COVER_MESSAGE,""+0);
            map.put(ShadowDB.UN_RINGTONE_FOR_CALL,""+1);
            map.put(ShadowDB.UN_VIBRATE_ON_CALL,""+0);
            map.put(ShadowDB.UN_SOUND_ON_CHAT,""+1);
            map.put(ShadowDB.UN_VIBRATE_ON_CHAT,""+0);
            map.put(ShadowDB.UN_IS_ENTER_NAME_PASS,""+0);
            map.put(ShadowDB.UN_IS_PASS_ON_X_TABS,""+1);
            map.put(ShadowDB.UN_IS_PASS_ON_X_SWIPE,""+1);
            map.put(ShadowDB.UN_SHAKE_LOGOUT,""+0);
            map.put(ShadowDB.UN_LOGOUT_TIME,""+15);
            map.put(ShadowDB.UN_IS_PASS_ON_X_TABS_FREE,""+0);
            map.put(ShadowDB.UN_IS_PASS_ON_X_SWIPE_FREE,""+0);
            //map.put(ShadowDB.UN_ONLINE_STATUS,""+-1);
            Shadow.shadowDB.initializeModeData(map);
            mCallback.onButtonClickEvent((Fragment)null, EventMaker.RefreshLockButton);
        }
    }

    /*Shadow Header*/
    ImageView shadow_edit,shadow_add,requests;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.sc_friends, (ViewGroup) null);
        shadowListView = (ListView)v.findViewById(R.id.shadow_user_list);
        shadowListView.setDividerHeight(0);
        c2callListView = (ListView)v.findViewById(R.id.sc_std_list);
        initHeader(v);
      /*  try {
            Bundle bundle = getArguments();
            String chat = null;
            if(bundle != null) {
                chat = bundle.getString("from", "");
                if (chat != null && chat.equals("chat")) {
                    Bitmap bitmap = takeScreenshot();
                    proformOnItemClick(bundle.getInt("position"), bitmap);
                }
         *//*   Logger.printLog(Logger.ERROR,"main page user id is only  position is "+delPosition);
            delPosition = bundle.getInt("delPosition",-1);
            Logger.printLog(Logger.ERROR,"main page user id is only  position is "+delPosition);*//*
            }
        }catch (Exception e){
            e.printStackTrace();
        }*/
        return v;
    }

    private void initHeader(View v){
        shadow_edit = (ImageView)v.findViewById(R.id.shadow_edit);
        requests = (ImageView)v.findViewById(R.id.requests);
        shadow_add = (ImageView)v.findViewById(R.id.shadow_add);
        requests.setOnClickListener(this);
        shadow_add.setOnClickListener(this);
        shadow_edit.setOnClickListener(this);
        mCallback.addToIncressClickArea(shadow_edit);
        mCallback.addToIncressClickArea(requests);
        mCallback.addToIncressClickArea(shadow_add);

        shadowListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                Bitmap bitmap = takeScreenshot();
                proformOnItemClick(position, bitmap);

            }
        });
        //setListVisible();
    }

    public void proformOnItemClick(final int position, Bitmap bitmap) {
        ShadowFriendDataFromTable data = null;
        try {
            data = friendsList.get(position).getFriendTableData();
            //int rightPosition = c2callListView.getPo
            Bitmap userImage =null;
            try {
                userImage = SdCardManager.getUserImgBitmap(friendsList.get(position).getFriendTableData().get_id());
            }catch (Exception e){
                userImage = null;
            }

            if (data != null) {
                Logger.printLog(Logger.ERROR, "not null");
                ShadowContactDetailFragment f = ShadowContactDetailFragment.create(data.get_id(),Integer.parseInt(data.getUsertype()), R.layout.sc_contact_detail,bitmap,userImage, friendsList.get(position).isAudio_call_missed(), friendsList.get(position).isVideo_call_missed(),position);
                if (f != null) {
                    mCallback.onButtonClickEvent(f, EventMaker.Friends);
                } else {
                    Logger.printLog(Logger.ERROR, "not open");
                }
            } else {
                Logger.printLog(Logger.ERROR, "not data");
                try {
                    data = CustomC2CallContentProvider.customeC2callDB.getSCFriendData(friendsList.get(position).getEmail());
                    if(data != null) {
                        friendsList.get(position).setFriendTableData(data);
                        proformOnItemClick(position, bitmap);
                    }else {
                        Logger.printLog(Logger.ERROR, "not data again");
                        new AlertDialogClass(getActivity()).open(resources.getString(R.string.connecting),
                                resources.getString(R.string.user_not_connected));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //getItemFactory();
    }

    private Bitmap takeScreenshot() {
        try {
            // image naming and path  to include sd card  appending name you choose for file
            //String mPath = SdCardManager.getSavePath() + "/screenShot.jpg";
            // create bitmap screen capture
            View v1 = getActivity().getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            return bitmap;

          /*  File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            openScreenshot(imageFile);*/
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
            return null;
        }
    }

    private void setListVisible(){
     /*   shadowListView.setVisibility(View.VISIBLE);
        c2callListView.setVisibility(View.INVISIBLE);*/

        shadowListView.setVisibility(View.GONE);
        c2callListView.setVisibility(View.VISIBLE);
    }

    public SCFriendListItemControllerFactory getItemFactory()
    {

        return new SCFriendListItemControllerFactory(null, null)
        {
            @Override
            public IFriendListItemController onCreateController(final View v, final SCViewDescription vd, final SCFriendData data)
            {
                final IFriendListItemController controller = new FriendListItemController(v, vd, data)
                {
                    @Override
                    public void onMainViewClick(View v)
                    {
                        Logger.printLog(Logger.ERROR,"Print Getdata ", data.getFirstname() + "  " + data.getEmail());
                        onShowContextDialog();
                        //onFriendClicked(this,v,data);

                    }

                    @Override
                    public void onMainViewLongClick(View v) {
                        //super.onMainViewLongClick(v);
                    }

                    /*String _pictureUrl = "";

                    @Override
                    public void reset() {
                        this._pictureUrl = null;
                    }

                    private void setPictureUrl(String url){
                        _pictureUrl = url;
                    }

                    @Override
                    protected void onPictureLoaded(Bitmap bitmap, final String url) {
                        Resources resources = Shadow.getAppContext().getResources();
                        bitmap = Bitmap.createScaledBitmap(bitmap, ((int)resources.getDimension(R.dimen.image_width)), ((int)resources.getDimension(R.dimen.image_height)), true);
                        if(StringExtra.isEqual(this._pictureUrl, url)) {
                            Ln.d("fc_tmp", " BasePictureMediator--- no need to u.pdate picture - ", new Object[]{url});
                        } else {
                            final Bitmap finalBitmap = bitmap;
                            this.getHandler().post(new Runnable() {
                                public void run() {
                                    if(getViewHolder() != null) {
                                        ImageView picture = ((IPictureViewHolder)getViewHolder()).getItemPicture();
                                        if(picture != null) {
                                            picture.setVisibility(View.VISIBLE);
                                            picture.setImageBitmap(finalBitmap);
                                            setPictureUrl(url);
                                        }

                                    }
                                }
                            });
                        }
                    }*/

                    @Override
                    protected void onPictureLoaded(Bitmap bitmap, String url) {
                        //super.onPictureLoaded(bitmap, url);
                        //adapter2.notifyDataSetChanged();
                    }

                };

                controller.setRequestListener(getRequestListener());
                return (controller);
            }
        };
    }

    /*
    check for tomorrow
    public void onDecoratePicture(IFriendListItemController m) {
                        if(m.getData() != null && ((IContactDetailViewHodler)m.getViewHolder()).getItemPicture() != null) {
                            final ImageView picture = ((IContactDetailViewHodler)m.getViewHolder()).getItemPicture();
                            String pictureUrl = ContactExtra.getPictureUrl(m.getData(), true);
                            Ln.d("fc_tmp", "ViewContactDetailmainSection.refreshPicture() - %s", new Object[]{pictureUrl});
                            if(pictureUrl != null && !m.getData().getManager().isLocal()) {
                                SCBitmapManager.instance().getRemoteBitmap(pictureUrl, m.getRemotePictureListener(), m.getData());
                            } else {
                                m.getHandler().post(new Runnable() {
                                    public void run() {
                                        if(picture instanceof QuickContactBadge) {
                                            ((QuickContactBadge)picture).setImageToDefault();
                                        } else {
                                            picture.setImageResource(com.c2call.sdk.R.drawable.ic_sc_std_picture_user_src);
                                        }

                                    }
                                });
                            }
                        }
                    }

     */

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.shadow_add:
                mCallback.onButtonClickEvent(new AddFriendFragment(),EventMaker.Friends);
                break;
            case R.id.shadow_edit:
                setDeleteBtnVisiblity();
                break;
            case R.id.requests:
                goRequest();
                break;
        }
    }

    private void goRequest(){
       // oldListSize = friendsList.size();
        mCallback.onButtonClickEvent(new Request(), EventMaker.Friends);
    }

    public void setRequestIcon(boolean b){
            requests.setImageResource(b ? R.drawable.icon_red : R.drawable.icon);
    }

    public void showRequestAlert(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_TRADITIONAL);
        alertDialogBuilder.setTitle(resources.getString(R.string.you_have_request));
        //alertDialogBuilder.setMessage(resources.getString(R.string.you_have_request));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(resources.getString(R.string.see_request), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();
                goRequest();
            }
        });
        alertDialogBuilder.setNegativeButton(resources.getString(R.string.close_window), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callApi(APIServiceAsyncTask.callAwaitRequest);
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void setDeleteBtnVisiblity(){
        int length = friendsList.size();
        if(length>0) {
            boolean b = friendsList.get(0).isShowDelete();
            BaseSampleActivity.isFriendEditorOpen = !b;
            for (int i = 0; i < length;i++){
                friendsList.get(i).setIsShowDelete(!b);
            }
            adapter2.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        callFriendList();
        callApi(APIServiceAsyncTask.blockUserlIST);
        BaseFragment.closeSoftKeyboard(getActivity());
    }



    private void callFriendList(){
        selectedUserList.clear();
        BaseSampleActivity.isFriendEditorOpen = false;
        callApi(APIServiceAsyncTask.getFrndList);
    }

            /*
                th = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(time);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    timerComplete();
                                  */ /* resetDelayHandler();
                                    delayHandler = new Handler();
                                    refreshListRunnable = new Runnable() {
                                        @Override
                                        public void run() {
                                            timerComplete();
                                        }
                                    };
                                    delayHandler.postDelayed(refreshListRunnable, time);*/
                    /* }
                    });
                            }
                            });
                            th.start();

                         */

   /* public void setRefresh(){
        if(isNewFriendAdd) {
            refreshListAfterDaley(true);
            isNewFriendAdd = false;
        }
    }

    private void refreshListAfterDaley(boolean b) {
        //if(b) {
            resetDelayHandler();
            delayHandler = new Handler();
            refreshListRunnable = new Runnable() {
                @Override
                public void run() {
                    timerComplete();
                }
            };
            delayHandler.postDelayed(refreshListRunnable, time);
      *//*  }else {
            delayHandler.removeCallbacks(refreshListRunnable);
            delayHandler = null;
        }*//*
    }

    private void resetDelayHandler(){
        if(delayHandler != null){
            delayHandler.removeCallbacks(refreshListRunnable);
            delayHandler = null;
        }
    }

    public void timerComplete() {
        Shadow.alertToast("working now");
        callApi(APIServiceAsyncTask.getFrndList);
        if(friendsList.size() == oldListSize && callCount < 4){
            callCount ++;
            time = 8000;
            refreshListAfterDaley(true);
        }else {
            resetDelayHandler();
        }
    }*/

    @Override
    public void onStop() {
        //isNewFriendAdd = false;
        //resetDelayHandler();
        super.onStop();
    }

    public static SimpleChatFriendsFragment create(final int layout)
    {
        final Bundle args = new Bundle();
        args.putInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);

        final SimpleChatFriendsFragment f = new SimpleChatFriendsFragment();
        f.setArguments(args);
        return f;
    }
    //String _pictureUrl = "";

    protected void onFriendPictureLoaded(final IFriendListItemController controller,Bitmap bitmap, final String url, final SCFriendData data){
        Resources resources = Shadow.getAppContext().getResources();
        bitmap = Bitmap.createScaledBitmap(bitmap, ((int)resources.getDimension(R.dimen.image_width)), ((int)resources.getDimension(R.dimen.image_height)), true);
        if(bitmap != null){
            Logger.printLog(Logger.ERROR,"bitmap not null");
        }else {
            Logger.printLog(Logger.ERROR,"bitmap null");
        }
        if(url != null && url.length()>0){
            Logger.printLog(Logger.ERROR,"url not null" + url);
        }else {
            Logger.printLog(Logger.ERROR,"bitmap not null");
        }
       /* if(StringExtra.isEqual(this._pictureUrl, url)) {
            Ln.d("fc_tmp", " BasePictureMediator--- no need to update picture - ", new Object[]{url});
        } else {*/
         /*   final Bitmap finalBitmap = bitmap;
            controller.getHandler().post(new Runnable() {
                public void run() {
                    if(controller.getViewHolder() != null) {
                        ImageView picture = ((IPictureViewHolder)controller.getViewHolder()).getItemPicture();
                        if(picture != null) {
                            picture.setVisibility(View.VISIBLE);
                            picture.setImageBitmap(finalBitmap);
                            //this._pictureUrl = url;

                        }

                    }
                }
            });*/
        //}
    }

    @Override
    protected void onFriendLongClicked(IFriendListItemController controller, View v, SCFriendData data) {
        //super.onFriendLongClicked(controller, v, data);
        //C2CallSdk.startControl().openContactDetail(getContext(), null, R.layout.sc_contact_detail, friend, StartType.Activity);
    }

    @Override
    protected void onFriendClicked(final IFriendListItemController controller,final View v, final SCFriendData data)
    {
      /*  Logger.printLog(Logger.ERROR,"check","onFriendClicked");
        Fragment fragment = ShadowContactDetailFragment.create(data.getId(), data.getUserType(), R.layout.sc_contact_detail);
        Logger.printLog(Logger.ERROR,"fragment name",""+fragment.getClass().getSimpleName());
        mCallback.onButtonClickEvent(fragment, EventMaker.Friends);*/
        /*

        public static class DeleteModusRunnable extends BaseItemRunnable<IFriendListItemController> {
            public DeleteModusRunnable(IFriendListItemController controller) {
                super(controller);
            }

            public void run() {
                ((IFriendListItemController)this.getController()).fireEvent(new SCListModusChangedEvent(SCListModus.Delete));
            }
        }*/

        /*
        SCChoiceDialog dlg = new SCChoiceDialog.Builder(getActivity())
                .addItem(R.string.app_dlg_call_audio, 0, 0, new Runnable()
                {
                    @Override
                    public void run()
                    {
                        call(data, false);
                    }
                })
                .addItem(R.string.app_dlg_call_video, 0, 0, new Runnable()
                {
                    @Override
                    public void run()
                    {
                        call(data, true);
                    }
                })
                .addItem(R.string.chat, 0, 0, new Runnable() {
                    @Override
                    public void run() {
                        SCNewMessageCache.instance().getNewMessages(data.getId()).clear();

                        SCCoreFacade.instance().postEvent(new RequestChatHistoryUpdateEvent(), false);
                        openChat(data);
                    }
                })
                .build();
        dlg.show();
            */
    }

   /* private void openChat(SCFriendData data)
    {
        final Intent intent = new Intent(getActivity(), BoardActivity.class);
        intent.putExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, R.layout.sc_board);
        intent.putExtra(SCExtraData.Board.EXTRA_DATA_USERID, data.getId());
        startActivity(intent);
    }

    private void call(final SCFriendData data, final boolean isVideo)
    {
        new SimpleAsyncTask<Boolean>(getActivity(), 0)
        {
            @Override
            protected Boolean doInBackground(Void... voids)
            {
                boolean isGroup = data.getManager().isGroup();

                return SCCoreFacade.instance().call(data.getId(), isVideo, isGroup);
            }

            @Override
            protected void onSuccess(Boolean result)
            {
                C2CallSdk.startControl().openCallbar(getActivity(), null, data.getId(), R.layout.sc_callbar, StartType.Activity);
            }
        }.execute();
    }*/

    private void callApi(ApiRequest serviceTask){
        mTask = new UserTask(getActivity());
        mTask.setServiceTaskMapStrParams(serviceTask, getParams(serviceTask.reqID));
        mTask.execute((Void) null);
    }

    private void callApi(String userName,String userId,int position,ApiRequest serviceTask){
        delPosition = -1;
        mTask = new UserTask(getActivity(),userId,position);
        mTask.setServiceTaskMapStrParams(serviceTask, getParams(userName, serviceTask.reqID));
        mTask.execute((Void) null);
    }


    public void callDeleteUser(final String userName ,final String userId, final int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.app_dlg_confirm_title)
                .setMessage(R.string.app_dlg_confirm_deletion_text)
                .setPositiveButton(R.string.sc_std_btn_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        callApi(userName, userId,position, APIServiceAsyncTask.removeFrndtoFrndList);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        dialog.cancel();
                    }
                });
        builder.show();
    }

    class UserTask extends APIServiceAsyncTask {
        String userId;
        int position;
        UserTask(Context mContext){
            super(mContext);
        }

        UserTask(Context mContext,String userId,int position){
            super(mContext);
            this.userId = userId;
            this.position = position;
        }

        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            if(serviceTaskType == APIServiceAsyncTask.BLOCK_USER_LIST) {
                addListToDatabase(jsonObj);
            }else if(serviceTaskType == APIServiceAsyncTask.GET_FRIENDS_LIST){
                try {
                    setFriendList(jsonObj);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if(delPosition != -1){
                        //callDeleteC2call();
                        Logger.printLog(Logger.ERROR,"main page user id is "+friendsList.get(delPosition).getFriendTableData().getDipslay_name()+"   id is  "+friendsList.get(delPosition).getFriendTableData().get_id() +"   position is "+delPosition+" size "+friendsList.size());
                        callApi(friendsList.get(delPosition).getFriendTableData().getDipslay_name(),
                                friendsList.get(delPosition).getFriendTableData().get_id(),
                                delPosition, APIServiceAsyncTask.removeFrndtoFrndList);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Shadow.alertToast(resources.getString(R.string.sorry_error_to_delete_user));
                }

            }else if(serviceTaskType == APIServiceAsyncTask.REMOVE_FRND_TO_FRND_LIST){
                try {
                    SCCoreFacade.instance().sendEvent(userId,"SCEVNT_SREF", (String)null);
                    Toast.makeText(getActivity(), "Event send ", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                }
                callDeleteC2call(userId);
                friendsList.remove(position);
                adapter2.notifyDataSetChanged();
                userId = "";
            }else if(serviceTaskType == APIServiceAsyncTask.AWAIT_REQUEST){

            }
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
            if(serviceTaskType == APIServiceAsyncTask.BLOCK_USER_LIST ) {
                Shadow.alertToast("Failed");
            }else if(serviceTaskType == APIServiceAsyncTask.GET_FRIENDS_LIST){
                Shadow.alertToast("Failed to fetch friends list");
            }else if(serviceTaskType == APIServiceAsyncTask.AWAIT_REQUEST){

            }
        }
    }

    private Map<String, String> getParams(int serviceTaskType){
        Map<String, String> params = new HashMap<>();
        if(serviceTaskType == APIServiceAsyncTask.BLOCK_USER_LIST) {

        }
        return params.size()>0?params:null;
    }
    private Map<String, String> getParams(String userName,int serviceTaskType){
        Map<String, String> params = new HashMap<>();
        if(serviceTaskType == APIServiceAsyncTask.REMOVE_FRND_TO_FRND_LIST) {
            params.put("username", userName);
        }
        return params.size()>0?params:null;
    }

    private void callDeleteC2call(String userId){

        Set<String> idSet = new HashSet<String>(Arrays.asList(userId));
        final Set friends = idSet; //SCSelectionManager.instance().getSelection((String)this.getSelectionKey().first);
        final Set contacts = null;//SCSelectionManager.instance().getSelection((String)this.getSelectionKey().second);
        /*
        OLD SDK
        (new SimpleAsyncTask(getActivity(), 0L, "") {
            @Override
            protected Boolean doInBackground(Object[] params) {
                int deleted = (new BatchDeleteHandler()).delete(friends, contacts);
                Ln.d("fc_tmp", "SCFriendsController.onDeleteFriends() - deleted items: %d", new Object[]{Integer.valueOf(deleted)});
                return Boolean.valueOf(true);
            }

            protected void onFinished() {
                //SshadowContactDetailFragment.this.onControllerEvent(new SCListModusChangedEvent(SCListModus.Normal));

            }
        }).execute(new Void[0]);*/

        /*

        (new SimpleAsyncTask(this.getActivity(), 0L, "") {
            @Override
            protected Object doInBackground(Object[] params) {
                int var2x = (new b()).a(friends, contacts);
                Ln.d("fc_tmp", "SCFriendsController.onDeleteFriends() - deleted items: %d", new Object[]{Integer.valueOf(var2x)});
                return Boolean.valueOf(true);
            }

            protected void onFinished() {
                //SCFriendsController.this.onControllerEvent(new SCListModusChangedEvent(SCListModus.Normal));
                getActivity().onBackPressed();
            }
        }).execute(new Void[0]);*/
        // TODO CHECK
        Logger.printLog(Logger.ERROR, "controller is " + getController() + "  friend  " + friends + " contact  " + contacts);
        ((FriendsController)getController()).deleteSelectedFriends(friends, contacts);
        //((IFriendListItemController)this.getController()).fireEvent(new SCListModusChangedEvent(SCListModus.Delete));
    }

    private void addListToDatabase(JSONObject jsonObj){
        if(jsonObj != null){
            try {
                JSONObject dataObj = jsonObj.getJSONObject("data");
                JSONArray blockUserArray = dataObj.getJSONArray("block_users");
                JSONObject arrayObj;
                if(blockUserArray.length()>0) {
                    for (int i = 0; i < blockUserArray.length(); i++) {
                        arrayObj = blockUserArray.getJSONObject(i);
                        Shadow.shadowDB.addBlockUser(null, arrayObj.getString("block_user_name"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setFriendList(JSONObject jsonObj){
        if(jsonObj != null){
            try {
                boolean awaiting = false;
                boolean cancled = false;
                boolean send_to_add_screen = false;
                awaiting = jsonObj.getBoolean("awaiting");
                cancled = jsonObj.getBoolean("cancled");
                send_to_add_screen = jsonObj.getBoolean("send_to_add_screen");
                if(awaiting && !cancled){
                    showRequestAlert();
                }
                setRequestIcon((awaiting || cancled)?true:false);

                JSONArray dataArray = jsonObj.getJSONArray(Const.FRIENDS);
                friendsList = new ArrayList<ShadowFriendsModel>();
                JSONObject dataObj;
                ShadowFriendsModel model;
                    for(int i=0;i<dataArray.length();i++){
                    dataObj = dataArray.getJSONObject(i);
                    SCFriendCursorAdapter adapter = (SCFriendCursorAdapter) c2callListView.getAdapter();

                    String userEmail = dataObj.getString(Const.EMAIL);
                    //String userName = dataObj.getString(Const.USERNAME);
                    ShadowFriendDataFromTable data = null;
                    SCFriendData data2 = null;
                    try {
                        data = CustomC2CallContentProvider.customeC2callDB.getSCFriendData(userEmail);
                        if(data != null) {
                            data2 = SCFriendData.dao().queryForId(data.get_id());
                            Logger.printLog(Logger.ERROR,"Data2 value is "+data2.toString());
                        }
                        //data = CustomC2CallContentProvider.customeC2callDB.getSCFriendData(userEmail);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    boolean isuserBlocked = false;
                    if(data != null)
                    isuserBlocked = Shadow.shadowDB.isUserBlocked(data.get_id());

                    Logger.printLog(Logger.ERROR,"user id and isUserBlocked is "+isuserBlocked+"  "+userEmail);
                        model = new ShadowFriendsModel(
                                dataObj.getString(Const.ID),
                                dataObj.getString(Const.USERNAME),
                                dataObj.getString(Const.PHONE),
                                dataObj.getString(Const.FIRST_NAME),
                                dataObj.getString(Const.LAST_NAME),
                                userEmail,
                                dataObj.getString(Const.SHADOW_ID),
                                dataObj.getString(Const.DISPLAY_EMAIL),
                                (!isuserBlocked?dataObj.getBoolean(Const.AUDIO_CALL_MISSED):false),
                                (!isuserBlocked?dataObj.getBoolean(Const.VIDEO_CALL_MISSED):false),
                                data2,
                                data);

                        String chatText = "";
                        try {
                            chatText = CustomC2CallContentProvider.customeC2callDB.getmissedChat(data.get_id());
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        if (chatText.length() > 0) {
                            model.setChat_missed(true);
                            model.setLastChat(chatText);
                        } else {
                            model.setChat_missed(false);
                        }
                        model.setIsShowDelete(false);
                        friendsList.add(model);
                  //  }
                }

                try {
                  /*  Collections.sort(friendsList, new Comparator<ShadowFriendsModel>() {
                        @Override
                        public int compare(ShadowFriendsModel a1, ShadowFriendsModel a2) {
                            // String implements Comparable
                            return (a1.getUsername().toString()).compareTo(a2.getUsername().toString());
                        }
                    });*/

                    adapter2 = new ShadowFriendsListAdapter(getActivity(),friendsList,c2callListView,this);
                    shadowListView.setAdapter(adapter2);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                        if((friendsList.size()==0) && send_to_add_screen){
                        checkAndGoAddFriends();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void checkAndGoAddFriends() {
        mCallback.onButtonClickEvent(new AddFriendFragment(),EventMaker.Friends);
    }


    public void notifyListAdapterOnChat(final SCMessageNotification n){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(int i=0;i<friendsList.size();i++){
                    if(n.getUid() !=null && n.getUid().length()>0 && friendsList.get(i).getFriendTableData() != null && friendsList.get(i).getFriendTableData().get_id() != null) {
                        Logger.printLog(Logger.ERROR, "n id is " + n.getUid() );
                        Logger.printLog(Logger.ERROR, "  listid is " + friendsList.size());

                        if (n.getUid().equals(friendsList.get(i).getFriendTableData().get_id())) {
                            friendsList.get(i).setChat_missed(true);
                            friendsList.get(i).setLastChat(n.getRawMessage());
                            adapter2.notifyDataSetChanged();
                        }
                    } else {
                       // Logger.printLog(Logger.ERROR,"Message not showing");
                    }
                }
            }
        });
    }

    public void refreshList(){
        callFriendList();
    }

}


