package com.app.android.shadow.samplechat.board;

import android.view.View;

import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.gui.core.controller.IControllerRequestListener;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.newmessage.controller.SCNewMessageController;

/**
 * Created by rails-dev on 17/10/15.
 */
public class ShadowNewMessageController extends SCNewMessageController {

    public ShadowNewMessageController(View view, SCViewDescription viewDescription) {
        super(view, viewDescription);
    }

    public ShadowNewMessageController(View view, SCViewDescription viewDescription, IControllerRequestListener requestListener, SCFriendData contact) {
        super(view, viewDescription, requestListener, contact);
    }

    public ShadowNewMessageController(View view, SCViewDescription viewDescription, IControllerRequestListener requestListener, SCFriendData contact, boolean showIdleDialogWhileSending) {
        super(view, viewDescription, requestListener, contact, showIdleDialogWhileSending);
    }



}
