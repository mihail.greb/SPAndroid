package com.app.android.shadow.samplechat.chathistory;

import android.view.View;

import com.app.android.shadow.utils.Str;
import com.app.android.shadow.R;
import com.c2call.sdk.pub.common.SCRichMessageType;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCNewMessageCache;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemTextController;
import com.c2call.sdk.pub.gui.boardlistitem.decorator.SCBoardListItemTextDecorator;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * The decorator that is used to bind values to the chat history item views.
 * Since each item represents the full chat with a certain chat partner we want to show the
 * chat partner's name, the text of the last message, the timstamp  and a new message badge here.
 */
public class ChatHistoryItemDecorator extends SCBoardListItemTextDecorator
{

    private static final SimpleDateFormat __dateFormat = new SimpleDateFormat(C2CallSdk.context().getString(R.string.sc_date_pattern_short_time));


    @Override
    public void decorate(final IBoardListItemTextController controller)
    {
        super.decorate(controller);

        onDecorateTime(controller);

        onDecorateUnreadMessages((ChatHistoryItemController) controller);
    }


    /**
     * ChatHistory items have an additional view element to show the number of unread messages for the given chat partner.
     * This is decorated here.
     * @param controller
     */
    private void onDecorateUnreadMessages(ChatHistoryItemController controller)
    {
        SCBoardEventData data = controller.getData();

        /*
        Get the new messages for this chat partner from the internal cache
         */
        Set<String> newMessages = SCNewMessageCache.instance().getNewMessages(data.getUserid());

        /*
        If we have some new messages then show the corresponding view, otherwise hide it
         */
        boolean show = !newMessages.isEmpty();
        setVisibility(controller.getTextUnreadMessages(), show);

        setText(controller.getTextUnreadMessages(), "" + newMessages.size());
    }


    @Override
    protected String getText(IBoardListItemTextController m)
    {
        /*
        Since this decorator is derived from SCBoardListItemTextDecorator we can only show text content here (no images, videos, etc.)
        For each item in this chat history we want to show the last message of the corresponding chat. In case that this was
        a media message, we will display a describing dummy text.
         */
        String text =  m.getData().getManager().getDisplayText();
        SCRichMessageType type = m.getData().getManager().getRichMessageType();


        if (type == null){
            return text;
        }


        switch(type){
            case Audio: return m.getContext().getString(R.string.app_chathistory_audio_message);
            case Video: return m.getContext().getString(R.string.app_chathistory_video_message);
            case Image: return m.getContext().getString(R.string.app_chathistory_image_message);
            case Location: return m.getContext().getString(R.string.app_chathistory_location_message);
            case Friend: return m.getContext().getString(R.string.app_chathistory_friend_message);
            case File: return m.getContext().getString(R.string.app_chathistory_file_message);
            default:
                return m.getContext().getString(R.string.app_chathistory_media_message);

        }
    }


    /**
     * Override this to set a custom text color for the items
     * @param m     the controller of the corresponding list item
     * @return      the text color that will be used for this item
     */
    @Override
    public int getTextColor(IBoardListItemTextController m)
    {
        return m.getContext().getResources().getColor(R.color.app_std_text_light_grey);
    }

    /**
     * Called to decorate the header and footer of the item.
     * In this case the header will used to show the message author's name,
     * and the footer will not be used and stay invisible (default)
     * @param m
     */
    @Override
    public void onDecorateHeaderAndFooter(final IBoardListItemTextController m)
    {
        final String author = Str.toEclipseString(getAuthor(m), 35);

        final String header = author;

        m.getViewHolder().getItemHeaderView().setText(header);
        m.getViewHolder().getItemHeaderView().setCompoundDrawables(null, null, null, null);

       setVisibility(m.getViewHolder().getItemHeaderView(), _showHeader);
    }


    private String getAuthor(final IBoardListItemTextController m)
    {
        if (m.getData() == null){
            return "";
        }

        String uid = m.getData().getManager().getFriendId();
        return C2CallSdk.contactResolver().getDisplayNameByUserid(uid, true);
    }


    /**
     * ChatHistory items have an additional view element to show the message's timestamp.
     * This is decorated here.
     * @param controller
     */
    protected void onDecorateTime(final IBoardListItemTextController controller)
    {
        final View textTime = controller.getViewHolder().getItemTime();
        setText(textTime, getTimeString(controller));
    }


    protected String getTimeString(final IBoardListItemTextController controller)
    {
        final long timestamp = controller.getData().getTimestamp();


        return __dateFormat.format(new Date(timestamp));
    }
}
