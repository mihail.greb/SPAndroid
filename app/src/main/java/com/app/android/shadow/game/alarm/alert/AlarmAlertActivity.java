package com.app.android.shadow.game.alarm.alert;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.app.android.shadow.game.alarm.Alarm;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.apicalls.ApiRequest;
import com.app.android.shadow.main.dbhelper.ShadowDB;
import com.app.android.shadow.main.FreeAppWayToAccess;
import com.app.android.shadow.R;
import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.main.tapmanager.settings.Modes;
import com.app.android.shadow.main.util.Logger;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class AlarmAlertActivity extends Activity implements OnClickListener {

	private Alarm alarm;
	private MediaPlayer mediaPlayer;

	private Vibrator vibrator;
	private boolean alarmActive;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final Window window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
				| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
				| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

		setContentView(R.layout.shadow_alarm_alert);

		Bundle bundle = this.getIntent().getExtras();
		alarm = (Alarm) bundle.getSerializable("alarm");

		this.setTitle(alarm.getAlarmName());


		((Button) findViewById(R.id.stopButton)).setOnClickListener(this);

		TelephonyManager telephonyManager = (TelephonyManager) this
				.getSystemService(Context.TELEPHONY_SERVICE);

		PhoneStateListener phoneStateListener = new PhoneStateListener() {
			@Override
			public void onCallStateChanged(int state, String incomingNumber) {
				switch (state) {
				case TelephonyManager.CALL_STATE_RINGING:
					Log.d(getClass().getSimpleName(), "Incoming call: "
							+ incomingNumber);
					try {
						mediaPlayer.pause();
					} catch (IllegalStateException e) {

					}
					break;
				case TelephonyManager.CALL_STATE_IDLE:
					Log.d(getClass().getSimpleName(), "Call State Idle");
					try {
						mediaPlayer.start();
					} catch (IllegalStateException e) {

					}
					break;
				}
				super.onCallStateChanged(state, incomingNumber);
			}
		};

		telephonyManager.listen(phoneStateListener,
				PhoneStateListener.LISTEN_CALL_STATE);

		// Toast.makeText(this, answerString, Toast.LENGTH_LONG).show();

		startAlarm();
		invalidateOptionsMenu();
		Shadow.setCurrentActivity(AlarmAlertActivity.this);
		initAutoLogoutTimer();
		//initHeader();

	}


	private void startAlarm() {
		if (alarm.getAlarmTonePath() != "") {
			mediaPlayer = new MediaPlayer();
			if (alarm.getVibrate()) {
				vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
				long[] pattern = { 1000, 200, 200, 200 };
				vibrator.vibrate(pattern, 0);
			}
			try {
				mediaPlayer.setVolume(1.0f, 1.0f);
				mediaPlayer.setDataSource(this,
						Uri.parse(alarm.getAlarmTonePath()));
				mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
				mediaPlayer.setLooping(true);
				mediaPlayer.prepare();
				mediaPlayer.start();

			} catch (Exception e) {
				mediaPlayer.release();
				alarmActive = false;
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		if (!alarmActive)
			super.onBackPressed();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		super.onPause();
		StaticWakeLock.lockOff(this);
	}


	private void onActivityClose(){
		stopAlarm();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.stopButton:
				isAppNotClose = true;
				stopAlarm();
				finish();
				break;
		}
	}

	public void stopAlarm(){
		alarmActive = false;
		if (vibrator != null)
			vibrator.cancel();
		try {
			mediaPlayer.stop();
		} catch (IllegalStateException ise) {

		}
		try {
			mediaPlayer.release();
		} catch (Exception e) {

		}
	}

	/*Shadow setup for timer*/

	public static boolean isAppNotClose = false;
	boolean isSafeMode;
	Timer timer = null;
	long lastTime = 0;
	int time;
	TimerTask timerTask;
	//we are going to use a handler to be able to run in our TimerTask
	final Handler handler = new Handler();


	void initAutoLogoutTimer(){
		isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
		time = (Modes.getLogoutTime((isSafeMode) ? ShadowDB.LOGOUT_TIME : ShadowDB.UN_LOGOUT_TIME));
		if(time == (-1)){
			time = 2;
		}
		resetAutoLogoutTimer();
	}

	void resetLastTouchTime(){
		lastTime = System.currentTimeMillis();
	}

	boolean isAutoLogout(){
		boolean isNeedToLogout = (lastTime+1000) < System.currentTimeMillis();
		return isNeedToLogout;
	}

	void resetAutoLogoutTimer(){
		if(isAutoLogout()) {
			resetLastTouchTime();
			if (timer == null) {
				resetTimer();
			} else {
				try {
					timer.cancel();
					timer = null;
					resetTimer();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void initializeTimerTask() {
		timerTask = new TimerTask() {
			public void run() {
				//use a handler to run a toast that shows the current timestamp
				handler.post(new Runnable() {
					public void run() {
						gotoReenterScreen();
					}
				});
			}
		};
	}

	public void gotoReenterScreen(){
		callApi(APIServiceAsyncTask.setUserOfflien);
		BaseActivity.setSaveTime0();
		BaseSampleActivity.isAppNotClose = true;
		isAppNotClose = true;
		onActivityClose();
		Logger.printLog(Logger.ERROR, "timer logout exit board");
		Intent intent = new Intent(this, (Shadow.isProApp() ? Shadow.getProActivity() : FreeAppWayToAccess.class));
		//Intent intent = new Intent(this, LauncherActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}

	void resetTimer(){
		timer = new Timer();
		initializeTimerTask();
		timer.schedule(timerTask, ((long) time * 60 * 1000));
	}

	/*private void initHeader() {
		((TextView)findViewById(R.id.title_head)).setText(getResources().getString(R.string.alarm));
		*//*findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});*//*
		findViewById(R.id.back).setVisibility(View.GONE);
	}*/

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		resetAutoLogoutTimer();
		return super.dispatchTouchEvent(ev);
	}

	@Override
	protected void onResume() {
		super.onResume();
		alarmActive = true;
		isAppNotClose = false;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			timer.cancel();
			timer = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*@Override
	protected void onStop() {
		super.onStop();
		if(!isAppNotClose) {
			//Logger.printLog(Logger.ERROR, "onStop exit board");
			//exittoApp();
		}
	}*/

	/*

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
			case android.R.id.home:
				Logger.printLog(Logger.ERROR,"print home working");
				Shadow.alertToast("working on home");
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
*/
	/*@Override
	protected void onUserLeaveHint() {
		if(!isFinishing()) {
			//Shadow.alertToast("working on home2");
			///BaseSampleActivity.isAppNotClose = false;
			isAppNotClose = true;
			onActivityClose();
			//exittoApp();
			super.onBackPressed();
		}
		super.onUserLeaveHint();
	}*/



/*	public void exittoApp() {
		callApi(APIServiceAsyncTask.setUserOfflien);
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		Logger.printLog(Logger.ERROR, "onExit Alarm Activity base");
		finish();
		System.exit(0);
	}*/

	public void callApi(ApiRequest req){
		ApiTaskBase mTask = new ApiTaskBase(this);
		mTask.setServiceTaskMapStrParams(req, null);
		mTask.execute((Void) null);
	}

	public class ApiTaskBase extends APIServiceAsyncTask {
		ApiTaskBase(Context mContext){
			super(mContext);
		}

		@Override
		protected void success(JSONObject jsonObj, int serviceTaskType) {
			if(APIServiceAsyncTask.OFFLINE_USER == serviceTaskType){
				Logger.printLog(Logger.ERROR, "success to offline " + jsonObj.toString());
			}
		}

		@Override
		protected void failure(JSONObject jsonObj, int serviceTaskType) {
			super.failure(jsonObj, serviceTaskType);
			failure("" + serviceTaskType);
			Logger.printLog(Logger.ERROR, "failed to offline " + jsonObj.toString());
		}

		@Override
		protected void failure(String message) {
			//super.failure(message);
			Logger.printLog(Logger.ERROR,"failed to offline null");
			if(APIServiceAsyncTask.OFFLINE_USER == Integer.parseInt(message)) {
			}
		}

	}


}
