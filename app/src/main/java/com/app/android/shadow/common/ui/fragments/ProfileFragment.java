package com.app.android.shadow.common.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.common.ui.data.profile.ProfileController;
import com.app.android.shadow.common.ui.data.profile.ProfileDecorator;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.apicalls.ApiRequest;
import com.app.android.shadow.main.tapmanager.extra.MoreController;
import com.app.android.shadow.main.tapmanager.settings.ShadowLogs;
import com.app.android.shadow.main.tapmanager.settings.Sounds;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.R;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.main.util.Const;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.SCThreadMode;
import com.c2call.sdk.pub.eventbus.events.SCCreditUpdateEvent;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.fragments.SCProfileFragment;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.profile.controller.IProfileController;

import org.json.JSONObject;

/**
 * Custom Profile fragment that adds an extra view to the default SCProfileFragment.
 * While in {@link ProfileController} is shown how to
 * extend the view by modifying the ViewHolder, this time we simply add the view management directly
 * to the Fragment (in this case a credit field)
 */
public class ProfileFragment extends SCProfileFragment implements View.OnClickListener
{
    /**
     * A reference to the TextView to display the current credit value
     */
    private TextView _txtCredit;
    private TextView emailEt,profile_label_credit_shadow;
    private EditText showName;

    public BaseActivity mActivity;

    public MoreController mCallback;

	public static ProfileFragment create(final int layout)
	{
		final Bundle args = new Bundle();
        args.putInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);

		final ProfileFragment f = new ProfileFragment();
		f.setArguments(args);
		return f;
	}

    @Override
    public void onStart() {
        super.onStart();
        BaseSampleActivity.mCurrentFragment = this;
    }

    @Override
	protected IProfileController onCreateController(final View v, final SCViewDescription vd)
	{
		final ProfileController controller = new ProfileController(v, vd);
		controller.setDecorator(new ProfileDecorator());
		return controller;
	}

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        mActivity		=	(BaseActivity) this.getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle)
    {
        Ln.d("c2app", "ProfileFragment.onCreateView()");
        View v = super.onCreateView(layoutInflater, viewGroup, bundle);
        setHeader(v);
        onInitChildren(v);
        callGetShadowProfile(v);
        profile_label_credit_shadow = (TextView)v.findViewById(R.id.profile_label_credit_shadow);
        //TODO comment time left feature
        profile_label_credit_shadow.setText(getActivity().getResources().getString(R.string.unlimited_free_call_chat));
        //new ApiTask(getActivity()).execute((Void) null);
        v.findViewById(R.id.sounds).setOnClickListener(this);
        v.findViewById(R.id.shadowlog).setOnClickListener(this);
        v.findViewById(R.id.delete_all_data).setOnClickListener(this);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (MoreController) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement Interface");
        }    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (MoreController) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement Interface");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sounds:
                mCallback.onButtonClickEvent(new Sounds(), EventMaker.SettingsFragment);
                break;
          case R.id.shadowlog:
              mCallback.onButtonClickEvent(new ShadowLogs(), EventMaker.SettingsFragment);
                break;
          case R.id.delete_all_data:
              showDeleteDailog();
                break;
        }
    }

    private void showDeleteDailog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_TRADITIONAL);
        alertDialogBuilder.setTitle(R.string.delete_profile);
        alertDialogBuilder.setMessage(R.string.delete_all_data_detail);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(R.string.yes_delete, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                callApi(APIServiceAsyncTask.deleteAllData);
                arg0.dismiss();
            }
        });
        alertDialogBuilder.setNegativeButton(R.string.no_cancel,new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void callApi(ApiRequest apiService) {
        ApiCallDelete mCall = new ApiCallDelete(getActivity());
        mCall.setServiceTaskMapStrParams(apiService, null);
        mCall.execute((Void) null);
    }

    class ApiCallDelete extends APIServiceAsyncTask {
        ApiCallDelete(Context mContext){super(mContext);}

        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            //super.success(jsonObj, serviceTaskType);
            Logger.printLog(Logger.ERROR, "String " + jsonObj.toString());
            if(serviceTaskType == APIServiceAsyncTask.DELETE_ALL_DATA) {
                Shadow.editor.putBoolean(Const.IS_LOGIN, false).commit();
                SCCoreFacade.instance().unsubscribe(this);
                mCallback.onButtonClickEvent((Fragment) null, EventMaker.CloseApp);
            }else {//if(serviceTaskType == CALL_HISTORY_SEEN){

            }
        }
    }

   /* class ApiTask extends CallTimeLeft {
        ApiTask(Context mContext){
            super(mContext);
        }
        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            super.success(jsonObj,serviceTaskType);

            //if(totalTimeInMS != 0){
                    profile_label_credit_shadow.setText(getDateStringToProfile());
            //}
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
            Shadow.alertToast("Failed");
        }
    }*/

    private void callGetShadowProfile(View v){
        TextView number = (TextView)v.findViewById(R.id.ph_number);
        emailEt = (TextView)v.findViewById(R.id.sc_profile_label_email_visible);
        TextView shadowid = (TextView)v.findViewById(R.id.profile_shadowid);
        TextView shadowVersion = (TextView)v.findViewById(R.id.shadow_version);

        String phNumber = Shadow.getAppPreferences().getString(Const.PHONE, "");
        if(phNumber.length() >4) {
            number.setText(phNumber);
        }else {
            number.setText("");
        }

        shadowid.setText(Shadow.getAppPreferences().getString(Const.SHADOW_ID,""));
        emailEt.setText(Shadow.getAppPreferences().getString(Const.DISPLAY_EMAIL,""));
        Logger.printLog(Logger.ERROR,"pring display email",Shadow.getAppPreferences().getString(Const.DISPLAY_EMAIL,""));
       /* if(emailEt.getText().toString().contains("yopmail.com")) {
            emailEt.setText("");
        }*/

        showName = (EditText)v.findViewById(R.id.sc_profile_edit_firstname);
        showName.setText(Shadow.getAppPreferences().getString(Const.USERNAME,""));

        /*Shadow Version*/
        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String version = pInfo.versionName;
            int verCode = pInfo.versionCode;
            shadowVersion.setText(Shadow.getAppVersionName()+"1.0-"+version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setHeader(View v){
        TextView title = (TextView) v.findViewById(R.id.title_head);
        title.setText(R.string.sc_profile_title);
        v.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle bundle)
    {
        Ln.d("c2app", "ProfileFragment.onActivityCreated()");
        super.onActivityCreated(bundle);
        /*
        Subcsribe to the C2Call EventBus to listen for credit upadte events
         */
        SCCoreFacade.instance().subscribe(this);
        //check user email or self created
    }

    @Override
    public void onDestroy()
    {
        /*
        Unsubscribe from the C2CallEventBus to avoid memory leaks.
         */
        SCCoreFacade.instance().unsubscribe(this);
        super.onDestroy();
        BaseSampleActivity.mCurrentFragment = null;
    }

    private void onInitChildren(View v)
    {
        _txtCredit = (TextView)v.findViewById(R.id.app_profile_label_credit);
    }

    /**
     * This method will be called from the C2Call SDK whenever the user's credit changed
     * @param evt the event containing some the new credit
     */
    @SCEventCallback(threadMode = SCThreadMode.MainThread, isSticky = true)
    private void onEvent(SCCreditUpdateEvent evt)
    {
        Ln.d("c2app", "ProfileFragment.onEvent() - evt: %s", evt);


        if (_txtCredit != null
            && evt.getValue() != null)
        {
            /*
            We received a creadit update evnt -> update the GUI
            */
            _txtCredit.setText(evt.getValue().getMoneyAmount().toCompactString());
        }

    }
}
