package com.app.android.shadow.common.ui.fragments;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.app.android.shadow.common.ui.data.friendsList.FriendListItemController;
import com.app.android.shadow.common.ui.data.friendsList.FriendsController;
import com.app.android.shadow.main.base.ShadowBaseFriendsFragment;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.R;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemController;
import com.c2call.sdk.pub.gui.friendlistitem.controller.SCFriendListItemControllerFactory;
import com.c2call.sdk.pub.gui.friends.controller.IFriendsController;

/**
 * Customised fragment to show a friend list with a corresponding customized menu.
 * For convenience this fragment contains the method #onFriendClicked() that will be called
 * whenever an item from the list is clicked. This can be used in derived classes to quickly
 * implement custom click actions.
 */
public class FriendsFragment extends ShadowBaseFriendsFragment
{

	private SCChoiceDialog _dlgAddContact;
    //IFriendListItemController controller = null;

    public static FriendsFragment create(final int layout)
    {
        final Bundle args = new Bundle();
        args.putInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);

        final FriendsFragment f = new FriendsFragment();
        f.setArguments(args);
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Ln.d("c2app", "FriendsFragment.onCreate()");
    }


    @Override
    public void onResume()
    {
        super.onResume();

        if(getActivity().getActionBar() != null){
            getActivity().getActionBar().setTitle(R.string.app_title_friends);
        }
    }


    @Override
    public void onDestroy()
    {
        Ln.d("c2app", "FriendsFragment.onDestroy()");
        if (_dlgAddContact != null){
            _dlgAddContact.dismiss();
            _dlgAddContact = null;
        }

       super.onDestroy();
    }

    /**
     * The returned factory is used by to create the item controllers. Here we show how to simply pass actions
     * from an item controller to the parent fragment.
     * @return
     */
    public SCFriendListItemControllerFactory getItemFactory()
    {

        return new SCFriendListItemControllerFactory(null, null)
        {
            @Override
            public IFriendListItemController onCreateController(final View v, final SCViewDescription vd, final SCFriendData data)
            {
                final IFriendListItemController controller = new FriendListItemController(v, vd, data)
                {
                    @Override
                    public void onMainViewClick(View v)
                    {
                        Logger.printLog(Logger.ERROR,"Print Getdata ", data.getFirstname() + "  " + data.getEmail());
                        onShowContextDialog();
                        //onFriendClicked(this,v,data);

                    }

                    @Override
                    public void onMainViewLongClick(View v) {
                        //super.onMainViewLongClick(v);

                    }

                };

                controller.setRequestListener(getRequestListener());
                return (controller);
            }
        };
    }

    /**
     * This method is called by the corresponding item controller whenever the item view is clicked.
     * @param controller
     * @param v
     * @param data
     */
    protected void onFriendClicked(IFriendListItemController controller, View v, SCFriendData data)
    {

        //controller.onMainViewClick(v);
    }

    protected void onFriendLongClicked(IFriendListItemController controller, View v, SCFriendData data)
    {
       // controller.onMainViewLongClick(v);
    }

    @Override
	protected IFriendsController onCreateController(final View v, final SCViewDescription vd)
	{
        Ln.d("c2app", "FriendsFragment.onCreateController()");
		return new FriendsController(v,
										vd,
										C2CallSdk.instance().getVD().friendListItem(),
                                        getItemFactory(),
										null);
	}




    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        Ln.d("c2app", "FriendsFragment.onCreateOptionsMenu()");

        if (getController() == null){
            return;
        }


        switch(getController().getListModus()){
            case Delete: inflater.inflate(R.menu.app_confirm_delete_friends, menu); break;
            default:
                inflater.inflate(R.menu.app_menu_friends, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item)
    {
        if (getController() == null) {
            return false;
        }

        final int id = item.getItemId();

        if (id == getId("menu_add_friend")) {
            mCallback.onButtonClickEvent(new AddFriendFragment(), EventMaker.Friends);
            /*
            final SCChoiceDialog dlg = C2CallSdk.dialogFactory().createAddFriendDialog(getController());
            if (dlg != null) {
                dlg.show();
            }*/
            return true;
        }
        else if (id == getId("menu_search")){
            getController().toggleFilterInput();
            return true;
        }
        else if (id == getId("menu_save")){
            getController().deleteSelectedFriends();
            return true;
        }
        else{
            return super.onOptionsItemSelected(item);
        }
    }

    /*public void setListSelectedData(SCFriendData data) {
        Logger.printLog(Logger.ERROR,"get data selected"," is "+data.isConfirmed());
        Logger.printLog(Logger.ERROR,"get data selected"," is "+data.getFirstname());
        Logger.printLog(Logger.ERROR,"get data selected"," is "+data.getEmail());
    }*/
}
