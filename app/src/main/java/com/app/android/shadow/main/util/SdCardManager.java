package com.app.android.shadow.main.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import com.app.android.shadow.main.Shadow;
//import com.c2call.sdk.lib.util.extra.StorageExtra;
//import com.c2call.sdk.lib.n.g.ak;
//import com.c2call.sdk.pub.storage.SCUserImageStore;

import java.io.File;
import java.net.URLConnection;
import java.util.ArrayList;

public class SdCardManager {

    public static File folderName = null;
    public static File packName = null;
    public static File appName = null;
    public static File cacheDir = null;
    public static File userImageDir = null;



    public static File getMainDirectory(Context context){
        folderName = new File(context.getFilesDir(),"shadow");
        return folderName;
    }

    public static void creatMainDirectory(){
       // folderName = new File(manageString(Environment.getRootDirectory()+""));
    }
    public static void createDirectory(){
        //if (Environment.getExternalStorageState() == null) {

      /*  packName = new File(manageString(Environment.getDataDirectory() + "/data"+ Shadow.getpackageNameAsStr()));
        appName = new File(manageString(Environment.getDataDirectory() + "/data"+ Shadow.getpackageNameAsStr()+"/"+ Shadow.getAppNameAsStr()));
        cacheDir = new File(manageString(Environment.getDataDirectory() + "/data/"+ Shadow.getpackageNameAsStr()+"/"+ Shadow.getAppNameAsStr()+"/thumbs"));
        userImageDir = new File(manageString(Environment.getDataDirectory() + "/data/"+ Shadow.getpackageNameAsStr()+"/"+ Shadow.getAppNameAsStr()+"/usr_images"));*/
      /*  }else {
            cacheDir = new File(manageString(Environment.getExternalStorageDirectory() + "/data/"+ Shadow.getpackageNameAsStr()+"/"+ Shadow.getAppNameAsStr()+"/thumbs"));
            userImageDir = new File(manageString(Environment.getExternalStorageDirectory() + "/data/"+ Shadow.getpackageNameAsStr()+"/"+ Shadow.getAppNameAsStr()+"/usr_images"));
        }*/

        //packName = new File(manageString(folderName.getAbsolutePath() + "/data"+ Shadow.getpackageNameAsStr()));
        //appName = new File(manageString(folderName.getAbsolutePath() + "/data"+ Shadow.getpackageNameAsStr()+"/"+ Shadow.getAppNameAsStr()));
        cacheDir = new File(manageString(folderName.getAbsolutePath() + "/thumbs"));
        userImageDir = new File(manageString(folderName.getAbsolutePath()+"/usr_images"));

        /*if(!folderName.exists())
            makeDirs(folderName);*/
       /* if(!packName.exists())
            makeDirs(packName);
        if(!appName.exists())
            makeDirs(appName);*/
        if(!cacheDir.exists())
            makeDirs(cacheDir);
        if(!userImageDir.exists())
            makeDirs(userImageDir);
    }

    public static String getLocation() {
        if (Environment.getExternalStorageState() == null) {
            return Environment.getDataDirectory().getAbsolutePath().toString();
        }else {
            return Environment.getExternalStorageDirectory().getAbsolutePath().toString();
        }
    }

    public static boolean makeDirs(File path){
        File data =  new File(manageString(getLocation() + "/data"));
        if(!data.exists()){
            if(!data.mkdir()){
                return false;
            }
        }
        File packName =  new File(manageString(getLocation() + "/data/"+ Shadow.getpackageNameAsStr()));
        if(!packName.exists()){
            if(!packName.mkdir()){
                return false;
            }
        }

        File appName =  new File(manageString(getLocation() + "/data/"+ Shadow.getpackageNameAsStr()+"/"+ Shadow.getAppNameAsStr()));
        if(!appName.exists()){
            if(!appName.mkdir()){
                return false;
            }
        }

        if(!path.exists()){
            if(!path.mkdir()){
                return false;
            }else {
                return true;
            }
        }
        return false;
    }

    public static String manageString(String path){
        //path = path.replace(" ", "%20");
        //path = path.replace(".", "'.'");
        Logger.printLog(Logger.ERROR,"path is "+path);
        return path;
    }

    public static String getAppNamePath(){
        if(folderName == null){
            creatMainDirectory();
        }
        return folderName.getAbsolutePath();
    }
    public static String getSavePath(){
        if(cacheDir == null){
            createDirectory();
        }
        return cacheDir.getAbsolutePath();
    }

    public static String getUserImgDir(){
        if(userImageDir == null){
            createDirectory();
        }
        return userImageDir.getAbsolutePath();
    }

    public static Bitmap getUserImgBitmap(String id){
        String dir = getUserImgDir();
        try {
            if (dir != null && dir.length() > 0) {
                File[] files = userImageDir.listFiles();
                if (files != null && files.length > 0) {
                    for (File f : files) {
                        if (f.exists()) {
                            if (f.getAbsolutePath().toString().contains(id)) {
                                String name = "usr_images_" + id;
                                Logger.printLog(Logger.ERROR,"Image Name", "is " + name);
                                //String path = MediaUtil.getMediaPath(SCDownloadType.None,name,false);
                                String path = f.getAbsolutePath();
                                Logger.printLog(Logger.ERROR,"Image path big", "is " + path);
                                if (path.length() > 0) {
                                    return BitmapFactory.decodeFile(path);
                                }
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static boolean removeOldImage(final String id){
        final String dir = getUserImgDir();
        try {
            File file = new File(""+getUserImgDir()+"/userimage://"+id+".jpg");
            if(file.exists()){
                if(file.delete()){
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
      /*  try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                        if (dir != null && dir.length() > 0) {
                            File[] files = userImageDir.listFiles();
                            if (files != null && files.length > 0) {
                                for (File f : files) {
                                    if (f.exists()) {
                                        if (f.getAbsolutePath().toString().contains(id)) {
                                            f.delete();
                                        }
                                    }
                                }
                            }
                        }
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        return false;
    }

   /* public static Bitmap getUserImgPath(String id){

    }*/



    public static File getMainDirectory(){
        return new File(getAppNamePath());
    }
    public static File getDirectory(){
        return new File(getSavePath());
    }

    public static File getFile(String url){
        //I identify images by hashcode. Not a perfect solution, good for the demo.
        String filename= String.valueOf(url.hashCode());
        //Another possible solution (thanks to grantland)
        //String filename = URLEncoder.encode(url);
        File f = new File(cacheDir, filename);
        return f;

    }

    public static File[] getFilesList(){
        File[] files=getDirectory().listFiles();
        Logger.printLog(Logger.ERROR,"Files size","is "+files.length);
        for (File f:files){
            Logger.printLog(Logger.ERROR,"Files path", "is " + f.getAbsolutePath());
        }
        return files;
    }



    public static ArrayList<File> getImageFileList(){
        File[] files=getDirectory().listFiles();
        ArrayList<File> fList = new ArrayList<File>();
        for(File f: files){
            if(isImageFile(f.getPath())){
                fList.add(f);
            }
        }
        return fList;
    }

    public static ArrayList<File> getVideoFileList(){
        File[] files=getDirectory().listFiles();
        ArrayList<File> fList = new ArrayList<File>();
        for(File f: files){
            if(isVideoFile(f.getPath())){
                fList.add(f);
            }
        }
        return fList;
    }


    public static boolean isImageFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.indexOf("image") == 0;
    }

    public static boolean isVideoFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.indexOf("video") == 0;
    }

    /*public static void deleteUserImage() {
        String id = SCProfileHandler.instance().getProfileUserId();
        // old code String filePath = StorageExtra.getDefaultDownloadDirectory(true) + "/profile" + id+"/"+id + ".jpg";;
        String filePath = ak.a(true) + "/profile" + id+"/"+id + ".jpg";
        File e = new File(filePath);
        if(e != null && e.exists()) {
            e.delete();
        }

    }*/

   /* public static File getTempFile(){
        File file = new File(""+getSavePath()+"/Temp.mp3");
        if(file.exists()) {
            file.delete();
        }
        return file;
    }

    public static File getNonExistsFile(){
        File file = new File(""+getSavePath()+"/Record1.mp3");
        int i = 2;
        if(file.exists()) {
            while (file.exists()) {
                file = new File("" + getSavePath() + "/Record" + i + ".mp3");
            }
        }
        return file;
    }


    public static boolean isTempExist(){
        File f = new File(cacheDir,"/Temp.mp3");
        return (f.exists());
    }

    synchronized public static String getNonExistsFileName(){

        File[] files=cacheDir.listFiles();

        if(files!=null){
            File f = files[(files.length-1)];
            String name = f.getName();
            if(name.contains("Temp") && name.contains("tts")){
                f= files[(files.length-2)];
                name = f.getName();
            }
            int i = Integer.parseInt(name.substring(6,7));
            Logger.printLog(Logger.ERROR,"CharAt 6",""+i);
            return new File("" + getSavePath() + "/Record" + ++i + ".mp3").getName();
        }else {
            return new File(""+getSavePath()+"/Record1.mp3").getName();
        }
    }

    public static String renameTemp(String fileName) {
        File from      = new File(cacheDir,"/Temp.mp3");
        File to        = new File(cacheDir,fileName);
        from.renameTo(to);
       *//* Log.i("Directory is", cacheDir.toString());
        Log.i("From path is", from.toString());
        Log.i("To path is", to.toString());*//*
       return to.getName();
    }

    public static boolean isAlreadExist(String fileName) {
        File file        = new File(cacheDir,fileName);
        int file_size = Integer.parseInt(String.valueOf(file.length() / 1024));
        if (file_size > Const.FILE_SIZE_LIMIT && file.exists()) {
            return true;
        }
        return false;
    }

    public static boolean isBiggerSize(String filePate) {
        File file        = new File(filePate);
        int file_size = Integer.parseInt(String.valueOf(file.length() / 1024));
        if (file_size > Const.FILE_SIZE_LIMIT) {
            return true;
        }
        return false;
    }

    public static void deleteFileByName(String path){
        File file = new File(cacheDir+"/"+path);
        Logger.printLog(Logger.ERROR,"FilePath", file.getAbsolutePath());
         boolean isDelete= file.delete();
         Logger.printLog(Logger.ERROR,"FilePath is ", "" + isDelete);
    }

    public static void deleteFileByPath(String path){
        File file = new File(path);
        Logger.printLog(Logger.ERROR,"FilePath", file.getAbsolutePath());
         boolean isDelete= file.delete();
         Logger.printLog(Logger.ERROR,"FilePath is ", "" + isDelete);
    }


    public static ArrayList<String> getFielList(){
        ArrayList<String> lst = new ArrayList<String>();
        File[] files=cacheDir.listFiles();
        if(files==null)
            return null;
        for(File f:files) {
            if(!f.getName().equals("tts"))
                lst.add(f.getName());
        }
        return lst;
    }

    public static String getRealPathFromURI(Uri contentURI,Context mContext) {
        String result;
        Cursor cursor = mContext.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }*/
}
