package com.app.android.shadow.common.ui.dialogs;

import com.app.android.shadow.main.custom.dialog.DialogConfirmation;
import com.c2call.sdk.pub.gui.core.controller.IController;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.gui.dialog.SCDialogFactory;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemController;

/**
 * Created by rails-dev on 8/10/15.
 */
public class ShadowSCDialogFactory extends SCDialogFactory {

    @Override
    public SCChoiceDialog createFriendListItemDialog(IFriendListItemController controller) {
        return ShadowDialogBuilderContactListItem.build(controller);
    }

    public SCChoiceDialog createConformationDialog(IFriendListItemController controller) {
        return DialogConfirmation.build(controller);
    }

    @Override
    public SCChoiceDialog createRichMessageDialog(IController<?> controller, boolean isSms) {
        return ShadowDialogBuilderRichMessage.build(controller, isSms);
    }

    @Override
    public SCChoiceDialog createPhotoDialog(IController<?> controller, String filename) {
        //return super.createPhotoDialog(controller, filename);
        return ShadowDialogBuilderPhoto.build(controller, filename);
    }

    @Override
    public SCChoiceDialog createVideoDialog(IController<?> controller, String filename) {
       // return super.createVideoDialog(controller, filename);
        return ShadowDialogBuilderVideo.build(controller, filename);
    }
}
