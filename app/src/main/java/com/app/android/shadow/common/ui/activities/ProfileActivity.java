package com.app.android.shadow.common.ui.activities;

import android.app.Fragment;

import com.app.android.shadow.common.ui.fragments.ProfileFragment;
import com.c2call.sdk.pub.activities.SCProfileFragmentActivity;
import com.c2call.sdk.pub.core.SCExtraData;


/**
 * Custom Profile Activity class.
 */
public class ProfileActivity extends SCProfileFragmentActivity
{
    @Override
    protected Fragment onCreateFragment()
    {
        final int layout = getIntent().getExtras().getInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT);

        return ProfileFragment.create(layout);
    }
}
