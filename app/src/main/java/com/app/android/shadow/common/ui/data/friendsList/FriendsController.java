package com.app.android.shadow.common.ui.data.friendsList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

import com.app.android.shadow.R;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.util.Logger;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.common.SCListModus;
import com.c2call.sdk.pub.gui.core.controller.IControllerRequestListener;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;
import com.c2call.sdk.pub.gui.core.loaderhandler.IFriendLoaderHandler;
import com.c2call.sdk.pub.gui.core.loaderhandler.ILoaderHandlerContextProvider;
import com.c2call.sdk.pub.gui.core.loaderhandler.SCBaseLoaderHandler;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemControllerFactory;
import com.c2call.sdk.pub.gui.friends.controller.SCFriendsController;
import com.c2call.sdk.pub.util.IListViewProvider;

import org.json.JSONObject;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class FriendsController extends SCFriendsController
{
    String pos;
    Collection<String> friends;
    Collection<String> contacts;
    int size;
    public FriendsController(final View view,
                             final SCViewDescription viewDescription,
                             final SCViewDescription itemViewDescription,
                             final IFriendListItemControllerFactory itemControllerFactory,
                             final IControllerRequestListener requestListener)
    {
        super(view, viewDescription, itemViewDescription, itemControllerFactory, requestListener);
        Ln.d("c2app", "FriendsController.<init>()");
    }

    @Override
    public void onCreate(final Activity activity, final SCActivityResultDispatcher scActivityResultDispatcher)
    {
        Ln.d("c2app", "FriendsController.onCreate()...");
        super.onCreate(activity, scActivityResultDispatcher);

    }

    @Override
    public void setListModus(SCListModus modus) {
        super.setListModus(modus);
    }

    @Override
    public void deleteSelectedFriends()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.app_dlg_confirm_title)
                .setMessage(R.string.app_dlg_confirm_deletion_text)
                .setPositiveButton(R.string.sc_std_btn_ok, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which)
                    {
                        FriendsController.super.deleteSelectedFriends();
                    }
                });
        builder.show();
    }

    public void deleteSelectedFriends(final Collection<String> friends, final Collection<String> contacts)
    {
        FriendsController.super.onDeleteFriends(friends,contacts);
       /* final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.app_dlg_confirm_title)
                .setMessage(R.string.app_dlg_confirm_deletion_text)
                .setPositiveButton(R.string.sc_std_btn_ok, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which)
                    {

                    }
                });
        builder.show();*/
    }


    private void callonDeleteSuper(){
        super.onDeleteFriends(this.friends,this.contacts);
    }

    @Override
    public IFriendLoaderHandler<?> onCreateLoaderHandler(final IListViewProvider listViewProvider,
                                                         final ILoaderHandlerContextProvider loaderHandlerMasterProvider,
                                                         final IFriendListItemControllerFactory itemMediatorFactory,
                                                         final SCViewDescriptionMap itemDescriptions)
    {
        Ln.d("c2app", "FriendsController.onCreateLoaderHandler()");
        return new FriendsLoaderHandler(loaderHandlerMasterProvider,
                                         listViewProvider,
                                         itemMediatorFactory,
                                         itemDescriptions.get(0),
                                         SCBaseLoaderHandler.LOADER_CONTACTS);
    }


    private void callApi(String userId){
        UserDeleteTask mSignUpTask = new UserDeleteTask(getContext());
        mSignUpTask.setServiceTaskMapStrParams(APIServiceAsyncTask.removeFrndtoFrndList, getParams(userId));
        mSignUpTask.execute((Void) null);
    }


    class UserDeleteTask extends APIServiceAsyncTask {
        UserDeleteTask(Context mContext){
            super(mContext);
        }
        String _idPos;
        @Override
        protected void onPreExecute() {
            _idPos = pos;
            --size;
            showProgress(true);
        }

        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            Logger.printLog(Logger.ERROR,"success","working");
            Logger.printLog(Logger.ERROR,"size is pre",""+size);
            if(size<=0){
                Logger.printLog(Logger.ERROR,"size is",""+size);
                callonDeleteSuper();
            }
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
            friends.remove(_idPos);
            if(friends.size()>0 && size<=0){
                callonDeleteSuper();
            }
        }
    }

    private Map<String, String> getParams(String userId){
        Map<String, String> params = new HashMap<>();
        params.put("username", userId);
        return params;
    }

}
