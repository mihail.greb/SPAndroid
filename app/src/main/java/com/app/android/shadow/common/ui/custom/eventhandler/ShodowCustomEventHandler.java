package com.app.android.shadow.common.ui.custom.eventhandler;

import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.main.util.Logger;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.events.SCCustomEvent;
import com.c2call.sdk.pub.facade.SCCoreFacade;

/**
 * Created by rails-dev on 4/3/16.
 */
public class ShodowCustomEventHandler {
    private static ShodowCustomEventHandler __instance = new ShodowCustomEventHandler();

    public static ShodowCustomEventHandler instance() {
        return __instance;
    }

    private ShodowCustomEventHandler() {
    }

    public void subscribe() {
        SCCoreFacade.instance().subscribe(this);
    }

    public void unsubscribe() {
        SCCoreFacade.instance().unsubscribe(this);
    }

    @SCEventCallback
    private void onEvent(SCCustomEvent var1) {
        Ln.d("fc_tmp", "SCCustomEventHandler.onEvent() - evt: %s", new Object[]{var1});
        String var2 = var1.getEventName();
        if("SCEVNT_SREF".equalsIgnoreCase(var2)) {
            Logger.printLog(Logger.ERROR,"Get Event name SCEVNT_SREF");
            BaseSampleActivity.__instance.refreshList();
        }
    }
}