package com.app.android.shadow.common.ui.data.friendsList;

import com.c2call.sdk.pub.common.SCOnlineStatus;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemController;
import com.c2call.sdk.pub.gui.friendlistitem.decorator.SCFriendListItemDecorator;
import com.app.android.shadow.R;

public class FriendListItemDecorator extends SCFriendListItemDecorator
{
    @Override
    protected int getStatusTextColor(IFriendListItemController controller, SCOnlineStatus status)
    {

        if (status == null){
            return 0;
        }

        return status.isOnline()
                    ? controller.getContext().getResources().getColor(R.color.app_blue)
                    : controller.getContext().getResources().getColor(R.color.app_grey);
    }

    @Override
    protected int getOnlineActiveIcon()
    {
        return R.drawable.sc_std_icon_online;
    }

    @Override
    protected int getOnlineIcon()
    {
        return R.drawable.sc_std_icon_online;
    }
}
