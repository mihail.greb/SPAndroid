package com.app.android.shadow.samplechat.board;

import android.view.View;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.SCNewMessageCache;
import com.c2call.sdk.pub.gui.board.controller.SCBoardController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemControllerFactory;
import com.c2call.sdk.pub.gui.core.controller.IControllerRequestListener;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;

/**
 * Basic custom board controller. This controls the chat history with a cetrain friend.
 * Further customization of the controller's functionality should be done here
 */
public class BoardController extends SCBoardController
{
    public BoardController(View view, SCViewDescription scViewDescription, SCViewDescriptionMap scViewDescriptionMap, IBoardListItemControllerFactory iBoardListItemControllerFactory, IControllerRequestListener iControllerRequestListener)
    {
        super(view, scViewDescription, scViewDescriptionMap, iBoardListItemControllerFactory, iControllerRequestListener);
    }

    @Override
    public void setUserId(String userid)
    {
        Ln.d("c2app", "BoardController.setUserId() - userid: %s", userid);
        super.setUserId(userid);
        if (userid != null) {
            /*
            Clear the new-message tags for this user.
             */
            SCNewMessageCache.instance().getNewMessages(userid).clear();
        }
    }

  /*  @Override
    public IBoardLoaderHandler<?> onCreateLoaderHandler(IListViewProvider listViewProvider, ILoaderHandlerContextProvider loaderHandlerMasterProvider, IBoardListItemControllerFactory itemMediatorFactory, SCViewDescriptionMap itemDescriptions) {
      //  return super.onCreateLoaderHandler(listViewProvider, loaderHandlerMasterProvider, itemMediatorFactory, itemDescriptions);
        Ln.d("fc_tmp", "SCBoardController.onCreateLoaderHandler()", new Object[0]);
        ShadowBoardLoaderHandler loaderHandler = new ShadowBoardLoaderHandler(loaderHandlerMasterProvider, listViewProvider, this.getUserId(), itemDescriptions, itemMediatorFactory);
        loaderHandler.setFilterMask(this.getFilterMask());
        return loaderHandler;
    }

    @Override
    public ILoaderHandlerContextProvider onCreateLoaderHandlerContextProvider() {
        return super.onCreateLoaderHandlerContextProvider();
    }*/

    /*public void deleteAllData(String userid){
        setUserId(userid);
        onClearHistoryConfirm();
    }*/


}
