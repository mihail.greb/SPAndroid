package com.app.android.shadow.utils.custom;

import com.c2call.sdk.pub.util.StringPair;
import java.util.ArrayList;
import java.util.UUID;

public class StringExtra {
    public static final String NON_ALPHABETIC = " \t0123456789.,:;@$+-?*/\\\"\'#~(){}[]<>=|&";

    public StringExtra() {
    }

    public static String toNiceString(Object s) {
        return s == null?"":s.toString();
    }

    public static String toNiceString(String s) {
        return s == null?"":s;
    }

    public static String toEclipseString(String s, int maxLength) {
        if(s.length() > maxLength && maxLength >= 4) {
            StringBuilder builder = new StringBuilder(maxLength);
            builder.append(s.substring(0, maxLength - 3)).append("...");
            return builder.toString();
        } else {
            return s;
        }
    }

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.length() == 0;
    }

    public static boolean isNullOrEmpty(String s, String... others) {
        if(others == null) {
            return isNullOrEmpty(s);
        } else {
            String[] arr$ = others;
            int len$ = others.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                String cur = arr$[i$];
                if(isNullOrEmpty(cur)) {
                    return true;
                }
            }

            return false;
        }
    }

    public static String trimIfNecessary(String s) {
        return isNullOrEmpty(s)?s:(s.charAt(0) != 32 && s.charAt(0) != 9 && s.charAt(s.length() - 1) != 32 && s.charAt(s.length() - 1) != 9?s:s.trim());
    }

    public static String toHexString(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        byte[] arr$ = a;
        int len$ = a.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            byte element = arr$[i$];
            sb.append(Character.forDigit((element & 240) >> 4, 16));
            sb.append(Character.forDigit(element & 15, 16));
        }

        return sb.toString();
    }

    public static byte[] hexStringToByteArray(String s) {
        if(isNullOrEmpty(s)) {
            return null;
        } else {
            int len = s.length();
            byte[] data = new byte[len / 2];

            for(int i = 0; i < len; i += 2) {
                data[i / 2] = (byte)((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
            }

            return data;
        }
    }

    public static boolean isPhonenumber(String s) {
        return !isNullOrEmpty(s) && s.charAt(0) == 43 && s.charAt(s.length() - 1) >= 48 && s.charAt(s.length() - 1) <= 57;
    }

    /*public static boolean isFuzzyPhonenumber(String s) {
        if(isNullOrEmpty(s)) {
            return false;
        } else {
            String number = PhoneNumberConverter.stripInvalidChars(s);
            return !isNullOrEmpty(number) && number.length() >= 2?s.charAt(0) == 43 || Character.isDigit(s.charAt(0)):false;
        }
    }*/

    public static boolean isEqual(String s1, String s2) {
        return s1 == s2?true:(s1 == null != (s2 == null)?false:s1.equals(s2));
    }

    public static boolean isWideString(String s) {
        char[] chars = s.toCharArray();
        char[] arr$ = chars;
        int len$ = chars.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            char c = arr$[i$];
            if(c > 255) {
                return true;
            }
        }

        return false;
    }

    public static boolean isInteger(String s) {
        if(isNullOrEmpty(s)) {
            return false;
        } else {
            for(int i = 0; i < s.length(); ++i) {
                char c = s.charAt(i);
                boolean isFirstNeg = i == 0 && c == 45;
                if((s.length() <= 1 || !isFirstNeg) && (c < 48 || c > 57)) {
                    return false;
                }
            }

            return true;
        }
    }

    public static boolean isPositiveInteger(String s) {
        if(isNullOrEmpty(s)) {
            return false;
        } else {
            for(int i = 0; i < s.length(); ++i) {
                char c = s.charAt(i);
                if(c < 48 || c > 57) {
                    return false;
                }
            }

            return true;
        }
    }

    public static String[] toNonEmptyStringArray(String... s) {
        if(s == null) {
            return null;
        } else {
            ArrayList list = new ArrayList();
            String[] arr$ = s;
            int len$ = s.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                String cur = arr$[i$];
                if(!isNullOrEmpty(cur)) {
                    list.add(cur);
                }
            }

            return list.isEmpty()?null:(String[])list.toArray(new String[list.size()]);
        }
    }

    public static String join(String[] array, String delimiter) {
        if(array != null && delimiter != null) {
            StringBuilder builder = new StringBuilder();

            for(int i = 0; i < array.length; ++i) {
                builder.append(array[i]);
                if(i < array.length - 1) {
                    builder.append(delimiter);
                }
            }

            return builder.toString();
        } else {
            return null;
        }
    }

    /*public static String generatePassward() {
        return generatePassward(6);
    }*/

   /* public static String generatePassward(int length) {
        String pass = RandomStringExtra.randomAlphanumeric(length);
        pass = pass.replace("l", "" + GlobalRand.nextInt(10));
        pass = pass.replace("I", "" + GlobalRand.nextInt(10));
        return pass;
    }*/

    public static String getRandom128() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static StringPair splitAtLast(String s, String d) {
        if(!isNullOrEmpty(s) && !isNullOrEmpty(d)) {
            int pos = s.lastIndexOf(d);
            if(pos < 0) {
                return new StringPair(s, (String)null);
            } else {
                String first = s.substring(0, pos);
                String second = s.substring(pos + d.length());
                return new StringPair(first, second);
            }
        } else {
            return new StringPair(s, (String)null);
        }
    }

    public static StringPair splitAtFirst(String s, String d) {
        if(!isNullOrEmpty(s) && !isNullOrEmpty(d)) {
            int pos = s.indexOf(d);
            if(pos < 0) {
                return new StringPair(s, (String)null);
            } else {
                String first = s.substring(0, pos);
                String second = s.substring(pos + d.length());
                return new StringPair(first, second);
            }
        } else {
            return new StringPair(s, (String)null);
        }
    }

    public static boolean isHtml(String s) {
        return s == null?false:s.startsWith("html://");
    }

    public static String createRepeat(String s, int count) {
        return createRepeat(s, count, ',');
    }

    public static String createRepeat(String s, int count, String separator) {
        StringBuilder b = new StringBuilder();

        for(int i = 0; i < count; ++i) {
            b.append(s);
            if(i < count - 1) {
                b.append(separator);
            }
        }

        return b.toString();
    }

    public static String createRepeat(String s, int count, char separator) {
        StringBuilder b = new StringBuilder();

        for(int i = 0; i < count; ++i) {
            b.append(s);
            if(i < count - 1) {
                b.append(separator);
            }
        }

        return b.toString();
    }

    public static String capitalize(String s) {
        if(isNullOrEmpty(s)) {
            return "";
        } else {
            char first = s.charAt(0);
            return Character.isUpperCase(first)?s:Character.toUpperCase(first) + s.substring(1);
        }
    }
}
