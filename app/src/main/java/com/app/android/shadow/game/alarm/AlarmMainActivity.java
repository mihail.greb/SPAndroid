package com.app.android.shadow.game.alarm;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.app.android.shadow.main.tapmanager.extra.MoreController;
import com.app.android.shadow.main.tapmanager.settings.MoreSignature;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.R;
import com.app.android.shadow.main.CountClickBase;
import com.app.android.shadow.main.util.Const;

import java.util.HashMap;
import java.util.Stack;

public class AlarmMainActivity extends CountClickBase implements MoreController {

    /* Your Tab host */
    protected TabHost mTabHost;
    protected View baseActivityView;
    /* A HashMap of stacks, where we use tab identifier as keys..*/
    public static HashMap<String, Stack<android.app.Fragment>> mStacks;
    public static Fragment mCurrentFragment;

    /*Save current tabs identifier in this..*/
    public static String mCurrentTab;

    public static String worldTimeZoneStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = getLayoutInflater();
        baseActivityView = inflater.inflate(R.layout.shadow_main_tab_fragment_layout_alarm, null);
        setContentView(baseActivityView);
        mStacks             =   new HashMap<String, Stack<Fragment>>();
        mStacks.put(Const.STOP_WATCH_TAB, new Stack<Fragment>());
        mStacks.put(Const.TIMER_TAB, new Stack<Fragment>());
        mStacks.put(Const.ALARM_TAB, new Stack<Fragment>());
        /*mStacks.put(Const.WORLD_CLOCK_TAB, new Stack<Fragment>());*/

        mTabHost                =   (TabHost)findViewById(android.R.id.tabhost);
        mTabHost.setOnTabChangedListener(listener);
        mTabHost.setup();

        initializeTabs();
        setChecked();

        initCustomeBox();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        worldTimeZoneStr = "";
    }

    private View createTabView(final int imageId,final String tag){
        View view = LayoutInflater.from(this).inflate(R.layout.app_main_tab_indicator, null);
        //final Drawable image = getResources().getDrawable(imageId);
        final TextView tabText = (TextView) view.findViewById(R.id.tab_text);
        final ImageView tabImage = (ImageView) view.findViewById(R.id.tab_image);
        tabText.setText(tag);
        tabImage.setImageResource(imageId);
        // image.setBounds(0, 0, 60, 60);
        // textView.setCompoundDrawables(null, image, null, null);
        return view;
    }

    @Override
    public void onButtonClickEvent(Fragment f, EventMaker maker) {
        if(f != null) {
           // pushFragments(f, false);
            pushFragments(Const.ALARM_TAB, f, false, true);

        }else {
            switch (maker){
                case OpenDialog:
                    Logger.printLog(Logger.ERROR, "OpenDialog");
                    showPopupWindow();
                    break;

                case NoDialogRequired:
                    Logger.printLog(Logger.ERROR, "NoDialogRequired");
                    goToNextScreen();
                    break;

                case ClickCount:
                    checkToGoNext();
                    break;

                case ClickSwipe:
                    checkToGoNextBySwipe();
                    break;


                case GoToShadow:
                    //onBackPressed();
                    goToNextScreen();
                    break;
            }
        }
    }

    @Override
    public void onModeChangeEvent(boolean isSafe) {

    }

    @Override
    public void onButtonClickEvent(MoreSignature name, EventMaker maker) {

    }

    @Override
    public void addToIncressClickArea(View v) {

    }

    @Override
    public void goToModes() {

    }

    public void initializeTabs(){

        // Setup your tab icons and content views.. Nothing special in this..
        TabHost.TabSpec spec    =   mTabHost.newTabSpec(Const.STOP_WATCH_TAB);
        mTabHost.setCurrentTab(-3);
        // STOP_WATCH_TAB
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        spec.setIndicator(createTabView(R.drawable.stop_watch, Const.STOP_WATCH_TAB));
        mTabHost.addTab(spec);

        // TIMER_TAB
        spec                    =   mTabHost.newTabSpec(Const.TIMER_TAB);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        spec.setIndicator(createTabView(R.drawable.timer, Const.TIMER_TAB));
        mTabHost.addTab(spec);

        // ALARM_TAB
        spec                    =   mTabHost.newTabSpec(Const.ALARM_TAB);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        spec.setIndicator(createTabView(R.drawable.alarm, Const.ALARM_TAB));
        mTabHost.addTab(spec);

        // WORLD_CLOCK_TAB
        /*spec                    =   mTabHost.newTabSpec(Const.WORLD_CLOCK_TAB);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        spec.setIndicator(createTabView(R.drawable.world_clock, Const.WORLD_CLOCK_TAB));
        mTabHost.addTab(spec);*/

    }

    TabHost.OnTabChangeListener listener    =   new TabHost.OnTabChangeListener() {
        public void onTabChanged(String tabId) {
            mCurrentTab                     =   tabId;

           if(mStacks.get(Const.ALARM_TAB).size() > 1){
               mStacks.get(Const.ALARM_TAB).pop();
           }

            if(mStacks.get(tabId).size() == 0){
                if(tabId.equals(Const.STOP_WATCH_TAB)){
                    pushFragments(tabId, new StopWatchFragment(), false, true);
                    //pushFragments(tabId, new FriendsListFragment(), false, true);
                }else if(tabId.equals(Const.TIMER_TAB)){
                    pushFragments(tabId, new AlarmTimerFragment(), false, true);
                }else if(tabId.equals(Const.ALARM_TAB)){
                    pushFragments(tabId,new AlarmFragment(), false,true);
                }/*else if(tabId.equals(Const.WORLD_CLOCK_TAB)){
                    pushFragments(tabId, new WorldTimeZone(), false,true);
                }*/
            }else {
                pushFragments(tabId, mStacks.get(tabId).lastElement(), false,false);
            }
            setChecked();
        }
    };

    private void setChecked(){
        int newColor =  colorGet(android.R.color.holo_blue_dark);
        // mTabHost.getCurrentTabView();
        int defaultColor = colorGet(android.R.color.black);
        int currentTab = mTabHost.getCurrentTab();
        for(int i =0;i<4;i++){
            View tabView =mTabHost.getTabWidget().getChildTabViewAt(i);
            //RelativeLayout rLayout =(RelativeLayout) tabView.findViewById(R.id.rlayout);
            try {
                ImageView imageView = (ImageView) tabView.findViewById(R.id.tab_image);
                TextView textView = (TextView) tabView.findViewById(R.id.tab_text);
                if(i==currentTab){
                    imageView.setColorFilter(newColor, PorterDuff.Mode.SRC_ATOP);
                    textView.setTextColor(newColor);
                }else {
                    imageView.setColorFilter(defaultColor, PorterDuff.Mode.SRC_ATOP);
                    textView.setTextColor(defaultColor);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void pushFragments(String tag, Fragment fragment,boolean shouldAnimate, boolean shouldAdd){
        Logger.printLog(Logger.ERROR,"fragment name3", "" + fragment.getClass().getSimpleName());
        mCurrentFragment = fragment;
        if(shouldAdd)
            mStacks.get(tag).push(fragment);
        FragmentManager manager         =   getFragmentManager();
        FragmentTransaction ft            =   manager.beginTransaction();
        /*if(shouldAnimate)
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);*/
        ft.replace(R.id.realtabcontent, fragment);
        ft.commit();
    }

    public void popFragments(){
      /*
       *    Select the second last fragment in current tab's stack..
       *    which will be shown after the fragment transaction given below
       */
        Fragment fragment             =   getCurrentFragment();
        mCurrentFragment = fragment;

      /*pop current fragment from stack.. */
        mStacks.get(mCurrentTab).pop();

      /* We have the target fragment in hand.. Just show it.. Show a standard navigation animation*/
        FragmentManager   manager         =   getFragmentManager();
        FragmentTransaction ft            =   manager.beginTransaction();
        ft.replace(R.id.realtabcontent, fragment);
        ft.commit();
    }

    protected Fragment getCurrentFragment(){
        return mStacks.get(mCurrentTab).elementAt(mStacks.get(mCurrentTab).size() - 2);
    }

    @Override
    public void onBackPressed() {
            //if(((BaseFragment)mStacks.get(mCurrentTab).lastElement()).onBackPressed() == false){
            if (mStacks.get(mCurrentTab).size() == 1) {
                Logger.printLog(Logger.ERROR,"print stack size", "is " + mCurrentTab);
                super.onBackPressed();  // or call finish..

            } else {
                Logger.printLog(Logger.ERROR,"print stack size", "is " + mStacks);
                Logger.printLog(Logger.ERROR,"print stack size", "is " + mStacks.get(mCurrentTab).size());
                popFragments();
            }
    }


}
