package com.app.android.shadow.main.apicalls;


/**
 * Created by rails-dev11 on 4/8/15.
 */
public class ApiRequest {

    public String path;
    public int method;
    public boolean authunticate;
    public boolean isDownload;
    public boolean noProgressBar;
    //public boolean isUpload;
    public int reqID;

    public ApiRequest(String path, int method, boolean authunticate, boolean isDownload, boolean noProgressBar,int reqID) {
        this.path = path;
        this.method = method;
        this.authunticate = authunticate;
        this.isDownload = isDownload;
        this.noProgressBar = noProgressBar;
        this.reqID = reqID;
    }

}
