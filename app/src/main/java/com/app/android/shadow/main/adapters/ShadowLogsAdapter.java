package com.app.android.shadow.main.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.adapters.model.ShadowLogsModel;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ShadowLogsAdapter extends ArrayAdapter {

    Context mcontext;
    ArrayList<ShadowLogsModel> lst= new ArrayList<ShadowLogsModel>();
    Resources resources;
    Fragment fragment;

    public ShadowLogsAdapter(Context mcontext, List<ShadowLogsModel> objects) {
        super(mcontext, R.layout.shadow_more_row, objects );
        this.mcontext =mcontext;
        this.lst= (ArrayList<ShadowLogsModel>) objects;
        this.resources = mcontext.getResources();
    }

    @Override
    public int getCount() {
        return lst.size();
    }

    @Override
    public String getItem(int position) {
        //return lst.get(position).get(Const.USERNAME);
        return null;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ShadowLogsModel item = lst.get(position);
        ViewHolderItem viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.shadow_logs_row, parent, false);

            viewHolder = new ViewHolderItem();
            viewHolder.callerName = (TextView)convertView.findViewById(R.id.callerName);
            viewHolder.callType = (TextView)convertView.findViewById(R.id.callType);
            viewHolder.callStatus = (ImageView)convertView.findViewById(R.id.callStatus);
            viewHolder.time = (TextView)convertView.findViewById(R.id.time);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolderItem) convertView.getTag();
        }
        viewHolder.callerName.setText(item.getGet_to_username().toString().trim());
        viewHolder.callType.setText((item.getCall_type().toString().equals("Audio"))?"Audio":"Video");
        viewHolder.callStatus.setImageResource(getImage(item));

        viewHolder.time.setText(getTime(item.getCreated_at()));

        return convertView;
    }

    private String getTime(String created_at) {

        try {  // 2015-12-05T07:24:31.739Z
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'", Locale.ENGLISH);
            Logger.printLog(Logger.ERROR,"Print date "+format.toString());
            Date newDate = format.parse(created_at);
            format = new SimpleDateFormat("dd-MMM-yyyy hh:mm a", Locale.ENGLISH);
            String date = format.format(newDate);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            Logger.printLog(Logger.ERROR,"Parsing to date of Created at "+created_at);
            return "";
        }
    }


    public int getImage(ShadowLogsModel item) {
        if((Integer.parseInt(Shadow.getAppPreferences().getString(Const.ID, "0").trim())) == item.getUser_id()){
            return R.drawable.out_going;
        }else {
            String status = item.getStatus();
            if (status.equals("missed")) {
                return R.drawable.missed;
            } else if (status.equals("rejected")) {
                return R.drawable.rejected;
            } else {//(status.equals("connected")){
                return R.drawable.connected;
            }
        }
    }

    private class ViewHolderItem{
        TextView callerName;
        TextView callType;
        ImageView callStatus;
        TextView time;
    }
}
