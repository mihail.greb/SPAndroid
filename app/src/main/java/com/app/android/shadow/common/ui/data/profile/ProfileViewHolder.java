package com.app.android.shadow.common.ui.data.profile;

import android.view.View;
import android.widget.EditText;

import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.profile.controller.SCProfileViewHolder;

/**
 * This class extends the default SCProfileViewHandler by an additional element - a country selection.
 * An instance of this class is used by the custom {@link ProfileController}
 */
public class ProfileViewHolder extends SCProfileViewHolder
{
	public static final int VD_EDIT_COUNTRY = nextVdIndex();

	private final EditText _editCountry;

	public ProfileViewHolder(final View v, final SCViewDescription d)
	{
		super(v, d);
		_editCountry = (EditText) d.getView(v, VD_EDIT_COUNTRY);
	}

	public EditText getItemEditCountry()
	{
		return _editCountry;
	}

}
