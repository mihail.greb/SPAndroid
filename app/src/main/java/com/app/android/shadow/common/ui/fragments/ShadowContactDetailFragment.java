package com.app.android.shadow.common.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.app.android.shadow.common.ui.controller.ContactDetailController;
import com.app.android.shadow.common.ui.data.friendsList.FriendsController;
import com.app.android.shadow.events.RequestChatHistoryUpdateEvent;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.main.apicalls.ApiRequest;
import com.app.android.shadow.main.apicalls.ServiceHandler;
import com.app.android.shadow.main.provider.CustomC2CallContentProvider;
import com.app.android.shadow.main.tapmanager.extra.MoreController;
import com.app.android.shadow.main.tapmanager.settings.Sounds;
import com.app.android.shadow.main.util.AlertDialogClass;
import com.app.android.shadow.samplechat.SimpleChatFriendsFragment;
import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.R;
import com.app.android.shadow.common.ui.activities.CallActivity;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.custom.rounded.RoundedImageView;
import com.app.android.shadow.main.tapmanager.extra.EventMaker;
import com.app.android.shadow.main.util.BlurEffect;
import com.app.android.shadow.main.util.CallTimeLeft;
import com.app.android.shadow.main.util.CheckLeftTime;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.main.util.Logger;
import com.app.android.shadow.samplechat.board.BoardActivity;
import com.app.android.shadow.utils.custom.StringExtra;
import com.c2call.lib.androidlog.Ln;
//import com.c2call.sdk.lib.data.core.C2CallServiceMediator;

//import com.c2call.sdk.lib.util.BatchDeleteHandler;
//import com.c2call.sdk.lib.util.extra.StringExtra;
//import com.c2call.lib.xml.StringExtra;
//import com.c2call.sdk.lib.n.b;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.core.SCNewMessageCache;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.db.data.SCMediaData;
import com.c2call.sdk.pub.db.util.board.BoardDeleteHandler;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.fragments.SCContactDetailFragment;
import com.c2call.sdk.pub.gui.contactdetail.controller.IContactDetailController;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.services.c2callservice.C2CallService;
import com.c2call.sdk.pub.services.c2callservice.C2CallServiceSlave;
import com.c2call.sdk.pub.util.SimpleAsyncTask;
import com.j256.ormlite.table.TableUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ShadowContactDetailFragment extends SCContactDetailFragment implements OnClickListener {

    private int curPosition;
    private Button setUserRingtone;
    private ImageView close_screen, delete_user;
    private RoundedImageView shadowImageDetail;
    private TextView blockUser, sc_contact_detail_label_name,delete_user_txt;
    private TextView chatMsg;
    private ApiTask mTask;
    private ImageView shadow_message, shadow_video_call, shadow_call;
    private TextView set_online_status;
    Resources resources;
    static long totalTimeInMS = -1;
    public MoreController mCallback;
    boolean iscallMissed = false, isVideoMissed = false;
    private Bitmap backgroundImg = null, userImage = null;
    LinearLayout detailContainerBlur;
    //LinearLayout detailContainer;
    ImageView backgroundCenter;
    private static final float BITMAP_SCALE = 0.4f;
    private static final float BLUR_RADIUS = 7.5f;
    private FriendsController friendsController = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //totalTimeInMS = -1;
        resources = getResources();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            iscallMissed = bundle.getBoolean("sc_extra_call_missed");
            isVideoMissed = bundle.getBoolean("sc_extra_video_missed");
        }
    }

    public static ShadowContactDetailFragment create(String id, int type, int layout,
                                                     Bitmap backgroundImg,
                                                     Bitmap userImage,
                                                     boolean iscallMissed,
                                                     boolean isVideoMissed, int position) {
        Bundle args = new Bundle();
        args.putString(SCExtraData.ContactDetail.EXTRA_DATA_USERID, id);
        args.putInt(SCExtraData.ContactDetail.EXTRA_DATA_USER_TYPE, type);
        args.putInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);
        args.putBoolean("sc_extra_call_missed", iscallMissed);
        args.putBoolean("sc_extra_video_missed", isVideoMissed);
        ShadowContactDetailFragment fragment = new ShadowContactDetailFragment();
        fragment.backgroundImg = backgroundImg;
        fragment.userImage = userImage;
        fragment.setArguments(args);
        fragment.curPosition = position;
        return fragment;
    }

    public ShadowContactDetailFragment() {
        super();
        Logger.printLog(Logger.ERROR,"print details3", "" + "Fragment");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (MoreController) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement Interface");
        }
    }

    public static long getLeftTimeInMs() {
        return totalTimeInMS;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        close_screen = (ImageView) v.findViewById(R.id.close_screen);
        delete_user = (ImageView) v.findViewById(R.id.delete_user);
        delete_user_txt = (TextView) v.findViewById(R.id.delete_user_txt);
        shadowImageDetail = (RoundedImageView) v.findViewById(R.id.shadow_contact_detail_picture);
        setImage();
        Logger.printLog(Logger.ERROR, "curPosition is " + curPosition);
        setUserRingtone = (Button) v.findViewById(R.id.set_user_ringtone);
        sc_contact_detail_label_name = (TextView) v.findViewById(R.id.sc_contact_detail_label_name);
        blockUser = (TextView) v.findViewById(R.id.block_user);
        chatMsg = (TextView) v.findViewById(R.id.chatMsg);
        // detailContainer = (LinearLayout) v.findViewById(R.id.detail_container);
        detailContainerBlur = (LinearLayout) v.findViewById(R.id.detail_container_blur);
        backgroundCenter = (ImageView) v.findViewById(R.id.backgroundCenter);
        blockUser.setOnClickListener(this);
        close_screen.setOnClickListener(this);
        setUserRingtone.setOnClickListener(this);
        delete_user.setOnClickListener(this);
        delete_user_txt.setOnClickListener(this);
        //mCallback.addToIncressClickArea(blockUser);
        mCallback.addToIncressClickArea(close_screen);
        mCallback.addToIncressClickArea(setUserRingtone);
        mCallback.addToIncressClickArea(delete_user);
        mCallback.addToIncressClickArea(delete_user_txt);

        shadow_call = (ImageView) v.findViewById(R.id.shadow_call);
        shadow_video_call = (ImageView) v.findViewById(R.id.shadow_video_call);
        shadow_message = (ImageView) v.findViewById(R.id.shadow_message);
        shadow_call.setOnClickListener(this);
        shadow_video_call.setOnClickListener(this);
        shadow_message.setOnClickListener(this);
        mCallback.addToIncressClickArea(shadow_call);
        mCallback.addToIncressClickArea(shadow_video_call);
        mCallback.addToIncressClickArea(shadow_message);

        set_online_status = (TextView) v.findViewById(R.id.set_online_status);

        if (backgroundImg != null) {
            try {
                Logger.printLog(Logger.ERROR, "backgroundImg is " + backgroundImg.getHeight() + "  w " + backgroundImg.getWidth());
                //Bitmap back = BlurEffect.fastblur(backgroundImg, 0.5f, 70);
                Bitmap back = BlurEffect.fastblur(backgroundImg, 0.5f, 2);
                //Bitmap back = backgroundImg;
                Logger.printLog(Logger.ERROR, "back is " + back.getHeight() + "  w " + back.getWidth());
                back = BlurEffect.getResizedBitmap(back, backgroundImg.getHeight(), backgroundImg.getWidth());
                Logger.printLog(Logger.ERROR, "back2 is " + back.getHeight() + "  w " + back.getWidth());
                backgroundCenter.setImageBitmap(back);


                //(getActivity(), backgroundImg));
                /*if(back == null) {
                    back = new BitmapDrawable(getResources(), backgroundImg);
                    detailContainerBlur.setBackground(resources.getDrawable(R.drawable.overlay));
                }*/
                /*int sdk = Build.VERSION.SDK_INT;
                if(sdk < Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    //detailContainer.setBackgroundDrawable(back);

                    // detailContainer.getBackground().setAlpha(100);
                    // detailContainerBlur.setBackgroundDrawable(resources.getDrawable(R.drawable.overlay));
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                      //  detailContainer.setBackground(back);
                    }else {
                      //  detailContainer.setBackgroundDrawable(back);
                    }
                }*/

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        String username = CustomC2CallContentProvider.customeC2callDB.getUserName(getUserid());
        callApi(new ApiRequest(("users/show_status/" + username), ServiceHandler.GET, true, false, false, APIServiceAsyncTask.ONLINE_STATUS_OF_USER));
        callApi(new ApiRequest("call_histories/" + username, ServiceHandler.PUT, true, false, true, APIServiceAsyncTask.CALL_HISTORY_SEEN_USER));
        return v;
    }

    private void setImage() {
        try {
            if (userImage != null) {
                shadowImageDetail.setImageBitmap(userImage);
            } else {
                setDefaultImage();
            }
        } catch (Exception e) {
            e.printStackTrace();
            setDefaultImage();
        }
    }

    private void setDefaultImage() {
        shadowImageDetail.setImageResource(R.drawable.ic_sc_callbar_user_pic_placeholder);
    }

/*
    public static Bitmap blur(Context ctx, Bitmap image) {
        int width = Math.round(image.getWidth() * BITMAP_SCALE);
        int height = Math.round(image.getHeight() * BITMAP_SCALE);

        Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height, false);
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

        RenderScript rs = RenderScript.create(ctx);
        ScriptIntrinsicBlur theIntrinsic = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        }else {
            return null;
        }
        Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            theIntrinsic.setRadius(BLUR_RADIUS);
            theIntrinsic.setInput(tmpIn);
            theIntrinsic.forEach(tmpOut);
        }
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }*/


    @Override
    protected IContactDetailController onCreateController(View v, SCViewDescription vd) {
        return new ContactDetailController(v, vd);
    }

    @Override
    public void onStart() {
        super.onStart();
        blockUser.setText(getBlockStatus(Shadow.shadowDB.isUserBlocked(getUserid())));
    }

    @Override
    public void onResume() {
        super.onResume();
        //TODO comment time left feature
        //callApi(APIServiceAsyncTask.callTimeLeft);
        checkChatStatus();
        CheckCallStatus();
    }

    private void CheckCallStatus() {
        shadow_call.setImageResource(iscallMissed ? R.drawable.phone_red : R.drawable.phone);
        shadow_video_call.setImageResource(isVideoMissed ? R.drawable.video_red : R.drawable.video);
    }

    private void checkChatStatus() {
        String chatText = "";
        try {
            chatText = CustomC2CallContentProvider.customeC2callDB.getmissedChat(getUserid());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        if (chatText.length() > 0) {
            shadow_message.setImageResource(R.drawable.chat_red);
            chatMsg.setText(chatText);
        } else {
            shadow_message.setImageResource(R.drawable.chat);
        }
    }

    @Override
    public void onClick(View v) {
        SCFriendData data;
        switch (v.getId()) {
            case R.id.delete_user:
                  //  onDeleteclicked();
            break;
            case R.id.delete_user_txt:
                onDeleteclicked();
            break;
            case R.id.set_user_ringtone:
                Bundle bundle = new Bundle();
                bundle.putString(Const.USER, getUserid());
                bundle.putString(Const.TAGS, Const.FRIENDS);
                Fragment f = new Sounds();
                f.setArguments(bundle);
                mCallback.onButtonClickEvent(f, EventMaker.Friends);
                break;

            case R.id.block_user:
                //blockUser(Shadow.shadowDB.isUserBlocked(getUserid()));
                if (Shadow.shadowDB.isUserBlocked(getUserid())) {
                    callApi(APIServiceAsyncTask.blockUserUnblock);
                } else {
                    callApi(APIServiceAsyncTask.blockUser);
                }
                break;
            case R.id.close_screen:
                getActivity().onBackPressed();
                Logger.printLog(Logger.ERROR, "id is " + getUserid());
                break;

            case R.id.shadow_call:
                //TODO comment time left feature
                data = getScFriendData();
                if (data != null) {
                    call(data, false);
                } else {
                    Shadow.alertToast(resources.getString(R.string.no_friend));
                }
                /*if (checkTimeAvailable()) {
                    data = getScFriendData();
                    if (data != null) {
                        call(data, false);
                    } else {
                        Shadow.alertToast(resources.getString(R.string.no_friend));
                    }
                } else {
                    //new AlertDialogClass(getActivity()).open(resources.getString(R.string.alert), resources.getString(Shadow.isProApp()?R.string.you_can_not_call_pro:R.string.you_can_not_call));
                    //new AlertDialogClass(getActivity()).open(resources.getString(R.string.alert), Html.fromHtml(resources.getString(Shadow.isProApp()?R.string.you_can_not_call:R.string.you_can_not_call)));
                    timeOverPopupwindow();
                    Logger.printLog(Logger.ERROR, "No time left of Api not working");
                }*/
                break;
            case R.id.shadow_video_call:
                //TODO comment time left feature
                data = getScFriendData();
                if (data != null) {
                    call(data, true);
                    // Shadow.alertToast("item id " + data.getId());
                    // Shadow.alertToast("item manager " + data.getManager().getDisplayName());
                } else {
                    Shadow.alertToast(getActivity().getResources().getString(R.string.no_friend));
                }
               /* if (checkTimeAvailable()) {
                    data = getScFriendData();
                    if (data != null) {
                        call(data, true);
                        // Shadow.alertToast("item id " + data.getId());
                        // Shadow.alertToast("item manager " + data.getManager().getDisplayName());
                    } else {
                        Shadow.alertToast(getActivity().getResources().getString(R.string.no_friend));
                    }
                } else {
                    //new AlertDialogClass(getActivity()).open(resources.getString(R.string.alert), resources.getString(Shadow.isProApp()?R.string.you_can_not_call_pro:R.string.you_can_not_call));
                    timeOverPopupwindow();
                    Logger.printLog(Logger.ERROR, "No time left of Api not working");
                }*/
                break;

            case R.id.shadow_message:
                data = getScFriendData();
                if (data != null) {
                    //BaseActivity.setSaveTime0();
                    SCNewMessageCache.instance().getNewMessages(data.getId()).clear();
                    SCCoreFacade.instance().postEvent(new RequestChatHistoryUpdateEvent(), false);
                    openChat(data);
                } else {
                    Shadow.alertToast(resources.getString(R.string.no_friend));

                }
                break;
        }
    }

    private void onDeleteclicked() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.app_dlg_confirm_title)
                .setMessage(R.string.app_dlg_confirm_deletion_text)
                .setPositiveButton(R.string.sc_std_btn_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        //callApi(APIServiceAsyncTask.removeFrndtoFrndList);
                        //callDeleteC2call();
                        Logger.printLog(Logger.ERROR, "Detail page user id is " + getUserid() + "   position is " + curPosition);

                        //TODO CHECK REMOVE CODE ERROR (ONES SET ARGUMENT IN FRAGMENT THEN SET FROM OTHER PAGES TOO)
                        // ((BaseSampleActivity) getActivity()).popFragments(curPosition);
                        SimpleChatFriendsFragment.delPosition = curPosition;
                        getActivity().onBackPressed();

                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        dialog.cancel();
                    }
                });
        builder.show();
    }

    public void timeOverPopupwindow(){
        try{
            LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = layoutInflater.inflate(R.layout.shadow_first_time_popup, null);

            LinearLayout.LayoutParams parm1=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            parm1.setMargins(20, 0, 20, 0);
            layout.setLayoutParams(parm1);
            final PopupWindow popup = new PopupWindow(getActivity());

            TextView close = (TextView) layout.findViewById(R.id.ok_popup);
            close.setTextColor(Color.WHITE);
            TextView titleMsg = (TextView) layout.findViewById(R.id.titleMsg);
            titleMsg.setText(resources.getString(R.string.alert));
            TextView msg = (TextView) layout.findViewById(R.id.msg_first_dialog);

            String stText = "";
            String emailText = "";
            if (!Shadow.isProApp()) {
                stText = resources.getString(R.string.you_can_not_call);
                emailText = resources.getString(R.string.you_can_not_call_upgrade_here);
            }else {
                stText = resources.getString(R.string.you_can_not_call_pro);
                emailText = resources.getString(R.string.you_can_not_call_pro_upgrade);
            }
            SpannableString ss = new SpannableString(stText);
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    goToProLink();
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            };
            ss.setSpan(clickableSpan, stText.indexOf(emailText.toString()), (stText.indexOf(emailText.toString())+emailText.toString().length()), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            msg.setText(ss);
            msg.setMovementMethod(LinkMovementMethod.getInstance());
            msg.setHighlightColor(Color.TRANSPARENT);


            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popup.dismiss();
                }
            });

            //popup.setAnimationStyle(R.style.MyCustomDialogTheme);
            popup.setContentView(layout);
            popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
            popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);

            //popup.setFocusable(true);
            BaseFragment.closeSoftKeyboard(popup.getContentView(), getActivity());
            BaseFragment.closeSoftKeyboard(getActivity());
            //popup.setBackgroundDrawable(new BitmapDrawable(getResources()));

            // popup.showAtLocation(layout, Gravity.LEFT | Gravity.TOP, rect.left -
            // v.getWidth(), getDeviceHeight() - rect.top);
            //popup.showAtLocation(layout, Gravity.CENTER, 0, 0);

            new Handler().postDelayed(new Runnable(){

                public void run() {
                    popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
                }

            }, 500L);

        }
        catch (Exception e){e.printStackTrace();
        }
    }

    private void goToProLink() {
      Shadow.alertToast("function is in-progress");
    }

    private boolean checkTimeAvailable() {
            if (totalTimeInMS > 0) {
                return true;
            } else {
                return false;
            }
    }

    private SCFriendData getScFriendData() {
        return getController().getData();
    }

    private void blockUser(boolean setBlock) {
        Logger.printLog(Logger.ERROR,"setBlock ", "" + setBlock);
        if (!setBlock) {

            Logger.printLog(Logger.ERROR,"setBlock ", "unblock run");
            Shadow.shadowDB.deleteBlockUser(getUserid());
            deleteCallHistory(getUserid());
        } else {
            Logger.printLog(Logger.ERROR,"setBlock ", "run block");
            Shadow.shadowDB.addBlockUser(getUserid(), null);
        }
        blockUser.setText(getBlockStatus(setBlock));
    }

    private String getBlockStatus(boolean b) {
        return resources.getString((b ? R.string.unblock_user : R.string.block_user), sc_contact_detail_label_name.getText().toString());
    }

    private void callApi(ApiRequest serviceTask) {
        String userName = CustomC2CallContentProvider.customeC2callDB.getUserName(getUserid());
        mTask = new ApiTask(getActivity());
        mTask.setServiceTaskMapStrParams(serviceTask, getParams(userName, serviceTask.reqID));
        mTask.execute((Void) null);
    }


    class ApiTask extends APIServiceAsyncTask {
        ApiTask(Context mContext) {
            super(mContext);
        }

        @Override
        protected void success(JSONObject jsonObj, int serviceTaskType) {
            if (serviceTaskType == APIServiceAsyncTask.REMOVE_FRND_TO_FRND_LIST) {
                //callDeleteC2call();
            } else if (serviceTaskType == APIServiceAsyncTask.BLOCK_USER) {
                blockUser(true);
            } else if (serviceTaskType == APIServiceAsyncTask.BLOCK_USER_UNBLOCK) {
                blockUser(false);
            } else if (serviceTaskType == APIServiceAsyncTask.BLOCK_USER_LIST) {

            } else if (serviceTaskType == APIServiceAsyncTask.CALL_TIME_LEFT) {
                try {
                    JSONObject timeObj = jsonObj.getJSONObject(Const.DATA);
                    if (Shadow.isProApp()) {
                            if(CallTimeLeft.isDayAvailible(timeObj.getString("call_expiry_date"))){
                            //if(CallTimeLeft.isDayAvailible("2016-02-31T09:27:14.889Z")){
                                totalTimeInMS = CheckLeftTime.convertTime("99:00:00");
                            }else {
                                totalTimeInMS = CheckLeftTime.convertTime("00:00:00");//timeObj.getString("time_left");
                            }
                    } else {
                        totalTimeInMS = CheckLeftTime.convertTime(timeObj.getString("time_left"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (serviceTaskType == APIServiceAsyncTask.ONLINE_STATUS_OF_USER) {
                Logger.printLog(Logger.ERROR, "print online state " + jsonObj.toString());
                try {
                    if (jsonObj.getBoolean("is_online")) {
                        set_online_status.setText("online");
                        set_online_status.setTextColor(BaseActivity.colorGet(R.color.white, getActivity()));
                    } else {
                        setOffline();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    setOffline();
                } /*catch (IllegalStateException e){
                    e.printStackTrace();
                    getActivity().finish();
                }*/
            } else if (serviceTaskType == APIServiceAsyncTask.CALL_HISTORY_SEEN_USER) {
                Logger.printLog(Logger.ERROR, "call seen " + jsonObj.toString());
            }
        }

        private void setOffline() {
            set_online_status.setText("offline");
            set_online_status.setTextColor(getResources().getColor(R.color.red));
        }

        @Override
        protected void failure(JSONObject jsonObj, int serviceTaskType) {
            if (serviceTaskType == APIServiceAsyncTask.REMOVE_FRND_TO_FRND_LIST) {
                Shadow.alertToast("Failure to delete user");
            } else if (serviceTaskType == APIServiceAsyncTask.BLOCK_USER) {
                try {
                    if ((jsonObj.getJSONArray(Const.ERRORS).getString(0)).equals(resources.getString(R.string.user_already_blocked))) {
                        blockUser(true);
                    } else
                        Shadow.alertToast("Failed");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Shadow.alertToast("Failed");
                }
            } else if (serviceTaskType == APIServiceAsyncTask.BLOCK_USER_UNBLOCK) {
                try {
                    if ((jsonObj.getJSONArray(Const.ERRORS).getString(0)).equals(resources.getString(R.string.user_already_unblocked))) {
                        blockUser(false);
                    } else
                        Shadow.alertToast("Failed");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Shadow.alertToast("Failed");
                }
                Shadow.alertToast("Failed");
            } else if (serviceTaskType == APIServiceAsyncTask.BLOCK_USER_LIST) {
                Shadow.alertToast("Failed");
            } else if (serviceTaskType == APIServiceAsyncTask.CALL_TIME_LEFT) {
                Shadow.alertToast("Failed");
            } else if (serviceTaskType == APIServiceAsyncTask.ONLINE_STATUS_OF_USER) {
                setOffline();
            }
        }
    }

    private Map<String, String> getParams(String userId, int serviceTaskType) {
        Map<String, String> params = new HashMap<>();
        if (serviceTaskType == APIServiceAsyncTask.REMOVE_FRND_TO_FRND_LIST) {
            params.put("username", userId);
        } else if (serviceTaskType == APIServiceAsyncTask.BLOCK_USER) {
            params.put("username", userId);
        } else if (serviceTaskType == APIServiceAsyncTask.BLOCK_USER_UNBLOCK) {
            params.put("username", userId);
        } else if(serviceTaskType == APIServiceAsyncTask.BLOCK_USER_LIST) {
                //do nothing
        } else if (serviceTaskType == APIServiceAsyncTask.ONLINE_STATUS_OF_USER) {
                //do nothing
        }
        return params;
    }

    private void callDeleteC2call() {

        /*Set<String> idSet = new HashSet<String>(Arrays.asList(getUserid()));
        final Set friends = idSet;
         //SCSelectionManager.instance().getSelection((String)this.getSelectionKey().first);
        final Set contacts = null;
        //SCSelectionManager.instance().getSelection((String)this.getSelectionKey().second);
        */

        /*
        OLD SDK
        (new SimpleAsyncTask(getActivity(), 0L, "") {
            @Override
            protected Boolean doInBackground(Object[] params) {
                int deleted = (new BatchDeleteHandler()).delete(friends, contacts);
                Ln.d("fc_tmp", "SCFriendsController.onDeleteFriends() - deleted items: %d", new Object[]{Integer.valueOf(deleted)});
                return Boolean.valueOf(true);
            }


            protected void onFinished() {
                //SshadowContactDetailFragment.this.onControllerEvent(new SCListModusChangedEvent(SCListModus.Normal));
                getActivity().onBackPressed();
            }
        }).execute(new Void[0]);*/

        /*

        (new SimpleAsyncTask(this.getActivity(), 0L, "") {
            @Override
            protected Object doInBackground(Object[] params) {
                int var2x = (new b()).a(friends, contacts);
                Ln.d("fc_tmp", "SCFriendsController.onDeleteFriends() - deleted items: %d", new Object[]{Integer.valueOf(var2x)});
                return Boolean.valueOf(true);
            }

            protected void onFinished() {
                //SCFriendsController.this.onControllerEvent(new SCListModusChangedEvent(SCListModus.Normal));
                getActivity().onBackPressed();
            }
        }).execute(new Void[0]);*/
        // TODO CHECK NO NEED TO CALL THIS METHOD
        //((IFriendListItemController)this.getController()).fireEvent(new SCListModusChangedEvent(SCListModus.Delete));


    }


    private void deleteCallHistory(final String userid) {
        (new SimpleAsyncTask(getActivity()) {
            @Override
            protected Integer doInBackground(Object[] params) {
                if (StringExtra.isNullOrEmpty(userid)) {
                    List toDelete = getAllMessageIds(userid);
                    //C2CallServiceMediator.instance().removeCallHistory(toDelete);
                    //com.c2call.sdk.lib.
                    //f.c.b.a().a(toDelete);
                    // TODO CHECK
                    C2CallServiceSlave c2CallServiceSlave = new C2CallServiceSlave(new C2CallService());
                    c2CallServiceSlave.removeCallHistory(toDelete);
                    //C2CallServiceMediator.instance().removeMessageHistory(toDelete);
                    //com.c2call.sdk.lib.
                    // f.c.b.a().b(toDelete);
                    c2CallServiceSlave.removeMessageHistory(toDelete);
                    return Integer.valueOf(0);
                } else {
                    return Integer.valueOf(BoardDeleteHandler.instance().clearHistory(userid));
                }
            }

            protected void onSuccess(Integer result) {
                try {
                    if (StringExtra.isNullOrEmpty(userid)) {
                        TableUtils.clearTable(SCBoardEventData.dao().getConnectionSource(), SCBoardEventData.class);
                        TableUtils.clearTable(SCMediaData.dao().getConnectionSource(), SCMediaData.class);
                    }

                    //((IBoardLoaderHandler)SCBoardController.this.getLoaderHandler()).restart();
                } catch (SQLException var3) {
                    var3.printStackTrace();
                }

                Timer t = new Timer();
                t.schedule(new TimerTask() {
                    public void run() {
                        Ln.d("fc_tmp", "SCBoardContrller.onClearHistoryConfirm() - delayed update event fired.", new Object[0]);
                        SCCoreFacade.instance().updateBoard();
                    }
                }, 5000L);
            }

            protected void onShowSuccess() {
                // ToastExtra.showToast(getActivity().getApplicationContext(), com.c2call.sdk.R.string.sc_msg_chat_history_deleted, 0, 17);
            }

            protected void onShowFailure() {
                //ToastExtra.showToast(getActivity(), com.c2call.sdk.R.string.sc_msg_unknown_error, 0, 17);
            }
        }).execute(new Void[0]);
    }

    private List<String> getAllMessageIds(String userid) {
        try {
            List e = !StringExtra.isNullOrEmpty(userid) ? SCBoardEventData.dao().queryForEq("userid", userid) : SCBoardEventData.dao().queryForAll();
            ArrayList result = new ArrayList(e.size());
            Iterator i$ = e.iterator();

            while (i$.hasNext()) {
                SCBoardEventData cur = (SCBoardEventData) i$.next();
                result.add(cur.getId());
            }

            Ln.d("fc_tmp", "SCBoardController.getAllMessageIds() - userid: %s, msg-ids: %s", new Object[]{userid, Arrays.toString(e.toArray())});
            return result;
        } catch (Exception var5) {
            var5.printStackTrace();
            return new ArrayList();
        }
    }


    /*Calling setup*/

    private void openChat(SCFriendData data) {
        if (Shadow.shadowDB.isUserBlocked(getUserid())) {
            new AlertDialogClass(getActivity()).open(Const.PLEASE_UNBLOCK);
        } else {
            BaseSampleActivity.isAppNotClose = true;
            //mCallback.onButtonClickEvent(ChatBoardBaseFragment.create(data.getId()), EventMaker.Friends);
            final Intent intent = new Intent(getActivity(), BoardActivity.class);
            intent.putExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, R.layout.sc_board);
            intent.putExtra(SCExtraData.Board.EXTRA_DATA_USERID, data.getId());
            Logger.printLog(Logger.ERROR,"Details UserId",data.getId());
            //intent.putExtra(SCExtraData.Board.EXTRA_DATA_FILTER_MASK, data.getFilterMask());
            intent.putExtra(SCExtraData.ContactDetail.EXTRA_DATA_USER_TYPE, data.getUserType());
            intent.putExtra("position", curPosition);
            getActivity().startActivity(intent);
            getActivity().finish();
        }
    }

    private void call(final SCFriendData data, final boolean isVideoEnb) {
        if (Shadow.shadowDB.isUserBlocked(getUserid())) {
            new AlertDialogClass(getActivity()).open(Const.PLEASE_UNBLOCK);
            //TODO call time left check
        } /*else if (ShadowContactDetailFragment.getLeftTimeInMs() <= 0) {
            new AlertDialogClass(getActivity()).open(Const.TIME_LIMIT_OVER);
        }*/ else {
            BaseActivity.setSaveTime0();
            BaseSampleActivity.isAppNotClose = true;
            new SimpleAsyncTask<Boolean>((Activity) getActivity(), 0) {
                @Override
                protected Boolean doInBackground(Void... voids) {
                    boolean isGroup = data.getManager().isGroup();

                    return SCCoreFacade.instance().call(data.getId(), isVideoEnb, isGroup);
                }

                @Override
                protected void onSuccess(Boolean result) {
                    try {
                        CallActivity.isVideo = isVideoEnb;
                        CallActivity.timeleft = totalTimeInMS;
                        CallActivity.isCalling = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    C2CallSdk.startControl().openCallbar(getActivity(), null, data.getId(), R.layout.sc_callbar, StartType.Activity);
                }
            }.execute();
        }
    }
}
