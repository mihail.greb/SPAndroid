package com.app.android.shadow.common.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

//import com.c2call.sdk.lib.c.d.c;

import com.app.android.shadow.common.ui.core.ExtraData;
import com.app.android.shadow.main.BaseActivity;
import com.app.android.shadow.utils.Str;
import com.app.android.shadow.R;
import com.app.android.shadow.events.RegisterSuccessEvent;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.apicalls.APIServiceAsyncTask;
import com.app.android.shadow.main.base.BaseFragment;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.main.util.Logger;
import com.c2call.lib.androidlog.Ln;
//import com.c2call.sdk.lib.c2callclient.util.LoginManager;
//import com.c2call.sdk.lib.c.d;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.fragments.SCLoginFragment;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.login.controller.ILoginController;
import com.c2call.sdk.pub.gui.login.controller.ILoginViewHolder;
import com.c2call.sdk.pub.gui.login.controller.SCLoginController;
import com.c2call.sdk.pub.util.Validate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A customized login fragment. In addition to the standard SCLoginFragment, this one
 * expects an additional argument of an Activtiy  class which should started when the login was successful.
 * This way we can simply reuse the fragment for all the samples in this demo.
 */
public class LoginFragment extends SCLoginFragment
{
    String emailId = "";
    private View _view;
    private View _btnRegister;
    ImageView backBtn;
    TextView done;
    EditText email,pass;
    boolean byGame;
    boolean isPass;
    boolean isLogin = false;
    RelativeLayout hideView;

    public static LoginFragment create(Bundle args)
    {
        final LoginFragment fragment =  new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Bundle args = this.getArguments();
        if(args != null) {
                try {
                    byGame = args.getBoolean(ExtraData.Login.BY_GAME);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    isPass = args.getBoolean(ExtraData.Login.PASS_LOGIN);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    isLogin = Shadow.getAppPreferences().getBoolean(Const.IS_LOGIN, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        SCCoreFacade.instance().subscribe(this);
    }

    @Override
    public void onDestroy()
    {
        SCCoreFacade.instance().unsubscribe(this);
        super.onDestroy();
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState)
    {
        _view = super.onCreateView(inflater, container, savedInstanceState);
        /*onInitChildren(_view);
        onInitActionListeners();*/

        email = (EditText)_view.findViewById(R.id.sc_login_edit_email);
        pass = (EditText)_view.findViewById(R.id.sc_login_edit_password);
        backBtn = (ImageView) _view.findViewById(R.id.back);
        done = (TextView) _view.findViewById(R.id.done);
        hideView = (RelativeLayout)_view.findViewById(R.id.hide_view);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getController().onButtonLoginClick(v);
            }
        });

        if(isPass){
            C2CallSdk.startControl().openRegister(getActivity(), null, 0, null, StartType.Activity);
        }

        if(isLogin && byGame){
            hideView.setVisibility(View.VISIBLE);
        }else {
            hideView.setVisibility(View.GONE);
        }

        return _view;
    }

   /* private void onInitChildren(View v){
        _btnRegister = v.findViewById(R.id.app_login_btn_register);
    }

    private void onInitActionListeners(){
        _btnRegister.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
            C2CallSdk.startControl().openRegister(getActivity(), null, 0, null, StartType.Activity);
            }
        });
    }*/

    @Override
    protected ILoginController onCreateController(final View v, SCViewDescription vd)
    {
         SCLoginController scLoingController =new SCLoginController(v, vd)
        {
          public View vi;
            @Override
            public void onButtonLoginClick(View v) {
                this.vi = v;
                callApi();
            }

            public void customOnButtonLoginClicked() {
                super.onButtonLoginClick(vi);
                try {
                    InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }
                BaseActivity.setSaveTime0();
                BaseFragment.closeSoftKeyboard(this.getContext());

               /* View v1 = getActivity().getCurrentFocus();
                if(v1 != null){
                    closeSoftKeyboard(v1);
                }
                closeSoftKeyboard();
                closeSoftKeyboard(email);
                closeSoftKeyboard(pass);*/

                /*ShadowLoginManager.instance().loginAsyncTask(this.getContext(), this.getUsernameForLogin(), this.getPasswordForLogin(), ((ILoginViewHolder) this.getViewHolder()).isRememberPasswordEnabled(), (String) null, new LoginManager.IResultListener() {
                    public void onResult(int error) {
                        switch (error) {
                            case 0:
                                    startLoginProcess();
                            default:
                        }
                    }
                });*/

               /* c.a().a(this.getContext(), this.getUsernameForLogin(), this.getPasswordForLogin(), ((ILoginViewHolder)this.getViewHolder()).isRememberPasswordEnabled(), (String)null, new c.a() {
                    public void onResult(int var1) {
                        switch(var1) {
                            case 0:
                                startLoginProcess();
                            default:
                        }
                    }
                });*/
            }


            /*void startLoginProcess(){
                onStartMainActivity();
                fireEvent(new SCFinishEvent());
                BaseActivity.setSaveTime0();
                //hideView.setVisibility(View.GONE);
            }*/



            @Override
            protected void onBindViewHolder(ILoginViewHolder viewHolder) {
                super.onBindViewHolder(viewHolder);
                if(isLogin && byGame){
                    //ShadowLoginManager.instance().setLoginValue(true,true);
                    email.setText(Shadow.getAppPreferences().getString(Const.USERNAME, ""));
                    pass.setText(Shadow.getAppPreferences().getString(Const.PASSWORD, ""));
                    customOnButtonLoginClicked();
                    //onButtonLoginClick(v);
                }else {
                    email.setText("");
                    pass.setText("");
                    //Set to testing auto login
                    /*if(Shadow.isTestingOn) {
                        email.setText("test001");
                        pass.setText("qwertyuiop");
                        //onButtonLoginClick(v);
                    }*/
                }
            }

            private void callApi(){
                UserLogin mSignUpTask = new UserLogin(getActivity());
                mSignUpTask.setServiceTaskMapStrParams(APIServiceAsyncTask.login, getLoginParams());
                mSignUpTask.execute((Void) null);
            }

            class UserLogin extends APIServiceAsyncTask {

                UserLogin(Context mContext){
                    super(mContext);
                }

                @Override
                protected void success(JSONObject jsonObj, int serviceTaskType) {
                   // super.success(jsonObj, serviceTaskType);
                    try {
                        String userName;
                        JSONObject data = jsonObj.getJSONObject(Const.DATA);
                        JSONObject user = data.getJSONObject(Const.USER);

                        try{
                            //TODO uncommit to maintain first time logs by API
                            //Shadow.editor.putBoolean(Const.FIRST_TIME_LOGGED, data.getBoolean(Const.FIRST_TIME_LOGGED)).commit();
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        Shadow.editor.putString(Const.AUTH_TOKEN,data.getString(Const.AUTH_TOKEN));
                        Shadow.editor.putString(Const.ID, user.getString(Const.ID));
                        Shadow.editor.commit();
                        userName = user.getString(Const.USERNAME);
                        Logger.printLog(Logger.ERROR,"User is2 " + userName);
                        Shadow.editor.putString(Const.USERNAME,userName).commit();
                        Shadow.editor.putString(Const.PASSWORD,pass.getText().toString()).commit();

                        try {
                            String phone = user.getString(Const.PHONE);
                            if(phone != null)
                                Shadow.editor.putString(Const.PHONE,phone).commit();
                            else
                                Shadow.editor.putString(Const.PHONE,"").commit();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        emailId = user.getString(Const.EMAIL);
                        Shadow.editor.putString(Const.EMAIL, emailId).commit();
                        Shadow.editor.putString(Const.DISPLAY_EMAIL,user.getString(Const.DISPLAY_EMAIL)).commit();
                        Shadow.editor.putString(Const.SHADOW_ID,user.getString(Const.SHADOW_ID)).commit();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    customOnButtonLoginClicked();

                }

                @Override
                protected void failure(JSONObject jsonObj, int serviceTaskType) {
                    super.failure(jsonObj, serviceTaskType);
                    hideView.setVisibility(View.GONE);
                }

                @Override
                protected void failure(String message) {
                    super.failure(message);
                    hideView.setVisibility(View.GONE);
                }
            }

            @Override
            public String getUsernameForLogin() {
                if(isLogin && byGame){
                    emailId = Shadow.getAppPreferences().getString(Const.EMAIL, "");
                    Logger.printLog(Logger.ERROR,"emailid is2 "+emailId);
                    return emailId;
                }
                Logger.printLog(Logger.ERROR, "emailid is " + emailId);
                return emailId;
            }

            @Override
            public void onStartMainActivity()
            {
                LoginFragment.this.onStartMainActivity();
            }

        };
        /*if(byGame){
           // scLoingController.onStartMainActivity();
           // scLoingController.fireEvent(new SCFinishEvent());
           *//* if(isLogin && byGame){
                email.setText(Shadow.getAppPreferences().getString(Const.USERNAME, ""));
                pass.setText(Shadow.getAppPreferences().getString(Const.PASSWORD, ""));
                scLoingController.onButtonLoginClick(getView());
            }*//*
        }*/
        return scLoingController;
    }

        private Map<String, String> getLoginParams(){
            Map<String, String> params = new HashMap<>();
            params.put("user[username]", email.getText().toString().toLowerCase());
            params.put("user[password]", pass.getText().toString().toLowerCase());
            params.put("app_name", Shadow.getAppCodeName().toString());
            return params;
        }


    /**
     * Start the activity that must be  passed as an argument with key ".LOGIN.MAINACTIVITY"
     */
    private void onStartMainActivity()
    {
        Class clsMain = (Class)getArguments().getSerializable(ExtraData.Login.MAIN_ACTIVITY);
        Ln.d("c2app", "LoginFragment.onStartMainActivity() - clas: %s", clsMain);
        Validate.notNull(clsMain, "Argument '"+Shadow.getpackageNameAsStr()+".LOGIN.MAINACTIVITY' must not be empty");
        Intent intent = new Intent(getActivity(), clsMain);
       intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
       /*  intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);*/
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    protected void onControllerPostCreate(final ILoginController controller)
    {
        super.onControllerPostCreate(controller);
        /*
        Some convenience to automatically login when the user presses "enter" on the soft-keyboard while focusing the password field
         */
        ((EditText)controller.getViewHolder().getItemEditPassword()).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId != EditorInfo.IME_ACTION_DONE) {
                    return false;
                }

                boolean isInputValid = !Str.isEmpty(controller.getViewHolder().getUsername(), controller.getViewHolder().getPassword());
                if (isInputValid) {
                    controller.onButtonLoginClick(controller.getViewHolder().getItemButtonLogin());
                }
                return true;
            }
        });
    }

    @SCEventCallback
    private void onEvent(RegisterSuccessEvent evt)
    {
        Ln.d("c2app", "LoginFragment.onEvent() - evt: %s", evt);

        onStartMainActivity();
    }

    @Override
    public void onStop() {
        super.onStop();
        View v1 = getActivity().getCurrentFocus();
        if(v1 != null){
            closeSoftKeyboard(v1);
        }
        closeSoftKeyboard(email);
        closeSoftKeyboard(pass);
        closeSoftKeyboard();
    }

    public void closeSoftKeyboard(){
        BaseFragment.closeSoftKeyboard(getActivity());
    }

    public void closeSoftKeyboard(View et){
        BaseFragment.closeSoftKeyboard(et,getActivity());
    }
}
