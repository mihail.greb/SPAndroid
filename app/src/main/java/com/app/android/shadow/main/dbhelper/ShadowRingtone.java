package com.app.android.shadow.main.dbhelper;

import com.c2call.sdk.pub.db.data.SCBaseData;
import com.c2call.sdk.pub.db.util.core.DatabaseHelper;
import com.c2call.sdk.pub.db.util.core.DefaultSortOrder;
import com.c2call.sdk.pub.db.util.core.ObservableDao;
import com.c2call.sdk.pub.db.util.core.UriPaths;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.simpleframework.xml.Root;

@DatabaseTable(
        tableName = "shadow_ringtone"
)
@UriPaths({"ringtones", "ringtones/#"})
@DefaultSortOrder("assign_to ASC")
@Root(
        strict = false
)


public class ShadowRingtone extends SCBaseData<Long> {
    public static final String ENTITY = "ringtone";
    public static final String ENTITY_PL = "ringtones";
    public static final String ASSIGN_BY ="assign_by";
    public static final String ASSIGN_TO ="assign_to";
    public static final String RINGTONE_PATH="ringtone_path";
    public static final String[] PROJECTION_ALL = new String[]{"_id","assign_to", "assign_by", "ringtone_path"};


    @DatabaseField(
            columnName = "_id",
            generatedId = true
    )
    private Long _id;

    @DatabaseField(
            columnName = "assign_to",
            unique = true
    )
    private String _assign_to;

    @DatabaseField(
            columnName = "assign_by",
            canBeNull = false
    )
    private String _assign_by;

    @DatabaseField(
            columnName = "ringtone_path",
            canBeNull = false
    )
    private String _ringtone_path;



    public ShadowRingtone() {
    }

    public ShadowRingtone(Long _id, String _assign_to, String _assign_by, String _ringtone_path) {
        this._id = _id;
        this._assign_to = _assign_to;
        this._assign_by = _assign_by;
        this._ringtone_path = _ringtone_path;
    }

    public static ObservableDao<ShadowRingtone, Long> dao() {
        return DatabaseHelper.getDefaultDao(ShadowRingtone.class);
    }

    public Long getId() {
        return this._id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    public String get_assign_to() {
        return _assign_to;
    }

    public void set_assign_to(String _assign_to) {
        this._assign_to = _assign_to;
    }

    public String get_assign_by() {
        return _assign_by;
    }

    public void set_assign_by(String _assign_by) {
        this._assign_by = _assign_by;
    }

    public String get_ringtone_path() {
        return _ringtone_path;
    }

    public void set_ringtone_path(String _ringtone_path) {
        this._ringtone_path = _ringtone_path;
    }
}
