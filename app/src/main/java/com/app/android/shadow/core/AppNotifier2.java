package com.app.android.shadow.core;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import com.app.android.shadow.main.custom.call.CustomCallIndication;
import com.app.android.shadow.main.dbhelper.ShadowDB;
import com.app.android.shadow.main.tapmanager.settings.Modes;
import com.app.android.shadow.main.Shadow;
import com.app.android.shadow.main.custom.call.ShadowSCIncomingCallHandler;
import com.app.android.shadow.main.tapmanager.settings.MoreSignature;
import com.app.android.shadow.main.util.Const;
import com.app.android.shadow.R;
import com.app.android.shadow.common.ui.activities.BaseSampleActivity;
import com.app.android.shadow.common.ui.activities.CallActivity;
import com.app.android.shadow.common.ui.activities.LoginActivity;
import com.app.android.shadow.main.util.Logger;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.notifictaions.NotificationType;
import com.c2call.sdk.pub.notifictaions.SCBaseNotification;
import com.c2call.sdk.pub.notifictaions.SCMessageNotification;
import com.c2call.sdk.pub.notifictaions.SCMissedCallNotification;
import com.c2call.sdk.pub.notifictaions.SCNotifier;

//import com.c2call.sdk.lib.util.extra.PreferenceExtra;
//import com.c2call.sdk.lib.util.extra.ResourcesExtra;
//import com.c2call.sdk.lib.n.g.ac;
//import com.c2call.sdk.lib.n.g;


/**
 * Custom Notifier
 */
public class AppNotifier2 extends SCNotifier
{

	public static final String CALL_NOTIFICATION_ID = "c2app_call_notification";


	@Override
	public int getNotifcationIcon(NotificationType notificationType)
	{
		return R.drawable.app_ic_notification;
	}

	@Override
	public void onMessageNotification(final Context context, final SCMessageNotification n)
	{
		Logger.printLog(Logger.ERROR, "display name is " + n.getDisplayName());

		/*Check time of GoToGameScreen*/
		long checkTime = Shadow.getAppPreferences().getLong(Const.SAVE_TIME_LOGOUT, 0);
		long sysCurTime = System.currentTimeMillis();
		Logger.printLog(Logger.ERROR, "checkTime " + checkTime);
		Logger.printLog(Logger.ERROR, "checkTime2  " + sysCurTime);
		if((Shadow.shadowDB.isUserBlockedByName(n.getDisplayName().toString().trim()))
				|| (checkTime != 0 && checkTime < sysCurTime ) || isBackgroundWithSafeMode(context)){
			return ;
		}else {
			//Cover message comment
			/*if(!Modes.getStatus(ShadowDB.IS_SAFE_MODE)
					//&& Modes.getStatus(ShadowDB.UN_COVER_MESSAGE)
					) {*/
			//super.onMessageNotification(context, n);
			//}
			try {
				BaseSampleActivity.__instance.notifyListAdapterOnChat(n);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private boolean isBackgroundWithSafeMode(Context context){
		boolean isSafeMode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
		return ((ShadowSCIncomingCallHandler.isAppIsInBackground(context))&& isSafeMode);
	}

	//cover_message

	@Override
	protected Notification onCreateMessageNotification(Context context, SCMessageNotification n) {
		Logger.printLog(Logger.ERROR,"display name ","is "+n.getDisplayName());
		Ln.d("fc_tmp", "SCNotifier.onCreateMessageNotification() - notification: %s", new Object[]{n});
		String messageSender = null;
		messageSender = this.getMessageSender(context, n);
		PendingIntent intent = this.onCreateMessageIntent(context, n);
		Notification notification;
		if ( Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			notification =(new Notification.Builder(context))
					//.setContentTitle(this.getTitle(context, n))
					.setContentText(this.getMessageText(context, messageSender, n))
					.setTicker(this.getMessageTicker(context, messageSender, n))
					.setSmallIcon(this.getNotifcationIcon(NotificationType.Message))
					//.setWhen(System.currentTimeMillis()).setContentIntent(intent)
					.getNotification();
		}else {
			notification = new Notification.Builder(context)
					//.setContentTitle(this.getTitle(context, n))
					.setContentText(this.getMessageText(context, messageSender, n))
					.setTicker(this.getMessageTicker(context, messageSender, n))
					.setSmallIcon(this.getNotifcationIcon(NotificationType.Message))
					//.setWhen(System.currentTimeMillis()).setContentIntent(intent)
					.build();
		}
		this.setSoundVibrate(context, notification, n);
		notification.flags |= this.getMessageNotificationFlags();
		this.setMessageNotificationLed(notification);
		return notification;
	}

	@Override
	public PendingIntent onCreateMissedCallIntent(final Context context, final SCMissedCallNotification n)
	{
		return null;
	}

	@Override
	public PendingIntent onCreateMessageIntent(final Context context, final SCMessageNotification n)
	{
		final Intent intent = new Intent(context, LoginActivity.class);
		return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}

	@Override
	public void onMissedCallNotification(final Context context, final SCMissedCallNotification n)
	{

	}

	@Override
	public String getTitle(final Context context, final SCBaseNotification notification)
	{
		return context.getString(R.string.app_name);
		// Cover message comment
		//return "";
	}

	/*@Override
	public String getMessageText(Context context, String sender, SCMessageNotification n) {
		return super.getMessageText(context, sender, n);
		// Cover message comment
		//return context.getString(R.string.cover_message_msg);
	}


	@Override
	public String getMessageTicker(Context context, String sender, SCMessageNotification n) {
		return super.getMessageTicker(context, sender, n);
		// Cover message comment
		//return context.getString(R.string.cover_message_msg);
	}*/

	public void callNotification(final Context context, final String userid, final String incomingCaller)
	{
		//Cover message comment
		/*if(!Modes.getStatus(ShadowDB.IS_SAFE_MODE)
				//&& Modes.getStatus(ShadowDB.UN_COVER_MESSAGE)
				) {*/
		if(isBackgroundWithSafeMode(context)){
			final Intent intent = new Intent(context, CallActivity.class);
			intent.putExtra(SCExtraData.Callbar.EXTRA_DATA_ID, userid);
			intent.putExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, R.layout.sc_callbar);
			intent.setAction(Intent.ACTION_VIEW);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			PendingIntent pintent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

			String name = "";
			try {
				name = getMessageText(context,userid,null);
			} catch (Exception e) {
				e.printStackTrace();
			}
			//C2CallSdk.contactResolver().getDisplayNameByUserid(userid, true);

			Notification notification;
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
				notification = new Notification.Builder(context)
						//Cover message uncomment 2 line title and icon
						.setContentTitle(context.getString(R.string.sc_notification_active_call))
						.setSmallIcon(R.drawable.sc_notification_icon_call)
						.setSmallIcon(this.getNotifcationIcon(NotificationType.Message))
						.setContentText(name)
						//.setContentIntent(pintent)
						.setAutoCancel(true)
						.getNotification();
			} else {
				notification = new Notification.Builder(context)
						//Cover message uncomment 2 line title and icon
						.setContentTitle(context.getString(R.string.sc_notification_active_call))
						.setSmallIcon(R.drawable.sc_notification_icon_call)
						.setSmallIcon(this.getNotifcationIcon(NotificationType.Message))
						.setContentText(name)
						//.setContentIntent(pintent)
						.setAutoCancel(true)
						.build();
			}
			//this.setSoundVibrateCall(context,notification);
			notification.flags = Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;

			final int notificationId = getNotificationId(CALL_NOTIFICATION_ID);

			NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

			nm.notify(notificationId, notification);
		}
	}

	protected int getMessageNotificationFlags() {
		return 17;
	}

	protected void setMessageNotificationLed(Notification notification) {
		notification.ledARGB = -16711936;
		notification.ledOnMS = 300;
		notification.ledOffMS = 1000;
	}

	private void setSoundVibrateCall(Context context, Notification notification) {
		//boolean mode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);
		//if(isVibrate(mode)){
		try {
			notification.vibrate = (new long[] { 1000, 1000, 1000, 1000, 1000 });
		}catch (Exception e){
			notification.defaults |= Notification.DEFAULT_VIBRATE;
		}
		//}
	}

	private void setSoundVibrate(Context context, Notification notification,SCMessageNotification n) {
		boolean mode = Modes.getStatus(ShadowDB.IS_SAFE_MODE);

		if(isVibrate(mode)){
			try {
				notification.vibrate = (new long[] { 1000, 1000, 1000, 1000, 1000 });
			}catch (Exception e){
				notification.defaults |= Notification.DEFAULT_VIBRATE;
			}
		}
		if(isRing(mode)){
			ShadowDB db = Shadow.shadowDB;

			String userTone = db.getRingTone(n.getDisplayName().toString().trim(), MoreSignature.MessageRingtone,true);
			String appTone = db.getRingTone(Const.DEFAULT_USER_ID,MoreSignature.MessageRingtone,true);
			if(userTone != null ){
				notification.sound = Uri.parse(userTone);
				return ;
			}else if(appTone != null){
				notification.sound = Uri.parse(appTone);
				return ;
			}else {
				try {
					//String e = PreferenceExtra.getString(context, com.c2call.sdk.R.string.key_ringtoneMessage, (String) null);
					String e = null;
					try {
						// TODO CHECK ADD CODE
						//e = ac.a(context, com.c2call.sdk.R.string.key_ringtoneCall, (String) null);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					if (e != null) {
						Ln.d("fc_tmp", "SCNotifier.setSound() - uri: %s (byPref)", new Object[]{e});
						notification.sound = Uri.parse(e);
					} else {
						e = CustomCallIndication.getRawResourceUri("sc_ringtone_msg");
						Ln.d("fc_tmp", "SCNotifier.setSound() - uri: %s (byRes)", new Object[]{e});
					}

					if (e != null) {
						notification.sound = Uri.parse(e);
					} else {
						Ln.d("fc_tmp", "SCNotifier.setSound() - set default sound.", new Object[0]);
						notification.defaults |= 1;
					}
				} catch (Exception var4) {
					var4.printStackTrace();
					notification.defaults |= 1;
				}
			}
		}else {
			return ;
		}
	}

	private boolean isRing(boolean mode){
		/*if(!Shadow.isProApp()){
			return true;
		}else*/
		return (mode) ? false : Modes.getStatus(ShadowDB.UN_SOUND_ON_CHAT);
	}

	private boolean isVibrate(boolean mode){
		/*if(!Shadow.isProApp()){
			return false;
		}else*/
		return Modes.getStatus((mode) ? ShadowDB.VIBRATE_FOR_CHAT:ShadowDB.UN_VIBRATE_ON_CHAT);
	}
}
