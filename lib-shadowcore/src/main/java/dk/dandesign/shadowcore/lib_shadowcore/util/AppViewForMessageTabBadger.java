package dk.dandesign.shadowcore.lib_shadowcore.util;


import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.c2call.lib.androidlog.Ln;

import dk.dandesign.shadowcore.lib_shadowcore.views.BadgeView;

public class AppViewForMessageTabBadger
{
    private static AppViewForMessageTabBadger __instance = new AppViewForMessageTabBadger();

    public static AppViewForMessageTabBadger instance()
    {
        return __instance;
    }

    private AppViewForMessageTabBadger()
    {

    }

    private BadgeView _badgeTab;
    private int _badgeNumber = 0;

    public void setViewForTab(Context context, View view)
    {
        _badgeTab = new BadgeView(context, view);
        _badgeTab.setBadgeMargin(5, 5);
        _badgeTab.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
    }


    public void setBadgeNumber(int number)
    {
        _badgeNumber = number;
        showBadgeForTab();
    }

    public void setBadge(TextView v, int number)
    {
        v.setText("" + number);
        if (number > 0) {
            v.setVisibility(View.VISIBLE);
        }
        else{
            v.setVisibility(View.GONE);
        }
    }

    public void showBadgeForTab()
    {
        if (_badgeTab == null)
        {
            Ln.d("fc_test", "_badgeTab is null");
            return;
        }

        if (_badgeNumber > 0)
        {
            _badgeTab.setText(String.valueOf(_badgeNumber));
            _badgeTab.show();
        } else
        {
            _badgeTab.hide();
        }
    }


}

