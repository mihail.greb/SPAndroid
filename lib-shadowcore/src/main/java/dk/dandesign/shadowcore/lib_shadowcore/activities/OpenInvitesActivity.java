package dk.dandesign.shadowcore.lib_shadowcore.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.c2call.sdk.pub.common.SCInvitedItem;
import com.c2call.sdk.pub.common.SCInvitedList;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.facade.SCMediaFacade;
import com.c2call.sdk.pub.util.SimpleAsyncTask;

import java.util.ArrayList;
import java.util.List;

import dk.dandesign.shadowcore.lib_shadowcore.R;

public class OpenInvitesActivity extends Activity {

    ListView listView ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_invites);

        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.open_invites_list);


        final Activity context = this;

        new SimpleAsyncTask<ArrayList<Invitation>>(this)
        {
            @Override
            protected ArrayList<Invitation> doInBackground(Void... voids)
            {
                SCInvitedList list = SCCoreFacade.instance().listRequestedInvitations();
                ArrayList<Invitation> invitations = new ArrayList<Invitation>();

                for (SCInvitedItem item : list.get()) {
                    invitations.add(new OpenInvitesActivity.Invitation(item));
                }

                return invitations;
            }

            @Override
            protected void onSuccess(ArrayList< Invitation > invitations)
            {

                if (invitations.size() == 0) {
                    onShowNoResulstDialog();
                    return;
                }

                OpenInvitesItemAdapter adapter = new OpenInvitesItemAdapter(context, R.layout.friend_requests_listitem, invitations);

                // Assign adapter to ListView
                listView.setAdapter(adapter);
            }
        }.execute();


    }

    protected void onShowNoResulstDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_title_noinvites)
                .setMessage(R.string.dialog_message_noinvites)
                .setPositiveButton(R.string.sc_std_btn_ok, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which)
                    {
                        finish();
                    }
                });

        builder.show();
    }

    public class OpenInvitesItemAdapter extends ArrayAdapter<OpenInvitesActivity.Invitation> {

        public OpenInvitesItemAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Invitation> objects) {
            super(context, resource, objects);
        }

        @Override
        public void remove(@Nullable OpenInvitesActivity.Invitation object) {
            super.remove(object);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;

            LayoutInflater inflater = ((Activity)getContext()).getLayoutInflater();
            row = inflater.inflate(R.layout.open_invites_listitem, parent, false);

            ImageView imageView = (ImageView)row.findViewById(R.id.listitem_userimage);
            TextView username = (TextView) row.findViewById(R.id.listitem_username);
            TextView comment = (TextView) row.findViewById(R.id.listitem_comment);

            Button revoke = (Button) row.findViewById(R.id.btnRevoke);

            final OpenInvitesActivity.Invitation invitation = getItem(position);

            SCFriendData profile = invitation.getProfile();
            if (profile != null) {
                username.setText(profile.getDisplayName());
            }


            if (invitation.getUserimage() != null) {
                imageView.setImageBitmap(invitation.getUserimage());
            }

            comment.setText("Hey, I would like to add you on Shadow");
            if (invitation.getInvitedItem().getComment() != null) {
                comment.setText(invitation.getInvitedItem().getComment());
            }


            revoke.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        SCCoreFacade.instance().revokeInviteForId(invitation.getInvitedItem().getId());
                        remove(invitation);
                        notifyDataSetChanged();
                    } catch (Exception e) {

                    }

                }
            });

            return row;
        }
    }

    protected class Invitation {
        protected SCInvitedItem     invitedItem;
        protected SCFriendData      profile;
        protected Bitmap userimage;

        public Invitation(SCInvitedItem invitedItem) {
            this.invitedItem = invitedItem;
            this.profile = SCCoreFacade.instance().getUserInfo(invitedItem.getInvitedUserid());

            if (this.profile != null) {
                userimage = SCMediaFacade.instance().getUserPicture(this.profile, false, R.drawable.ic_sc_std_picture_user_src, 30);
            }
        }

        public Bitmap getUserimage() {
            return userimage;
        }

        public SCInvitedItem getInvitedItem() {
            return invitedItem;
        }

        public SCFriendData getProfile() {
            return profile;
        }
    }

}
