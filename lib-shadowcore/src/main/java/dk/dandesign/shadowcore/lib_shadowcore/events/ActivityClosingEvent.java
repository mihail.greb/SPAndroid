package dk.dandesign.shadowcore.lib_shadowcore.events;


import com.c2call.sdk.pub.eventbus.events.SCSingleValueEvent;

public class ActivityClosingEvent extends SCSingleValueEvent<String> {

    public ActivityClosingEvent(final String userid) {
        super(userid);
    }
}
