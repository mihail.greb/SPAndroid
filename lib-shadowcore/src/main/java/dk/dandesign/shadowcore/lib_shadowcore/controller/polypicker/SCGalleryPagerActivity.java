package dk.dandesign.shadowcore.lib_shadowcore.controller.polypicker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.c2call.sdk.pub.gui.videorecord.statehandler.SCActivityStateHandler;
import com.c2call.sdk.pub.gui.videorecord.statehandler.SCState;

import java.util.List;

public class SCGalleryPagerActivity extends Activity
{
    public static final String EXTRA_ITEMS = "com.c2call.sdk.SCGalleryPagerActivity.items";
    public static final String EXTRA_LIMIT = "com.c2call.sdk.SCGalleryPagerActivity.limit";
    
    @SCState
    private List<String> _items;
    
    private SCGalleryPager _galleryPager;
    private int _limit;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        _items = getIntent().getStringArrayListExtra(EXTRA_ITEMS);
        _limit = getIntent().getIntExtra(EXTRA_LIMIT, Integer.MAX_VALUE);
        Log.d("tmp", "SCGalleryPagerActivity.onCreate() - items: " + _items.size());
        
        _galleryPager = new SCGalleryPager(this);
        setContentView(_galleryPager);
        
        _galleryPager.setLimit(_limit);
        if (savedInstanceState == null) {
            _galleryPager.setFiles(_items);
        }
    }
    
    protected void finishWithResult()
    {
        List<SCImageAndDescription> result = _galleryPager.getImageAndDescriptionList();

        Intent intent = new Intent();
        intent.putExtra(EXTRA_ITEMS, result.toArray(new SCImageAndDescription[result.size()]));
        setResult(Activity.RESULT_OK, intent);
        finish();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        SCActivityStateHandler.saveInstanceState(this, outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        SCActivityStateHandler.restoreInstanceState(this, savedInstanceState);
        _galleryPager.setFiles(_items);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        new SCPolyPickerResultHandler().onActivityResult(requestCode, resultCode, intent, new SCPolyPickerResultHandler.ResultListener()
        {
            @Override
            public void onResult(List<String> result)
            {
                _items = result;
                _galleryPager.setFiles(result);
            }
        });

    }

    
}
