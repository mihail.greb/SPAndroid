package dk.dandesign.shadowcore.lib_shadowcore.events;

import android.app.Activity;

/**
 * Created by tgreinert on 24.10.14.
 */
public class BackPressedEvent
{
    private Activity _source;

    public BackPressedEvent(Activity source)
    {
        _source = source;
    }

    public Activity getSource()
    {
        return _source;
    }

    @Override
    public String toString()
    {
        return "BackPressedEvent{" +
               "_source=" + _source +
               '}';
    }
}
