package dk.dandesign.shadowcore.lib_shadowcore.controller.chathistory;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.adapter.SCBoardCursorAdapter;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemControllerFactory;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemType;
import com.c2call.sdk.pub.gui.boardlistitem.decorator.SCBoardListItemBaseDecorator;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import dk.dandesign.shadowcore.lib_shadowcore.R;

public class ChatHistoryCursorAdapter extends SCBoardCursorAdapter
{

    @SuppressWarnings("rawtypes")
    public static  Map<SCBoardListItemType, SCBoardListItemBaseDecorator> DECORATOR_MAP = new TreeMap<SCBoardListItemType, SCBoardListItemBaseDecorator>();
    private static final SimpleDateFormat __dateFormat = new SimpleDateFormat(C2CallSdk.context().getString(R.string.sc_date_pattern_without_time));

    static
    {
        DECORATOR_MAP.put(SCBoardListItemType.Call, new ChatHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Text, new ChatHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Image, new ChatHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Video, new ChatHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Audio, new ChatHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Location, new ChatHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Friend, new ChatHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.File, new ChatHistoryItemDecorator());
    }


    public static interface IViewInflator
    {
        public abstract View onInflateView(ChatHistoryCursorAdapter adapter, LayoutInflater inflater, Cursor cursor, SCBoardEventData data, ViewGroup parent);
    }


    private long _currentGroupValue;

    private IViewInflator _viewInflator;

    @SuppressWarnings("rawtypes")
    public ChatHistoryCursorAdapter(final Context context,
                                    final Cursor c,
                                    final IBoardListItemControllerFactory medatorFactory,
                                    final SCViewDescriptionMap viewDescriptions,
                                    final Map<SCBoardListItemType, SCBoardListItemBaseDecorator> decorators,
                                    final int flags)
    {
        super(context,
              c,
              R.layout.sc_std_list_header,
              R.id.list_header_title, medatorFactory,
              viewDescriptions,
              decorators,
              flags);
    }

    public void setViewInflator(IViewInflator viewInflator)
    {
        _viewInflator = viewInflator;
    }

    @Override
    protected View onInflateView(final LayoutInflater inflater, final Cursor cursor, final SCBoardEventData data, final ViewGroup parent)
    {
        if (_viewInflator != null){
            return _viewInflator.onInflateView(this, inflater, cursor, data, parent);
        }


        int layout = 0;
        final int type = getItemViewType(cursor);
        switch(type){
            case TYPE_IN_TEXT:
            case TYPE_OUT_TEXT:
                layout = R.layout.app_chathistory_list_item; break;
            default:
                layout = R.layout.app_chathistory_list_item; break;
        }


        return inflater.inflate(layout, parent, false);
    }

    @Override
    protected void onPreCalculateSectionHeaders()
    {
        _currentGroupValue = 0;
    }

    @Override
    protected String getGroupName(final Cursor c, final int groupColumn)
    {
        final SCBoardEventData data = onCreateData(c);


        return  __dateFormat.format(new Date(data.getTimestamp()));
    }
}
