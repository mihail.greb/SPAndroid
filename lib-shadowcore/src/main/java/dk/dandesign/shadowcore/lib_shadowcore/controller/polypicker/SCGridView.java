package dk.dandesign.shadowcore.lib_shadowcore.controller.polypicker;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.GridView;

import dk.dandesign.shadowcore.lib_shadowcore.R;


public class SCGridView extends GridView
{
    private final int maxHeight;

    public SCGridView(Context context) {
        this(context, null);
    }

    public SCGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SCGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.SCGridView);
            maxHeight = a.getDimensionPixelSize(R.styleable.SCGridView_maxHeight, Integer.MAX_VALUE);
            a.recycle();
        } else {
            maxHeight = 0;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int measuredHeight = MeasureSpec.getSize(heightMeasureSpec);
        if (maxHeight > 0 && maxHeight < measuredHeight) {
            int measureMode = MeasureSpec.getMode(heightMeasureSpec);
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(maxHeight, measureMode);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
