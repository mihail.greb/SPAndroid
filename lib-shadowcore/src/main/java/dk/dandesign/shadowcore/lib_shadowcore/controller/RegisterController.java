package dk.dandesign.shadowcore.lib_shadowcore.controller;

import android.app.Activity;
import android.text.Editable;
import android.view.View;
import android.widget.TextView;

import com.c2call.sdk.pub.common.SCRegistrationData;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.register.controller.SCRegisterController;

import dk.dandesign.shadowcore.lib_shadowcore.fragments.RegisterFragment;

/**
 * Created by mikel on 13.05.17.
 */

public class RegisterController extends SCRegisterController {

    protected boolean isValid = false;


    public RegisterController(View view, SCViewDescription scViewDescription, SCRegistrationData data) {
        super(view, scViewDescription, data);
    }

    @Override
    public void onCreate(Activity activity, SCActivityResultDispatcher scActivityResultDispatcher) {
        super.onCreate(activity, scActivityResultDispatcher);

    }

    @Override
    public boolean onValidateInput()
    {
        if (getViewHolder() == null){
            return false;
        }

        boolean isValid = true;


        final String email = getText(getViewHolder().getItemEditEmail());
        final String pass = getText(getViewHolder().getItemEditPassword());
        final String passRetype = getText(getViewHolder().getItemEditPasswordRetype());


        if (email == null || email.length() < 4){
            setError(getViewHolder().getItemEditEmail(), "Please enter a valid username");
            isValid = false;
        }
        else{
            setError(getViewHolder().getItemEditEmail(), null);
        }

        if (pass == null || pass.length() < 6){
            setError(getViewHolder().getItemEditPassword(), "Password required, min 6 character");
            isValid = false;
        }
        else{
            setError(getViewHolder().getItemEditPassword(), null);
        }

        if (passRetype == null || passRetype.length() < 6 || !passRetype.equals(pass)){
            setError(getViewHolder().getItemEditPasswordRetype(), "Password not the same");
            isValid = false;
        }
        else{
            setError(getViewHolder().getItemEditPasswordRetype(), null);
        }

        if (isValid){
            onInputValid();
        }
        else{
            onInputInvalid();
        }

        final boolean validityChanged = (this.isValid != isValid);

        this.isValid = isValid;

        if (validityChanged){
            onInputValidityChanged();
        }
        return isValid;
    }

    @Override
    public void onEditEmailChanged(final TextView v, final Editable text)
    {
        if (getData() == null) {
            return;
        }

        getData().setFirstName(text.toString());
        getData().setEmail(text.toString() + "-shadow@dandesign.dk");

        onValidateInput();
    }

    @Override
    public void onRegisterSuccess() {
        ((RegisterFragment)getFragment()).login(getData());
    }
}
