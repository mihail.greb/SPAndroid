package dk.dandesign.shadowcore.lib_shadowcore.controller;

import android.app.Activity;
import android.view.View;

import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.login.controller.SCLoginController;

/**
 * Created by mikel on 13.05.17.
 */

public class LoginController extends SCLoginController {

    public LoginController(View view, SCViewDescription scViewDescription) {
        super(view, scViewDescription);
    }

    @Override
    public String getUsernameForLogin() {
        return super.getUsernameForLogin() + "-shadow@dandesign.dk";
    }

    @Override
    public void onStartMainActivity() {
        getContext().setResult(Activity.RESULT_OK);
        finish();
    }
}

