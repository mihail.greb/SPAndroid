package dk.dandesign.shadowcore.lib_shadowcore.dialogs;

import android.widget.Toast;

import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.contactdetail.controller.SCContactDetailController;
import com.c2call.sdk.pub.gui.dialog.BaseItemRunnable;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.util.SimpleAsyncTask;

import dk.dandesign.shadowcore.lib_shadowcore.R;

public class DialogBuilderFriendMenu
{

    public static class VideoRunnable extends BaseItemRunnable<SCContactDetailController>
    {
        public VideoRunnable(final SCContactDetailController controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            final SCChoiceDialog dlg = C2CallSdk.dialogFactory().createVideoDialog(getController(), null);
            if (dlg != null) {
                dlg.show();
            }

        }
    }


    public static class VoiceRunnable extends BaseItemRunnable<SCContactDetailController>
    {
        public VoiceRunnable(final SCContactDetailController controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            C2CallSdk.startControl().openAudioRecord(getController().getContext(), getController().getFragment(), null, com.c2call.sdk.R.layout.sc_audio_record, null, StartType.Activity);
//            final String filename = UUID.randomUUID().toString().replace("-", "");
//            final String output = SCMediaFacade.instance().getMediaPath(SCDownloadType.Image, filename);
//
//
//            final Intent intent = new Intent(getContext(), AudioRecordFragmentActivity.class);
//            intent.putExtra(SCExtraData.AudioRecord.EXTRA_DATA_OUTPUT_PATH, output);
//
//            startActivityForResult(intent, SCActivityRequestCodes.RECORD_AUDIO);
//            getContext().overridePendingTransition(R.anim.slide_in_right,
//                    android.R.anim.slide_out_right);
        }
    }

    public static class SendCallmeRunnable extends BaseItemRunnable<SCContactDetailController>
    {
        public SendCallmeRunnable(final SCContactDetailController controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            new SimpleAsyncTask<Boolean>(getController().getContext())
            {
                @Override
                protected Boolean doInBackground(Void... voids)
                {
                    return SCCoreFacade.instance().sendCallmeMessage(getController().getUserid());
                }

                @Override
                protected void onSuccess(Boolean aBoolean)
                {
                    Toast.makeText(getContext(), R.string.callme_notification_sent_success, Toast.LENGTH_SHORT).show();
                }
            }.execute();
        }
    }

    public static class AddFavRunnable extends BaseItemRunnable<SCContactDetailController>
    {
        public AddFavRunnable(final SCContactDetailController controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            getController().toggleFavorite();
        }
    }

    public static class AddGroupRunnable extends BaseItemRunnable<SCContactDetailController>
    {
        public AddGroupRunnable(final SCContactDetailController controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {

        }
    }


    public static SCChoiceDialog build(final SCContactDetailController controller)
    {
        final SCChoiceDialog.Builder builder = new SCChoiceDialog.Builder(controller.getContext(), R.layout.sc_std_one_line_list_row);

        builder.addItem(R.string.sc_dlg_offline_action_audio,
                        0,
                        0, //mic,
                        new VoiceRunnable(controller));

        builder.addItem(R.string.sc_dlg_offline_action_video,
                        0,
                        0,//R.drawable.sc_device_access_video,
                        new VideoRunnable(controller));


        builder.addItem(R.string.sc_dlg_offline_action_callme,
                        0,
                        0, //R.drawable.app_ic_chat,
                        new SendCallmeRunnable(controller));



        return builder.build();
    }
}
