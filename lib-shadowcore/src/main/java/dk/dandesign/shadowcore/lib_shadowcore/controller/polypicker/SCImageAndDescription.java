package dk.dandesign.shadowcore.lib_shadowcore.controller.polypicker;

import android.os.Parcel;
import android.os.Parcelable;

public class SCImageAndDescription
        implements Parcelable
{
    public String image;
    public String description;


   
    
    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("SCImageAndDescription{");
        sb.append("image='").append(image).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.image);
        dest.writeString(this.description);
    }

    public SCImageAndDescription()
    {
    }

    protected SCImageAndDescription(Parcel in)
    {
        this.image = in.readString();
        this.description = in.readString();
    }

    public static final Creator<SCImageAndDescription> CREATOR = new Creator<SCImageAndDescription>()
    {
        public SCImageAndDescription createFromParcel(Parcel source)
        {
            return new SCImageAndDescription(source);
        }

        public SCImageAndDescription[] newArray(int size)
        {
            return new SCImageAndDescription[size];
        }
    };
}
