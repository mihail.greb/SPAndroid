package dk.dandesign.shadowcore.lib_shadowcore.events;


public class ChatHistoryUpdateEvent
{
    private int _count;

    public ChatHistoryUpdateEvent(int count)
    {
        _count = count;
    }

    public int getCount()
    {
        return _count;
    }

    public void setCount(int count)
    {
        _count = count;
    }

    @Override
    public String toString()
    {
        return "ChatHistoryUpdateEvent{" +
               "_count=" + _count +
               '}';
    }
}
