package dk.dandesign.shadowcore.lib_shadowcore.events;

/**
 * Created by tgreinert on 25.11.14.
 */
public class CallHistoryUpdateEvent
{
    private int _count;

    public CallHistoryUpdateEvent(int count)
    {
        _count = count;
    }

    public int getCount()
    {
        return _count;
    }

    public void setCount(int count)
    {
        _count = count;
    }

    @Override
    public String toString()
    {
        return "CallHistoryUpdateEvent{" +
               "_count=" + _count +
               '}';
    }
}
