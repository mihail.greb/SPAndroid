package dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.adapter.SCBoardCursorAdapter;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemControllerFactory;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemType;
import com.c2call.sdk.pub.gui.boardlistitem.decorator.SCBoardListItemBaseDecorator;
import com.c2call.sdk.pub.gui.boardlistitem.decorator.SCBoardListItemRichDecorator;
import com.c2call.sdk.pub.gui.boardlistitem.decorator.SCBoardListItemTextDecorator;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import dk.dandesign.shadowcore.lib_shadowcore.R;

public class CallHistoryCursorAdapter extends SCBoardCursorAdapter
{

    @SuppressWarnings("rawtypes")
    public static Map<SCBoardListItemType, SCBoardListItemBaseDecorator> DECORATOR_MAP = new TreeMap<SCBoardListItemType, SCBoardListItemBaseDecorator>();
    private static final SimpleDateFormat __dateFormat = new SimpleDateFormat(C2CallSdk.context().getString(R.string.sc_date_pattern_without_time));

    static
    {
        DECORATOR_MAP.put(SCBoardListItemType.Call, new CallHistoryItemDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Text, new SCBoardListItemTextDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Image, new SCBoardListItemRichDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Video, new SCBoardListItemRichDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Audio, new SCBoardListItemRichDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Location, new SCBoardListItemRichDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.Friend, new SCBoardListItemRichDecorator());
        DECORATOR_MAP.put(SCBoardListItemType.File, new SCBoardListItemRichDecorator());
    }



    public static interface IViewInflator
    {
        public abstract View onInflateView(CallHistoryCursorAdapter adapter, LayoutInflater inflater, Cursor cursor, SCBoardEventData data, ViewGroup parent);
    }

    private IViewInflator _viewInflator;

    @SuppressWarnings("rawtypes")
    public CallHistoryCursorAdapter(final Context context,
                                    final Cursor c,
                                    final IBoardListItemControllerFactory medatorFactory,
                                    final SCViewDescriptionMap viewDescriptions,
                                    final Map<SCBoardListItemType, SCBoardListItemBaseDecorator> decorators,
                                    final int flags)
    {
        super(context, c, medatorFactory, viewDescriptions, decorators, flags);
    }

    public void setViewInflator(IViewInflator viewInflator)
    {
        _viewInflator = viewInflator;
    }

    @Override
    protected View onInflateView(final LayoutInflater inflater, final Cursor cursor, final SCBoardEventData data, final ViewGroup parent)
    {
        if (_viewInflator != null){
            return _viewInflator.onInflateView(this, inflater, cursor, data, parent);
        }

        int layout = 0;
        final int type = getItemViewType(cursor);
        switch(type){
            case TYPE_IN_CALL:
            case TYPE_OUT_CALL:
                layout = R.layout.sc_callhistory_list_item; break;
            default:
                layout = R.layout.sc_callhistory_list_item; break;
                /*
                Ln.e("fc_error", "* * * Error: BoardCursorAdapter.newView() - unknown type: %d", type);
                Debug.ensure(false, "unhandled type: " + type);
                return null;
                */
        }

        return inflater.inflate(layout, parent, false);
    }


    @Override
    protected String getGroupName(final Cursor c, final int groupColumn)
    {
        final SCBoardEventData data = onCreateData(c);


        return  __dateFormat.format(new Date(data.getTimestamp()));
    }
}
