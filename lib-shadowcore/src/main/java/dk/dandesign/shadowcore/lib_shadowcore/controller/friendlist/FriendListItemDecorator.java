package dk.dandesign.shadowcore.lib_shadowcore.controller.friendlist;

import com.c2call.lib.xml.StringExtra;
import com.c2call.sdk.pub.common.SCOnlineStatus;
import com.c2call.sdk.pub.gui.core.common.SCListModus;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemController;
import com.c2call.sdk.pub.gui.friendlistitem.decorator.SCFriendListItemDecorator;

import dk.dandesign.shadowcore.lib_shadowcore.R;

public class FriendListItemDecorator extends SCFriendListItemDecorator
{

    @Override
    protected int getStatusTextColor(IFriendListItemController controller, SCOnlineStatus status)
    {

        if (status == null){
            return 0;
        }

        return status.isOnline()
                    ? controller.getContext().getResources().getColor(R.color.app_blue)
                    : controller.getContext().getResources().getColor(R.color.app_grey);
    }

    @Override
    protected int getOnlineActiveIcon()
    {
        return R.drawable.sc_std_icon_online;
    }

    @Override
    protected int getOnlineIcon()
    {
        return R.drawable.sc_std_icon_online;
    }

    @Override
    protected void onDecorateFavoriteIcon(IFriendListItemController iFriendListItemController) {
        if (iFriendListItemController == null){
            return ;
        }

        final String id = iFriendListItemController.getData().getId();

        int res = 0;

        if (StringExtra.isNullOrEmpty(id)
                || iFriendListItemController.getModus() != SCListModus.Normal)
        {
            res = R.drawable.sc_std_icon_star_small_off;
        }
        else{
            final boolean isFavorite = iFriendListItemController.getData().getManager().isFavorite();
            if (isFavorite){
                res = R.drawable.sc_std_icon_star_small_on;
            }
            else {
                res = R.drawable.sc_std_icon_star_small_off;
            }
        }

        setImageResource(iFriendListItemController.getViewHolder().getItemFavoriteIcon(), res);

    }
}
