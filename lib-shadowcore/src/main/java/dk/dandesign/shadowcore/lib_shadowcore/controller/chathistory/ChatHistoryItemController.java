package dk.dandesign.shadowcore.lib_shadowcore.controller.chathistory;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.c2call.lib.androidlog.Ln;
import com.c2call.lib.security.Str;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCNewMessageCache;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.SCThreadMode;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemTextViewHolder;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemTextController;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.events.ActivityClosingEvent;
import dk.dandesign.shadowcore.lib_shadowcore.events.RequestChatHistoryUpdateEvent;
import dk.dandesign.shadowcore.lib_shadowcore.util.AppIconBadger;

public class ChatHistoryItemController extends SCBoardListItemTextController
{

    private TextView _textUnreadMessages;

    public ChatHistoryItemController(final View view, final SCViewDescription viewDescription, final SCBoardEventData data)
    {
        super(view, viewDescription, data);
    }

    @Override
    public void onCreate(Activity activity, SCActivityResultDispatcher scActivityResultDispatcher) {
        super.onCreate(activity, scActivityResultDispatcher);
        SCCoreFacade.instance().subscribe(this);
    }

    @Override
    public synchronized void onDestroy() {
        SCCoreFacade.instance().unsubscribe(this);
        super.onDestroy();
    }

    @Override
    protected void onBindViewHolder(IBoardListItemTextViewHolder vh)
    {
        super.onBindViewHolder(vh);

        _textUnreadMessages = (TextView)getView().findViewById(R.id.app_boarditem_unread);

    }

    public TextView getTextUnreadMessages()
    {
        return _textUnreadMessages;
    }

    @Override
    protected SCViewDescription getViewDescription()
    {
        return super.getViewDescription();
    }

    @Override
    public void onMainViewClick(final View v)
    {
        Ln.d("c2app", "ChatHistoryItemController.onMainViewClick() - userid: %s", getData().getUserid());
        final String userid = getData().getUserid();
        if (userid == null){
            Ln.d("c2app", "* * * Warning: ChatHistoryItemController.onMainViewClick() - userid is null!");
            return;
        }

        SCNewMessageCache.instance().clear(getData().getUserid(), getData().getLine());
        AppIconBadger.instance().showBadge(getContext());

        try{
            if (dk.dandesign.shadowcore.lib_shadowcore.util.Str.isPhonenumber(userid)){
                Ln.d("c2app", "ChatHistoryItemController.onMainViewClick() - phone number: %s", getData().getUserid());
                SCNewMessageCache.instance().getNewMessages(userid).clear();
                SCCoreFacade.instance().postEvent(new RequestChatHistoryUpdateEvent(), false);
                C2CallSdk.startControl().openBoard(getContext(), null, R.layout.sc_board, userid, StartType.Activity);
            }
            else{
                Ln.d("c2app", "ChatHistoryItemController.onMainViewClick() - friend user: %s", getData().getUserid());
                SCNewMessageCache.instance().getNewMessages(userid).clear();
                SCCoreFacade.instance().postEvent(new RequestChatHistoryUpdateEvent(), false);

                C2CallSdk.startControl().openBoard(getContext(), null, R.layout.sc_board, getData().getUserid(), StartType.Activity);
            }
        }
        catch(final Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onPictureViewClick(final View v)
    {
        onMainViewClick(v);
//        final String userid = getData().getUserid();
//        Ln.d("c2app", "ChatHistoryItemController.onPictureViewClick() - userid: %s", userid);
//
//
//        String phoneNumber = null;
//
//
//        if (Str.isPhonenumber(userid)){
//            phoneNumber = userid;
//        }
//        else{
//            try{
//                final SCFriendData friend = SCFriendData.dao().queryForId(userid);
//                Ln.d("c2app", "ChatHistoryItemController.onPictureViewClick() - matched friend: %s", friend.getDisplayName());
//                if (friend != null){
//                    phoneNumber = friend.getOwnNumber();
//                }
//            }
//            catch(final Exception e){
//                e.printStackTrace();
//            }
//        }
//
//        if (phoneNumber == null){
//            Ln.d("c2app", "ChatHistoryItemController.onPictureViewClick() - phone number not found in local address book: %s", phoneNumber);
//            return;
//        }
//
//        final Long contactId = ContactsUtil.phoneNumberLookup(phoneNumber, false);
//        Ln.d("c2app", "ChatHistoryItemController.onPictureViewClick() - found contact of nnumber %s: %s", phoneNumber, contactId);
//        if (contactId == null){
//            return;
//        }
//
//        final SCFriendData friend = new SCFriendData("" + contactId);
//        friend.setUserType(SCFriendManager.TYPE_LOCAL_CONTACT);
//        C2CallSdk.startControl().openContactDetail(getContext(), null, R.layout.sc_contact_detail, friend, StartType.Activity);

    }

    @Override
    public void onHandleReadReport()
    {
        Ln.d("c2app", "ChatHistoryItemController.onHandleReadReport() - don't send read report in chat history");
    }

    @SCEventCallback(threadMode = SCThreadMode.MainThread, isSticky = false)
    private void onEvent(ActivityClosingEvent evt) {
        Ln.d("c2app", "ChatHistoryController.ActivityClosingEvent() - evt: %s", evt);
        if (evt.getValue().equals(getData().getUserid())) {
            setVisibility(getTextUnreadMessages(), false);
        }

    }
}
