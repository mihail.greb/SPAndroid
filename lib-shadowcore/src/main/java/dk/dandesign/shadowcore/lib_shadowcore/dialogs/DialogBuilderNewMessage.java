package dk.dandesign.shadowcore.lib_shadowcore.dialogs;

import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.gui.core.controller.IController;
import com.c2call.sdk.pub.gui.dialog.BaseItemRunnable;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;

import dk.dandesign.shadowcore.lib_shadowcore.R;

public class DialogBuilderNewMessage
{
    public static class NewMessageRunnable extends BaseItemRunnable<IController<?>>
    {

        public NewMessageRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            C2CallSdk.startControl().openNewMessage(getController().getContext(), null, R.layout.sc_new_message, null, StartType.Activity);
        }
    }


    public static SCChoiceDialog build(final IController<?> controller)
    {
        final SCChoiceDialog.Builder builder = new SCChoiceDialog.Builder(controller.getContext());
        builder
                .addItem(R.string.app_dlg_new_message_title,
                        R.string.app_dlg_new_message_summary,
                        R.drawable.ic_message_grey_48dp,
                        new NewMessageRunnable(controller));


        return builder.build();
    }
}

