package dk.dandesign.shadowcore.lib_shadowcore.controller.polypicker;


import java.util.List;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import nl.changer.polypicker.Config;

public class PPDefaultConfig
        extends Config
{
    public PPDefaultConfig(List<String> selectedItems, int limit)
    {
        super(R.color.white, R.color.apptheme_color, limit, R.color.apptheme_color, selectedItems);
    }
}
