package dk.dandesign.shadowcore.lib_shadowcore.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.SCThreadMode;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.fragments.SCBoardFragment;
import com.c2call.sdk.pub.gui.board.controller.IBoardController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemType;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.common.SCListModus;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.activities.SDCTabBarControllerActivity;
import dk.dandesign.shadowcore.lib_shadowcore.controller.chathistory.ChatHistoryController;
import dk.dandesign.shadowcore.lib_shadowcore.controller.chathistory.ChatHistoryListItemControllerFactory;
import dk.dandesign.shadowcore.lib_shadowcore.events.BackPressedEvent;
import dk.dandesign.shadowcore.lib_shadowcore.events.ConsumeBackPressEvent;
import dk.dandesign.shadowcore.lib_shadowcore.util.BackPressConsumer;

public class ChatHistoryFragment extends SCBoardFragment
{
    private static SCViewDescriptionMap _viewDescriptionMap = new SCViewDescriptionMap();

    static{
        SCViewDescription vd = C2CallSdk.vd().boardListItem().get(SCBoardListItemType.Text.value());
        _viewDescriptionMap.setDefaultViewDescription(vd);
    }

    public static ChatHistoryFragment create(final int layout)
    {
        final Bundle args = new Bundle();
        args.putInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);

        final ChatHistoryFragment f = new ChatHistoryFragment();
        f.setArguments(args);
        //f.setUserVisibleHint(true);
        return f;
    }

    private final SCActivityResultDispatcher _resultDispatcher = new SCActivityResultDispatcher();

    public void stopRefreshing() {
        if (getController() != null) {
            ((ChatHistoryController) getController()).stopRefreshing();
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        SCCoreFacade.instance().subscribe(this);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        SCCoreFacade.instance().unsubscribe(this);

        super.onDestroy();
    }

    @Override
    protected int getDefaultLayout() {
        return R.layout.app_chathistory;
    }

    @Override
    protected IBoardController onCreateController(final View v, final SCViewDescription vd)
    {
        return new ChatHistoryController(v,
                vd,
                _viewDescriptionMap,
                new ChatHistoryListItemControllerFactory(_resultDispatcher),
                null);
    }

    @Override
    protected SCActivityResultDispatcher onCreateResultDispactcher()
    {
        return _resultDispatcher;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    protected boolean hasOptionsMenu()
    {
        return true;
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        inflater.inflate(R.menu.sc_menu_chathistory, menu);
    }

    @Override
    public void onPrepareOptionsMenu (Menu menu) {
        MenuItem mitem = menu.findItem(R.id.menu_chat_edit);
        if (mitem != null) {
            if (getView() == null) {
                mitem.setVisible(false);
                return;
            }

            boolean v = getView().getVisibility() == View.VISIBLE;
            boolean isDelete = ((ChatHistoryController)getController()).getListModus() == SCListModus.Delete;
            mitem.setVisible(v && !isDelete);
        }

        mitem = menu.findItem(R.id.menu_save);
        if (mitem != null) {
            if (getView() == null) {
                mitem.setVisible(false);
                return;
            }

            boolean v = getView().getVisibility() == View.VISIBLE;
            boolean isDelete = ((ChatHistoryController)getController()).getListModus() == SCListModus.Delete;
            mitem.setVisible(v && isDelete);
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item)
    {

        if (item.getItemId() ==  R.id.menu_chat_edit) {
            ((ChatHistoryController)getController()).toggleListModus();
            getActivity().invalidateOptionsMenu();
            return true;
        }
        if (item.getItemId() ==  R.id.menu_save) {
            ((ChatHistoryController)getController()).onConfirmDeletion();
            getActivity().invalidateOptionsMenu();
            return true;
        }


        return false;
    }

    @Override
    public void onPause() {
        super.onPause();
        SCCoreFacade.instance().postEvent(new ConsumeBackPressEvent(false, BackPressConsumer.CHATHISTORY), false);
    }

    @SCEventCallback(threadMode = SCThreadMode.MainThread)
    private void onEvent(BackPressedEvent evt) {

        ChatHistoryController controller = (ChatHistoryController) getController();

        controller.setListModus(SCListModus.Normal);

        getActivity().invalidateOptionsMenu();
    }

}
