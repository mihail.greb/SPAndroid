package dk.dandesign.shadowcore.lib_shadowcore.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.activities.resulthandlers.SCUserImageResultHandler;
import com.c2call.sdk.pub.common.SCCredentials;
import com.c2call.sdk.pub.common.SCProfile;
import com.c2call.sdk.pub.common.SCRegistrationData;
import com.c2call.sdk.pub.common.SCRichMessageType;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCDownloadType;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.core.SCProfileHandler;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.SCThreadMode;
import com.c2call.sdk.pub.eventbus.events.SCUploadEvent;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.facade.SCMediaFacade;
import com.c2call.sdk.pub.fragments.SCRegisterFragment;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.gui.register.controller.IRegisterController;
import com.c2call.sdk.pub.richmessage.SCRichMessagingManager;
import com.c2call.sdk.pub.util.MediaUtil;
import com.c2call.sdk.pub.util.SimpleAsyncTask;
import com.c2call.sdk.pub.util.SimpleLock;
import com.c2call.sdk.thirdparty.soundcloud.android.crop.Crop;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.controller.RegisterController;
import dk.dandesign.shadowcore.lib_shadowcore.dialogs.DialogBuilderRegisterPhoto;
import dk.dandesign.shadowcore.lib_shadowcore.util.Str;

/**
 * Created by mikel on 13.05.17.
 */

public class RegisterFragment extends SCRegisterFragment {

    private View _view;
    private ImageView _imagePicture;

    private String _userImageFile;

    private String _mediakey;

    private SimpleLock _imageUploadLock = new SimpleLock(false);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SCCoreFacade.instance().subscribe(this);
    }

    @Override
    public void onDestroy() {
        SCCoreFacade.instance().unsubscribe(this);
        super.onDestroy();
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        Ln.d("c2app", "RegisterController.onActivityResult() - requestCode: %d, resultCode: %d", requestCode, resultCode);
        super.onActivityResult(requestCode, resultCode, data);

        final String id = SCProfileHandler.instance().getProfileUserId();
        UserImageResultHandler resultHandler = new UserImageResultHandler();
        resultHandler.onActivityResult(getActivity(), this, requestCode, resultCode, data, id);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        _view = super.onCreateView(inflater, container, savedInstanceState);
        onInitChildren(_view);
        onInitActionListners();
        return _view;
    }

    private void onInitChildren(final View v) {
        _imagePicture = (ImageView) v.findViewById(R.id.sc_register_picture);
    }

    private void onInitActionListners() {
        _imagePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPictureClicked();
            }
        });
    }

    @Override
    protected IRegisterController onCreateController(final View v, final SCViewDescription vd)
    {
        final SCRegistrationData data = getArguments().getParcelable(SCExtraData.Register.EXTRA_DATA_REGISTRATION);
        return new RegisterController(v, vd, data);
    }

    private void onPictureClicked() {
        final String filename = MediaUtil.TMP_IMAGE;

        final SCChoiceDialog dlg = onCreatePictureDialog(filename);
        if (dlg != null) {
            dlg.show();
        }
    }

    protected SCChoiceDialog onCreatePictureDialog(final String filename) {
        return new DialogBuilderRegisterPhoto().build(this, filename);
    }

    @SCEventCallback(threadMode = SCThreadMode.MainThread)
    public void onEvent(SCUploadEvent evt) {
        Ln.d("fc_test", "RegisterFragment.onEvent(SCUploadEvent) - evt: %s", evt);
        if (!_mediakey.equals(evt.getKey())) {
            return;
        }

        switch (evt.getState()) {
            case Finished:
                Ln.d("fc_test", "SCProfileController.update() - finish");
                _imageUploadLock.setValue(false);
                break;
            case Error:
                _imageUploadLock.setValue(false);
                Toast.makeText(getActivity(), com.c2call.sdk.R.string.sc_msg_unknown_error, Toast.LENGTH_SHORT);
            default:
        }
    }

    public void login(final SCRegistrationData data) {

        final SCCredentials cred = new SCCredentials(data.getEmail(), data.getPassword());
        Ln.d("fc_test", "LoginController.login() - cred: %s", cred);
        if (cred == null){
            Toast.makeText(getActivity(), "login failed - invalid credentials", Toast.LENGTH_SHORT).show();
            return;
        }


        new SimpleAsyncTask<Integer>(getActivity(), 0) {
            @Override
            protected Integer doInBackground(final Void... params) {
                final int result = SCCoreFacade.instance().login(cred.username, cred.password, true);

                if (result == 0)
                {
                    handleExtraProfileData();
                }

                return result;
            }

            @Override
            protected void onSuccess(final Integer result) {
                if (getActivity() == null)
                {
                    return;
                }

                switch (result) {
                    case 0: /* RESULT_SUCCESS */
                        //C2CallSdk.startControl().openMain(getActivity());
                        getActivity().setResult(Activity.RESULT_OK);
                        getActivity().finish();
                        break;
                    case 1:
                        Toast.makeText(getActivity(), com.c2call.sdk.R.string.sc_hint_connection_error, Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        Toast.makeText(getActivity(), com.c2call.sdk.R.string.sc_msg_login_invalid_data, Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(getActivity(), com.c2call.sdk.R.string.sc_msg_login_failed, Toast.LENGTH_SHORT).show();
                        break;
                }
            }

        }.execute();
    }

    private void handleExtraProfileData() {
        SCProfile profile = SCProfileHandler.instance().getProfile();

        addUserPictureToProfile(profile);

//        if (((RegisterController)getController()).hasPhoneNumber()) {
//            addPhonenumberToProfile(profile);
//        }

        saveProfile(profile);
    }

    private String addUserPictureToProfile(SCProfile profile) {
        Ln.d("fc_test", "RegisterFragment.addUserPictureToProfile() - profile: %s", profile);

        String id = profile.getId();
        try {
            if (Str.isEmpty(_userImageFile)) {
                return null;
            }

            _mediakey = SCRichMessageType.UserImage.getPrefix() + id + ".jpg";

            final String filename = id + ".jpg";

            Ln.d("fc_test", "filename: %s", filename);
            Ln.d("fc_test", "_userImageFile: %s", _userImageFile);

            //_imageUploadLock.setValue(true);
            SCRichMessagingManager.instance().uploadUserImage(filename, _userImageFile, true, true);

            Ln.d("fc_test", "RegisterFragment.addUserPictureToProfile() - wait for finish()...");

            //_imageUploadLock.waitUntilFalse(0);

            Ln.d("fc_test", "RegisterFragment.addUserPictureToProfile() - wait for finish()... - done");
            profile.setLargeImage(_mediakey);
            return _mediakey;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void saveProfile(SCProfile profile) {
        Ln.d("fc_test", "EnterPinCodeFragment.saveUserImage() - profile: %s", profile.getId());

        try {
            boolean success = SCCoreFacade.instance().updateProfile(profile);
            Ln.d("fc_test", "EnterPinCodeFragment.saveUserImage() - done. success: %b", success);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class UserImageResultHandler extends SCUserImageResultHandler {

        private UserImageResultHandler() {
            setCropParams(320, 320, 1, 1);
        }

        protected void handleCrop(final Activity activity,
                                  final Fragment fragment,
                                  final Intent data,
                                  final String id) {
            try {
                Uri imageUri = Crop.getOutput(data);
                final Bitmap photo = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), imageUri);

                _userImageFile = SCMediaFacade.instance().getMediaPath(SCDownloadType.Image, MediaUtil.TMP_IMAGE);

                final File outFile = new File(_userImageFile);

                Ln.d("fc_tmp", "UserImageResultHandler.handleCrop() - tmpFile: %s", outFile);

                final BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(outFile));
                photo.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.close();

                _imagePicture.setImageBitmap(photo);

                Ln.d("fc_tmp", "UserImageResultHandler.handleCrop() - done.");
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
    }

}
