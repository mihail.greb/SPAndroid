package dk.dandesign.shadowcore.lib_shadowcore.dialogs;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.activities.common.SCActivityRequestCodes;
import com.c2call.sdk.pub.core.GlobalDepot;
import com.c2call.sdk.pub.core.SCDownloadType;
import com.c2call.sdk.pub.gui.core.controller.IController;
import com.c2call.sdk.pub.gui.dialog.BaseItemRunnable;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.util.MediaUtil;

import java.io.File;

public class DialogBuilderPhoto
{
    public static class ExistingRunnable extends BaseItemRunnable<IController<?>>
    {
        public ExistingRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            final Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, SCActivityRequestCodes.PICK_IMAGE);

        }
    }

    public static class CamRunnable extends BaseItemRunnable<IController<?>>
    {
        private final String _filename;
        public CamRunnable(final IController<?> controller, final String filename)
        {
            super(controller);
            _filename = filename;
        }

        @Override
        public void run()
        {
            try{
//              final String filename = UUID.randomUUID().toString().replace("-", "") + ".jpg";
                final String filename = _filename != null
                                        ? _filename
                                        : MediaUtil.TMP_IMAGE;

                final String output = MediaUtil.getMediaPath(SCDownloadType.Image, filename, true);

                Ln.d("fc_tmp", "handleTakePhoto() - %s", output);
                GlobalDepot.put(GlobalDepot.KEY_LAST_PHOTO, output);
                final File file = new File(output);

                final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                startActivityForResult(intent, SCActivityRequestCodes.TAKE_PHOTO);
            }
            catch(final Exception e){
                e.printStackTrace();
            }
        }
    }

    public static SCChoiceDialog build(final IController<?> controller, final String filename)
    {
        final SCChoiceDialog.Builder builder = new SCChoiceDialog.Builder(controller.getContext());

        builder
                .addItem(com.c2call.sdk.R.string.sc_dlg_rich_message_existing_photo_title,
                        com.c2call.sdk.R.string.sc_dlg_rich_message_existing_photo_summary,
                        com.c2call.sdk.R.drawable.ic_photo_blue_24dp,
                        new ExistingRunnable(controller))

                .addItem(com.c2call.sdk.R.string.sc_dlg_rich_message_take_photo_title,
                        com.c2call.sdk.R.string.sc_dlg_rich_message_take_photo_summary,
                        com.c2call.sdk.R.drawable.ic_photo_camera_blue_24dp,
                        new CamRunnable(controller, filename));


        return builder.build();
    }
}
