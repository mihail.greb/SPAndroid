package dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory;

public enum CallHistoryFilter
{
    All,
    Missed
}
