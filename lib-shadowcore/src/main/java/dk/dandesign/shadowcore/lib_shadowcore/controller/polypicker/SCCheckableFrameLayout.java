package dk.dandesign.shadowcore.lib_shadowcore.controller.polypicker;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.FrameLayout;

import dk.dandesign.shadowcore.lib_shadowcore.R;


public class SCCheckableFrameLayout
        extends FrameLayout implements Checkable
{
    boolean _isChecked = false;

    public SCCheckableFrameLayout(Context context)
    {
        super(context);
    }

    public SCCheckableFrameLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public SCCheckableFrameLayout(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setChecked(boolean b)
    {
        _isChecked = b;
        if (_isChecked) {
            this.setForeground(getContext().getResources().getDrawable(R.drawable.sc_gridview_item_selected));
        }
        else{
            this.setForeground(null);
        }
    }

    @Override
    public boolean isChecked()
    {
        return _isChecked;
    }

    @Override
    public void toggle()
    {
        setChecked(!_isChecked);
    }
}
