package dk.dandesign.shadowcore.lib_shadowcore.dialogs;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.common.SCPhoneNumberType;
import com.c2call.sdk.pub.gui.contactdetail.controller.IContactDetailController;
import com.c2call.sdk.pub.gui.contactdetail.controller.IContactDetailNumberRow;
import com.c2call.sdk.pub.gui.contactdetail.controller.SCContactDetailNumberRow;
import com.c2call.sdk.pub.gui.core.controller.SCBaseController;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.util.Validate;

import java.util.EnumSet;

public class DialogBuilderAddNumber {
    public static class AddNumberRunnable implements Runnable {
        private final SCPhoneNumberType _type;
        private final IContactDetailController _controller;

        public AddNumberRunnable(final IContactDetailController c, final SCPhoneNumberType type) {
            _controller = c;
            _type = type;
        }

        @Override
        public void run() {
            Ln.d("fc_tmp", "AdNumberRunnable.run() - Type: %s", _type);
            if (_type == null
                    || _type == SCPhoneNumberType.None) {
                return;
            }
            _controller.onCreateExtraDataIfNecessary();
            if (_controller.getExtraData() == null) {
                Ln.e("fc_error", "* * * Error: AdNumberRunnable.run() - unable to create contact!");
            }


            if (_controller.getViewHolder().getItemTableExtraNumbers() != null) {
                Ln.d("fc_test", "AddNumberRunnable ExtraNumber: %s", _controller.getViewHolder().getItemTableExtraNumbers());
                _controller.getExtraData().getManager().setPhoneNumber(_type, "");
                _controller.getViewHolder().getItemTableExtraNumbers().addView(new SCContactDetailNumberRow(_controller,
                        _controller.getExtraData().getManager(),
                        _type,
                        "",
                        IContactDetailNumberRow.Modus.Edit));
            }

            SCBaseController.setVisibility(_controller.getViewHolder().getItemButtonSave(), true);

            _controller.setEditing(true, true);
        }
    }

    public static SCChoiceDialog build(final IContactDetailController c) {
        Validate.notNull(c, "Controller must not be null!");
        Validate.notNull(c.getContext(), "Controller must not be null!");


        EnumSet<SCPhoneNumberType> itemMask = null;
        if (c.getExtraData() == null) {
            itemMask = EnumSet.allOf(SCPhoneNumberType.class);
        } else {
            itemMask = EnumSet.complementOf(c.getExtraData().getManager().getPhoneNumberMask());
        }

        if (itemMask.isEmpty()) {
            return null;
        }

        final SCChoiceDialog.Builder builder = new SCChoiceDialog.Builder(c.getContext());
        if (itemMask.contains(SCPhoneNumberType.Work)) {
            builder.addItem(com.c2call.sdk.R.string.sc_std_label_phone_work, 0, 0, new AddNumberRunnable(c, SCPhoneNumberType.Work));
        }

        if (itemMask.contains(SCPhoneNumberType.Mobile)) {
            builder.addItem(com.c2call.sdk.R.string.sc_std_label_phone_mobile, 0, 0, new AddNumberRunnable(c, SCPhoneNumberType.Mobile));
        }

        if (itemMask.contains(SCPhoneNumberType.Home)) {
            builder.addItem(com.c2call.sdk.R.string.sc_std_label_phone_home, 0, 0, new AddNumberRunnable(c, SCPhoneNumberType.Home));
        }

        if (itemMask.contains(SCPhoneNumberType.Other)) {
            builder.addItem(com.c2call.sdk.R.string.sc_std_label_phone_other, 0, 0, new AddNumberRunnable(c, SCPhoneNumberType.Other));
        }

        return builder.build();
    }
}
