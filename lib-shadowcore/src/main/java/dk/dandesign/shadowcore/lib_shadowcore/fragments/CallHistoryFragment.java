package dk.dandesign.shadowcore.lib_shadowcore.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ToggleButton;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.SCThreadMode;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.fragments.SCBoardFragment;
import com.c2call.sdk.pub.gui.board.controller.IBoardController;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.common.SCListModus;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory.CallHistoryController;
import dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory.CallHistoryFilter;
import dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory.CallHistoryListItemControllerFactory;
import dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory.CallHistoryLoaderHandler;
import dk.dandesign.shadowcore.lib_shadowcore.events.BackPressedEvent;
import dk.dandesign.shadowcore.lib_shadowcore.events.ConsumeBackPressEvent;
import dk.dandesign.shadowcore.lib_shadowcore.util.BackPressConsumer;

public class CallHistoryFragment extends SCBoardFragment
{

    public static CallHistoryFragment create(final int layout)
    {
        final Bundle args = new Bundle();
        args.putInt(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, layout);

        final CallHistoryFragment f = new CallHistoryFragment();
        f.setArguments(args);
        return f;
    }

    private ToggleButton _btnFilterAll;
    private ToggleButton _btnFilterMissed;

    private final SCActivityResultDispatcher _resultDispatcher = new SCActivityResultDispatcher();

    @Override
    protected IBoardController onCreateController(final View v, final SCViewDescription vd)
    {
        return new CallHistoryController(v,
                                         vd,
                                         C2CallSdk.instance().getVD().boardListItem(),
                                         new CallHistoryListItemControllerFactory(_resultDispatcher),
                                         null);
    }

    @Override
    protected SCActivityResultDispatcher onCreateResultDispactcher()
    {
        return _resultDispatcher;
    }

    @Override
    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);

        SCCoreFacade.instance().subscribe(this);


    }

    @Override
    public void onDestroy() {
        SCCoreFacade.instance().unsubscribe(this);

        super.onDestroy();
    }

    @Override
    protected int getDefaultLayout() {
        return R.layout.sc_callhistory;
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState)
    {
        final View v = super.onCreateView(inflater, container, savedInstanceState);

        Ln.d("fc_tmp", "CallHistoryFragment.onCreateView() - layout: %s", getArguments());
        onInitChildren(v);
        onInitActionListeners();

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    protected boolean hasOptionsMenu()
    {
        return true;
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater)
    {
        inflater.inflate(R.menu.sc_menu_callhistory, menu);
    }

    @Override
    public void onPrepareOptionsMenu (Menu menu) {

        MenuItem mitem = menu.findItem(R.id.menu_calls_edit);
        if (mitem != null) {
            if (getView() == null) {
                mitem.setVisible(false);
                return;
            }

            boolean v = getView().getVisibility() == View.VISIBLE;
            boolean isDelete = ((CallHistoryController)getController()).getListModus() == SCListModus.Delete;
            mitem.setVisible(v && isDelete);
        }

        mitem = menu.findItem(R.id.menu_calls_save);
        if (mitem != null) {
            if (getView() == null) {
                mitem.setVisible(false);
                return;
            }
            boolean v = getView().getVisibility() == View.VISIBLE;
            boolean isDelete = ((CallHistoryController)getController()).getListModus() == SCListModus.Delete;

            mitem.setVisible(v && isDelete);
        }
    }


    @Override
    public boolean onOptionsItemSelected(final MenuItem item)
    {

        if (item.getItemId() == R.id.menu_calls_edit) {
            ((CallHistoryController) getController()).toggleListModus();
            getActivity().invalidateOptionsMenu();
            return true;
        }

        if (item.getItemId() == R.id.menu_calls_save) {
            ((CallHistoryController) getController()).onConfirmDeletion();
            return true;
        }
        return false;
    }


    @Override
    public void onStart()
    {
        super.onStart();


        if (_btnFilterAll != null) {
            _btnFilterAll.setChecked(true);
        }

        if (_btnFilterMissed != null) {
            _btnFilterMissed.setChecked(false);
        }
    }

    private void onInitChildren(final View v)
    {
        _btnFilterAll = (ToggleButton)v.findViewById(R.id.sc_callhistory_btn_filter_all);
        _btnFilterMissed = (ToggleButton)v.findViewById(R.id.sc_callhistory_btn_filter_missed);

        if (_btnFilterAll != null) {
            _btnFilterAll.setTag(CallHistoryFilter.All);
        }

        if (_btnFilterMissed != null) {
            _btnFilterMissed.setTag(CallHistoryFilter.Missed);
        }
    }

    private void onInitActionListeners()
    {
        final View.OnClickListener listener = new View.OnClickListener()
        {

            @Override
            public void onClick(final View v)
            {
                _btnFilterAll.setChecked(false);
                _btnFilterMissed.setChecked(false);

                ((ToggleButton)v).setChecked(true);

                final CallHistoryFilter filter = (CallHistoryFilter)v.getTag();
                final CallHistoryController controller = (CallHistoryController)getController();

                ((CallHistoryLoaderHandler)controller.getLoaderHandler()).setFilter(filter);
            }
        };


        _btnFilterAll.setOnClickListener(listener);
        _btnFilterMissed.setOnClickListener(listener);
    }

    @SCEventCallback(threadMode = SCThreadMode.MainThread)
    private void onEvent(BackPressedEvent evt) {
        Ln.d("fc_test", "ChatHistoryFragment.onEvent() - listModus: %s, evt: %s", ((CallHistoryController)getController()), evt);

        ((CallHistoryController)getController()).setListModus(SCListModus.Normal);

        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onPause() {
        super.onPause();
        SCCoreFacade.instance().postEvent(new ConsumeBackPressEvent(false, BackPressConsumer.CALLHISTORY), false);
    }


}

