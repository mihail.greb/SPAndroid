package dk.dandesign.shadowcore.lib_shadowcore.events;

public class FriendsUpdateEvent
{
    private int _count;

    public FriendsUpdateEvent(int count)
    {
        _count = count;
    }

    public int getCount()
    {
        return _count;
    }

    public void setCount(int count)
    {
        _count = count;
    }

    @Override
    public String toString()
    {
        return "FriendListUpdateEvent{" +
                "_count=" + _count +
                '}';
    }
}
