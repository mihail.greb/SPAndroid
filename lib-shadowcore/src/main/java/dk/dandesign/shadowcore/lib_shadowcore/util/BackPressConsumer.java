package dk.dandesign.shadowcore.lib_shadowcore.util;


public class BackPressConsumer
{
    public static final int FRIENDLIST = 0;
    public static final int CHATHISTORY = 1;
    public static final int CALLHISTORY = 2;
    public static final int FRIENDSFLOATINGACTIONSMENU = 3;
    public static final int HISTORYFLOATINGACTIONSMENU = 4;
}
