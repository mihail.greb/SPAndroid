package dk.dandesign.shadowcore.lib_shadowcore.util;

import android.content.Context;

import com.c2call.sdk.pub.core.SCCallIndication;

/**
 * Created by mikel on 10.06.17.
 */

public class SDCCallIndication extends SCCallIndication {
    protected boolean   callIndicationRunning = false;

    @Override
    public synchronized void startIndication(Context context) {
        super.startIndication(context);
        callIndicationRunning = true;
    }

    @Override
    public synchronized void stopIndication(Context context) {
        super.stopIndication(context);
        callIndicationRunning = false;
    }

    public boolean isCallIndicationRunning() {
        return callIndicationRunning;
    }
}
