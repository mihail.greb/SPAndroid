package dk.dandesign.shadowcore.lib_shadowcore.controller.friendlist;

import android.database.Cursor;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.core.adapter.SCBaseControllerCursorAdapter;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.core.decorator.IDecorator;
import com.c2call.sdk.pub.gui.core.filter.IBaseFilter;
import com.c2call.sdk.pub.gui.core.filter.SCAndFilter;
import com.c2call.sdk.pub.gui.core.filter.SCFriendFilterFactory;
import com.c2call.sdk.pub.gui.core.loaderhandler.ILoaderHandlerContextProvider;
import com.c2call.sdk.pub.gui.core.loaderhandler.SCFriendLoaderHandler;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemController;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemControllerFactory;
import com.c2call.sdk.pub.util.IListViewProvider;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.events.FriendsUpdateEvent;

public class FriendsLoaderHandler extends SCFriendLoaderHandler
{

    private int _itemLayout;
    private boolean _hasObserver = true;

    public FriendsLoaderHandler(final ILoaderHandlerContextProvider contextProvider,
                                final IListViewProvider listViewProvider,
                                final IFriendListItemControllerFactory itemControllerFactory,
                                final SCViewDescription itemviewDescription,
                                final int loaderId)
    {
        this(contextProvider, listViewProvider, itemControllerFactory, itemviewDescription, loaderId, R.layout.sc_friend_listitem);
    }

    public FriendsLoaderHandler(final ILoaderHandlerContextProvider contextProvider,
                                final IListViewProvider listViewProvider,
                                final IFriendListItemControllerFactory itemControllerFactory,
                                final SCViewDescription itemviewDescription,
                                final int loaderId,
                                final int itemLayout)
    {
        super(contextProvider, listViewProvider, itemControllerFactory, itemviewDescription, loaderId);

        setFilterMask(SCFriendFilterFactory.FLAG_USER_AND_GROUP);

        _itemLayout = itemLayout;
        Ln.d("c2app", "FriendListLoaderHandler() - contextProvider: %s", contextProvider);
    }

    @Override
    public void setFilterMask(int i) {
        super.setFilterMask(i | SCFriendFilterFactory.FLAG_USER_AND_GROUP);
    }

    @Override
    protected IBaseFilter<SCFriendData, String> onCreateFilter(String filterText, int filterMask)
    {
        SCAndFilter<SCFriendData, String> filter = (SCAndFilter<SCFriendData, String>)super.onCreateFilter(filterText, filterMask);

//        filter.add(new SCBaseFilter<SCFriendData, String>()
//        {
//            @Override
//            public Where<SCFriendData, String> filter(Dao<SCFriendData, String> dao, Where<SCFriendData, String> where) throws SQLException
//            {
//                return where.notIn(SCFriendData._ID, PublicGroupCache.instance().getPublicGroupIds());
//            }
//        });

        return filter;
    }

    @Override
    protected SCBaseControllerCursorAdapter<SCFriendData,
            IFriendListItemController,
            IFriendListItemControllerFactory,
            IDecorator<IFriendListItemController>>
    onCreateAdapder(Cursor cursor,
                    IFriendListItemControllerFactory itemFactory,
                    SCViewDescription vd)
    {
        return new FriendsAdapter(getContext(),
                                  cursor,
                                  _itemLayout,
                                  getItemControllerFactory(),
                                  getItemViewDescription(),
                                  0);
    }



    @Override
    protected synchronized void onContactsLoadFinished(Cursor cursor, IFriendListItemControllerFactory iFriendListItemControllerFactory)
    {
        Ln.d("fc_tmp", "BroadcastLoaderHandler.onContactsLoadFinished() - count: %d", (cursor != null ? cursor.getCount() : -1));
        FriendsUpdateEvent evt = (cursor != null)
                ? new FriendsUpdateEvent(cursor.getCount())
                : new FriendsUpdateEvent(0);

        SCCoreFacade.instance().postEvent(evt, true);
        super.onContactsLoadFinished(cursor, iFriendListItemControllerFactory);
    }
}
