package dk.dandesign.shadowcore.lib_shadowcore.controller.friendlist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;

import com.c2call.lib.androidlog.Ln;
import com.c2call.lib.xml.StringExtra;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCStartControl;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.core.controller.IListItemController;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.core.decorator.IDecorator;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemViewHolder;
import com.c2call.sdk.pub.gui.friendlistitem.controller.SCFriendListItemController;

import dk.dandesign.shadowcore.lib_shadowcore.R;


public class FriendListItemController extends SCFriendListItemController
{
	public FriendListItemController(final View view, final SCViewDescription viewDescription, final SCFriendData data)
	{
		super(view, viewDescription, data);
        if (data.getUserData() != null){
        }
	}

    @Override
    public void setData(SCFriendData data, boolean decorate)
    {
        super.setData(data, decorate);
    }

    @Override
	protected void onBindViewHolder(final IFriendListItemViewHolder viewHolder)
	{
	    super.onBindViewHolder(viewHolder);
		onBindButtonFavorite(viewHolder);

		View v = viewHolder.getMainView();

		final View callVoIP = v.findViewById(R.id.sc_frienditem_btn_call_voice);
		View callVideo = v.findViewById(R.id.sc_frienditem_btn_call_video);
		View chat =  v.findViewById(R.id.sc_frienditem_btn_chat);

		callVoIP.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				oncallVoIPClicked(view);
			}
		});

		callVideo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				oncallVideoClicked(view);
			}
		});


		chat.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onChatClicked(view);
			}
		});
	}

    @Override
    protected IDecorator<IListItemController<SCFriendData, IFriendListItemViewHolder>> onCreateDecorator()
    {
        return new FriendListItemDecorator();
    }

	private void onBindButtonFavorite(final IFriendListItemViewHolder viewHolder)
	{
		bindClickListener(viewHolder.getItemFavoriteIcon(), new View.OnClickListener()
		{

			@Override
			public void onClick(final View v)
			{
				onButtonFavoriteClick(v);
			}
		});
	}

    

	public void onButtonFavoriteClick(View v)
	{
		toggleFavorites();
	}

	@Override
	public void onMainViewLongClick(final View v)
	{
		final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
		builder.setTitle("Delete " + getData().getDisplayName())
				.setMessage(R.string.sc_dlg_confirm_deletion_text)
				.setPositiveButton(R.string.sc_std_btn_ok, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(final DialogInterface dialog, final int which)
					{
						SCCoreFacade.instance().deleteFriend(getData().getId());
					}
				});

		builder.show();
	}

	@Override
	public void onPictureViewClick(final View v)
	{
		Ln.d("c2app", "BroadcastListItemController.onPictureViewClick()");
		super.onPictureViewClick(v);
	}

	@Override
	public void onButtonDetailsClick(final View anchor)
	{
		Ln.d("c2app", "BroadcastListItemController.onButtonDetailsClick()");
		super.onButtonDetailsClick(anchor);
	}


	@Override
    protected void onShowGroupDetail(final View anchor)
    {
	    Ln.d("c2app", "BroadcastListItemController.onShowGroupDetail() - userData: %s", getData().getUserData());
        C2CallSdk.startControl().openGroupDetail(getContext(), getView(), R.layout.sc_group_detail, getData().getId(), StartType.Activity);

//      ContactExtra.showGroupDetilPopup(getContext(), getData().getUserId(), getView());
    }

    @Override
    protected void onShowContactDetail(final View anchor)
    {
        Ln.d("c2app", "BroadcastListItemController.onShowContactDetail() - context: ", getContext());
        C2CallSdk.startControl().openContactDetail(getContext(), getView(), R.layout.sc_contact_detail, getData(), StartType.Activity);
	}


	protected void oncallVoIPClicked(View v) {
		new Thread()
		{
			@Override
			public void run()
			{
				C2CallSdk.startControl().openCallbar(getContext(), null, getData().getId(), R.layout.sc_callbar, StartType.Activity);
				SCCoreFacade.instance().call(getData().getId(), false, false);

			}
		}.start();
	}

	protected void oncallVideoClicked(View v) {
		new Thread()
		{
			@Override
			public void run()
			{
				C2CallSdk.startControl().openCallbar(getContext(), null, getData().getId(), R.layout.sc_callbar, StartType.Activity);
				SCCoreFacade.instance().call(getData().getId(), true, false);

			}
		}.start();
	}

	protected void onChatClicked(View v) {
		if (getData() == null) {
			return;
		}

		C2CallSdk.instance().getStartControl().openBoard(getContext(),
				v,
				com.c2call.sdk.R.layout.sc_board,
				getData().getId(),
				StartType.Activity);

	}

}
