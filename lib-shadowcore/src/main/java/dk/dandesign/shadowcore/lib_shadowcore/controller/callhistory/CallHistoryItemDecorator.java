package dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory;

import android.view.View;
import android.widget.TextView;

import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCNewMessageCache;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.db.datamanager.SCMissedCallManager;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemCallController;
import com.c2call.sdk.pub.gui.boardlistitem.decorator.SCBoardListItemCallDecorator;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.util.AppViewForCallTabBadger;
import dk.dandesign.shadowcore.lib_shadowcore.util.Str;
import dk.dandesign.shadowcore.lib_shadowcore.util.TimeUtil;

public class CallHistoryItemDecorator extends SCBoardListItemCallDecorator
{

    @Override
    public void decorate(final IBoardListItemCallController controller)
    {
        super.decorate(controller);

        onDecorateTime(controller);

        onDecorateMissedCall();
    }

    @Override
    public void onDecorateHeaderAndFooter(final IBoardListItemCallController m)
    {
        final String author = Str.toEclipseString(getAuthorName(m), 35);

        final String header = author;
        final String footer = getNumberType(m);


        setText(m.getViewHolder().getItemFooterView(), footer);
        setText(m.getViewHolder().getItemHeaderView(), header);

        m.getViewHolder().getItemHeaderView().setCompoundDrawables(null, null, null, null);

        setVisibility(m.getViewHolder().getItemHeaderView(), true);
        setVisibility(m.getViewHolder().getItemFooterView(), true);
    }


    @Override
    public void onDecorateCallIcon(final IBoardListItemCallController m)
    {
        int icon = 0;

        final String id = m.getData().getUserid();

        SCBoardEventData data = m.getData();

        boolean isMissedByDuration = data.getManager().getEventType().isIncoming()
                && data.getDuration() == 0;

        final boolean isMissed = SCMissedCallManager.isMissed(id, m.getData().getTimestamp())
                || isMissedByDuration;

        if (isMissed)
        {
            icon = com.c2call.sdk.R.drawable.ic_sc_call_histroy_icon_missedcall_src;
        } else
        {
            icon = (data.getManager().getEventType().isIncoming())
                    ? com.c2call.sdk.R.drawable.ic_sc_call_histroy_icon_callout_online_src
                    : com.c2call.sdk.R.drawable.ic_sc_call_histroy_icon_callin_online_src;
        }


        TextView footer = m.getViewHolder().getItemFooterView();
        footer.setCompoundDrawablesWithIntrinsicBounds(icon, 0, 0, 0);


    }

    private String getNumberType(final IBoardListItemCallController m)
    {
        if (m.getData() == null)
        {
            return "";
        }

        String uid = m.getUserid();
        boolean isPhoneNumber = Str.isPhonenumber(uid);

        return isPhoneNumber
                ? m.getUserid()
                : "";
//        if (!isPhoneNumber)
//        {
//            return "";
//        }
//
//
//        SCPhoneNumberType type = C2CallSdk.instance().getPhoneNumberCache().lookupNumberType(m.getData().getUserid());
//        if (type == null)
//        {
//            type = SCPhoneNumberType.Other;
//        }
//
//        return type.toString(m.getContext());
    }


    protected String getAuthorName(final IBoardListItemCallController m)
    {
        if (m.getData() == null)
        {
            return "";
        }

        if (m.getData().getManager().isAnonymousCaller())
        {
            return m.getContext().getString(R.string.sc_callhistory_anonymous_caller);
        }
        else
        {
            return C2CallSdk.contactResolver().getDisplayNameByUserid(m.getData().getUserid(), true);
        }

    }

    public void onDecorateTime(final IBoardListItemCallController controller)
    {
        final View textTime = controller.getViewHolder().getItemTime();


        setText(textTime, getTimeString(controller));
    }


    protected String getTimeString(final IBoardListItemCallController controller)
    {
        final long timestamp = controller.getData().getTimestamp();

        boolean isPhoneNumber = Str.isPhonenumber(controller.getUserid());

        StringBuilder builder = new StringBuilder();
        if (isPhoneNumber)
        {
            builder.append(", ");
        }

        String timeString = TimeUtil.getTimeAgoString(controller.getContext(), timestamp);
        builder.append(timeString);

        return builder.toString();
    }

    public void onDecorateMissedCall()
    {
        AppViewForCallTabBadger.instance().setBadgeNumber(SCNewMessageCache.instance().getMissedCallCount());
    }

    @Override
    protected String getOutgoingPictureUrl(IBoardListItemCallController iBoardListItemCallController, SCFriendData friend) {
        return friend.getManager().getImage(false);
    }

}
