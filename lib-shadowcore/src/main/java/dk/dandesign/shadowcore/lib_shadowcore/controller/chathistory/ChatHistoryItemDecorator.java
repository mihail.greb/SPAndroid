package dk.dandesign.shadowcore.lib_shadowcore.controller.chathistory;

import android.view.View;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.common.SCRichMessageType;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCNewMessageCache;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemTextController;
import com.c2call.sdk.pub.gui.boardlistitem.decorator.SCBoardListItemTextDecorator;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.util.AppViewForMessageTabBadger;
import dk.dandesign.shadowcore.lib_shadowcore.util.Str;

public class ChatHistoryItemDecorator extends SCBoardListItemTextDecorator
{

    private static final SimpleDateFormat __dateFormat = new SimpleDateFormat(C2CallSdk.context().getString(R.string.sc_date_pattern_short_time));


    @Override
    public void decorate(final IBoardListItemTextController controller)
    {
        super.decorate(controller);

        onDecorateTime(controller);

        onDecorateUnreadMessages((ChatHistoryItemController) controller);
    }




    private void onDecorateUnreadMessages(ChatHistoryItemController controller)
    {
        SCBoardEventData data = controller.getData();
        Set<String> newMessages = SCNewMessageCache.instance().getNewMessages(data.getUserid());

        boolean show = !newMessages.isEmpty();
        setVisibility(controller.getTextUnreadMessages(), show);
        setText(controller.getTextUnreadMessages(), "" + newMessages.size());

        AppViewForMessageTabBadger.instance().setBadgeNumber(SCNewMessageCache.instance().getUnreadMessageCount());
    }

    @Override
    protected void onDecorateOutgoingPicture(IBoardListItemTextController controller, SCFriendData friend)
    {
        onDecorateIncomingPicture(controller, friend);
    }

    @Override
    protected String getText(IBoardListItemTextController m)
    {

        String text = m.getData().getManager().getDisplayText();
        SCRichMessageType type = m.getData().getManager().getRichMessageType();

        Ln.d("fc_test", "ChatHistoryItemDecorator.getText() - text: %s, type: %s", text, type);

        if (type == null){
            return text;
        }


        switch(type){
            case Audio: return m.getContext().getString(R.string.app_chathistory_audio_message);
            case Video: return m.getContext().getString(R.string.app_chathistory_video_message);
            case Image: return m.getContext().getString(R.string.app_chathistory_image_message);
            case Location: return m.getContext().getString(R.string.app_chathistory_location_message);
            case Friend: return m.getContext().getString(R.string.app_chathistory_friend_message);
            case File: return m.getContext().getString(R.string.app_chathistory_file_message);
            default:
                return m.getContext().getString(R.string.app_chathistory_media_message);

        }
    }

    @Override
    public int getTextColor(IBoardListItemTextController m)
    {
        return m.getContext().getResources().getColor(R.color.app_std_text_light_grey);
    }

    @Override
    public void onDecorateHeaderAndFooter(final IBoardListItemTextController m)
    {
        Ln.d("c2app", "ChatHistoryItemDecorator.onDecorateHeaderAndFooter() - data: %s", m.getData());
        final String author = Str.toEclipseString(getAuthorName(m), 35);

        final String header = author;

        m.getViewHolder().getItemHeaderView().setText(header);
        m.getViewHolder().getItemHeaderView().setCompoundDrawables(null, null, null, null);

       setVisibility(m.getViewHolder().getItemHeaderView(), _showHeader);
    }


    @Override
    public void onDecorateStatusIcon(final IBoardListItemTextController m)
    {
        super.onDecorateStatusIcon(m);

//        final ImageView statusIcon = (ImageView)m.getView().findViewById(R.id.app_boarditem_icon_status);
//        Ln.d("swytch", "ChatHistoryItemDecorator.onDecorateStatusIcon()  - userid: %s, friend: %s", m.getData().getUserid(), m.getData().getFriend());
//        final SCFriendData friend = m.getData().getFriend();
//        if (friend != null
//             && friend.getManager().getStatus() != SCOnlineStatus.Offline)
//        {
//            setVisibility(statusIcon, true);
//        }
//        else{
//            setVisibility(statusIcon, false);
//        }
    }



    protected String getAuthorName(final IBoardListItemTextController m)
    {
        if (m.getData() == null){
            return "";
        }


        String uid = m.getData().getManager().getFriendId();
        String name = C2CallSdk.contactResolver().getDisplayNameByUserid(uid, true);
        Ln.d("fc_tmp", "ChatHistoryItemDecorator.getAuthor() - uid: %s, name: %s, ContactResolver: %s", uid, name, C2CallSdk.contactResolver());
        return name;
    }

    public void onDecorateTime(final IBoardListItemTextController controller)
    {
        final View textTime = controller.getViewHolder().getItemTime();
        setText(textTime, getTimeString(controller));
    }


    protected String getTimeString(final IBoardListItemTextController controller)
    {
        final long timestamp = controller.getData().getTimestamp();


        return __dateFormat.format(new Date(timestamp));
    }
}
