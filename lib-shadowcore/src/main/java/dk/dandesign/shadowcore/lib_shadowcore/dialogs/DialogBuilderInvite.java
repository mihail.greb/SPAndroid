package dk.dandesign.shadowcore.lib_shadowcore.dialogs;

import android.content.Intent;
import android.net.Uri;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.gui.core.controller.IController;
import com.c2call.sdk.pub.gui.dialog.BaseItemRunnable;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;

import java.util.Locale;

import dk.dandesign.shadowcore.lib_shadowcore.R;

public class DialogBuilderInvite
{
    public static class FacebookRunnable extends BaseItemRunnable<IController<?>>
    {
        public FacebookRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            final String url = "http://www.friendcaller.com/landingpage/fbmobile/index.html?lang=" + Locale.getDefault().toString();
            Ln.d("fc_tmp", "DialgoInviteChoiceHandler.handleFacebookInvite() - %s", url);

            final Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            getController().getContext().startActivity(i);
        }
    }

    public static class LocalRunnable extends BaseItemRunnable<IController<?>>
    {
        public LocalRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            C2CallSdk.startControl().openContactInvite(getController().getContext(), null, R.layout.sc_select_contacts, StartType.Activity);
        }
    }

    public static class SmsRunnable extends BaseItemRunnable<IController<?>>
    {
        public SmsRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            String text = getContext().getString(R.string.sc_invite_sms_text);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("sms:"));
            intent.putExtra("sms_body", text);

            Ln.d("fc_tmp", "DialgoInviteChoiceHandler.SmsRunnable() - intent: %s", intent);
            getContext().startActivity(intent);

//            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//            intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);  //should filter only contacts with phone numbers
//            startActivityForResult(intent, SCActivityRequestCodes.PICK_NUMBER);
        }
    }

    public static class SingleRunnable extends BaseItemRunnable<IController<?>>
    {
        public SingleRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            /*
            String body = getContext().getString(R.string.sc_invite_email_text);
            EmailUtil.sendEmail(getContext(),
                    null,
                    getContext().getString(R.string.sc_invite_email_subject),
                    body);
            */
//            C2CallSdk.startControl().openInvite(getController().getContext(), null, R.layout.sc_invite, null, StartType.Activity);
        }
    }

    public static class AddFriendRunnable extends BaseItemRunnable<IController<?>>
    {
        public AddFriendRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            C2CallSdk.startControl().openAddFriend(getController().getContext(), null, R.layout.sc_addfriend, StartType.Activity);
        }
    }


    public static class CreateGroupRunnable extends BaseItemRunnable<IController<?>>
    {
        public CreateGroupRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            C2CallSdk.startControl().openAddGroup(getController().getContext(), null, com.c2call.sdk.R.layout.sc_select_contacts,  StartType.Auto);
        }
    }

    public static SCChoiceDialog build(final IController<?> controller)
    {
        final SCChoiceDialog.Builder builder = new SCChoiceDialog.Builder(controller.getContext());

        builder.addItem(R.string.sc_dlg_invite_add_friend_title,
                         R.string.sc_dlg_invite_add_friend_summary,
                         R.drawable.sc_social_add_person,
                         new AddFriendRunnable(controller))
                .addItem(R.string.sc_dlg_spread_invite_email_title,
                         R.string.sc_dlg_spread_invite_email_summary,
                         R.drawable.sc_content_email,
                         new SingleRunnable(controller));



        /*
        if (DeviceUtil.isTelephonyAvailable(controller.getContext())
            && DeviceUtil.checkPermission(controller.getContext(), "android.permission.SEND_SMS"))
        {
            builder.addItem(R.string.sc_dlg_invite_sms_title,
                            R.string.sc_dlg_invite_sms_summary,
                            R.drawable.sc_social_chat,
                            new SmsRunnable(controller));
        }
        */
        builder.addItem(com.c2call.sdk.R.string.sc_dlg_spread_create_group_title,
                        com.c2call.sdk.R.string.sc_dlg_spread_create_group_title_summary,
                        com.c2call.sdk.R.drawable.sc_social_add_group,
                        new CreateGroupRunnable(controller));


        return builder.build();
    }
}
