package dk.dandesign.shadowcore.lib_shadowcore.dialogs;


import com.c2call.sdk.pub.gui.core.controller.IController;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.gui.dialog.SCDialogFactory;

public class DialogFactory extends SCDialogFactory {

    @Override
    public SCChoiceDialog createRichMessageDialog(final IController<?> controller, final boolean isSms)
    {
        return DialogBuilderRichMessage.build(controller, isSms);
    }

    public SCChoiceDialog createNewMessageDialog(final IController<?> controller)
    {
        return DialogBuilderNewMessage.build(controller);
    }

    @Override
    public SCChoiceDialog createPhotoDialog(IController<?> controller, String filename)
    {
        return DialogBuilderMultiPhoto.build(controller, filename);
    }
}
