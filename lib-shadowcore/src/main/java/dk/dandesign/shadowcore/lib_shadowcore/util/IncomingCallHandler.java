package dk.dandesign.shadowcore.lib_shadowcore.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.call.incoming.SCIncomingCallHandler;
import com.c2call.sdk.pub.common.SCIncomingCallData;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.facade.SCMediaFacade;
import com.c2call.sdk.pub.services.common.SCOverlayService;

import dk.dandesign.shadowcore.lib_shadowcore.ShadowCoreApp;
import dk.dandesign.shadowcore.lib_shadowcore.activities.IncomingCallActivity;

public class IncomingCallHandler
        extends SCIncomingCallHandler
{
    protected SCIncomingCallData        pendingCall = null;

    @Override
    public void onIncomingCall(Context context, SCIncomingCallData data)
    {

        Ln.d("c2app", "IncomingCallHandler.onIncomingCall() - incoming call");

        if (ShadowCoreApp.getInstance().getShadowMode() == ShadowCoreApp.SDC_SHADOW_LOCKED) {
            Ln.e("c2app", "IncomingCallHandler.onIncomingCall() - SDC_SHADOW_LOCKED");
            return;
        }

        if (ShadowCoreApp.getInstance().getShadowMode() == ShadowCoreApp.SDC_SHADOW_LIMITED) {
            Ln.e("c2app", "IncomingCallHandler.onIncomingCall() - SDC_SHADOW_LIMITED");
            SCMediaFacade.instance().startCallIndication(context);
            pendingCall = data;
            return;
        }

        Intent serviceIntent = new Intent(context, SCOverlayService.class);
        ComponentName name = context.startService(serviceIntent);
        Ln.d("c2app", "IncomingCallHandler.onIncomingCall() - start SCOverlayService, name: %s", name);

        Bundle extras = new Bundle();
        extras.putParcelable(SCExtraData.IncomingCall.EXATR_DATA_INCOMING_CALL, data);

        Intent intent = new Intent(SCOverlayService.getActionName(context));
        intent.putExtra(SCExtraData.LockOverlay.ACTIVITY, IncomingCallActivity.class.getName());
        intent.putExtra(SCExtraData.LockOverlay.DATA, extras);
        context.sendBroadcast(intent);
    }

    public void presentPendingCall(Context context) {
        if (pendingCall != null) {
            onIncomingCall(context, pendingCall);
            pendingCall = null;
        }
    }

    public void resetPendingCall() {
        pendingCall = null;
    }
}
