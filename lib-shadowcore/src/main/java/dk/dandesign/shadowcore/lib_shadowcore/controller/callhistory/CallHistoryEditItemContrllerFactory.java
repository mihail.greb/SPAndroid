package dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory;


import android.view.View;

import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemCallController;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.controller.IControllerRequestListener;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;

public class CallHistoryEditItemContrllerFactory extends CallHistoryListItemControllerFactory
{
    public CallHistoryEditItemContrllerFactory(SCActivityResultDispatcher resultDispatcher, IControllerRequestListener requestListener)
    {
        super(resultDispatcher, requestListener);
    }

    public CallHistoryEditItemContrllerFactory(SCActivityResultDispatcher resultDispatcher)
    {
        super(resultDispatcher);
    }

    protected IBoardListItemCallController onCreateCallItemController(final View v, final SCViewDescription vd, final SCBoardEventData data)
    {
        return new CallHistoryEditItemController(v, vd, data);
    }
}
