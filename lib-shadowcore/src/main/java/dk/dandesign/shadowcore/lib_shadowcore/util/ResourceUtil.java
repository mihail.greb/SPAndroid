package dk.dandesign.shadowcore.lib_shadowcore.util;

import com.c2call.sdk.pub.core.C2CallSdk;


public class ResourceUtil
{
    public static int getId(final String name)
    {
        return getResource("id", name);
    }

    public static int getColor(final String name)
    {
        return getResource("color", name);
    }

    public static int getString(final String name)
    {
        return getResource("string", name);
    }

    public static int getResource(String type, final String name)
    {
        final String p = C2CallSdk.instance().getContext().getPackageName();
        final int res = C2CallSdk.instance().getContext().getResources().getIdentifier(name, type, p);
        return res;
    }

    public static int toQuantity(long value)
    {
        if (value <= 0){
            return 0;
        }
        else if (value == 1){
            return 1;
        }
        else{
            return 2;
        }
    }
}
