package dk.dandesign.shadowcore.lib_shadowcore;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;

import com.actai.logger.SipLogger;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.affiliate.AffiliateCredentials;
import com.c2call.sdk.pub.common.SCInvitedItem;
import com.c2call.sdk.pub.common.SCInvitedList;
import com.c2call.sdk.pub.common.SCProfile;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCExtraData;
import com.c2call.sdk.pub.core.SCProfileHandler;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.notifictaions.NotificationType;
import com.c2call.sdk.pub.util.SimpleAsyncTask;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;

import dk.dandesign.shadowcore.lib_shadowcore.activities.FriendRequestsActivity;
import dk.dandesign.shadowcore.lib_shadowcore.activities.LaunchScreenActivity;
import dk.dandesign.shadowcore.lib_shadowcore.activities.RegisterFragmentActivity;
import dk.dandesign.shadowcore.lib_shadowcore.activities.SDCTabBarControllerActivity;
import dk.dandesign.shadowcore.lib_shadowcore.util.IncomingCallHandler;
import dk.dandesign.shadowcore.lib_shadowcore.util.RingingHandler;
import dk.dandesign.shadowcore.lib_shadowcore.util.SDCCallIndication;
import dk.dandesign.shadowcore.lib_shadowcore.util.SDCNotifier;

/**
 * Created by mikel on 09.05.17.
 */

public class ShadowCoreApp extends Application {

    public static final int ACTIVITY_LAUNCHSCREEN = 100 + 1;

    public static final String PREFRERENCES = "ShadowCorePrefs";

    protected static ShadowCoreApp instance;
    protected static boolean isInBackground = false;

    protected IncomingCallHandler   incomingCallHandler;
    protected SDCCallIndication     callIndication;

    protected String affiliateId = "B5566D2614FB7074076";
    protected String secret = "64e633fc8216db943908ba96d2d5ea20";
    protected String applicationId = "dk.dandesign.shadowcore.shadowcoresdk";

    protected String coverMessage = "Ready to crush some numbers";

    protected boolean enableDebug = true;

    protected int     shadowMode = SDC_SHADOW_OPEN;
    public static final int SDC_SHADOW_OPEN = 0;
    public static final int SDC_SHADOW_LIMITED = 1;
    public static final int SDC_SHADOW_LOCKED = 2;


    protected SCInvitedList     friendRequests = null;

    // Public properties
    public boolean   useApplicationBadge = true;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        ApplicationLifecycleHandler handler = new ApplicationLifecycleHandler();
        registerActivityLifecycleCallbacks(handler);
        registerComponentCallbacks(handler);

        final AffiliateCredentials credentials = new AffiliateCredentials(this.affiliateId, this.applicationId, secret);

       /*
         Initialise the C2CallSdk instance. This must be called before
         using any C2Call functionality
        */
        C2CallSdk.instance().init(this, credentials);

        C2CallSdk.instance().setIncomingCallHandler(incomingCallHandler = new IncomingCallHandler());

        C2CallSdk.instance().setNotifier(new SDCNotifier());
        C2CallSdk.instance().setCallIndication(callIndication = new SDCCallIndication());

        SharedPreferences settings = getSharedPreferences(PREFRERENCES, 0);
        shadowMode = settings.getInt("SDCShadowMode", SDC_SHADOW_OPEN);

        RingingHandler.instance().subscribe();

       /*
         Whether the SDK should produce verbose logging.
         This will produce a lot of logs which may be helpful
         for debugging. Beside that you should disable it.
        */

        Ln.enableLogging(enableDebug);
        SipLogger.enableDebug(enableDebug);

        //SCCoreFacade.instance().enableStartStopServices(true);
    }


    public void terminateShadowCore(Activity context) {
        Ln.e("app", "terminateShadowCore");
        C2CallSdk.instance().close(context);
    }

    public boolean performLoginCheck(Activity activity, boolean autoOpenShadow) {
        if (SCCoreFacade.instance().isAutoLoginPossible()) {
            tryAutoLogin(activity);

            if (getShadowMode() == SDC_SHADOW_OPEN && autoOpenShadow) {
                Intent intent = new Intent(this, SDCTabBarControllerActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(intent);
            }
            return false;
        }

        Intent intent = new Intent(this, LaunchScreenActivity.class);
        activity.startActivityForResult(intent, ACTIVITY_LAUNCHSCREEN);

        return true;
    }

    public boolean isCallIndicationRunning() {
        return callIndication.isCallIndicationRunning();
    }

    public void presentPendingCall(final Activity activity) {
        incomingCallHandler.presentPendingCall(activity);
    }

    public void resetPendingCall() {
        incomingCallHandler.resetPendingCall();
    }

    private void tryAutoLogin(final Activity activity)
    {
        if (SCCoreFacade.instance().isAutoLoginPossible())
        {
            Ln.d("c2app", "MainActivity - tryAutoLogin");
            new AsyncTask<Void, Void, Integer>()
            {
                @Override
                protected Integer doInBackground(final Void... params)
                {
                    final int response = SCCoreFacade.instance().autoLoginIfPossible(activity, true);
                    return response;
                }

                @Override
                protected void onPostExecute(final Integer result)
                {
                    if (result == 0)
                    {
                        Ln.d("c2app", "Autologin Success");
                    } else
                    {
                        Ln.d("c2app", "Autologin Failed: %d", result);
                    }

                }
            }.execute();
        }
    }

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    @Override
    public void onTerminate() {
        Ln.e("app", "onTerminate()");
        super.onTerminate();
    }

    public void onEnterBackground() {
        Ln.e("app", "onEnterBackground");
    }

    public void onEnterForeground() {
        Ln.e("app", "onEnterForeground");
    }

    public void setShadowMode(int shadowMode)
    {
        this.shadowMode = shadowMode;

        SharedPreferences settings = getSharedPreferences(PREFRERENCES, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("SDCShadowMode", (int)shadowMode);

        // Commit the edits!
        editor.commit();

        switch (shadowMode) {
            case SDC_SHADOW_OPEN:
                enableNotifications();
                break;
            case SDC_SHADOW_LIMITED:
                limitNotifications();
                break;
            case SDC_SHADOW_LOCKED:
                disableNotifications();
                break;
        }
    }

    public int getShadowMode() {
        return shadowMode;
    }

    protected void enableNotifications() {
        this.useApplicationBadge = true;

        SCProfile profile = SCCoreFacade.instance().getProfile();
        if (profile != null && profile.getUserData("SCPushOptions") != null) {
            profile.removeUserData("SCPushOptions");
            SCCoreFacade.instance().updateProfile(profile);
        }

        C2CallSdk.instance().getNotifier().unblockNotifications("SDCNotifications");
        enableNotificationSound();

    }

    protected void limitNotifications() {
        this.useApplicationBadge = true;

        SCProfile profile = SCCoreFacade.instance().getProfile();
        if (profile != null && profile.getUserData("SCPushOptions") != null) {
            profile.removeUserData("SCPushOptions");
            SCCoreFacade.instance().updateProfile(profile);
        }

        C2CallSdk.instance().getNotifier().unblockNotifications("SDCNotifications");
        enableNotificationSound();
    }

    protected void disableNotifications() {
        this.useApplicationBadge = false;

        SCProfile profile = SCCoreFacade.instance().getProfile();
        if (profile != null) {
            profile.setUserData("SCPushOptions", "0", false);
            SCCoreFacade.instance().updateProfile(profile);

        }

        EnumSet<NotificationType> notifications = EnumSet.of(NotificationType.IncomingCall, NotificationType.Message, NotificationType.MissedCall, NotificationType.Push);
        C2CallSdk.instance().getNotifier().blockNotications("SDCNotifications", notifications);
        disableNotificationSound();
    }


    protected void disableNotificationSound() {


    }

    protected void enableNotificationSound() {

    }

    protected boolean isNewInvites(SCInvitedList invites) {

        if (invites == null || invites.get().size() == 0) {
            return false;
        }

        if (friendRequests == null) {
            return true;
        }

        HashSet<String> existing = new HashSet<String>();
        for (SCInvitedItem item : friendRequests.get()) {
            existing.add(item.getByUserid());
        }

        for (SCInvitedItem item : invites.get()) {
            if (!existing.contains(item.getByUserid())) {
                return true;
            }
        }

        return false;
    }

    public void refreshInvites(final Activity context) {
        new SimpleAsyncTask<SCInvitedList>(context)
        {
            @Override
            protected SCInvitedList doInBackground(Void... voids)
            {
                SCInvitedList list = SCCoreFacade.instance().listOpenInvitations();

                return list;
            }

            @Override
            protected void onSuccess(SCInvitedList invitations)
            {
                if (isNewInvites(invitations)) {
                    onShowFriendRequestsDialog(context);
                }
                friendRequests = invitations;
            }
        }.execute();

    }

    protected void onShowFriendRequestsDialog(final Activity context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.dialog_title_friend_requests)
                .setMessage(R.string.dialog_message_new_friend_requests)
                .setPositiveButton(R.string.sc_std_btn_ok, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which)
                    {
                        Intent intent = new Intent(context, FriendRequestsActivity.class);
                        context.startActivity(intent);
                    }
                })
                .setNegativeButton(R.string.sc_std_btn_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
        });

        builder.show();
    }

    public String getCoverMessage() {
        return coverMessage;
    }

    public void setCoverMessage(String coverMessage) {
        this.coverMessage = coverMessage;
    }


    public static ShadowCoreApp getInstance() {
        return instance;
    }

    public class ApplicationLifecycleHandler implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {


        @Override
        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        @Override
        public void onActivityStarted(Activity activity) {
        }

        @Override
        public void onActivityResumed(Activity activity) {

            if(isInBackground){
                Ln.e("app", "app went to foreground");
                isInBackground = false;
                onEnterForeground();
            }
        }

        @Override
        public void onActivityPaused(Activity activity) {
        }

        @Override
        public void onActivityStopped(Activity activity) {
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
        }

        @Override
        public void onConfigurationChanged(Configuration configuration) {
        }

        @Override
        public void onLowMemory() {
        }

        @Override
        public void onTrimMemory(int i) {
            Ln.e("app", "onTrimMemory: " + i);

            if(i == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN){
                Ln.e("app", "app went to background");
                isInBackground = true;
                onEnterBackground();
            }
        }
    }
}
