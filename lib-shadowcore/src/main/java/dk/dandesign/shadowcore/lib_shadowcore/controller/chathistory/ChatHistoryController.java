package dk.dandesign.shadowcore.lib_shadowcore.controller.chathistory;

import android.app.Activity;
import android.database.Cursor;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CursorAdapter;
import android.widget.ListAdapter;
import android.widget.ProgressBar;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.db.util.board.BoardDeleteHandler;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.SCThreadMode;
import com.c2call.sdk.pub.eventbus.events.SCBoardUpdateEvent;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.board.controller.IBoardViewHolder;
import com.c2call.sdk.pub.gui.board.controller.SCBoardController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemControllerFactory;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemType;
import com.c2call.sdk.pub.gui.boardlistitem.decorator.SCBoardListItemBaseDecorator;
import com.c2call.sdk.pub.gui.boardlistitem.decorator.SCBoardListItemRichDecorator;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.common.SCListModus;
import com.c2call.sdk.pub.gui.core.controller.IControllerRequestListener;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;
import com.c2call.sdk.pub.gui.core.filter.SCBoardFilterFactory;
import com.c2call.sdk.pub.gui.core.loaderhandler.ILoaderHandlerContextProvider;
import com.c2call.sdk.pub.gui.core.loaderhandler.SCBoardLoaderHandler;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.gui.dialog.SCDialogFactory;
import com.c2call.sdk.pub.util.IListViewProvider;
import com.c2call.sdk.pub.util.SCSelectionManager;
import com.c2call.sdk.pub.util.SimpleAsyncTask;
import com.c2call.sdk.thirdparty.ormlite.stmt.QueryBuilder;
import com.c2call.sdk.thirdparty.ormlite.stmt.Where;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.dialogs.DialogFactory;
import dk.dandesign.shadowcore.lib_shadowcore.events.ChatHistoryUpdateEvent;
import dk.dandesign.shadowcore.lib_shadowcore.events.ConsumeBackPressEvent;
import dk.dandesign.shadowcore.lib_shadowcore.events.RequestChatHistoryUpdateEvent;
import dk.dandesign.shadowcore.lib_shadowcore.util.BackPressConsumer;
import dk.dandesign.shadowcore.lib_shadowcore.util.Str;

public class ChatHistoryController extends SCBoardController
{
    public static final String SEL_KEY = ChatHistoryController.class.getName();

    private SCListModus _modus = SCListModus.Normal;


    private View btnEmptyChatsAdd;

    private ProgressBar _fragmentProgressBar;

    private SwipeRefreshLayout _swipeRefreshLayout;

    public ChatHistoryController(final View view,
                                 final SCViewDescription viewDescription,
                                 final SCViewDescriptionMap itemViewDescriptions,
                                 final IBoardListItemControllerFactory itemControllerFactory,
                                 final IControllerRequestListener requestListener)
    {
        super(view, viewDescription, itemViewDescriptions, itemControllerFactory, requestListener);

    }

    @Override
    public void setFilterMask(int mask)
    {
        super.setFilterMask(mask | SCBoardFilterFactory.FLAG_FILTER_MESSAGES | SCBoardFilterFactory.FLAG_FILTER_NOMEETINGS | SCBoardFilterFactory.FLAG_FILTER_NOBROADCASTS);
    }

    @Override
    public SCBoardLoaderHandler onCreateLoaderHandler(final IListViewProvider listViewProvider,
                                                      final ILoaderHandlerContextProvider loaderHandlerMasterProvider,
                                                      final IBoardListItemControllerFactory itemMediatorFactory,
                                                      final SCViewDescriptionMap itemDescriptions)
    {
        return new ChatHistoryLoaderHandler(loaderHandlerMasterProvider,
                listViewProvider,
                getUserId(),
                itemDescriptions,
                itemMediatorFactory);
    }


    @Override
    public void onCreate(Activity activity, SCActivityResultDispatcher scActivityResultDispatcher)
    {
        super.onCreate(activity, scActivityResultDispatcher);

        SCCoreFacade.instance().subscribe(this);
    }

    @Override
    public synchronized void onDestroy()
    {

        SCCoreFacade.instance().unsubscribe(this);

        super.onDestroy();
    }

    @Override
    protected void onBindViewHolder(final IBoardViewHolder vh)
    {
        super.onBindViewHolder(vh);

        _fragmentProgressBar = (ProgressBar) getView().findViewById(R.id.fragment_progressBar);

        _swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_container);
        _swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light, android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        _swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {

            @Override
            public void onRefresh()
            {

                if (getLoaderHandler() != null)
                {
                    SCCoreFacade.instance().updateBoard();
                }

            }
        });

        btnEmptyChatsAdd = getView().findViewById(R.id.btnEmptyChatsAdd);
        if (btnEmptyChatsAdd != null) {
            btnEmptyChatsAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnEmptyChatsAddClicked();
                }
            });
        }

        super.showFilterInput(false);

    }

    private void onActionButtonClicked()
    {
        final SCChoiceDialog dlg = ((DialogFactory) C2CallSdk.dialogFactory()).createNewMessageDialog(this);
        dlg.show();
    }

    public void onConfirmDeletion()
    {

        new SimpleAsyncTask<Boolean>(getContext(), 0 , null, getContext().getResources().getString(R.string.app_error_select))
        {
            @Override
            protected Boolean doInBackground(Void... voids)
            {
                return deleteSelectedMessages() == BoardDeleteHandler.ERROR_NONE;
            }

            @Override
            protected void onSuccess(Boolean aBoolean)
            {
                SCSelectionManager.instance().clear(SEL_KEY);
                toggleListModus();
                getContext().invalidateOptionsMenu();
            }
        }.execute();

    }

    private int deleteSelectedMessages()
    {

        Set<String> selection = SCSelectionManager.instance().getSelection(SEL_KEY);
        Ln.d("fc_test", "selection: %s", selection);

        if (selection == null) {
            return BoardDeleteHandler.ERROR_UNKNOWN;
        }

        List<SCBoardEventData> toDelete = getSelectedMessages();

        for (SCBoardEventData cur : toDelete)
        {
            Ln.d("fc_tmp", "ChatHistoryController.onConfirmDeletion() - %s / %s / %s / %s", cur.getId(), cur.getUserid(), cur.getLine(), cur.getDescription());
        }

        List<String> ids = new ArrayList<String>();
        for (SCBoardEventData cur : toDelete)
        {
            ids.add(cur.getId());
        }

        int result = BoardDeleteHandler.instance().deleteMessages(ids);

        Ln.d("fc_tmp", "ChatHistoryController.onConfirmDeletion() - result: %d", result);
        return result;
    }

    private List<SCBoardEventData> getSelectedMessages()
    {
        List<SCBoardEventData> result = new ArrayList<SCBoardEventData>();

        try
        {
            Set<String> rawSel = SCSelectionManager.instance().getSelection(SEL_KEY);
            List<SCBoardEventData> selection = SCBoardEventData.dao().queryBuilder()
                    .where()
                    .in(SCBoardEventData._ID, rawSel).query();


            for (SCBoardEventData cur : selection)
            {

                QueryBuilder<SCBoardEventData, String> qb = SCBoardEventData.dao().queryBuilder();

                Where<SCBoardEventData, String> where = qb.where().eq(SCBoardEventData.USERID, cur.getUserid());

                if (!Str.isEmpty(cur.getLine()))
                {
                    where.and().eq(SCBoardEventData.LINE, cur.getLine());
                }

                List<SCBoardEventData> part = qb.query();
                result.addAll(part);
            }


        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return result;
    }

    @SCEventCallback(threadMode = SCThreadMode.MainThread, isSticky = true)
    private void onEvent(ChatHistoryUpdateEvent evt)
    {
        Ln.d("fc_test", "ChatHistoryController.onEvent() - evt: %s", evt);

        if (_swipeRefreshLayout.isRefreshing())
        {
            _swipeRefreshLayout.setRefreshing(false);
        }

        if (_fragmentProgressBar != null)
        {
            setVisibility(_fragmentProgressBar, false);
        }

        long count = evt.getCount();
        /*
        if (count == 0){
            // workaround for messages that are only available in our database and not on the server
            try{
                count = (long)SCBoardEventData.dao().countOf();
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
        */

        if (count == 0 && _fragmentProgressBar != null && !_fragmentProgressBar.isShown())
        {
            setVisibility(getView().findViewById(R.id.app_empty_list_placeholder), true);
        }
        else
        {
            setVisibility(getView().findViewById(R.id.app_empty_list_placeholder), false);
        }
    }



    @SCEventCallback(threadMode = SCThreadMode.MainThread, isSticky = false)
    private void onEvent(RequestChatHistoryUpdateEvent evt)
    {
        Ln.d("fc_tmp", "ChatHistoryController.onEvent() - evt: %s", evt);
        getLoaderHandler().restart(false, null);
    }


    @SCEventCallback(threadMode = SCThreadMode.MainThread, isSticky = false)
    private void onEvent(SCBoardUpdateEvent evt)
    {
        Ln.d("fc_tmp", "ChatHistoryController.onEvent() - evt: %s", evt);

        int count = evt.getValue() != null
                        ? evt.getValue().size()
                        : 0;
        onEvent(new ChatHistoryUpdateEvent(count));
    }


    public void toggleListModus()
    {
        if (_modus == SCListModus.Normal)
        {
            _modus = SCListModus.Delete;
        } else
        {
            SCSelectionManager.instance().clear(SEL_KEY);
            _modus = SCListModus.Normal;
        }

        setListModus(_modus);
    }


    protected ListAdapter onCreateDeleteModeAdapter(final ListAdapter curAdapter)
    {
        if (!(curAdapter instanceof CursorAdapter))
        {
            throw new IllegalStateException("the current adapter is not a CursorAdapter. If you have overriden the default adapter then you have to override this method, too");
        }


        Map<SCBoardListItemType, SCBoardListItemBaseDecorator> decmap = new TreeMap<SCBoardListItemType, SCBoardListItemBaseDecorator>();
        decmap.put(SCBoardListItemType.Call, new SCBoardListItemRichDecorator());
        decmap.put(SCBoardListItemType.Text, new ChatHistoryEditItemDecorator());
        decmap.put(SCBoardListItemType.Image, new ChatHistoryEditItemDecorator());
        decmap.put(SCBoardListItemType.Video, new ChatHistoryEditItemDecorator());
        decmap.put(SCBoardListItemType.Audio, new ChatHistoryEditItemDecorator());
        decmap.put(SCBoardListItemType.Location, new ChatHistoryEditItemDecorator());
        decmap.put(SCBoardListItemType.Friend, new ChatHistoryEditItemDecorator());
        decmap.put(SCBoardListItemType.File, new ChatHistoryEditItemDecorator());

        final Cursor c = ((CursorAdapter) curAdapter).getCursor();

        ChatHistoryCursorAdapter adapter = new ChatHistoryCursorAdapter(getContext(),
                c,
                new ChatHistoryEditItemContrllerFactory(getResultDispatcher()),
                C2CallSdk.vd().boardListItem(),
                decmap,
                0);

        adapter.setViewInflator(new ChatHistoryCursorAdapter.IViewInflator()
        {
            @Override
            public View onInflateView(ChatHistoryCursorAdapter adapter, LayoutInflater inflater, Cursor cursor, SCBoardEventData data, ViewGroup parent)
            {
                return inflater.inflate(R.layout.app_chathistory_edit_list_item, parent, false);
            }
        });

        return adapter;
    }

    protected ListAdapter onCreateNormalModeAdapter(final ListAdapter curAdapter)
    {
        if (!(curAdapter instanceof CursorAdapter))
        {
            throw new IllegalStateException("the current adapter is not a CursorAdapter. If you have overriden the default adapter then you have to override this method, too");
        }


        final Cursor c = ((CursorAdapter) curAdapter).getCursor();

        ChatHistoryCursorAdapter adapter = new ChatHistoryCursorAdapter(getContext(),
                c,
                getListItemControllerFactory(),
                C2CallSdk.vd().boardListItem(),
                ChatHistoryCursorAdapter.DECORATOR_MAP,
                0);

        return adapter;
    }


    protected ListAdapter onCreateAdapter(final SCListModus modus, final ListAdapter curAdapter)
    {
        if (modus == SCListModus.Delete)
        {
            return onCreateDeleteModeAdapter(curAdapter);
        } else
        {
            return onCreateNormalModeAdapter(curAdapter);
        }
    }

    private void updateAdapterModus()
    {
        final AbsListView list = getViewHolder().getItemList();
        if (list == null)
        {
            return;
        }


        if (_modus == SCListModus.Delete)
        {
            SCSelectionManager.instance().clear(getSelectionKey());
        }


        final int curPos = list.getFirstVisiblePosition();

        final ListAdapter adapter = onCreateAdapter(_modus, list.getAdapter());

        list.setAdapter(adapter);
        final int maxPos = list.getCount() - 1;

        list.setSelection(Math.min(curPos, maxPos));
    }

    public void setListModus(SCListModus modus)
    {
        _modus = modus;
        updateAdapterModus();
        getContext().invalidateOptionsMenu();
        boolean consumeBackPress = (modus != SCListModus.Normal);
        SCCoreFacade.instance().postEvent(new ConsumeBackPressEvent(consumeBackPress, BackPressConsumer.CHATHISTORY), false);
    }

    public SCListModus getListModus()
    {
        return _modus;
    }

    public String getSelectionKey()
    {
        return SEL_KEY;
    }


    public void close()
    {
    }

    public void stopRefreshing() {
        setVisibility(_fragmentProgressBar, false);
    }

    protected void btnEmptyChatsAddClicked() {
        C2CallSdk.startControl().openNewMessage(getContext(), null, R.layout.sc_selectfriend, null, StartType.Activity);
    }
}
