package dk.dandesign.shadowcore.lib_shadowcore.controller.chathistory;

import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemTextController;
import com.c2call.sdk.pub.util.SCSelectionManager;

public class ChatHistoryEditItemDecorator extends ChatHistoryItemDecorator
{

    @Override
    public void decorate(IBoardListItemTextController controller)
    {
        super.decorate(controller);

        onDecorateCheckbox((ChatHistoryEditItemController)controller);
    }

    private void onDecorateCheckbox(ChatHistoryEditItemController controller)
    {
        if (controller == null){
            return;
        }

        boolean isChecked = SCSelectionManager.instance().isSelected(ChatHistoryController.SEL_KEY, controller.getData().getId());
        controller.getCheckBox().setChecked(isChecked);
    }
}
