package dk.dandesign.shadowcore.lib_shadowcore.dialogs;

import android.content.Context;
import android.content.Intent;
import android.provider.ContactsContract;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.activities.SCShareLocationActivity;
import com.c2call.sdk.pub.activities.common.SCActivityRequestCodes;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.gui.core.controller.IController;
import com.c2call.sdk.pub.gui.dialog.BaseItemRunnable;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.util.SCSelectionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import dk.dandesign.shadowcore.lib_shadowcore.R;

public class DialogBuilderRichMessage
{
    public static class PhotoRunnable extends BaseItemRunnable<IController<?>>
    {
        public PhotoRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            final SCChoiceDialog dlg = C2CallSdk.dialogFactory().createPhotoDialog(getController(), null);
            if (dlg != null){
                dlg.show();
            }
        }
    }

    public static class VideoRunnable extends BaseItemRunnable<IController<?>>
    {
        public VideoRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            final SCChoiceDialog dlg = C2CallSdk.dialogFactory().createVideoDialog(getController(), null);
            if (dlg != null){
                dlg.show();
            }

        }
    }

    public static class LocationRunnable extends BaseItemRunnable<IController<?>>
    {
        public LocationRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            Ln.d("fc_tmp", "DialogBuilderRichMessage.LocationRunnable.run()");
            final Intent intent = new Intent(getContext(), SCShareLocationActivity.class);
            startActivityForResult(intent, SCActivityRequestCodes.PICK_LOCATION);
            getContext().overridePendingTransition(R.anim.sc_fade_in,
                    android.R.anim.slide_out_right);
        }
    }

    public static class VoiceRunnable extends BaseItemRunnable<IController<?>>
    {
        public VoiceRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            C2CallSdk.startControl().openAudioRecord(getController().getContext(), getController().getFragment(), null, R.layout.sc_audio_record, null, StartType.Activity);
        }
    }

    public static class ContactRunnable extends BaseItemRunnable<IController<?>>
    {
        public ContactRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            try{
                final Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, SCActivityRequestCodes.PICK_CONTACT);
            }
            catch(final Exception e){
                e.printStackTrace();
            }
        }
    }

    public static class FriendRunnable extends BaseItemRunnable<IController<?>>
    {
        public FriendRunnable(final IController<?> controller)
        {
            super(controller);
        }

        @Override
        public void run()
        {
            try{
              C2CallSdk.startControl().openSelectFriends(getController().getContext(),
                                                         getController().getFragment(),
                                                          null,
                                                          R.layout.sc_select_contacts,
                                                          null,
                                                          SCSelectionManager.KEY_CONTACTS, StartType.Activity);
          }
          catch(final Exception e){
              e.printStackTrace();
          }
        }
    }

    protected static boolean isMapsApiAvailable(Context ctx)
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(ctx);
        return resultCode == ConnectionResult.SUCCESS;
    }

    public static SCChoiceDialog build(final IController<?> controller, final boolean isSms)
    {
        final SCChoiceDialog.Builder builder = new SCChoiceDialog.Builder(controller.getContext());

        builder.addItem(R.string.sc_dlg_rich_message_photo_title,
                          R.string.sc_dlg_rich_message_photo_summary,
                          R.drawable.ic_insert_photo_blue_24dp,
                          new PhotoRunnable(controller));

        builder.addItem(R.string.sc_dlg_rich_message_video_title,
                          R.string.sc_dlg_rich_message_video_summary,
                          R.drawable.ic_movie_blue_24dp,
                          new VideoRunnable(controller));


        if (isMapsApiAvailable(controller.getContext()))
        {
            builder.addItem(R.string.sc_dlg_rich_message_location_title,
                              R.string.sc_dlg_rich_message_location_summary,
                              R.drawable.ic_location_on_blue_24dp,
                              new LocationRunnable(controller));
        }

        builder.addItem(R.string.sc_dlg_rich_message_voice_title,
                          R.string.sc_dlg_rich_message_voice_summary,
                          R.drawable.ic_mic_blue_24dp,
                          new VoiceRunnable(controller));

        builder.addItem(R.string.sc_dlg_rich_message_contact_title,
                          R.string.sc_dlg_rich_message_contact_summary,
                          R.drawable.ic_contact_phone_blue_24dp,
                          new ContactRunnable(controller));

        if (!isSms){
            builder.addItem(R.string.sc_dlg_rich_message_friend_title,
                              R.string.sc_dlg_rich_message_friend_summary,
                              R.drawable.ic_face_blue_24dp,
                              new FriendRunnable(controller));
        }


        return builder.build();
    }
}
