package dk.dandesign.shadowcore.lib_shadowcore.events;

/**
 * Created by tgreinert on 24.10.14.
 */
public class ConsumeBackPressEvent
{
    private boolean _isActive;
    private int _id;


    public ConsumeBackPressEvent(boolean isActive, int id)
    {
        _isActive = isActive;
        _id = id;
    }

    public int getId()
    {
        return _id;
    }

    public boolean isActive()
    {
        return _isActive;
    }

    @Override
    public String toString()
    {
        return "ConsumeBackPressEvent{" +
               "_isActive=" + _isActive +
               ", _id=" + _id +
               '}';
    }
}
