package dk.dandesign.shadowcore.lib_shadowcore.fragments;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.fragments.SCFriendsFragment;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemController;
import com.c2call.sdk.pub.gui.friendlistitem.controller.SCFriendListItemControllerFactory;
import com.c2call.sdk.pub.gui.friends.controller.IFriendsController;
import com.c2call.sdk.pub.gui.friends.controller.SCFriendsController;
import com.c2call.sdk.pub.util.KeyboardUtil;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.activities.InviteFriendActivity;
import dk.dandesign.shadowcore.lib_shadowcore.controller.friendlist.FriendListItemController;
import dk.dandesign.shadowcore.lib_shadowcore.controller.friendlist.FriendsController;

/**
 * Created by mikel on 20.05.17.
 */

public class FriendsFragment extends SCFriendsFragment {

    private final SCFriendListItemControllerFactory friendListItemControllerFactory = new SCFriendListItemControllerFactory(null, null) {
        @Override
        public IFriendListItemController onCreateController(final View v, final SCViewDescription vd, final SCFriendData data) {
            // Instantiate MyFriendListItemController
            final IFriendListItemController controller = new FriendListItemController(v, vd, data);

            controller.setRequestListener(getRequestListener());

            return (controller);
        }
    };


    @Override
    protected IFriendsController onCreateController(View view, SCViewDescription vd) {
        IFriendsController friendController =  new FriendsController(view,
                vd,
                C2CallSdk.instance().getVD().friendListItem(),
                friendListItemControllerFactory,
                null);
        return friendController;
    }

    @Override
    protected boolean hasOptionsMenu()
    {
        return true;
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        inflater.inflate(R.menu.sc_menu_friendlist, menu);
    }

    @Override
    public void onPrepareOptionsMenu (Menu menu) {

        MenuItem mitem = menu.findItem(R.id.menu_add_friend);
        if (mitem != null) {
            if (getView() == null) {
                mitem.setVisible(false);
                return;
            }

            boolean v = getView().getVisibility() == View.VISIBLE;
            mitem.setVisible(v);
        }

    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (getController() == null) {
            return false;
        }

        if (item.getItemId() == R.id.menu_add_friend) {
            Intent intent = new Intent(getActivity(), InviteFriendActivity.class);
            startActivity(intent);
            return true;
        }

        return false;
    }

}
