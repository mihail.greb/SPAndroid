package dk.dandesign.shadowcore.lib_shadowcore.fragments;

import android.view.View;

import com.c2call.sdk.pub.fragments.SCLoginFragment;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.login.controller.ILoginController;

import dk.dandesign.shadowcore.lib_shadowcore.controller.LoginController;

/**
 * Created by mikel on 13.05.17.
 */

public class LoginFragment extends SCLoginFragment {

    @Override
    protected ILoginController onCreateController(View view, SCViewDescription scViewDescription) {
        return new LoginController(view, scViewDescription);
    }
}
