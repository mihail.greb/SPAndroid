package dk.dandesign.shadowcore.lib_shadowcore.fragments;

import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ToggleButton;

import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.SCStartControl;
import com.c2call.sdk.pub.core.StartType;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.ShadowCoreApp;
import dk.dandesign.shadowcore.lib_shadowcore.activities.SDCTabBarControllerActivity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class SettingsFragment extends Fragment {

    protected ToggleButton      toggleBtnSafeMode;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_settings, container, false);

        toggleBtnSafeMode = (ToggleButton) v.findViewById(R.id.toggleBtnSafeMode);

        if (ShadowCoreApp.getInstance().getShadowMode() == ShadowCoreApp.SDC_SHADOW_LOCKED) {
            toggleBtnSafeMode.setChecked(true);
        } else {
            toggleBtnSafeMode.setChecked(false);
        }


        if (getActivity() instanceof SDCTabBarControllerActivity) {

            toggleBtnSafeMode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((SDCTabBarControllerActivity)getActivity()).onToggleSafeModeClicked(toggleBtnSafeMode);
                }
            });
        }
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
