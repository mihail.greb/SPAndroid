package dk.dandesign.shadowcore.lib_shadowcore.util;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.thirdparty.expertiseandroid.lib.contactslib.ContactsHelper;
import com.c2call.sdk.thirdparty.expertiseandroid.lib.contactslib.model.EmailAddress;
import com.c2call.sdk.thirdparty.expertiseandroid.lib.contactslib.model.Person;
import com.c2call.sdk.thirdparty.expertiseandroid.lib.contactslib.model.TelephoneNumber;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ContactsUtil
{



    public static List<String> getAllEmailAddresses()
    {
        Ln.d("c2app", "ContactsUtil.getAllEmailAddresses()...");

        final List<String> list = new ArrayList<String>();
        final String[] projection = { Data._ID, EmailAddress.EMAIL};
//        final String mimetype = TelephoneNumber.CONTENT_ITEM_TYPE;
        final String mimetype = EmailAddress.CONTENT_ITEM_TYPE;
        final Cursor c = C2CallSdk.context().getContentResolver().query(Data.CONTENT_URI,
                                                                        projection,
                                                                        Data.MIMETYPE + "=?",
                                                                        new String[] { String.valueOf(mimetype) },
                                                                        null);
        while (c.moveToNext()) {
            final String address = c.getString(1).toLowerCase();
            Ln.d("c2app", "ContactsUtil.getAllEmailAddresses() -  address:  %s", address);
            list.add(address);
        }

        Ln.d("c2app", "ContactsUtil.getAllEmailAddresses()... - done.");
        return list;

    }


    public static List<String> getAllPhoneNumbers()
    {
        Ln.d("c2app", "ContactsUtil.getAllPhoneNumbers()...");
        final List<String> list = new ArrayList<String>();

        final String[] projection = { Data._ID, TelephoneNumber.NUMBER };

        final String mimetype = TelephoneNumber.CONTENT_ITEM_TYPE;
        final Cursor c = C2CallSdk.context().getContentResolver().query(
                                                    Data.CONTENT_URI,
                                                    projection,
                                                    Data.MIMETYPE + "=?",
                                                    new String[] { String.valueOf(mimetype) },
                                                    null);
        while (c.moveToNext()) {
            final String number = c.getString(1);
            final String e164 = SCCoreFacade.instance().convertToE164(number, SCCoreFacade.instance().getDefaultCountryCode());
            Ln.d("c2app", "ContactsUtil.getAllPhoneNumbers() -  number:  %s (%s)", number, e164);
            if (!Str.isLonger(e164, 4)){
                Ln.d("c2app", "ContactsUtil.getAllPhoneNumbers() -> (%s) seems not to be a real number -> ignore");
                continue;
            }


            list.add(e164);
        }

        Ln.d("c2app", "ContactsUtil.getAllPhoneNumbers()... - done.");

        return list;
    }

    public static Long phoneNumberLookup(final String number, final boolean useCache)
    {
        if (useCache){
            return C2CallSdk.phoneNumberCache().lookupId(number);
        }

        final Iterator<Person> contacts = new ContactsHelper(C2CallSdk.context()).telephoneSearch(number);
        if (contacts == null
             || !contacts.hasNext())
        {
            return null;
        }

        return contacts.next().id;
    }

    public static String phoneNumberLookupName(final String number, final boolean useCache)
    {
        if (useCache){
            return C2CallSdk.phoneNumberCache().lookupName(number);
        }

        final Iterator<Person> contacts = new ContactsHelper(C2CallSdk.context()).telephoneSearch(number);
        if (contacts == null
             || !contacts.hasNext())
        {
            return null;
        }

        return contacts.next().displayName();
    }

    public static void openLocalContact(final Activity ctx, final String contactId)
    {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        final Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contactId));
        intent.setData(uri);
        ctx.startActivity(intent);
    }

    public static String lookupNameByFriend(SCFriendData friend, String uid)
    {
        Ln.d("c2app", "ContactsUtil.lookupNameByFriend() - friend: %s, id: %s", friend, uid);

        if (Str.isEmpty(uid)) {
            if (friend == null){
                return null;
            }

            uid = friend.getId();
        }

        if (Str.isEmpty(uid)){
            return "";
        }

        Ln.d("c2app", "ContactsUtil.lookupNameByFriend() - id: %s", uid);

        if (friend != null){
            Ln.d("c2app", "ContactsUtil.lookupNameByFriend() -> userid...");
            String name = friend.getManager().getDisplayName();
            Ln.d("c2app", "ContactsUtil.lookupNameByFriend() -> userid... name: %s", name);
            return name;
        }
        else{
            return uid;
        }
    }
}
