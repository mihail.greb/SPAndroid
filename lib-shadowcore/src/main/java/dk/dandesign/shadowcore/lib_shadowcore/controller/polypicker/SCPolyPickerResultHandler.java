package dk.dandesign.shadowcore.lib_shadowcore.controller.polypicker;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import nl.changer.polypicker.ImagePickerActivity;

public class SCPolyPickerResultHandler
{
    public interface ResultListener
    {
        void onResult(List<String> result);
    }
    
    public boolean onActivityResult(int requestCode, int resuleCode, Intent intent, ResultListener listener)
    {

        boolean isRequestCodeValid = requestCode == PPRequestCodes.PICK_IMAGES
                                     || requestCode == PPRequestCodes.PICK_N_IMAGES;

        if (resuleCode != Activity.RESULT_OK
            || !isRequestCodeValid) {
            return false;
        }

        Parcelable[] parcelableUris = intent.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);

        if (parcelableUris == null) {
            if (listener != null){
                listener.onResult(new ArrayList<String>());
            }
            return true;
        }

        // Java doesn't allow array casting, this is a little hack
        Uri[] uris = new Uri[parcelableUris.length];
        System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);

        if (uris != null) {
            List<String> files = new ArrayList<>();
            for (Uri uri : uris) {
                String file = uri.toString();
                files.add(file);
                Log.d("tmp", String.format("MainActivity.onActivityResult() - uri: %s -> %s", uri, file));
            }

            if (listener != null){
                listener.onResult(files);
            }
        }
        
        return true;
    }
}
