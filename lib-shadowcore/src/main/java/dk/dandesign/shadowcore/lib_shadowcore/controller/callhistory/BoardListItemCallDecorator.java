package dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory;

import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemCallController;
import com.c2call.sdk.pub.gui.boardlistitem.decorator.SCBoardListItemCallDecorator;

/**
 * Created by mikel on 29.06.17.
 */

public class BoardListItemCallDecorator extends SCBoardListItemCallDecorator {
    public BoardListItemCallDecorator() {
    }

    @Override
    protected void onDecorateOutgoingPicture(IBoardListItemCallController iBoardListItemCallController, SCFriendData scFriendData) {
        super.onDecorateOutgoingPicture(iBoardListItemCallController, scFriendData);
    }

    @Override
    protected String getOutgoingPictureUrl(IBoardListItemCallController iBoardListItemCallController, SCFriendData friend) {
        return friend.getManager().getImage(false);
    }
}
