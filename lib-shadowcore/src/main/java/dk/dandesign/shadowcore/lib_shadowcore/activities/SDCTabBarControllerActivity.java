package dk.dandesign.shadowcore.lib_shadowcore.activities;

import dk.dandesign.shadowcore.lib_shadowcore.R.id;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.amazonaws.services.s3.internal.ServiceUtils;
import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.common.SCCallStatus;
import com.c2call.sdk.pub.common.SCFriendGroup;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.fragments.SCBoardFragment;
import com.c2call.sdk.pub.fragments.SCFriendsFragment;
import com.c2call.sdk.pub.fragments.communication.IBoardFragmentCommunictation;
import com.c2call.sdk.pub.fragments.communication.IFriendsFragmentCommunictation;
import com.c2call.sdk.pub.gui.core.events.SCBaseControllerEvent;
import com.c2call.sdk.pub.gui.core.loaderhandler.ILoaderHandlerContext;
import com.c2call.sdk.pub.util.SimpleAsyncTask;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.ShadowCoreApp;
import dk.dandesign.shadowcore.lib_shadowcore.fragments.CallHistoryFragment;
import dk.dandesign.shadowcore.lib_shadowcore.fragments.ChatHistoryFragment;
import dk.dandesign.shadowcore.lib_shadowcore.fragments.SettingsFragment;

public class SDCTabBarControllerActivity extends Activity implements SCFriendsFragment.Callbacks, SCBoardFragment.Callbacks, ILoaderHandlerContext
{

    private TextView mTextMessage;

    protected ChatHistoryFragment       chatHistoryFragment;
    protected CallHistoryFragment       callHistoryFragment;
    protected SCFriendsFragment         friendsFragment;
    protected SettingsFragment          settingsFragment;
    protected Fragment                  activeFragment;
    protected boolean                   isInitialized = false;
    protected boolean                   isAttached = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sdctab_bar_controller);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        setTheme(R.style.AppTheme);

        if (ShadowCoreApp.getInstance().getShadowMode() == ShadowCoreApp.SDC_SHADOW_LIMITED) {
            ShadowCoreApp.getInstance().setShadowMode(ShadowCoreApp.SDC_SHADOW_OPEN);
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            setActionBarTitle(item.getItemId());

            if (item.getItemId() == id.navigation_friends) {
                showFragment(friendsFragment);
                hideFragment(chatHistoryFragment);
                hideFragment(callHistoryFragment);
                hideFragment(settingsFragment);
                return true;
            }

            if (item.getItemId() == id.navigation_chats) {
                showFragment(chatHistoryFragment);
                hideFragment(friendsFragment);
                hideFragment(callHistoryFragment);
                hideFragment(settingsFragment);
                return true;
            }
            if (item.getItemId() == id.navigation_calls) {
                showFragment(callHistoryFragment);
                hideFragment(friendsFragment);
                hideFragment(chatHistoryFragment);
                hideFragment(settingsFragment);
                return true;
            }
            if (item.getItemId() == id.navigation_settings) {
                showFragment(settingsFragment);
                hideFragment(friendsFragment);
                hideFragment(chatHistoryFragment);
                hideFragment(callHistoryFragment);
                return true;
            }
            if (item.getItemId() == id.navigation_close) {
                ShadowCoreApp.getInstance().setShadowMode(ShadowCoreApp.SDC_SHADOW_LIMITED);
                finish();
                return true;
            }


            return false;
        }

    };

    protected void setActionBarTitle(int tabid) {
        if (getActionBar() == null) {
            return;
        }
        if (tabid == id.navigation_friends) {
            getActionBar().setTitle(R.string.sdc_tabbar_title_friends);
        }

        if (tabid == id.navigation_chats) {
            getActionBar().setTitle(R.string.sdc_tabbar_title_chats);
        }

        if (tabid == id.navigation_calls) {
            getActionBar().setTitle(R.string.sdc_tabbar_title_calls);
        }

        if (tabid == id.navigation_settings) {
            getActionBar().setTitle(R.string.sdc_tabbar_title_settings);
        }
    }

    protected void showFragment(Fragment fragment) {

        if (fragment != null) {
            fragment.getView().setVisibility(View.VISIBLE);

            if (!fragment.equals(activeFragment)) {
                activeFragment = fragment;
                invalidateOptionsMenu();
            }
        }

    }

    protected void hideFragment(Fragment fragment) {

        if (fragment != null) {
            fragment.getView().setVisibility(View.INVISIBLE);
        }
    }

    public Fragment getActiveFragment() {
        return activeFragment;
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);


        if (fragment instanceof ChatHistoryFragment) {
            chatHistoryFragment = (ChatHistoryFragment) fragment;
            chatHistoryFragment.stopRefreshing();
        }

        if (fragment instanceof CallHistoryFragment) {
            callHistoryFragment = (CallHistoryFragment) fragment;

        }
        if (fragment instanceof SCFriendsFragment) {
            friendsFragment = (SCFriendsFragment) fragment;
            //friendsFragment.stopRefreshing();
        }

        if (fragment instanceof SettingsFragment) {
            settingsFragment = (SettingsFragment) fragment;
        }

        if (chatHistoryFragment != null && callHistoryFragment != null && friendsFragment != null && !isInitialized) {
            isInitialized = true;

            if (isAttached) {
                initTabBarController();
            }
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        isAttached = true;
        if (isInitialized) {
            initTabBarController();
        }

    }

    protected void initTabBarController() {
        setActionBarTitle(id.navigation_friends);
        showFragment(friendsFragment);
        hideFragment(chatHistoryFragment);
        hideFragment(callHistoryFragment);
        hideFragment(settingsFragment);
        //friendsFragment.stopRefreshing();

        if (ShadowCoreApp.getInstance().isCallIndicationRunning()) {
            ShadowCoreApp.getInstance().presentPendingCall(this);
        } else {
            ShadowCoreApp.getInstance().resetPendingCall();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        ShadowCoreApp app = (ShadowCoreApp) getApplication();
        app.refreshInvites(this);
    }

    @Override
    public void onRegisterFriendsCommunictation(IFriendsFragmentCommunictation iFriendsFragmentCommunictation) {

    }

    @Override
    public void onShowBoard(String s) {

    }

    @Override
    public void onShowContactDetail(String s, int i) {

    }

    @Override
    public void onShowGroupDetail(SCFriendGroup scFriendGroup) {

    }

    @Override
    public void onControllerEvent(SCBaseControllerEvent scBaseControllerEvent) {

    }

    @Override
    public void onRegisterBoardCommunictation(IBoardFragmentCommunictation iBoardFragmentCommunictation) {

    }

    @Override
    public Context getLoaderContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    /**
     * Click Listener
     */

    public void onMenuProfileClicked(View v) {
        C2CallSdk.instance().getStartControl().openProfile(this, v, 0, StartType.Auto);
    }

    public void onMenuDecoyAppsClicked(View v) {

    }

    public void onMenuOpenFriendRequestsClicked(View v) {
        Intent intent = new Intent(this, FriendRequestsActivity.class);
        startActivity(intent);
    }

    public void onMenuOpenInvitesClicked(View v) {
        Intent intent = new Intent(this, OpenInvitesActivity.class);
        startActivity(intent);

    }

    public void onMenuEmailInvitesClicked(View v) {
        final Intent emailIntent = new Intent( android.content.Intent.ACTION_SEND);

        emailIntent.setType("plain/text");


        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                "Please join me on Shadow");

        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                "Please join me on Shadow.\nDownload on\nhttp://install.shadowapps.com\n");

        startActivity(Intent.createChooser(
                emailIntent, "Send email invite..."));
    }

    public void onMenuLogoutClicked(View v) {
        new SimpleAsyncTask<Boolean>(v.getContext())
        {
            @Override
            protected Boolean doInBackground(final Void... params)
            {
                Ln.e("c2app", "logout");
                boolean response = false;
                try {
                    response = SCCoreFacade.instance().logout();
                } catch (Throwable e) {
                    Ln.e("c2app", "SCCoreFacade.logout", e);
                }
                Ln.e("c2app", "logout: %b", response);

                return response;
            }

            @Override
            protected void onSuccess(final Boolean result) {
                Ln.e("c2app", "onSuccess: %b", result);
                if (result) {
                    Ln.e("c2app", "logout Success");
                    finish();
                } else {
                    Ln.e("c2app", "logout Failed");
                }
            }
        }.execute();

    }

    public void onToggleSafeModeClicked(ToggleButton btnSafeMode) {

        if (btnSafeMode.isChecked()) {
            ShadowCoreApp.getInstance().setShadowMode(ShadowCoreApp.SDC_SHADOW_LOCKED);
        } else {
            ShadowCoreApp.getInstance().setShadowMode(ShadowCoreApp.SDC_SHADOW_OPEN);
        }
    }

}
