package dk.dandesign.shadowcore.lib_shadowcore.controller.polypicker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;


import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import nl.changer.polypicker.Config;
import nl.changer.polypicker.ImagePickerActivity;
import nl.changer.polypicker.utils.BitmapUtil;
import nl.changer.polypicker.utils.FileUtil;

public class SCGalleryPager
        extends LinearLayout
{

    private ExecutorService _executor = Executors.newFixedThreadPool(4);

    private ImageView   _imgPreview;
    private ImageView   _imgVidOverlay;
    private GridView    _gridThumbs;
    private EditText    _editTitle;
    private ProgressBar _progressIdle;
    private View _btnDelete;
    private VideoView _videoView;
    private int _limit;
    private int _maxRows = 2;
    
    private LruCache<String, Bitmap> _thumbCache = new LruCache<String, Bitmap>(80)
    {
        @Override
        protected void entryRemoved(boolean evicted, String key, Bitmap oldValue, Bitmap newValue)
        {
            super.entryRemoved(evicted, key, oldValue, newValue);
            if (evicted){
                oldValue.recycle();
            }
        }
    };



    private Map<String, String> _descriptions = new HashMap<>();

    private String _curItem = null;
    
    private boolean _fistWindowFocusChanged = true;


//    private Set<AsyncTask<?, ?, ?>> _tasks = Collections.newSetFromMap(new ConcurrentHashMap<AsyncTask<?, ?, ?>, Boolean>());

//    private AsyncTask<String, Void, Bitmap> _previewLoadingTask;


    public SCGalleryPager(Context context)
    {
        this(context, null);
    }

    public SCGalleryPager(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    public SCGalleryPager(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);

        init(context, attrs);
    }
    

    private void init(Context context, AttributeSet attrs)
    {
        log("SCGalleryPager.init()");


        LayoutInflater.from(context).inflate(R.layout.sc_gallery_pager, this, true);


        _videoView = (VideoView) findViewById(R.id.videoView);
        _imgPreview = (ImageView) findViewById(R.id.imgPreview);
        _imgVidOverlay = (ImageView) findViewById(R.id.imgVideoOverlay);
        _gridThumbs = (GridView) findViewById(R.id.gridThumbs);
        _editTitle = (EditText) findViewById(R.id.txtTitle);
        _progressIdle = (ProgressBar) findViewById(R.id.progressIdle);
        _btnDelete = findViewById(R.id.btnDelete);

        MediaController mediaController = new MediaController(getContext());
        mediaController.setAnchorView(_videoView);
        _videoView.setMediaController(mediaController);

        _gridThumbs.setChoiceMode(GridView.CHOICE_MODE_SINGLE);

        _gridThumbs.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id)
            {
                ImageAdapter adapter = (ImageAdapter)_gridThumbs.getAdapter();
                if (adapter.getItemViewType(pos) == ImageAdapter.TYPE_ADD) {
                    showMediaPicker();
                }
                else {
                    startPreviewLoadingTask(adapter.getItem(pos));
                }

                log("SCGalleryPager.onClick() - view: %s", view);
//                _gridThumbs.setItemChecked(pos, true);
//                if (view instanceof Checkable){
//                    ((Checkable)view).setChecked(true);
//                }
                
            }
        });
        
        _gridThumbs.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int pos, long id)
            {
                removeItemWithAnimation(pos);
                
                return true;
            }
        });
        
        
        _imgPreview.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                boolean isVideo = FileUtil.isVideoFile(_curItem);
                log("SCGalleryPager.onClick() - file: %s, isVideo: %b", _curItem, isVideo);
                if (isVideo){
                    startVideo();
                }
            }
        });
        
        
        _editTitle.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                _descriptions.put(_curItem, charSequence.toString());
//                log("SCGalleryPager.onTextChanged() - file: %s, description: %s", _curItem, charSequence.toString()));
            }

            @Override
            public void afterTextChanged(Editable editable)
            {

            }
        });
        
        _btnDelete.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                removeItemWithAnimation(getCurrentItemPos());
            }
        });
    }

    public List<SCImageAndDescription> getImageAndDescriptionList()
    {
        List<SCImageAndDescription> result = new ArrayList<>();
        List<String> items = getItems();
        if (items == null){
            return result;
        }

        for (String item : items) {
            String description = _descriptions.get(item);
            SCImageAndDescription cur = new SCImageAndDescription();
            cur.image = item;
            cur.description = description;
            result.add(cur);
        }
        
        return result;
    }

    private int getCurrentItemPos()
    {
        return getItems().indexOf(_curItem);
    }
    
    private boolean removeItemWithAnimation(final int pos)
    {
        
        View v = getThumbByPosition(pos);
        
        if (pos < 0 || pos >= getItems().size()){
            return false;
        }

        if (v == null){
            removeItem(pos);
        }
        else {
            v.animate().setDuration(300)
                    .alpha(0)
                    .scaleX(0).scaleY(0)
                    .withEndAction(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            removeItem(pos);
                        }
                    });
        }
        
        return true;
    }

    private void removeItem(int pos)
    {
        final ImageAdapter adapter = (ImageAdapter)_gridThumbs.getAdapter();
        List<String> items = getItems();

        items.remove(pos);
        adapter.notifyDataSetChanged();

        if (items.size() == 0) {
            _imgPreview.setImageBitmap(null);
        }
        else if (pos == items.size()) {
            startPreviewLoadingTask(items.get(pos - 1));
            _gridThumbs.setItemChecked(pos - 1, true);
        }
        else {
            startPreviewLoadingTask(items.get(pos));
            _gridThumbs.setItemChecked(pos, true);
        }
    }


    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus)
    {
        super.onWindowFocusChanged(hasWindowFocus);

        if (hasWindowFocus
            && _fistWindowFocusChanged
            && getItems().size() > 0) 
        {
            startPreviewLoadingTask(getItems().get(0));
            _gridThumbs.setItemChecked(0, true);
            _fistWindowFocusChanged = false;
        }
    }
    
    public List<String> getItems()
    {
        if (_gridThumbs == null || _gridThumbs.getAdapter() == null) {
            return new ArrayList<>();
        }
        
        return ((ImageAdapter)_gridThumbs.getAdapter()).getItems();
    }

    @Override
    protected void onAttachedToWindow()
    {
        _executor = Executors.newFixedThreadPool(2);

        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow()
    {

        _executor.shutdown();
        
        _thumbCache.evictAll();
        super.onDetachedFromWindow();
    }
    
    private void startVideo()
    {
        showVideo(true);
        _videoView.setVideoPath(_curItem);
        _videoView.start();
        _videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
        {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer)
            {
                showVideo(false);
            }
        });
    }
    
    private void stopVideo()
    {
        _videoView.stopPlayback();
    }
    
    private void showVideo(boolean show)
    {
        if (show){
            _imgVidOverlay.setVisibility(View.GONE);
            _videoView.setVisibility(View.VISIBLE);
            _imgPreview.setVisibility(View.GONE);
        }
        else{
            stopVideo();
            _videoView.setVisibility(View.GONE);
            _imgPreview.setVisibility(View.VISIBLE);

            updateVideoOverlay();
        }
    }

    private void startPreviewLoadingTask(String file)
    {
        _curItem = file;

        showVideo(false);
        String curDescription = _descriptions.get(file);

        _editTitle.setText(curDescription);


        _progressIdle.setVisibility(View.VISIBLE);

        updateVideoOverlay();
        
        _executor.execute(new ImageLoadingTask(getContext(), _imgPreview, file, false));
        
    }
    
    private void updateVideoOverlay()
    {
        if (FileUtil.isVideoFile(_curItem)){
            _imgVidOverlay.setVisibility(View.VISIBLE);
        }
        else{
            _imgVidOverlay.setVisibility(View.GONE);
        }
    }

    public void setLimit(int limit)
    {
        _limit = (limit != 0)
                    ? limit
                    : Integer.MAX_VALUE;
    }

    public void setFiles(final List<String> files)
    {

        if (files == null) {
            return;
        }

        final ImageAdapter adapter = new ImageAdapter(getContext(), files);
        _gridThumbs.setAdapter(adapter);

        if (files.size() > 0) {
            _gridThumbs.setItemChecked(0, true);
            startPreviewLoadingTask(files.iterator().next());
        }

    }


    private void showMediaPicker()
    {
        Intent intent = new Intent(getContext(), ImagePickerActivity.class);
        Config config = new PPDefaultConfig(getItems(), _limit);
//        Config config = new Config.Builder()
//                .setTabBackgroundColor(R.color.white)    // set tab background color. Default white.
//                .setTabSelectionIndicatorColor(R.color.blue)
//                .setCameraButtonColor(R.color.orange)
//                .setSelectedItems(getItems())
//                .setSelectionLimit(_limit)
//                .build();
        ImagePickerActivity.setConfig(config);
        ((Activity) getContext()).startActivityForResult(intent, PPRequestCodes.PICK_IMAGES);
    }


    public static Bitmap toThumb(Context ctx, String file)
    {

        if (FileUtil.isVideoFile(file)) {
            return ThumbnailUtils.createVideoThumbnail(file, MediaStore.Video.Thumbnails.MINI_KIND);
        }
        else {
            int size = ctx.getResources().getDimensionPixelSize(R.dimen.sc_gallery_pager_thumb_size);
            Bitmap b = decodeFile(file, size, size);
            return b;
        }
    }

    public static Bitmap decodeFile(String file, int reqWidth, int reqHeight)
    {


        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap b = BitmapFactory.decodeFile(file, options);

        b = BitmapUtil.correctRotation(file, b);

        return b;

    }


    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
    {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                   && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    public View getThumbByPosition(int pos) {
        final int first = _gridThumbs.getFirstVisiblePosition();
        final int last = first + _gridThumbs.getChildCount() - 1;

        if (pos < first || pos > last ) {
            return null;
        } else {
            final int childIndex = pos - first;
            return _gridThumbs.getChildAt(childIndex);
        }
    }
    
//    private int getItemSize()
//    {
//        int count = _gridThumbs.getAdapter().getCount();
//        
//        int w = _gridThumbs.getWidth();
//        int h = _gridThumbs.getHeight();
//        int s = getResources().getDimensionPixelSize(R.dimen.sc_gallery_pager_item_spacing);
//
//
//
//        int maxSize = getContext().getResources().getDimensionPixelSize(R.dimen.sc_gallery_pager_thumb_size);
//
//        int size = maxSize;
//        
//        if (w == 0 || _maxRows == 0)
//        {
//            return maxSize;
//        }
//
//        int itemsPerRow = w / (maxSize + s);
//
//
//        int rows = (int)Math.ceil((double)count / itemsPerRow);
//        int remaining = count % itemsPerRow;
//
//        double scaleFactor = 1.;
//        
//        if (rows > _maxRows ) {
//            scaleFactor = .5;//1. - ((double)remaining / count);
//            size = (int)(scaleFactor * size);
//        }
//
//
//        log("ImageAdapter.getItemSize() - girdView: %dx%d, maxSize: %d, count: %d, spacing: %d, itemsPerRow: %d, rows: %d (%d) -> f: %f, size: %d", w, h, maxSize, count, s, itemsPerRow, rows, remaining, scaleFactor, size);
//
//        return size;
//    }

    public class ImageAdapter
            extends ArrayAdapter<String>
    {
        public static final int TYPE_IMAGE = 0;
        public static final int TYPE_VIDEO = 1;
        public static final int TYPE_ADD   = 2;

        private Context      _ctx;
        private List<String> _items;
        

        public ImageAdapter(Context context, List<String> items)
        {
            super(context, 0, items);
            _ctx = context;
            _items = items;
        }

        public List<String> getItems()
        {
            return _items;
        }

        @Override
        public int getCount()
        {
            return _items.size() + 1;
        }

        @Override
        public int getViewTypeCount()
        {
            return 3;
        }

        @Override
        public int getItemViewType(int position)
        {
            if (position == _items.size()) {
                return TYPE_ADD;
            }

            boolean isVideo = FileUtil.isVideoFile(getItem(position));

            return isVideo
                   ? TYPE_VIDEO
                   : TYPE_IMAGE;
        }

        

        private View createView(int type)
        {
            log("ImageAdapter.createView() - tpye: " + type);
            View v = null;
            int size = getContext().getResources().getDimensionPixelSize(R.dimen.sc_gallery_pager_thumb_size);

            switch (type) {
                case TYPE_IMAGE:
                    v = LayoutInflater.from(_ctx).inflate(R.layout.sc_gallery_item_image, null);
                    break;
                case TYPE_VIDEO:
                    v = LayoutInflater.from(_ctx).inflate(R.layout.sc_gallery_item_video, null);
                    break;
                case TYPE_ADD:
                    v = LayoutInflater.from(_ctx).inflate(R.layout.sc_gallery_item_add, null);
                    break;

            }

            if (v != null) {
                v.setLayoutParams(new GridView.LayoutParams(size, size));
            }
            return v;
        }


        public View getView(int position, View convertView, ViewGroup parent)
        {

//            log("ImageAdapter.getView() - pos: %d, type: %d", position, getItemViewType(position)));

            View v = convertView;
            ImageView imageView;
            
            if (v == null) {
                v = createView(getItemViewType(position));

                if (getItemViewType(position) == TYPE_ADD) {
                    return v;
                }

                imageView = (ImageView) v.findViewById(R.id.image);
                v.setTag(new ViewHolder(imageView));
            }
            else {
                if (getItemViewType(position) == TYPE_ADD) {
                    return convertView;
                }

                ViewHolder holder = (ViewHolder)v.getTag(); 
                imageView = holder.image;
            }

            
            imageView.setHasTransientState(true);


            String curFile = _items.get(position);

            _executor.execute(new CreateThumbnailTask(getContext(), imageView, curFile).preExecuteNow());
            restoreView(v);
            return v;
        }
        

        private void restoreView(View v)
        {
            v.setAlpha(1);
            v.setScaleX(1);
            v.setScaleY(1);
        }


        private class ViewHolder
        {
            public ViewHolder(ImageView image)
            {
                this.image = image;
            }

            ImageView image;
        }

    }

    public class CreateThumbnailTask
            extends ImageLoadingTask
    {

        public CreateThumbnailTask(Context ctx, ImageView view, String file)
        {
            super(ctx, view, file, true);
        }

        @Override
        protected Bitmap doInBackground(Context ctx, String file)
        {
            Bitmap cached = _thumbCache.get(file);
            if (cached != null){
                log("CreateThumbnailTask.doInBackground() - cache hit: " + file);
                return cached;
            }
            
            Bitmap b = toThumb(ctx, file);
            _thumbCache.put(file, b);
            return b;
        }
    }


    public class ImageLoadingTask
            implements Runnable
    {
        private final WeakReference<ImageView> _imgRef;
        private final WeakReference<Context>   _ctxRef;
        private final boolean                  _placeholderWhileLoading;
        private       String                   _file;
        private boolean _preExecuteDone = false;

        public ImageLoadingTask(Context ctx, ImageView imageView, String file, boolean placeholderWhileLoading)
        {
            _imgRef = new WeakReference<>(imageView);
            _ctxRef = new WeakReference<>(ctx);
            _file = file;
            _placeholderWhileLoading = placeholderWhileLoading;
        }
        
        public ImageLoadingTask preExecuteNow()
        {
            _preExecuteDone = true;
            onPreExecute();
            return this;
        }

        protected void onPreExecute()
        {
            ImageView imageView = _imgRef.get();
            
            boolean cacheHit = _thumbCache.get(_file) != null;
            
            if (imageView != null
                && _placeholderWhileLoading
                && !cacheHit)
            {
                imageView.setImageResource(R.drawable.sc_placeholder_image);
            }
            _preExecuteDone = true;
        }

        protected Bitmap doInBackground(Context ctx, String file)
        {

            long start = System.currentTimeMillis();

            ImageView v = _imgRef.get();
            if (v == null) {
                return null;
            }


//            log("ImageLoadingTask.doInBackground() - size: %dx%d", v.getWidth(), v.getHeight()));

            if (FileUtil.isVideoFile(_file)) {
                return BitmapUtil.createVideoThumbnail(_file, v.getWidth(), v.getHeight());
//                return ThumbnailUtils.createVideoThumbnail(_file, MediaStore.Video.Thumbnails.MINI_KIND);
            }

            Bitmap b = decodeFile(_file, v.getWidth(), v.getHeight());

            log("decoding duration: " + (System.currentTimeMillis() - start));

            return b;
        }

        @Override
        public void run()
        {

            Context ctx = _ctxRef.get();
            if (ctx == null) {
                Log.e("err", "* * * Error: CreateThumbnailTask.run() - context is null!");
                return;
            }

            Bitmap result = null;
            try {
                if (getHandler() == null) {
                    return;
                }
                getHandler().post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        onPreExecute();
                    }
                });


                result = doInBackground(ctx, _file);

                final Bitmap fresult = result;


                if (getHandler() == null) {
                    return;
                }

                getHandler().post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        onPostExecute(fresult);
                    }
                });
            }
            catch (Exception e) {
                if (result != null) {
                    result.recycle();
                }

                e.printStackTrace();
            }
            finally {
                ImageView imageView = _imgRef.get();
                if (imageView != null) {
                    imageView.setHasTransientState(false);
                }
            }
        }

        protected void onPostExecute(Bitmap bitmap)
        {
            if (bitmap == null) {
                Log.w("tmp", "* * * Warning: CreateThumbnailTask.onPostExecute() - bitmap is null!");
                return;
            }

//            log("ImageLoadingTask.onPostExecute() - file: %s, bitmap: %dx%d", _file, bitmap.getWidth(), bitmap.getHeight()));

            ImageView imageView = _imgRef.get();
            if (imageView != null) {
                if (bitmap != null) {
                    imageView.setImageBitmap(bitmap);
                }
                else {
                    Drawable placeholder = imageView.getContext().getResources().getDrawable(R.drawable.sc_placeholder_image);
                    imageView.setImageDrawable(placeholder);

                }
            }

            if (imageView == _imgPreview) {
                _progressIdle.setVisibility(View.GONE);
                
            }
        }
    }
    
    private void log(String msg, Object... args)
    {
        Log.d("tmp", String.format(msg, args));
    }

}
