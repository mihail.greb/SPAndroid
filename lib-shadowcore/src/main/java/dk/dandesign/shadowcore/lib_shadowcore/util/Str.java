package dk.dandesign.shadowcore.lib_shadowcore.util;

import android.telephony.PhoneNumberUtils;

import com.c2call.sdk.pub.util.StringPair;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Str
{
    private static final Pattern __emailRegex = Pattern.compile("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+"
                    + "(aero|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)\\b",
            Pattern.CASE_INSENSITIVE);


    public static final String NON_ALPHABETIC = " \t0123456789.,:;@$+-?*/\\\"'#~(){}[]<>=|&";
    public static String toSafeString(final Object s)
    {
        return (s == null)
                    ? ""
                    : s.toString();
    }

    public static String toSafeString(final String s)
    {
        return (s == null)
                    ? ""
                    : s;
    }

    public static String toString(final InputStream stream) throws IOException
    {
        final ByteArrayOutputStream baf = new ByteArrayOutputStream();
        int read = 0;
        final int bufSize = 512;
        final byte[] buffer = new byte[bufSize];
        while((read = stream.read(buffer)) >= 0)
        {
            baf.write(buffer, 0, read);
        }

        return new String(baf.toByteArray(), "utf-8");
    }


    public static boolean isLonger(final String s, final int len)
    {
        return isLonger(s, len, false);
    }

    public static boolean isLonger(final String s, final int len, final boolean acceptEqual)
    {
        if (s == null){
            return false;
        }

        return acceptEqual
                ? s.length() >= len
                : s.length() > len;
    }


    public static boolean isInteger(final String s)
    {
        if (isEmpty(s)){
            return false;
        }

        for (int i = 0; i < s.length(); ++i){
            final char c = s.charAt(i);

            final boolean isFirstNeg = (i == 0 && c == '-');
            if (s.length() > 1
                && isFirstNeg)
            {
                continue;
            }

            if (c < '0' || c > '9')
            {
                return false;
            }
        }

        return true;
    }

    public static boolean isPositiveInteger(final String s)
    {
        if (isEmpty(s)){
            return false;
        }

        for (int i = 0; i < s.length(); ++i){
            final char c = s.charAt(i);
            if (c < '0' || c > '9'){
                return false;
            }
        }

        return true;
    }

    public static String toEclipseString(final String s, final int maxLength)
    {
        if (s.length() <= maxLength
            || maxLength < 4)
        {
            return s;
        }

        final StringBuilder builder = new StringBuilder(maxLength);
        builder.append(s.substring(0, maxLength - 3)).append("...");

        return builder.toString();
    }

    public static boolean isEmpty(final String s)
    {
        return (s == null
                || s.length() == 0);
    }

    public static boolean isEmpty(final String s, final String... others)
    {
        if (others == null){
            return isEmpty(s);
        }
        else{
            for (final String cur : others){
                if (isEmpty(cur)){
                    return true;
                }
            }
        }

        return false;
    }


    public static String trimIfNecessary(final String s)
    {
        if (isEmpty(s)){
            return s;
        }

        if (s.charAt(0) == ' '
            || s.charAt(0) == '\t'
            || s.charAt(s.length() - 1) == ' '
            || s.charAt(s.length() -1) == '\t')
        {
            return s.trim();
        }

        return s;
    }

    public static String toHexString(final byte[] a)
    {
        final StringBuilder sb = new StringBuilder(a.length * 2);
        for (final byte element : a) {
            sb.append(Character.forDigit((element & 0xf0) >> 4, 16));
            sb.append(Character.forDigit(element & 0x0f, 16));
        }
        return sb.toString();
    }

    public static byte[] hexStringToByteArray(final String s) {
        if (isEmpty(s)){
            return null;
        }

        final int len = s.length();
        final byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static boolean isPhonenumber(final String s)
    {
        return !isEmail(s) && PhoneNumberUtils.isGlobalPhoneNumber(s);
    }

    public static boolean isEqual(final String s1, final String s2)
    {
        if (s1 == s2){
            return true;
        }

        if ((s1 == null) != (s2 == null) ){
            return false;
        }

        return s1.equals(s2);
    }

    public static boolean isWideString(final String s)
    {
        final char[] chars = s.toCharArray();
        for (final char c : chars){
            if (c > 255){
                return true;
            }
        }

        return false;
    }



    public static String[] toNonEmptyStringArray(final String... s)
    {
        if (s == null){
            return null;
        }

        final ArrayList<String> list = new ArrayList<String>();

        for (final String cur : s){
            if (!isEmpty(cur)){
                list.add(cur);
            }
        }

        return list.isEmpty()
                    ? null
                    : list.toArray(new String[list.size()]);
    }

    public static String join(final String[] array, final String delimiter)
    {
        if (array == null
            || delimiter == null)
        {
            return null;
        }

        final StringBuilder builder = new StringBuilder();

        for (int i = 0; i < array.length; ++i){
            builder.append(array[i]);
            if (i < array.length - 1){
                builder.append(delimiter);
            }
        }

        return builder.toString();
    }



    public static String getRandom128()
    {
        return UUID.randomUUID().toString().replace("-", "");
    }


    public static StringPair splitAtLast(final String s, final String d)
    {
        if (isEmpty(s)
            || isEmpty(d))
        {
            return new StringPair(s, null);
        }

        final int pos = s.lastIndexOf(d);
        if (pos < 0){
            return new StringPair(s, null);
        }

        final String first = s.substring(0, pos);
        final String second = s.substring(pos + d.length());
        return new StringPair(first, second);
    }

    public static StringPair splitAtFirst(final String s, final String d)
    {
        if (isEmpty(s)
            || isEmpty(d))
        {
            return new StringPair(s, null);
        }

        final int pos = s.indexOf(d);
        if (pos < 0){
            return new StringPair(s, null);
        }

        final String first = s.substring(0, pos);
        final String second = s.substring(pos + d.length());
        return new StringPair(first, second);
    }

    public static boolean isHtml(final String s)
    {
        if (s == null){
            return false;
        }


        return s.startsWith("html://");
    }


    public static String createRepeat(final String s, final int count)
    {
        return createRepeat(s, count, ',');
    }


    public static String createRepeat(final String s, final int count, final String separator)
    {
        final StringBuilder b = new StringBuilder();

        for (int i = 0; i < count; ++i){
            b.append(s);
            if (i < count - 1){
                b.append(separator);
            }
        }

        return b.toString();
    }

    public static String createRepeat(final String s, final int count, final char separator)
    {
        final StringBuilder b = new StringBuilder();

        for (int i = 0; i < count; ++i){
            b.append(s);
            if (i < count - 1){
                b.append(separator);
            }
        }

        return b.toString();
    }

    public static boolean containsIgnoreCase(final String s, final String pattern)
    {
        if (s == null
            || pattern == null)
        {
            return false;
        }

        return s.toLowerCase().contains(pattern.toLowerCase());
    }

    public static boolean isEmail(final String s)
    {
        if (Str.isEmpty(s)){
            return false;
        }

        final Matcher m = __emailRegex.matcher(s);
        return m.find();
    }

    public static boolean isEqualNotEmpty(final String s1, final String s2)
    {
        return !isEmpty(s1, s2) && isEqual(s1, s2);
    }
}
