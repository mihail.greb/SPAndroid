package dk.dandesign.shadowcore.lib_shadowcore.controller.friendlist;

import android.content.Context;
import android.database.Cursor;

import com.c2call.sdk.pub.db.adapter.SCFriendCursorAdapter;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.core.decorator.IDecorator;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemController;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemControllerFactory;

public class FriendsAdapter extends SCFriendCursorAdapter
{


    public FriendsAdapter(final Context context,
                                 final Cursor c,
                                 final int layout,
                                 final IFriendListItemControllerFactory mediatorFactory,
                                 final SCViewDescription vd,
                                 final int flags)
    {
        super(context, c, layout,mediatorFactory, vd, flags);
        setEnableSections(false);

    }



    @Override
    protected IDecorator<IFriendListItemController> onCreateDecorator(final IFriendListItemController m)
    {
        return new FriendListItemDecorator();
    }

}
