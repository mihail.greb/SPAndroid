package dk.dandesign.shadowcore.lib_shadowcore.events;

import com.c2call.sdk.pub.eventbus.events.SCSingleValueEvent;

import dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory.CallHistoryItemController;

public class CallHistoryItemClickEvent
        extends SCSingleValueEvent<CallHistoryItemController>
{
    public CallHistoryItemClickEvent(CallHistoryItemController c)
    {
        super(c);
    }
}
