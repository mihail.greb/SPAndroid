package dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemCallViewHolder;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.util.SCSelectionManager;

import dk.dandesign.shadowcore.lib_shadowcore.R;

public class CallHistoryEditItemController extends CallHistoryItemController
{

    private CheckBox _cb;

    public CallHistoryEditItemController(View view, SCViewDescription viewDescription, SCBoardEventData data)
    {
        super(view, viewDescription, data);
    }

    public CheckBox getCheckBox()
    {
        return _cb;
    }

    @Override
    public void onMainViewClick(View v)
    {
        Ln.d("swtch", "CallHistoryEditItemController.onMainViewClick()");

        toggleSelection();

        onDecorate();
    }

    @Override
    protected void onBindViewHolder(IBoardListItemCallViewHolder vh)
    {
        super.onBindViewHolder(vh);

        _cb = (CheckBox)getView().findViewById(R.id.sw_callhistory_item_cb);
        _cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked)
            {
                if (isChecked) {
                    SCSelectionManager.instance().addToSelection(CallHistoryController.SEL_KEY, getData().getId());
                }
                else{
                    SCSelectionManager.instance().removeFromSelection(CallHistoryController.SEL_KEY, getData().getId());
                }
            }
        });

        closeSectionExpand();
    }

    private void toggleSelection()
    {
        SCSelectionManager.instance().toogleSelection(CallHistoryController.SEL_KEY, getData().getId());
    }
}
