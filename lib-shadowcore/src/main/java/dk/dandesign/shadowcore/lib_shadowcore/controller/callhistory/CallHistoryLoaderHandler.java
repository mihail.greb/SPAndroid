package dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory;

import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.db.util.core.DatabaseHelper;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemBaseController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemBaseViewHolder;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemControllerFactory;
import com.c2call.sdk.pub.gui.core.adapter.SCBaseControllerCursorAdapter;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;
import com.c2call.sdk.pub.gui.core.decorator.IDecorator;
import com.c2call.sdk.pub.gui.core.filter.IBaseFilter;
import com.c2call.sdk.pub.gui.core.filter.SCBoardFilterFactory;
import com.c2call.sdk.pub.gui.core.loader.SCBoardLoader;
import com.c2call.sdk.pub.gui.core.loaderhandler.ILoaderHandlerContextProvider;
import com.c2call.sdk.pub.gui.core.loaderhandler.SCBoardLoaderHandler;
import com.c2call.sdk.pub.util.IListViewProvider;
import com.c2call.sdk.pub.util.Validate;
import com.c2call.sdk.thirdparty.ormlite.dao.Dao;

import dk.dandesign.shadowcore.lib_shadowcore.events.CallHistoryUpdateEvent;
import dk.dandesign.shadowcore.lib_shadowcore.util.LoaderHandlers;

public class CallHistoryLoaderHandler extends SCBoardLoaderHandler
{
    private static final String[] PROJECTION = SCBoardEventData.PROJECTION_ALL;


    private CallHistoryFilter _filter;

    @Override
    public void onCreate()
    {
        super.onCreate();

        setAutoScroll(false);
    }

    public CallHistoryLoaderHandler(final ILoaderHandlerContextProvider controller,
                                    final IListViewProvider listViewProvider,
                                    final String filterUserid,
                                    final SCViewDescriptionMap viewDescriptions,
                                    final IBoardListItemControllerFactory controllerFactory)
    {
        super(controller, listViewProvider, filterUserid, viewDescriptions, controllerFactory);
        setFilterMask(SCBoardFilterFactory.FLAG_FILTER_CALLS);
        setLoaderId(LoaderHandlers.CALLHISTORY);
    }

    public void setFilter(final CallHistoryFilter filter)
    {
        Validate.notNull(filter, "filter must not be null");
        _filter = filter;

        switch (_filter)
        {
            case Missed:
                setFilterMask(SCBoardFilterFactory.FLAG_FILTER_MISSED_CALLS | SCBoardFilterFactory.FLAG_FILTER_NOMEETINGS | SCBoardFilterFactory.FLAG_FILTER_NOBROADCASTS);
                break;
            default:
                setFilterMask(SCBoardFilterFactory.FLAG_FILTER_CALLS | SCBoardFilterFactory.FLAG_FILTER_NOMEETINGS | SCBoardFilterFactory.FLAG_FILTER_NOBROADCASTS);

        }

        restart();
    }

    @Override
    protected SCBaseControllerCursorAdapter<SCBoardEventData,
            IBoardListItemBaseController<? extends IBoardListItemBaseViewHolder>,
            IBoardListItemControllerFactory,
            IDecorator<IBoardListItemBaseController<?>>>
    onCreateAapder(final Cursor c,
                   final IBoardListItemControllerFactory controllerFactory,
                   final SCViewDescriptionMap viewDescriptions)
    {
        return new CallHistoryCursorAdapter(getContext(),
                c,
                controllerFactory,
                viewDescriptions,
                CallHistoryCursorAdapter.DECORATOR_MAP,
                0);
    }


    @Override
    public Loader<Cursor> onCreateLoader(final int id, final Bundle arg)
    {
        Ln.d("swytch", "CallHistoryLoaderHandler.onCreateLoader() - id: %d, filterMask: %d  /  this: %s", id, getFilterMask(), this);

        switch (id)
        {
            case LoaderHandlers.CALLHISTORY:
                if (_filter != null) {
                    switch (_filter) {
                        case Missed:
                            setFilterMask(SCBoardFilterFactory.FLAG_FILTER_MISSED_CALLS | SCBoardFilterFactory.FLAG_FILTER_NOMEETINGS | SCBoardFilterFactory.FLAG_FILTER_NOBROADCASTS);
                            break;
                        default:
                            setFilterMask(SCBoardFilterFactory.FLAG_FILTER_CALLS | SCBoardFilterFactory.FLAG_FILTER_NOMEETINGS | SCBoardFilterFactory.FLAG_FILTER_NOBROADCASTS);

                    }
                } else {
                    setFilterMask(SCBoardFilterFactory.FLAG_FILTER_CALLS | SCBoardFilterFactory.FLAG_FILTER_NOMEETINGS | SCBoardFilterFactory.FLAG_FILTER_NOBROADCASTS);
                }


                final Dao<SCBoardEventData, String> dao = DatabaseHelper.getObservableDao(getContext(), SCBoardEventData.class);
                final SCBoardLoader loader = new SCBoardLoader(getContext(), dao, PROJECTION);
                loader.addOrderBy(SCBoardEventData.TIMEVALUE, false);
                loader.setReverse(true);


                IBaseFilter<SCBoardEventData, String> filter = onCreateFilter(getFilterUserid(), getFilterQuery(), getFilterMask());

                loader.setGroupBy(SCBoardEventData.USERID);

                loader.setFilter(filter);
                return loader;
            default:
                //Debug.ensure(false, "unhandled loader id: " + id);
                return null;
        }
    }


    @Override
    protected void onMessagesLoadFinished(Cursor cursor)
    {
        Ln.d("fc_tmp", "CallHistoryLoaderHandler.addLineFilter() - size: %d", (cursor != null) ? cursor.getCount() : -1);
        CallHistoryUpdateEvent evt = (cursor != null)
                ? new CallHistoryUpdateEvent(cursor.getCount())
                : new CallHistoryUpdateEvent(0);

        SCCoreFacade.instance().postEvent(evt, true);

        super.onMessagesLoadFinished(cursor);
    }


}
