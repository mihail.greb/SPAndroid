package dk.dandesign.shadowcore.lib_shadowcore.controller.friendlist;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.c2call.lib.androidlog.Ln;
import com.c2call.lib.xml.StringExtra;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.SCThreadMode;
import com.c2call.sdk.pub.eventbus.events.SCFriendsUpdateEvent;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.common.SCListModus;
import com.c2call.sdk.pub.gui.core.controller.IControllerRequestListener;
import com.c2call.sdk.pub.gui.core.controller.IFilterListViewHolder;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;
import com.c2call.sdk.pub.gui.core.filter.SCFriendFilterFactory;
import com.c2call.sdk.pub.gui.core.loaderhandler.IFriendLoaderHandler;
import com.c2call.sdk.pub.gui.core.loaderhandler.ILoaderHandlerContextProvider;
import com.c2call.sdk.pub.gui.core.loaderhandler.SCBaseLoaderHandler;
import com.c2call.sdk.pub.gui.custom.ClearableEditText;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.gui.friendlistitem.controller.IFriendListItemControllerFactory;
import com.c2call.sdk.pub.gui.friends.controller.IFriendsViewHolder;
import com.c2call.sdk.pub.gui.friends.controller.SCFriendsController;
import com.c2call.sdk.pub.util.IListViewProvider;

import java.util.List;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.activities.SDCTabBarControllerActivity;
import dk.dandesign.shadowcore.lib_shadowcore.events.ConsumeBackPressEvent;
import dk.dandesign.shadowcore.lib_shadowcore.util.BackPressConsumer;

public class FriendsController extends SCFriendsController {

    public FriendsController(View view, SCViewDescription scViewDescription, SCViewDescription scViewDescription1, IFriendListItemControllerFactory iFriendListItemControllerFactory, IControllerRequestListener iControllerRequestListener) {
        super(view, scViewDescription, scViewDescription1, iFriendListItemControllerFactory, iControllerRequestListener);
    }

}
