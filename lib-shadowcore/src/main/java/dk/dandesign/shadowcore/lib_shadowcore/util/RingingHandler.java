package dk.dandesign.shadowcore.lib_shadowcore.util;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.common.SCCallStatus;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.events.SCCallEvent;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.facade.SCMediaFacade;

public class RingingHandler
{
    private static final RingingHandler __instance = new RingingHandler();
    public static final RingingHandler instance()
    {
        return __instance;
    }

    private RingingHandler()
    {

    }

    public void subscribe()
    {
        SCCoreFacade.instance().subscribe(this);
    }

    public void unsubscribe()
    {
        SCCoreFacade.instance().unsubscribe(this);
    }

    @SCEventCallback
    private void onEvent(final SCCallEvent evt)
    {
        Ln.d("c2app", "RingingHandler.onEvent() - evt: %s", evt);

        if (Str.isPhonenumber(evt.getUserid())){
            Ln.d("c2app", "RingingHandler.onEvent() - phone number -> ignore");
            return;
        }

        if (evt.getStatus() == SCCallStatus.Ringing){
            SCMediaFacade.instance().startRinging();
        }
        else{
            SCMediaFacade.instance().stopRinging();
        }
    }

}
