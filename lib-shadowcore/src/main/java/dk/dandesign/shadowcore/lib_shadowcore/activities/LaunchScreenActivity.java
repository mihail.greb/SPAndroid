package dk.dandesign.shadowcore.lib_shadowcore.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.c2call.sdk.pub.activities.SCLoginFragmentActivity;
import com.c2call.sdk.pub.activities.SCRegisterFragmentActivity;
import com.c2call.sdk.pub.core.SCExtraData;

import dk.dandesign.shadowcore.lib_shadowcore.R;

public class LaunchScreenActivity extends Activity {

    public static final int ACTIVITY_LOGIN = 200 + 1;
    public static final int ACTIVITY_REGISTER = 200 + 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_screen);
    }

    public void btnLoginClicked(View v) {
        Intent intent = new Intent(this, LoginFragmentActivity.class);
        intent.putExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, R.layout.sc_login);
        startActivityForResult(intent, ACTIVITY_LOGIN);
    }

    public void btnRegisterClicked(View v) {
        Intent intent = new Intent(this, RegisterFragmentActivity.class);
        intent.putExtra(SCExtraData.BaseFragmentData.EXTRA_DATA_LAYOUT, R.layout.sc_register);
        startActivityForResult(intent, ACTIVITY_REGISTER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Just in case we want to do something specific for Login or Register
        if (requestCode == ACTIVITY_LOGIN) {
            if (resultCode == Activity.RESULT_OK) {
                setResult(resultCode);
                finish();
            }
        }

        if (requestCode == ACTIVITY_REGISTER) {
            if (resultCode == Activity.RESULT_OK) {
                setResult(resultCode);
                finish();
            }
        }
    }
}
