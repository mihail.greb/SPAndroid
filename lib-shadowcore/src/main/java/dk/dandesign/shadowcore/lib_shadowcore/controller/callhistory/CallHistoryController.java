package dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory;

import android.app.Activity;
import android.database.Cursor;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CursorAdapter;
import android.widget.ListAdapter;

import com.c2call.lib.androidlog.Ln;
import com.c2call.lib.security.Str;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.db.util.board.BoardDeleteHandler;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.SCThreadMode;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.board.controller.IBoardViewHolder;
import com.c2call.sdk.pub.gui.board.controller.SCBoardController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemControllerFactory;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemType;
import com.c2call.sdk.pub.gui.boardlistitem.decorator.SCBoardListItemBaseDecorator;
import com.c2call.sdk.pub.gui.boardlistitem.decorator.SCBoardListItemRichDecorator;
import com.c2call.sdk.pub.gui.boardlistitem.decorator.SCBoardListItemTextDecorator;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.common.SCListModus;
import com.c2call.sdk.pub.gui.core.controller.IControllerRequestListener;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;
import com.c2call.sdk.pub.gui.core.loaderhandler.ILoaderHandlerContextProvider;
import com.c2call.sdk.pub.gui.core.loaderhandler.SCBoardLoaderHandler;
import com.c2call.sdk.pub.util.IListViewProvider;
import com.c2call.sdk.pub.util.SCSelectionManager;
import com.c2call.sdk.pub.util.SimpleAsyncTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.events.CallHistoryUpdateEvent;
import dk.dandesign.shadowcore.lib_shadowcore.events.ConsumeBackPressEvent;
import dk.dandesign.shadowcore.lib_shadowcore.util.BackPressConsumer;

public class CallHistoryController extends SCBoardController
{
    public static final String SEL_KEY = CallHistoryController.class.getName();

    private View _placeholderEmptyList;

    private SCListModus _modus = SCListModus.Normal;

    private SwipeRefreshLayout _swipeRefreshLayout;

    public CallHistoryController(final View view,
                                 final SCViewDescription viewDescription,
                                 final SCViewDescriptionMap itemViewDescriptions,
                                 final IBoardListItemControllerFactory itemMediatorFactory,
                                 final IControllerRequestListener requestListener)
    {
        super(view, viewDescription, itemViewDescriptions, itemMediatorFactory, requestListener);

    }


    @Override
    public SCBoardLoaderHandler onCreateLoaderHandler(final IListViewProvider listViewProvider,
                                                      final ILoaderHandlerContextProvider loaderHandlerMasterProvider,
                                                      final IBoardListItemControllerFactory itemMediatorFactory,
                                                      final SCViewDescriptionMap itemDescriptions)
    {
        return new CallHistoryLoaderHandler(loaderHandlerMasterProvider,
                listViewProvider,
                getUserId(),
                itemDescriptions,
                itemMediatorFactory);
    }

    @Override
    public void onCreate(Activity activity, SCActivityResultDispatcher scActivityResultDispatcher)
    {
        super.onCreate(activity, scActivityResultDispatcher);

        SCCoreFacade.instance().subscribe(this);
    }


    @Override
    public synchronized void onDestroy()
    {
        super.onDestroy();

        SCCoreFacade.instance().unsubscribe(this);
    }

    @Override
    protected void onBindViewHolder(final IBoardViewHolder vh)
    {
        super.onBindViewHolder(vh);

        //_placeholderEmptyList = getView().findViewById(R.id.sc_empty_list_placeholder);

        _swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_container);
        _swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light, android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        _swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                if (getLoaderHandler() != null) {
                    SCCoreFacade.instance().updateBoard();
                }

            }
        });

    }

    @SCEventCallback(threadMode = SCThreadMode.MainThread, isSticky = true)
    private void onEvent(CallHistoryUpdateEvent evt)
    {
        //Ln.d("swytch", "CallHistoryController.onEvent() - evt: %s, searchText: %s, placeHolder: %s (%s)", evt, getSearchText(), _placeholderEmptyList, getView().findViewById(R.id.sc_empty_list_placeholder));

        boolean visible = evt.getCount() == 0
                && Str.isEmpty(getSearchText());

        setVisibility(_placeholderEmptyList, visible);

        if (_swipeRefreshLayout.isRefreshing()) {
            _swipeRefreshLayout.setRefreshing(false);
        }
    }

    public SCListModus getListModus() {
        return _modus;
    }

    public void toggleListModus()
    {
        if (_modus == SCListModus.Normal)
        {
            _modus = SCListModus.Delete;
        } else
        {
            SCSelectionManager.instance().clear(SEL_KEY);
            _modus = SCListModus.Normal;
        }

        setListModus(_modus);
    }

    public void onConfirmDeletion()
    {

        new SimpleAsyncTask<Boolean>(getContext(), 0 , null, getContext().getResources().getString(R.string.app_error_select))
        {
            @Override
            protected Boolean doInBackground(Void... voids)
            {
                return deleteSelectedMessages() == BoardDeleteHandler.ERROR_NONE;
            }

            @Override
            protected void onSuccess(Boolean aBoolean)
            {
                SCSelectionManager.instance().clear(SEL_KEY);
                toggleListModus();
                getContext().invalidateOptionsMenu();
            }
        }.execute();

    }

    private int deleteSelectedMessages()
    {
        Set<String> selection = SCSelectionManager.instance().getSelection(SEL_KEY);
        Ln.d("fc_test", "selection: %s", selection);

        if (selection == null) {
            return BoardDeleteHandler.ERROR_UNKNOWN;
        }

        List<String> selectionAsList = new ArrayList<String>(selection);

        int result = BoardDeleteHandler.instance().deleteCalls(selectionAsList);

        Ln.d("swytch", "CallHistoryController.deleteSelectedMessages() - result: %d", result);
        return result;
    }


    protected ListAdapter onCreateDeleteModeAdapter(final ListAdapter curAdapter)
    {
        if (!(curAdapter instanceof CursorAdapter))
        {
            throw new IllegalStateException("the current adapter is not a CursorAdapter. If you have overriden the default adapter then you have to override this method, too");
        }


        Map<SCBoardListItemType, SCBoardListItemBaseDecorator> decmap = new TreeMap<SCBoardListItemType, SCBoardListItemBaseDecorator>();
        decmap.put(SCBoardListItemType.Call, new CallHistoryEditItemDecorator());
        decmap.put(SCBoardListItemType.Text, new SCBoardListItemTextDecorator());
        decmap.put(SCBoardListItemType.Image, new SCBoardListItemRichDecorator());
        decmap.put(SCBoardListItemType.Video, new SCBoardListItemRichDecorator());
        decmap.put(SCBoardListItemType.Audio, new SCBoardListItemRichDecorator());
        decmap.put(SCBoardListItemType.Location, new SCBoardListItemRichDecorator());
        decmap.put(SCBoardListItemType.Friend, new SCBoardListItemRichDecorator());
        decmap.put(SCBoardListItemType.File, new SCBoardListItemRichDecorator());

        final Cursor c = ((CursorAdapter) curAdapter).getCursor();

        CallHistoryCursorAdapter adapter = new CallHistoryCursorAdapter(getContext(),
                c,
                new CallHistoryEditItemContrllerFactory(getResultDispatcher()),
                C2CallSdk.vd().boardListItem(),
                decmap,
                0);

        adapter.setViewInflator(new CallHistoryCursorAdapter.IViewInflator()
        {
            @Override
            public View onInflateView(CallHistoryCursorAdapter adapter, LayoutInflater inflater, Cursor cursor, SCBoardEventData data, ViewGroup parent)
            {
                return inflater.inflate(R.layout.sc_callhistory_edit_list_item, parent, false);
            }
        });

        return adapter;
    }

    protected ListAdapter onCreateNormalModeAdapter(final ListAdapter curAdapter)
    {
        if (!(curAdapter instanceof CursorAdapter))
        {
            throw new IllegalStateException("the current adapter is not a CursorAdapter. If you have overriden the default adapter then you have to override this method, too");
        }


        final Cursor c = ((CursorAdapter) curAdapter).getCursor();

        CallHistoryCursorAdapter adapter = new CallHistoryCursorAdapter(getContext(),
                c,
                getListItemControllerFactory(),
                C2CallSdk.vd().boardListItem(),
                CallHistoryCursorAdapter.DECORATOR_MAP,
                0);

        return adapter;
    }


    protected ListAdapter onCreateAdapter(final SCListModus modus, final ListAdapter curAdapter)
    {
        if (modus == SCListModus.Delete)
        {
            return onCreateDeleteModeAdapter(curAdapter);
        } else
        {
            return onCreateNormalModeAdapter(curAdapter);
        }
    }

    private void updateAdapterModus()
    {
        final AbsListView list = getViewHolder().getItemList();
        if (list == null)
        {
            return;
        }


        if (_modus == SCListModus.Delete)
        {
            SCSelectionManager.instance().clear(getSelectionKey());
        }


        final int curPos = list.getFirstVisiblePosition();

        final ListAdapter adapter = onCreateAdapter(_modus, list.getAdapter());

        list.setAdapter(adapter);
        final int maxPos = list.getCount() - 1;

        list.setSelection(Math.min(curPos, maxPos));
    }

    public String getSelectionKey()
    {
        return SEL_KEY;
    }

    public void setListModus(SCListModus modus) {
        _modus= modus;
        updateAdapterModus();
        getContext().invalidateOptionsMenu();
        boolean consumeBackPress = (modus != SCListModus.Normal);
        SCCoreFacade.instance().postEvent(new ConsumeBackPressEvent(consumeBackPress, BackPressConsumer.CALLHISTORY), false);

        Ln.d("fc_test", "consumeBackPress: %b", consumeBackPress);
    }

}
