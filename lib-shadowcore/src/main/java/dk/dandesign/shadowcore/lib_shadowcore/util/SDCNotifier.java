package dk.dandesign.shadowcore.lib_shadowcore.util;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.notifictaions.NotificationType;
import com.c2call.sdk.pub.notifictaions.SCMessageNotification;
import com.c2call.sdk.pub.notifictaions.SCMissedCallNotification;
import com.c2call.sdk.pub.notifictaions.SCNotifier;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.ShadowCoreApp;

/**
 * Created by mikel on 10.06.17.
 */

public class SDCNotifier extends SCNotifier {
    public final int coverNotificationId = 24568;

    public SDCNotifier() {
    }

    @Override
    public synchronized boolean isBlocked(NotificationType notificationType) {
        boolean isBlocked = super.isBlocked(notificationType);

        Ln.e("Notifier", "isBlocked: " + isBlocked);
        return isBlocked;
    }

    @Override
    public void onMessageNotification(Context context, SCMessageNotification scMessageNotification) {
        super.onMessageNotification(context, scMessageNotification);
    }

    @Override
    protected Notification onCreateMessageNotification(Context context, SCMessageNotification n)
    {
        Ln.d("fc_tmp", "SCNotifier.onCreateMessageNotification() - notification: %s", n);
        String messageSender = null;

        messageSender = getMessageSender(context, n);

        final PendingIntent intent = onCreateMessageIntent(context, n);

        Notification notification = null;
        if (ShadowCoreApp.getInstance().getShadowMode() == ShadowCoreApp.SDC_SHADOW_OPEN) {
            notification = new Notification.Builder(context)
                    .setContentTitle(getTitle(context, n))
                    .setContentText(getMessageText(context, messageSender, n))
                    .setTicker(getMessageTicker(context, messageSender, n))
                    .setSmallIcon(getNotifcationIcon(NotificationType.Message))
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(intent)
                    .getNotification();
        } else {
            notification = new Notification.Builder(context)
                    //.setContentTitle(getTitle(context, n))
                    //.setContentTitle("")
                    .setContentText(ShadowCoreApp.getInstance().getCoverMessage())
                    .setTicker("")
                    .setSmallIcon(R.drawable.app_bg_circle_black_trans)
                    //.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.app_bg_circle_black_trans))
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(intent)
                    .getNotification();
        }

        setSound(context, notification);

        notification.flags |= getMessageNotificationFlags();

        setMessageNotificationLed(notification);
        return notification;
    }

    @Override
    public void onMissedCallNotification(final Context context, final SCMissedCallNotification n)
    {
        if (ShadowCoreApp.getInstance().getShadowMode() == ShadowCoreApp.SDC_SHADOW_OPEN) {
            super.onMissedCallNotification(context, n);
            return;
        }

        final PendingIntent intent = onCreateMissedCallIntent(context, n);

        final Notification notification = new Notification.Builder(context)
                .setTicker("")
                .setWhen(System.currentTimeMillis())
                .setContentIntent(intent)
                .setContentTitle("")
                .setSmallIcon(R.drawable.app_bg_circle_black_trans)
                .setContentText(ShadowCoreApp.getInstance().getCoverMessage())
                .getNotification();

        notification.flags |= getMessageNotificationFlags();

        setMessageNotificationLed(notification);

        final int notificationId = getNotificationId(n.getType().getNotificationId());
        submitNotification(context, notificationId, notification);

    }


    public void onCoverMessageNotification(Activity context) {

        Notification notification = new Notification.Builder(context)
                .setContentTitle("")
                //.setContentTitle("")
                .setContentText(ShadowCoreApp.getInstance().getCoverMessage())
                .setTicker("")
                .setSmallIcon(R.drawable.app_bg_circle_black_trans)
                //.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.app_bg_circle_black_trans))
                .setWhen(System.currentTimeMillis())
                //.setContentIntent(intent)
                .getNotification();

        NotificationManager nm =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(coverNotificationId, notification);
    }

    //@Override
    public void onMissedCallNotificationX(Context context, SCMissedCallNotification scMissedCallNotification) {

        if (ShadowCoreApp.getInstance().getShadowMode() == ShadowCoreApp.SDC_SHADOW_OPEN) {
            super.onMissedCallNotification(context, scMissedCallNotification);
        }
    }
}
