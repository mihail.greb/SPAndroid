package dk.dandesign.shadowcore.lib_shadowcore.activities;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.activities.SCIncomingCallFragmentActivity;

public class IncomingCallActivity
        extends SCIncomingCallFragmentActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Ln.d("c2app", "IncomingCallActivity.onCreate()");

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        super.onCreate(savedInstanceState);


        if (getActionBar() != null){
            getActionBar().hide();
        }
    }

}
