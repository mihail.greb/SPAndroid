package dk.dandesign.shadowcore.lib_shadowcore.activities;

import android.app.Fragment;
import android.view.View;

import com.c2call.sdk.pub.activities.SCRegisterFragmentActivity;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.gui.register.controller.IRegisterController;

import dk.dandesign.shadowcore.lib_shadowcore.fragments.RegisterFragment;

/**
 * Created by mikel on 13.05.17.
 */

public class RegisterFragmentActivity extends SCRegisterFragmentActivity {


    @Override
    protected Fragment onCreateFragment() {
        Fragment f = new RegisterFragment();

        // forward arguments to the fragment
        f.setArguments(getIntent().getExtras());

        return f;
    }
}
