package dk.dandesign.shadowcore.lib_shadowcore.controller.chathistory;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemTextViewHolder;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.util.SCSelectionManager;

import dk.dandesign.shadowcore.lib_shadowcore.R;

public class ChatHistoryEditItemController extends ChatHistoryItemController
{

    private CheckBox _cb;

    public ChatHistoryEditItemController(View view, SCViewDescription viewDescription, SCBoardEventData data)
    {
        super(view, viewDescription, data);
    }

    public CheckBox getCheckBox()
    {
        return _cb;
    }

    @Override
    public void onMainViewClick(View v)
    {
        Ln.d("fc_tmp", "ChatHistoryEditItemController.onMainViewClick()");

        toggleSelection();

        onDecorate();
    }

    @Override
    protected void onBindViewHolder(IBoardListItemTextViewHolder vh)
    {
        super.onBindViewHolder(vh);

        _cb = (CheckBox)getView().findViewById(R.id.app_chathistory_item_cb);
        _cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked)
            {
                if (isChecked) {
                    SCSelectionManager.instance().addToSelection(ChatHistoryController.SEL_KEY, getData().getId());
                }
                else{
                    SCSelectionManager.instance().removeFromSelection(ChatHistoryController.SEL_KEY, getData().getId());
                }
            }
        });
    }

    private void toggleSelection()
    {
        SCSelectionManager.instance().toogleSelection(ChatHistoryController.SEL_KEY, getData().getId());
    }
}
