package dk.dandesign.shadowcore.lib_shadowcore.controller.polypicker;

public class PPRequestCodes
{
    public static final int PICK_IMAGES   = 100;
    public static final int PICK_N_IMAGES = 101;

    public static final int PROCESS_IMAGES = 102;
}
