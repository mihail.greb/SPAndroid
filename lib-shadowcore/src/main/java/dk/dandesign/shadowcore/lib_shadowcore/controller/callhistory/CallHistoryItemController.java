
package dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory;

import android.animation.Animator;
import android.app.Activity;
import android.view.View;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.core.C2CallSdk;
import com.c2call.sdk.pub.core.StartType;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.db.datamanager.SCMissedCallLookupManager;
import com.c2call.sdk.pub.db.datamanager.SCMissedCallManager;
import com.c2call.sdk.pub.eventbus.SCEventCallback;
import com.c2call.sdk.pub.eventbus.SCThreadMode;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemCallViewHolder;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemCallController;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;
import com.c2call.sdk.pub.notifictaions.NotificationType;

import dk.dandesign.shadowcore.lib_shadowcore.R;
import dk.dandesign.shadowcore.lib_shadowcore.events.CallHistoryItemClickEvent;
import dk.dandesign.shadowcore.lib_shadowcore.util.ContactsUtil;
import dk.dandesign.shadowcore.lib_shadowcore.util.Str;

public class CallHistoryItemController extends SCBoardListItemCallController
{
    private static String __expandedListItem;

    private View _sectionExpand;
    private View _videoCall;
    private View _audioCall;

    public CallHistoryItemController(final View view, final SCViewDescription viewDescription, final SCBoardEventData data)
    {
        super(view, viewDescription, data);
    }

    @Override
    public void onCreate(Activity activity, SCActivityResultDispatcher scActivityResultDispatcher)
    {
        super.onCreate(activity, scActivityResultDispatcher);

        SCCoreFacade.instance().subscribe(this);
    }

    @Override
    public synchronized void onDestroy()
    {
        super.onDestroy();
        SCCoreFacade.instance().unsubscribe(this);
    }

    @Override
    protected void onBindViewHolder(IBoardListItemCallViewHolder iBoardListItemCallViewHolder)
    {
        super.onBindViewHolder(iBoardListItemCallViewHolder);
        _sectionExpand = getView().findViewById(R.id.sc_boarditem_section_expand);
        _videoCall = getView().findViewById(R.id.sc_videocall_btn);
        _audioCall = getView().findViewById(R.id.sc_audiocall_btn);

        _videoCall.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                new Thread()
                {
                    @Override
                    public void run()
                    {
                        C2CallSdk.startControl().openCallbar(getContext(), null, getData().getUserid(), R.layout.sc_callbar, StartType.Activity);
                        SCCoreFacade.instance().call(getData().getUserid(), true, false);

                    }
                }.start();


                _videoCall.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        closeSectionExpand();
                    }
                }, 500);

            }
        });

        _audioCall.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                new Thread()
                {
                    @Override
                    public void run()
                    {
                        C2CallSdk.startControl().openCallbar(getContext(), null, getData().getUserid(), R.layout.sc_callbar, StartType.Activity);
                        SCCoreFacade.instance().call(getData().getUserid(), false, false);

                    }
                }.start();

                _audioCall.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        closeSectionExpand();
                    }
                }, 500);

            }
        });
    }

    public void closeSectionExpand()
    {
        __expandedListItem = null;
        setVisibility(_sectionExpand, false);
    }

    @Override
    public void onDecorate()
    {
        super.onDecorate();

        updateExpandViewVisibility();
        updateCallButtons();
    }

    protected void updateCallButtons()
    {
        if (getData() == null){
            return;
        }

        boolean showVideo = !Str.isPhonenumber(getData().getUserid());
        setVisibility(_videoCall, showVideo);
    }

    protected void updateExpandViewVisibility()
    {
        Ln.d("c2app", "CallHistoryItemController.updateExpandViewVisibility() - expandedListItem: %s, data: %s", __expandedListItem, getData());
        if (Str.isEmpty(__expandedListItem)
            || getData() == null)
        {
            setVisibility(_sectionExpand, false);
            return;
        }


        boolean visible = __expandedListItem.equals(getData().getId());
        setVisibility(_sectionExpand, visible);
    }

    @Override
    public void onMainViewClick(final View v)
    {
        Ln.d("c2app", "CallHistoryItemController.onMainViewClick() - userid: %s", getData().getUserid());

//        ServiceUtil.callWithLineSelect(getContext(), getData().getUserid());

        if (getData().getManager().isAnonymousCaller())
        {
            onHandleAnonymousClicked(getData());
        }
        else
        {
            onExpandViewClick(_sectionExpand);
        }

        SCCoreFacade.instance().postEvent(new CallHistoryItemClickEvent(this), false);
    }

    @SCEventCallback(threadMode = SCThreadMode.MainThread)
    private void onEvent(CallHistoryItemClickEvent evt)
    {
        if (evt.getValue() != this) {
            setVisibility(_sectionExpand, false);
        }
    }

    private void onHandleAnonymousClicked(SCBoardEventData data)
    {
        Ln.d("c2app", "CallHistoryItemController.onHandleAnonymousClicked() - data: %s", data);
        SCMissedCallManager.clear(data.getUserid());
        (C2CallSdk.notifier()).cancelNotification(getContext(), NotificationType.MissedCall.getNotificationId());
        SCMissedCallLookupManager.clear(data.getUserid());
    }

    private SCFriendData tryGetContact()
    {
        final String userid = getData().getUserid();
        Ln.d("c2app", "CallHistoryItemController.tryGetContact() - userid: %s", userid);

        String phoneNumber = null;


        if (Str.isPhonenumber(userid))
        {
            phoneNumber = userid;
        } else
        {
            try
            {
                final SCFriendData friend = SCFriendData.dao().queryForId(userid);
                Ln.d("c2app", "CallHistoryItemController.tryGetContact() - matched friend: %s", friend.getDisplayName());
                if (friend != null)
                {
                    phoneNumber = friend.getOwnNumber();
                }
            } catch (final Exception e)
            {
                e.printStackTrace();
            }
        }

        if (phoneNumber == null)
        {
            Ln.d("c2app", "CallHistoryItemController.tryGetContact() - phone number not found in local address book: %s", phoneNumber);
            return null;
        }

        final Long contactId = ContactsUtil.phoneNumberLookup(phoneNumber, false);
        Ln.d("c2app", "CallHistoryItemController.tryGetContact() - found contact of nnumber %s: %s", phoneNumber, contactId);
        if (contactId == null)
        {
            return null;
        }

        final SCFriendData friend = new SCFriendData("" + contactId);

        return friend;
    }

    @Override
    public void onPictureViewClick(final View v)
    {
//        final SCFriendData friend = tryGetContact();
//        friend.setUserType(SCFriendManager.TYPE_LOCAL_CONTACT);
//        C2CallSdk.startControl().openContactDetail(getContext(), null, R.layout.sc_contact_detail, friend, StartType.Activity);
    }

    private void onExpandViewClick(final View v)
    {
        Ln.d("c2app", "CallHistoryItemController.onExpandViewClick()");

        if (v.getVisibility() == View.GONE)
        {
            v.setVisibility(View.VISIBLE);
            v.setAlpha(0);

            v.animate()
                    .setDuration(300)
                    .alpha(1)
                    .setListener(new Animator.AnimatorListener()
                    {
                        @Override
                        public void onAnimationStart(Animator animator)
                        {
                            getView().requestLayout();
                        }

                        @Override
                        public void onAnimationEnd(Animator animator)
                        {
                            __expandedListItem = getData().getId();
                        }

                        @Override
                        public void onAnimationCancel(Animator animator)
                        {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator)
                        {

                        }
                    });
        } else
        {
            v.animate()
                    .setDuration(300)
                    .alpha(0)
                    .setListener(new Animator.AnimatorListener()
                    {
                        @Override
                        public void onAnimationStart(Animator animator)
                        {

                        }

                        @Override
                        public void onAnimationEnd(Animator animator)
                        {
                            v.setVisibility(View.GONE);
                            __expandedListItem = null;
                        }

                        @Override
                        public void onAnimationCancel(Animator animator)
                        {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator)
                        {

                        }
                    });
        }
    }


}
