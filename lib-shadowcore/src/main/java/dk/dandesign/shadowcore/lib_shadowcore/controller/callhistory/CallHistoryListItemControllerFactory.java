package dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory;

import android.view.View;

import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemBaseController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemBaseViewHolder;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemCallController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemControllerFactory;
import com.c2call.sdk.pub.gui.boardlistitem.controller.SCBoardListItemType;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.controller.IControllerRequestListener;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;

public class CallHistoryListItemControllerFactory extends SCBoardListItemControllerFactory
{
    public CallHistoryListItemControllerFactory(final SCActivityResultDispatcher resultDispatcher, final IControllerRequestListener requestListener)
    {
        super(resultDispatcher, requestListener);
    }

    public CallHistoryListItemControllerFactory(final SCActivityResultDispatcher resultDispatcher)
    {
        super(resultDispatcher);
    }

    @Override
    public IBoardListItemBaseController<? extends IBoardListItemBaseViewHolder> create(final SCBoardListItemType type, final View v, final SCViewDescription vd, final SCBoardEventData data)
    {
//      Ln.d("fc_tmp", "IBoardListItemBaseMediator.create() - content: %s, type: %s", data.getDescription(), type);
        if (type == null
            || v == null
            || vd == null
            || data == null)
        {
            throw new IllegalArgumentException();
        }

        switch(type){
            case Call: return onCreateCallItemController(v, vd, data);
            default:
                return onCreateCallItemController(v, vd, data);
                //Debug.ensure(false, "unhandled item type: " + type);
                //return null;

        }
    }

    protected IBoardListItemCallController onCreateCallItemController(final View v, final SCViewDescription vd, final SCBoardEventData data)
    {
        return new CallHistoryItemController(v, vd, data);
    }


    @Override
    public IBoardListItemBaseController<? extends IBoardListItemBaseViewHolder> onCreateController(final View v, final SCViewDescription vd, final SCBoardEventData data)
    {
        return create(data.getManager().getBoardListItemType(), v, vd, data);
    }
}
