package dk.dandesign.shadowcore.lib_shadowcore.util;

import android.content.Context;

import dk.dandesign.shadowcore.lib_shadowcore.R;


public class TimeUtil
{
    public static String getTimeAgoString(Context ctx, long timestamp)
    {
        long diff = System.currentTimeMillis() - timestamp;

        long diffSeconds = diff / 1000;
        long diffMinutes = diffSeconds / 60;
        long diffHours = diffMinutes / 60;
        long diffDays = diffHours / 24;


        if (diffDays > 0) {
            return ctx.getResources().getQuantityString(R.plurals.sc_time_ago_days, ResourceUtil.toQuantity(diffDays), diffDays);
        }
        else if (diffHours > 0) {
            return ctx.getResources().getQuantityString(R.plurals.sc_time_ago_hours, ResourceUtil.toQuantity(diffHours), diffHours);
        }
        else if (diffMinutes > 0) {
            return ctx.getResources().getQuantityString(R.plurals.sc_time_ago_minutes, ResourceUtil.toQuantity(diffMinutes), diffMinutes);
        }
        else {
            return ctx.getString(R.string.sc_time_ago_moments);
        }
    }

    public static long getDiffInDays(long d1, long d2)
    {
        return (d2 - d1) / (24 * 60 * 60 * 1000) + 1;
    }

}
