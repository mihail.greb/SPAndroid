package dk.dandesign.shadowcore.lib_shadowcore.controller.chathistory;


import android.view.View;

import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemTextController;
import com.c2call.sdk.pub.gui.core.common.SCActivityResultDispatcher;
import com.c2call.sdk.pub.gui.core.controller.IControllerRequestListener;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescription;

public class ChatHistoryEditItemContrllerFactory extends ChatHistoryListItemControllerFactory
{
    public ChatHistoryEditItemContrllerFactory(SCActivityResultDispatcher resultDispatcher, IControllerRequestListener requestListener)
    {
        super(resultDispatcher, requestListener);
    }

    public ChatHistoryEditItemContrllerFactory(SCActivityResultDispatcher resultDispatcher)
    {
        super(resultDispatcher);
    }

    protected IBoardListItemTextController onCreateChatItemController(final View v, final SCViewDescription vd, final SCBoardEventData data)
    {
        return new ChatHistoryEditItemController(v, vd, data);
    }
}
