package dk.dandesign.shadowcore.lib_shadowcore.dialogs;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.activities.common.SCActivityRequestCodes;
import com.c2call.sdk.pub.core.GlobalDepot;
import com.c2call.sdk.pub.core.SCDownloadType;
import com.c2call.sdk.pub.gui.dialog.SCChoiceDialog;
import com.c2call.sdk.pub.util.MediaUtil;

import java.io.File;
import java.lang.ref.WeakReference;

public class DialogBuilderRegisterPhoto
{
    public static class ExistingRunnable implements Runnable
    {
        private WeakReference<Fragment> _fragment = new WeakReference<Fragment>(null);

        public ExistingRunnable(Fragment fragment)
        {
            _fragment = new WeakReference<Fragment>(fragment);
        }

        @Override
        public void run()
        {
            final Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");

            Fragment fragment = _fragment.get();
            if (fragment != null) {
                fragment.startActivityForResult(intent, SCActivityRequestCodes.PICK_IMAGE);
            }
        }
    }

    public static class CamRunnable implements Runnable
    {
        private WeakReference<Fragment> _fragment = new WeakReference<Fragment>(null);
        private final String _filename;

        public CamRunnable(Fragment fragment, final String filename)
        {
            _fragment = new WeakReference<Fragment>(fragment);
            _filename = filename;
        }

        @Override
        public void run()
        {
            try{
                final String filename = _filename != null
                                        ? _filename
                                        : MediaUtil.TMP_IMAGE;

                final String output = MediaUtil.getMediaPath(SCDownloadType.Image, filename, true);

                Ln.d("c2app", "handleTakePhoto() - %s", output);
                GlobalDepot.put(GlobalDepot.KEY_LAST_PHOTO, output);
                final File file = new File(output);

                final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

                Fragment fragment = _fragment.get();
                if (fragment != null) {
                    fragment.startActivityForResult(intent, SCActivityRequestCodes.TAKE_PHOTO);
                }
            }
            catch(final Exception e){
                e.printStackTrace();
            }
        }
    }

    public static SCChoiceDialog build(Fragment fragment, final String filename)
    {
        final SCChoiceDialog.Builder builder = new SCChoiceDialog.Builder(fragment.getActivity());

        builder
                .addItem(com.c2call.sdk.R.string.sc_dlg_rich_message_existing_photo_title,
                         com.c2call.sdk.R.string.sc_dlg_rich_message_existing_photo_summary,
                         com.c2call.sdk.R.drawable.ic_photo_blue_24dp,
                         new ExistingRunnable(fragment))

                .addItem(com.c2call.sdk.R.string.sc_dlg_rich_message_take_photo_title,
                         com.c2call.sdk.R.string.sc_dlg_rich_message_take_photo_summary,
                         com.c2call.sdk.R.drawable.ic_photo_camera_blue_24dp,
                         new CamRunnable(fragment, filename));


        return builder.build();
    }
}
