package dk.dandesign.shadowcore.lib_shadowcore.activities;

import android.app.Fragment;

import com.c2call.sdk.pub.activities.SCLoginFragmentActivity;

import dk.dandesign.shadowcore.lib_shadowcore.fragments.LoginFragment;
import dk.dandesign.shadowcore.lib_shadowcore.fragments.RegisterFragment;

/**
 * Created by mikel on 13.05.17.
 */

public class LoginFragmentActivity extends SCLoginFragmentActivity {

    @Override
    protected Fragment onCreateFragment() {
        Fragment f = new LoginFragment();

        // forward arguments to the fragment
        f.setArguments(getIntent().getExtras());

        return f;
    }

}
