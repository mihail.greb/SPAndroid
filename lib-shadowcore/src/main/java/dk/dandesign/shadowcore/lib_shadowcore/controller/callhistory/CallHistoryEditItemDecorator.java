package dk.dandesign.shadowcore.lib_shadowcore.controller.callhistory;

import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemCallController;
import com.c2call.sdk.pub.util.SCSelectionManager;

public class CallHistoryEditItemDecorator extends CallHistoryItemDecorator
{

    @Override
    public void decorate(IBoardListItemCallController controller)
    {
        super.decorate(controller);

        onDecorateCheckbox((CallHistoryEditItemController)controller);
    }

    private void onDecorateCheckbox(CallHistoryEditItemController controller)
    {
        if (controller == null){
            return;
        }

        boolean isChecked = SCSelectionManager.instance().isSelected(CallHistoryController.SEL_KEY, controller.getData().getId());
        controller.getCheckBox().setChecked(isChecked);
    }
}
