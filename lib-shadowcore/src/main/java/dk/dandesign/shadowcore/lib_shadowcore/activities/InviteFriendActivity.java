package dk.dandesign.shadowcore.lib_shadowcore.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.c2call.sdk.pub.db.data.SCFriendData;
import com.c2call.sdk.pub.facade.SCCoreFacade;

import dk.dandesign.shadowcore.lib_shadowcore.R;

public class InviteFriendActivity extends Activity {

    protected EditText      username;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friend);

        username = (EditText) findViewById(R.id.invite_friend_username);
    }

    public void onButtonInviteClicked(View v) {
        if (username.getText().length() > 0) {
            String email = username.getText().toString() + "-shadow@dandesign.dk";

            boolean exists = SCCoreFacade.instance().checkExistingUser(email);
            if (exists) {
                SCCoreFacade.instance().addInviteForEmail(email, "Please add me to your contacts");
                onShowInvitedDialog();
            } else {
                onShowNotFoundDialog();
            }
        }
    }

    protected void onShowInvitedDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_title_invite_friend)
                .setMessage(R.string.dialog_message_friend_invited)
                .setPositiveButton(R.string.sc_std_btn_ok, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which)
                    {
                        finish();
                    }
                });

        builder.show();
    }

    protected void onShowNotFoundDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_title_invite_friend)
                .setMessage(R.string.dialog_message_friend_not_found)
                .setPositiveButton(R.string.sc_std_btn_ok, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which)
                    {
                    }
                });

        builder.show();
    }

}
