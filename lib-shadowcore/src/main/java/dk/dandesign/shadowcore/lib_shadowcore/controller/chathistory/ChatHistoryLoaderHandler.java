package dk.dandesign.shadowcore.lib_shadowcore.controller.chathistory;

import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;

import com.c2call.lib.androidlog.Ln;
import com.c2call.sdk.pub.db.data.SCBoardEventData;
import com.c2call.sdk.pub.db.util.core.DatabaseHelper;
import com.c2call.sdk.pub.facade.SCCoreFacade;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemBaseController;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemBaseViewHolder;
import com.c2call.sdk.pub.gui.boardlistitem.controller.IBoardListItemControllerFactory;
import com.c2call.sdk.pub.gui.core.adapter.SCBaseControllerCursorAdapter;
import com.c2call.sdk.pub.gui.core.controller.SCViewDescriptionMap;
import com.c2call.sdk.pub.gui.core.decorator.IDecorator;
import com.c2call.sdk.pub.gui.core.filter.IBaseFilter;
import com.c2call.sdk.pub.gui.core.filter.SCBoardFilterFactory;
import com.c2call.sdk.pub.gui.core.loader.SCBoardLoader;
import com.c2call.sdk.pub.gui.core.loaderhandler.ILoaderHandlerContextProvider;
import com.c2call.sdk.pub.gui.core.loaderhandler.SCBoardLoaderHandler;
import com.c2call.sdk.pub.util.IListViewProvider;
import com.c2call.sdk.thirdparty.ormlite.dao.Dao;

import dk.dandesign.shadowcore.lib_shadowcore.events.ChatHistoryUpdateEvent;
import dk.dandesign.shadowcore.lib_shadowcore.util.LoaderHandlers;

public class ChatHistoryLoaderHandler extends SCBoardLoaderHandler
{


    private static final String[] PROJECTION = SCBoardEventData.PROJECTION_ALL;


    @Override
    public void onCreate()
    {
        super.onCreate();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }


    public ChatHistoryLoaderHandler(final ILoaderHandlerContextProvider controller,
                                    final IListViewProvider listViewProvider,
                                    final String filterUserid,
                                    final SCViewDescriptionMap viewDescriptions,
                                    final IBoardListItemControllerFactory controllerFactory)
    {
        super(controller, listViewProvider, filterUserid, viewDescriptions, controllerFactory);
        setFilterMask(SCBoardFilterFactory.FLAG_FILTER_MESSAGES | SCBoardFilterFactory.FLAG_FILTER_NOMEETINGS | SCBoardFilterFactory.FLAG_FILTER_NOBROADCASTS); //
        setLoaderId(LoaderHandlers.CHATHISTORY);
    }



    @Override
    protected SCBaseControllerCursorAdapter<SCBoardEventData,
                                                IBoardListItemBaseController<? extends IBoardListItemBaseViewHolder>,
                                                IBoardListItemControllerFactory,
                                                IDecorator<IBoardListItemBaseController<?>>>
    onCreateAapder(final Cursor c,
                   final IBoardListItemControllerFactory controllerFactory,
                   final SCViewDescriptionMap viewDescriptions)
    {
        SCBaseControllerCursorAdapter<SCBoardEventData,
                        IBoardListItemBaseController<? extends IBoardListItemBaseViewHolder>,
                        IBoardListItemControllerFactory,
                        IDecorator<IBoardListItemBaseController<?>>>
        adapter = new ChatHistoryCursorAdapter(getContext(),
                                                c,
                                                controllerFactory,
                                                viewDescriptions,
                                                ChatHistoryCursorAdapter.DECORATOR_MAP,
                                                0);


        return adapter;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor)
    {
        Ln.d("fc_tmp", "ChatHistoryLoaderHandler.onLoadFinished() - loader: %s, size: %d", loader, (cursor != null ? cursor.getCount() : -1));
        super.onLoadFinished(loader, cursor);
    }

    @Override
    public Loader<Cursor> onCreateLoader(final int id, final Bundle arg)
    {
        Ln.d("fc_tmp", "ChatHistoryLoaderHandler.onCreateLoader() - id: %d /  this: %s", id, this);

        switch(id){
            case LoaderHandlers.CHATHISTORY:
                setFilterMask(SCBoardFilterFactory.FLAG_FILTER_MESSAGES | SCBoardFilterFactory.FLAG_FILTER_NOMEETINGS | SCBoardFilterFactory.FLAG_FILTER_NOBROADCASTS);
                final Dao<SCBoardEventData, String> dao = DatabaseHelper.getObservableDao(getContext(), SCBoardEventData.class);

                String extraProjection = "MAX(" + SCBoardEventData.TIMEVALUE + ")";
                final SCBoardLoader loader =  new SCBoardLoader(getContext(), dao, PROJECTION, extraProjection);
                loader.addOrderBy(SCBoardEventData.TIMEVALUE, false);
                loader.setReverse(true);

                IBaseFilter<SCBoardEventData, String> filter = onCreateFilter(getFilterUserid(), getFilterQuery(), getFilterMask());

                loader.setFilter(filter);
                loader.setGroupBy(SCBoardEventData.USERID);
                //loader.setDebugQuery(true);
                return loader;
            default:
                //Debug.ensure(false, "unhandled loader id: " + id);
                return null;
        }
    }


    @Override
    protected void onMessagesLoadFinished(Cursor cursor)
    {
        Ln.e("fc_test", "ChatHistoryLoaderHandler.onMessagesLoadFinished()- id: %d, count: %d, this: %s, ", getLoaderId(), (cursor != null ? cursor.getCount() : -1), this);
        ChatHistoryUpdateEvent evt = (cursor != null)
                                        ? new ChatHistoryUpdateEvent(cursor.getCount())
                                        : new ChatHistoryUpdateEvent(0);

        SCCoreFacade.instance().postEvent(evt, true);

        super.onMessagesLoadFinished(cursor);
    }
}
